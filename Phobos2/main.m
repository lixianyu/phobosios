//
//  main.m
//  Phobos2
//
//  Created by Li Xianyu on 13-11-10.
//  Copyright (c) 2013年 Li Xianyu. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "BIDAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([BIDAppDelegate class]));
    }
}
