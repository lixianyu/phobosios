//
//  BIDBLEManager6.m
//  Phobos2
//
//  Created by Li Xianyu on 13-12-1.
//  Copyright (c) 2013年 Li Xianyu. All rights reserved.
//

#import "BIDBLEiOS6Delegate.h"

@implementation BIDBLEiOS6Delegate

#pragma mark - CBPeripheralDelegate
- (void)peripheralDidInvalidateServices:(CBPeripheral *)peripheral {//NS_DEPRECATED(NA, NA, 6_0, 7_0);
    NSLog(@"%s", __func__);
}
@end
