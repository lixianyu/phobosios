//
//  BIDBallScrollView.h
//  TestRadar
//
//  Created by Li Xianyu on 13-11-19.
//  Copyright (c) 2013年 Li Xianyu. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreMotion/CoreMotion.h>

@interface BIDBallScrollView : UIScrollView
@property (strong, nonatomic) UIImage *imageBall;
@property (assign, nonatomic) CGPoint currentPoint;
@property (assign, nonatomic) CGPoint previousPoint;
@property (assign, nonatomic) CMAcceleration acceleration;
@property (assign, nonatomic) CGFloat ballXVelocity;
@property (assign, nonatomic) CGFloat ballYVelocity;
@property (assign, nonatomic) CGFloat currentScale;
@property (assign, nonatomic) CGPoint draggingPoint;
@property (assign, nonatomic) CGFloat ballAlpha;

- (void)update1;
- (void)updateDragging;
- (void)drawRadar;
@end
