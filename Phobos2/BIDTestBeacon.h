//
//  BIDTestBeacon.h
//  Phobos2
//
//  Created by Li Xianyu on 13-12-24.
//  Copyright (c) 2013年 Li Xianyu. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreLocation/CoreLocation.h>

@interface BIDTestBeacon : NSObject
- (void)doTest;
@end
