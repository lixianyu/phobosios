//
//  BIDBeginViewController2.h
//  Phobos2
//
//  Created by Li Xianyu on 13-11-15.
//  Copyright (c) 2013年 Li Xianyu. All rights reserved.
//

#import <UIKit/UIKit.h>
@class BIDRadarViewController6iPad;

@interface BIDBeginViewController2iPad : UITableViewController <UITableViewDataSource,UITableViewDelegate>
@property (strong, nonatomic) BIDRadarViewController6iPad *detailViewController;
@property UIView *semiTransparentView;
@property (strong, nonatomic) UIView *customAddNewView;
@property (strong, nonatomic) UIView *customAddNewViewReal;
@property (strong, nonatomic) NSMutableArray *dwarves;
@property (strong, nonatomic) NSMutableArray *uuids;
@property (strong, nonatomic) UIBarButtonItem *barButtonZongshu;
@end
