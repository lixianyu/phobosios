//
//  ViewController.h
//  TestRadar
//
//  Created by Li Xianyu on 13-11-19.
//  Copyright (c) 2013年 Li Xianyu. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BIDBallScrollView.h"

@interface BIDRadarViewController4 : UIViewController <UIScrollViewDelegate>
@property (nonatomic,retain) BIDBallScrollView *myscrollview;
@property (nonatomic, strong) UIImageView *imageView;
@end
