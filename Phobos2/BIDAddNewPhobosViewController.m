//
//  BIDAddNewPhobosViewController.m
//  Phobos2
//
//  Created by Li Xianyu on 13-11-15.
//  Copyright (c) 2013年 Li Xianyu. All rights reserved.
//

#import "BIDAddNewPhobosViewController.h"
#import "BIDAppDelegate.h"
#import "BIDBLEManager.h"
#import "MMProgressHUD.h"
#import <CoreBluetooth/CoreBluetooth.h>

typedef NS_ENUM(NSInteger, AddState){
    AddStateIdle = 0,
    AddStatePairing
};

static NSString *CellIdentifier = @"CellSetting";

@interface BIDAddNewPhobosViewController () <CBCentralManagerDelegate, CBPeripheralDelegate>
@property (assign, nonatomic) AddState addState;
@property (strong, nonatomic) CBCentralManager *bleManager;
@property (strong, nonatomic) NSMutableDictionary *foundPeripheral;
@property (strong, nonatomic) NSMutableDictionary *unknowBLEs;
@property (strong, nonatomic) NSMutableArray *uuids;
@property (copy, nonatomic) NSString *curSelectUUID;
@end

@implementation BIDAddNewPhobosViewController {
    CLBeaconMajorValue major;
    CLBeaconMinorValue minor;
    CBCharacteristic *majorminorChic;
    UInt32 aRandom;
}

- (id)initWithCoder:(NSCoder *)aDecoder {
    NSLog(@"%s", __func__);
    self = [super initWithCoder:aDecoder];
    if (self) {
        
    }
    return self;
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    NSLog(@"%s", __func__);
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    NSLog(@"%s", __func__);
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    [self.tableView registerClass:[UITableViewCell class] forCellReuseIdentifier:CellIdentifier];
    
    //For test...
//    if (_unknowBLEs == nil) {
//        _unknowBLEs = [[NSMutableDictionary alloc] initWithCapacity:2];
//    }
//    if (_uuids == nil) {
//        _uuids = [[NSMutableArray alloc] initWithCapacity:2];
//    }
//    [_unknowBLEs setObject:[@"haha" stringByAppendingFormat:@"%d", [self getNewPhobosCounter]] forKey:@"6D9DC93A-F0D7-4DF9-B627-41174140DFCC"];
//    [_uuids addObject:@"6D9DC93A-F0D7-4DF9-B627-41174140DFCC"];
}

- (void)viewWillAppear:(BOOL)animated {
    NSLog(@"%s", __func__);
    [super viewWillAppear:animated];
    
}
- (void)didReceiveMemoryWarning
{
    NSLog(@"%s", __func__);
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (NSInteger)getNewPhobosCounter {
    NSLog(@"%s", __func__);
    NSNumber *oldNumber = [[NSUserDefaults standardUserDefaults] objectForKey:@"newPhobosCounter"];
    if (oldNumber == nil) {
        [self setNewPhobosCounter:1];
        return 0;
    }
    NSInteger counter = [oldNumber integerValue];
    [self setNewPhobosCounter:counter+1];
    return counter;
}

- (void)setNewPhobosCounter:(NSInteger)number {
    NSLog(@"%s: number = %d", __func__, number);
    [[NSUserDefaults standardUserDefaults] setInteger:number forKey:@"newPhobosCounter"];
}

#pragma mark - Table view data source
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    NSLog(@"%s", __func__);
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    NSLog(@"%s", __func__);
    // Return the number of rows in the section.
    if (_unknowBLEs.count > 0) {
        return _unknowBLEs.count;
    } else {
        return 0;
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSLog(@"%s", __func__);
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
    
    // Configure the cell...
//    cell.textLabel.text = _names[indexPath.row];
//    cell.layer.cornerRadius = 7.0f;
//    CGRect myFrame = cell.frame;
//    myFrame.size.width = myFrame.size.width - 20;
//    [cell setFrame:myFrame];
//    NSLog(@"%s: cell = %@", __func__, cell);
    if (_uuids == nil) {
        return nil;
    }
    NSString *aUUID = _uuids[indexPath.row];
    cell.textLabel.text = _unknowBLEs[aUUID];
    return cell;
}

#pragma mark - UITableViewDelegate
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    NSLog(@"%s", __func__);
    [UIView animateWithDuration:0.5f
                     animations:^() {
                         self.labelPlease.alpha = 0.0f;
                         self.tableView.alpha = 0.0f;
                         self.inputANameView.layer.cornerRadius = 11.0f;
                         self.inputANameView.alpha = 1.0f;
                     }];
    _curSelectUUID = _uuids[indexPath.row];
    _textField.text = _unknowBLEs[_curSelectUUID];
//    _textField.delegate = self;
    [self performSelector:@selector(checkTextField) withObject:nil afterDelay:1.0];
//    [self.textField becomeFirstResponder];
    [self.textField performSelector:@selector(becomeFirstResponder) withObject:nil afterDelay:0.1];
}

#pragma mark - Button control
- (IBAction)cancelButton:(id)sender {
    NSLog(@"%s", __func__);
    [NSObject cancelPreviousPerformRequestsWithTarget:self];
    [self.textField resignFirstResponder];
    [UIView animateWithDuration:0.5f
                     animations:^() {
                         self.labelPlease.alpha = 1.0f;
                         self.tableView.alpha = 1.0f;
                         self.inputANameView.alpha = 0.0f;
                     }];
}

- (IBAction)okButton:(id)sender {
    NSLog(@"Enter %s", __func__);
    [NSObject cancelPreviousPerformRequestsWithTarget:self];
    [MMProgressHUD setDisplayStyle:MMProgressHUDDisplayStylePlain];
    [MMProgressHUD showWithTitle:@"配对" status:@"正在配对，请稍后……"];
    [self.textField endEditing:YES];
    
    self.deviceUUID = _curSelectUUID;

    [_bleManager connectPeripheral:_foundPeripheral[_curSelectUUID] options:nil];
    [self startTimeout];
    _addState = AddStatePairing;
    NSLog(@"Leave %s", __func__);
}

- (void)reConnect:(CBPeripheral*)peripheral {
    NSLog(@"Enter %s", __func__);
    [_bleManager connectPeripheral:peripheral options:nil];
}

static bool addaphobos = false;
+ (BOOL)ifAddAPhobos {
    if (addaphobos == true) {
        return YES;
    }
    return NO;
}

+ (void)resetIfAddAPhobosFlag {
    addaphobos = false;
}

- (void)savePhobos:(NSString*)name deviceUUID:(NSString*)uuid {
    NSLog(@"%s: uuid = %@", __func__, uuid);
    BIDAppDelegate *appDelegate = (BIDAppDelegate*)[UIApplication sharedApplication].delegate;
    NSManagedObjectContext *context = [appDelegate managedObjectContext];
    NSManagedObject *thePhobos = [NSEntityDescription insertNewObjectForEntityForName:kPhobosEntityName inManagedObjectContext:context];
    [thePhobos setValue:name forKey:kPhobosNameKey];
    [thePhobos setValue:uuid forKey:kPhobosDeviceUUIDKey];
    if (major == 0 && minor == 0) {
        // Do nothing.
    }
    else {
        aRandom = (UInt32)((major << 16) | (minor));
    }
    [thePhobos setValue:[NSNumber numberWithLongLong:aRandom] forKey:kPhobosMajorMinorKey];
    [appDelegate saveContext];
}

- (IBAction)textFieldDoneEditing:(id)sender {
    NSLog(@"%s", __func__);
    [sender resignFirstResponder];
}

- (IBAction)backgroundTap:(id)sender {
    NSLog(@"%s", __func__);
    [self.textField resignFirstResponder];
}

#pragma mark - For test
NSString * gen_uuid()
{
    NSLog(@"%s", __func__);
    CFUUIDRef uuid_ref = CFUUIDCreate(NULL);
    CFStringRef uuid_string_ref = CFUUIDCreateString(NULL, uuid_ref);

    CFRelease(uuid_ref);
    
    NSString *uuid = [NSString stringWithString:(__bridge NSString*)uuid_string_ref];
    
    CFRelease(uuid_string_ref);
    NSLog(@"uuid_string = %@", uuid);
    return uuid;
}

#pragma mark -
- (void)closeMe {
    NSLog(@"%s", __func__);
    [self stopScan];
    [NSObject cancelPreviousPerformRequestsWithTarget:self];
}

- (void)stopScan {
    NSLog(@"%s", __func__);
    if (_foundPeripheral && _curSelectUUID && _foundPeripheral[_curSelectUUID]) {
        [_bleManager cancelPeripheralConnection:_foundPeripheral[_curSelectUUID]];
    }
    
    [_bleManager stopScan];
    _bleManager = nil;
    _unknowBLEs = nil;
    _foundPeripheral = nil;
    _uuids = nil;
    _curSelectUUID = nil;
}

- (void)scanNewPhobos {
    NSLog(@"%s", __func__);
    _addState = AddStateIdle;
    major = minor = 0;
    majorminorChic = nil;
    aRandom = 0;
    [_tableView reloadData];
    _textField.text = @"";
    [self setNewPhobosCounter:_beginViewController.uuids.count];

    if (_bleManager == nil) {
        _bleManager = [[CBCentralManager alloc] initWithDelegate:self queue:nil];
    } else {
        [self scanPeripherals];
    }
}

- (void)scanPeripherals {
    NSLog(@"%s", __func__);
    NSDictionary *optionsdict = @{CBCentralManagerScanOptionAllowDuplicatesKey : [NSNumber numberWithBool:YES]};
    [_bleManager scanForPeripheralsWithServices:nil options:optionsdict];

//                NSArray *arrayUUID = [NSArray arrayWithObjects:[CBUUID UUIDWithString:@"853C5886-90EA-7499-847B-96A29C28D2C8"],
//                                      [CBUUID UUIDWithString:@"1802"], nil];
//                [_bleManager scanForPeripheralsWithServices:arrayUUID options:nil];

//    [_bleManager scanForPeripheralsWithServices:nil options:nil];
}

//增加配对成功概率
- (void) startTimeout {
    NSLog(@"%s", __func__);
    [self performSelector:@selector(timeoutControl) withObject:nil afterDelay:30];
}

- (void) reStartTimeout {
    [NSObject cancelPreviousPerformRequestsWithTarget:self];
    [self performSelector:@selector(timeoutControl) withObject:nil afterDelay:20];
}

- (void) cancelTimeout {
    [NSObject cancelPreviousPerformRequestsWithTarget:self];//取消timeoutControl
}

- (void)timeoutControl {
    NSLog(@"%s", __func__);
    _addState = AddStateIdle;
    [_bleManager cancelPeripheralConnection:_foundPeripheral[_curSelectUUID]];
    [MMProgressHUD dismissWithError:@"配对失败，请重试!" afterDelay:2.0];
}

- (void)prePairOK:(CBPeripheral*)peripheral {
    NSLog(@"%s, major = 0x%02X, minor = 0x%02X", __func__, major, minor);
    if (major == 0 && minor == 0) {
        aRandom = arc4random();
        uint8_t connData[11];
        connData[0] = (aRandom >> 24)&0xFF;
        connData[1] = (aRandom >> 16)&0xFF;
        connData[2] = (aRandom >> 8) &0xFF;
        connData[3] = (aRandom & 0xFF);

//        connData[0] = 0x01;
//        connData[1] = 0x00;
//        connData[2] = 0x00;
//        connData[3] = 0x00;
        
        connData[4] = 0xC5;
        connData[5] = 0x02;
        connData[6] = 0;
        
        // 下面四个字节是密码，只有密码对了，写入的东西才起作用
        connData[7] = 0xFF;
        connData[8] = 0x02;
        connData[9] = 0x9D;
        connData[10] = 0x8E;
        [peripheral writeValue:[NSData dataWithBytes:connData length:sizeof(connData)] forCharacteristic:majorminorChic type:CBCharacteristicWriteWithResponse];
    }
    else {
        [self pairOK:peripheral];
    }
}

- (void)pairOK:(CBPeripheral*)peripheral {
    NSLog(@"%s", __func__);
    //配对成功
    NSString *text = [[NSString alloc] initWithString:self.textField.text];
    [self savePhobos:text deviceUUID:self.deviceUUID];
    
    NSLog(@"Before remove: _unknowBLEs.count = %d, _uuids.count = %d", _unknowBLEs.count, _uuids.count);
    [_unknowBLEs removeObjectForKey:_curSelectUUID];
    [_uuids removeObject:_curSelectUUID];
    NSLog(@"After remove: _unknowBLEs.count = %d, _uuids.count = %d", _unknowBLEs.count, _uuids.count);
    [_tableView reloadData];
    
    NSNotificationCenter *center = [NSNotificationCenter defaultCenter];
    NSArray *array = @[text, self.deviceUUID];
    [center postNotificationName:@"Notification_GetNewPhobos" object:array];
    NSLog(@"你刚刚输入的是：%@", text);
    
    [UIView animateWithDuration:0.5f
                     animations:^() {
                         self.labelPlease.alpha = 1.0f;
                         self.tableView.alpha = 1.0f;
                         self.inputANameView.alpha = 0.0f;
                     }];
    addaphobos = true;
    
    BIDBLEManager *bm = [BIDBLEManager sharedInstance];
    [bm initBLEManager];
    [bm saveIt:peripheral]; //配对成功后保存下来，这样以后无需再scan了。
    //        [bm connectPhobos:peripheral.identifier.UUIDString];
    [bm performSelector:@selector(connectPhobos:) withObject:peripheral.identifier.UUIDString afterDelay:1.0];
    NSDictionary *aDictionary = @{@"uuid" : peripheral.identifier.UUIDString,
                                  @"name" : text};
    [bm performSelector:@selector(changePhobosName:) withObject:aDictionary afterDelay:3.0];
    [MMProgressHUD dismissWithSuccess:@"配对成功!"];
    if (isPad) {
        NSNotificationCenter *center = [NSNotificationCenter defaultCenter];
        [center postNotificationName:@"Notification_justAddOnePhobos" object:aDictionary];
    }
}

- (void)readMajorMinor:(CBPeripheral*)peripheral {
    NSLog(@"%s", __func__);
//    [peripheral readValueForCharacteristic:<#(CBCharacteristic *)#>]
}

#pragma mark - CBCentralManagerDelegate
- (void)centralManagerDidUpdateState:(CBCentralManager *)central {
    NSLog(@"%s", __func__);
    NSLog(@"state = %d", central.state);
    switch (central.state) {
        case CBCentralManagerStatePoweredOn: {
            NSLog(@"CBCentralManagerStatePoweredOn");
            [self scanPeripherals];
            break;
        }
        case CBCentralManagerStatePoweredOff:
            NSLog(@"CBCentralManagerStatePoweredOff");
            break;
        case CBCentralManagerStateResetting:
            NSLog(@"CBCentralManagerStateResetting");
            break;
        case CBCentralManagerStateUnauthorized:
            NSLog(@"CBCentralManagerStateUnauthorized");
            break;
        case CBCentralManagerStateUnknown:
            NSLog(@"CBCentralManagerStateUnknown");
            break;
        case CBCentralManagerStateUnsupported:
            NSLog(@"CBCentralManagerStateUnsupported");
            break;
        default:
            break;
    }
}

- (void)centralManager:(CBCentralManager *)central didDiscoverPeripheral:(CBPeripheral *)peripheral advertisementData:(NSDictionary *)      advertisementData RSSI:(NSNumber *)RSSI {
    NSLog(@"Did discover peripheral. peripheral: %@, rssi: %@, UUID: %@, advertisementData: %@", peripheral, RSSI, peripheral.identifier.UUIDString, advertisementData);
    if (_addState == AddStatePairing) {
        CBPeripheral *aFoundPeripheral = _foundPeripheral[_curSelectUUID];
        if ([peripheral.identifier isEqual:aFoundPeripheral.identifier] ) {
            NSLog(@"Oh, addState == AddStatePairing, so let us reconnect it.");
//            [central connectPeripheral:aFoundPeripheral options:nil];
            [self performSelector:@selector(reConnect:) withObject:aFoundPeripheral afterDelay:4.7];
            return;
        }
    }
    int iRssi = [RSSI intValue];
    NSLog(@"iRssi = %d", iRssi);
    // 只有当手机离Phobos很近的时候，才会识别。
    // 这样做是为了避免周围有很多Phobos，对配对产生混乱。
    if (iRssi < -59 || iRssi > 0) {
        return;
    }
    NSString *localName = advertisementData[@"kCBAdvDataLocalName"];
    NSLog(@"localName = %@, peripheral.name = %@", localName, peripheral.name);
    if ([localName isEqualToString:@"Phobos"] ||
         [localName isEqualToString:@"AprilBeacon_4207DB"]) {
        
        goto tocheckit;
    }
    else {
        return;
    }
//    _foundPeripheral = peripheral;
tocheckit:
    if (_unknowBLEs == nil) {
        _unknowBLEs = [[NSMutableDictionary alloc] initWithCapacity:2];
    }
    if (_foundPeripheral == nil) {
        _foundPeripheral = [[NSMutableDictionary alloc] initWithCapacity:2];
    }
    if (_uuids == nil) {
        _uuids = [[NSMutableArray alloc] initWithCapacity:2];
    }
    if ([_unknowBLEs objectForKey:peripheral.identifier.UUIDString] == nil) {
        if (_unknowBLEs.count >= 1) {
            NSLog(@"oh，一次只能添加一个哟");
            return;
        }
        for (NSString *uuid in _beginViewController.uuids) {
            if ([uuid isEqualToString:peripheral.identifier.UUIDString]) {
                NSLog(@"%@ alreay in Phobos array, so just return.", uuid);
                return;
            }
        }
        [central stopScan];
        NSLog(@"哦，发现了一个新的待配对的Phobos！");
        NSString *name = @"Phobos";
        NSInteger number = [self getNewPhobosCounter];
        [_unknowBLEs setObject:[name stringByAppendingFormat:@"%d", number] forKey:peripheral.identifier.UUIDString];
        [_foundPeripheral setObject:peripheral forKey:peripheral.identifier.UUIDString];
        [_uuids addObject:peripheral.identifier.UUIDString];
        peripheral.delegate = self;
        NSLog(@"_beginViewController.uuids.count = %d", _beginViewController.uuids.count);
        [_tableView reloadData];
    }
}

- (void)centralManager:(CBCentralManager *)central didConnectPeripheral:(CBPeripheral *)peripheral {
    NSLog(@"%s: peripheral = %@", __func__, peripheral);
#if 0
    BIDBLEManager *bm = [BIDBLEManager sharedInstance];
    [bm initBLEManager];
    [bm saveIt:peripheral]; //配对成功后保存下来，这样以后无需再scan了。
    [MMProgressHUD dismissWithSuccess:@"配对成功!"];
#else
    
    if ([peripheral.name isEqualToString:@"AprilBeacon"]) {
        [self cancelTimeout];
        [self pairOK:peripheral];
        return;
    }
    [self reStartTimeout];
    NSArray *sUUID = @[[CBUUID UUIDWithString:@"013d8e3b-1877-4d5c-bc59-aaa7e5082346"]];
    [peripheral discoverServices:sUUID];
#endif
}

- (void)centralManager:(CBCentralManager *)central didFailToConnectPeripheral:(CBPeripheral *)peripheral error:(NSError *)error {
    NSLog(@"%s, error = %@", __func__, error);
}

- (void)centralManager:(CBCentralManager *)central didDisconnectPeripheral:(CBPeripheral *)peripheral error:(NSError *)error {
    NSLog(@"%s, error = %@", __func__, error);
}

#pragma mark - CBPeripheralDelegate
- (void)peripheralDidUpdateName:(CBPeripheral *)peripheral {//NS_AVAILABLE(NA, 6_0);
    NSLog(@"%s", __func__);
}

- (void)peripheralDidInvalidateServices:(CBPeripheral *)peripheral {//NS_DEPRECATED(NA, NA, 6_0, 7_0);
    NSLog(@"%s", __func__);
}

- (void)peripheral:(CBPeripheral *)peripheral didModifyServices:(NSArray *)invalidatedServices {//NS_AVAILABLE(NA, 7_0);
    NSLog(@"%s", __func__);
}

- (void)peripheralDidUpdateRSSI:(CBPeripheral *)peripheral error:(NSError *)error {
    NSLog(@"%s, error = %@", __func__, error);
    if (error) {
        return;
    }
    if (peripheral.RSSI == nil) {
        return;
    }
    NSLog(@"RSSI = %@", peripheral.RSSI);
}

- (void)peripheral:(CBPeripheral *)peripheral didDiscoverServices:(NSError *)error {
    NSLog(@"%s, error = %@", __func__, error);
    [self reStartTimeout];
    for (CBService *s in peripheral.services) {
        NSLog(@"Service found : %@",s.UUID);
//        NSArray *cUUID = @[[CBUUID UUIDWithString:@"51567b6d-a035-499d-a2ea-1e27b5ae8b37"]];
//        [peripheral discoverCharacteristics:cUUID forService:s];
        [peripheral discoverCharacteristics:nil forService:s];
    }
}

- (void)peripheral:(CBPeripheral *)peripheral didDiscoverIncludedServicesForService:(CBService *)service error:(NSError *)error {
    NSLog(@"%s, error = %@", __func__, error);
}

- (void)peripheral:(CBPeripheral *)peripheral didDiscoverCharacteristicsForService:(CBService *)service error:(NSError *)error {
    NSLog(@"%s, error = %@", __func__, error);
//    [NSObject cancelPreviousPerformRequestsWithTarget:self];
#ifdef DEBUG_PHOBOS
    CGFloat systemVersion = [[[UIDevice currentDevice] systemVersion] floatValue];
#endif
    for ( CBCharacteristic *characteristic in service.characteristics ) {
#ifdef DEBUG_PHOBOS
        NSLog(@"characteristic.properties = 0x%x", characteristic.properties);
        if (systemVersion >= 7.1) {
            NSLog(@"characteristic.UUID.UUIDString = %@", characteristic.UUID.UUIDString);
        }
        else {
            NSLog(@"characteristic.UUID.UUIDString = %@", characteristic.UUID);
        }
#endif
        if ([characteristic.UUID isEqual:[CBUUID UUIDWithString:@"51567b6d-a035-499d-a2ea-1e27b5ae8b37"]]) {
            [self reStartTimeout];
            [peripheral readValueForCharacteristic:characteristic];
        }
        else if ([characteristic.UUID isEqual:[CBUUID UUIDWithString:@"153117a7-311b-4cc7-904a-820adc5d8461"]]) {
            [self reStartTimeout];
//            [self performSelector:@selector(readMajorMinor:) withObject:peripheral afterDelay:3.0];
//            [peripheral readValueForCharacteristic:characteristic];
            majorminorChic = characteristic;
        }
    }
}

- (void)peripheral:(CBPeripheral *)peripheral didUpdateValueForCharacteristic:(CBCharacteristic *)characteristic error:(NSError *)error {
    NSLog(@"%s, characteristic = %@, error = %@", __func__, characteristic,error);
    if (!error) {
        if ([characteristic.UUID isEqual:[CBUUID UUIDWithString:@"153117a7-311b-4cc7-904a-820adc5d8461"]]) {
            unsigned char data[characteristic.value.length];
            [characteristic.value getBytes:&data];
            for (int i = 0; i < characteristic.value.length; i++) {
                NSLog(@"0x%02X", data[i]);
            }
            major = (data[1] & 0xff) | ((data[0] << 8) & 0xff00);
            minor = (data[3] & 0xff) | ((data[2] << 8) & 0xff00);
            NSLog(@"major = 0x%02X, minor = 0x%02X", major, minor);
            
            [self cancelTimeout];
            [self prePairOK:peripheral];
        }
        else if ([characteristic.UUID isEqual:[CBUUID UUIDWithString:@"51567b6d-a035-499d-a2ea-1e27b5ae8b37"]]) {
//            [self cancelTimeout];
//            [self prePairOK:peripheral];
            [peripheral readValueForCharacteristic:majorminorChic];
        }
    }
    else {
        [MMProgressHUD dismissWithError:@"配对失败，请重试!" afterDelay:2.0];
    }
}

- (void)peripheral:(CBPeripheral *)peripheral didWriteValueForCharacteristic:(CBCharacteristic *)characteristic error:(NSError *)error {
    NSLog(@"%s, error = %@", __func__, error);
    [self pairOK:peripheral];
}

- (void)peripheral:(CBPeripheral *)peripheral didUpdateNotificationStateForCharacteristic:(CBCharacteristic *)characteristic error:(NSError *)error {
    NSLog(@"%s, error = %@", __func__, error);
}

- (void)peripheral:(CBPeripheral *)peripheral didDiscoverDescriptorsForCharacteristic:(CBCharacteristic *)characteristic error:(NSError *)error {
    NSLog(@"%s, error = %@", __func__, error);
}

- (void)peripheral:(CBPeripheral *)peripheral didUpdateValueForDescriptor:(CBDescriptor *)descriptor error:(NSError *)error {
    NSLog(@"%s, error = %@", __func__, error);
}

- (void)peripheral:(CBPeripheral *)peripheral didWriteValueForDescriptor:(CBDescriptor *)descriptor error:(NSError *)error {
    NSLog(@"%s, error = %@", __func__, error);
}

#if 0
#pragma mark - UITextFieldDelegate
- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    NSLog(@"%s, range.location = %d, range.length = %d,replacementString = %@", __func__, range.location, range.length, string);
    NSLog(@"textField.text = %@", textField.text);
    NSString *toBeString = [textField.text stringByReplacingCharactersInRange:range withString:string];
    NSLog(@"toBeString.length = %d", toBeString.length);
    if ([toBeString length] > 10) {
        textField.text = [toBeString substringToIndex:10];
        UIAlertView * alert = [[UIAlertView alloc] initWithTitle:nil message:@"超过最大字数，不能输入了" delegate:nil cancelButtonTitle:nil otherButtonTitles:@"知道了", nil];
        alert.alertViewStyle = UIAlertViewStyleDefault;
        [alert show];
        return NO;
    }
    return YES;
}
#endif

- (void)checkTextField {
    NSLog(@"%s, _textField.text = %@, length = %d", __func__, _textField.text, _textField.text.length);
    NSString *toBeString = _textField.text;
    NSData *data = [toBeString dataUsingEncoding:NSUTF8StringEncoding allowLossyConversion:YES];
    NSLog(@"data.length = %d\n", data.length);
    if (data.length > 18) {
        _textField.text = [toBeString substringToIndex:[toBeString length]-1];
        UIAlertView * alert = [[UIAlertView alloc] initWithTitle:nil message:@"超过最大字数，不能输入了" delegate:nil cancelButtonTitle:nil otherButtonTitles:@"知道了", nil];
        alert.alertViewStyle = UIAlertViewStyleDefault;
        [alert show];
        [self deleteOne];
        return;
    }
    [self performSelector:@selector(checkTextField) withObject:nil afterDelay:1.0];
}

- (void)deleteOne {
    NSLog(@"%s", __func__);
    NSString *toBeString = _textField.text;
    NSData *data = [toBeString dataUsingEncoding:NSUTF8StringEncoding allowLossyConversion:YES];
    NSLog(@"data.length = %d\n", data.length);
    if (data.length > 18) {
        _textField.text = [toBeString substringToIndex:[toBeString length]-1];
        [self performSelector:@selector(deleteOne) withObject:nil afterDelay:0.2];
    }
    else {
        [self performSelector:@selector(checkTextField) withObject:nil afterDelay:1.0];
    }
}
@end