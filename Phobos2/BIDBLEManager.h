//
//  BIDBLEManager.h
//  Phobos2
//
//  Created by Li Xianyu on 13-12-1.
//  Copyright (c) 2013年 Li Xianyu. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreBluetooth/CoreBluetooth.h>

@protocol BIDBLEManagerDelegate
@optional
- (void)didConnectPeripheral:(NSString*)uuid;
- (void)didFailToConnectPeripheral:(NSString*)uuid;
- (void)didDisconnectPeripheral:(NSString*)uuid;
- (void)peripheralDidUpdateRSSI:(NSString*)uuid RSSI:(NSNumber*)rssi;
- (void)didDeleteOne:(NSString*)uuid;
- (void)didUpdateBattery:(float)batteryLevel UUID:(NSString*)uuid;
- (void)didUpdateTemperature:(float)temperature;
- (void)stateUpdate;
//@required
@end

@interface BIDBLEManager : NSObject <CBCentralManagerDelegate, CBPeripheralDelegate>
//@property (strong, nonatomic) NSMutableDictionary *allConnectPeripheral; //当前所有已连接的
@property (assign, nonatomic) BOOL readingOrWriting;
@property (nonatomic) float batteryLevel;
@property (nonatomic) float tempLevel;

+ (BIDBLEManager *)sharedInstance;
- (BOOL)isConnected:(NSString*)uuid;
- (void)initBLEManager;
- (void)changePhobosName:(NSMutableDictionary *)info;
- (void)connectPhobos:(NSString *)uuid;
- (void)connectAllPhobos;
- (void)saveIt:(CBPeripheral*)peripheral;
- (void)readRSSI;
- (void)readRSSIwithUUID:(NSString*)uuid;
- (void)appEnterBackground;
- (void)ruClosed:(NSString*)uuid MajorMinor:(UInt32)majorminor;
- (void)ruOpened:(NSString*)uuid MajorMinor:(UInt32)majorminor;
- (void)playWinSound;
- (void)addDelegateObserver:(id)observer;
- (void)removeDelegateObserver:(id)observer;
- (void)chuAlertAllNecessary;
- (void)saveZuoBiao:(BOOL)flag UUID:(NSString*)uuid longitude:(double)longitude latitude:(double)latitude;
- (void)stopScanPeripherals;

//-(void) soundBuzzer:(Byte)buzVal peripheral:(CBPeripheral *)peripheral;
//-(void) soundBuzzer:(Byte)buzVal UUID:(NSString *)uuid;
- (void)soundBuzzer:(NSString *)uuid;
- (void)readBattery:(NSString*)uuid;
- (void)readTemperature:(NSString*)uuid;
- (void)setLinkLoss:(NSString *)uuid value:(Byte)buzVal;
@end
