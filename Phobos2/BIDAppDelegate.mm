//
//  BIDAppDelegate.m
//  Phobos2
//
//  Created by Li Xianyu on 13-11-10.
//  Copyright (c) 2013年 Li Xianyu. All rights reserved.
//

#import "BIDAppDelegate.h"
#import "MMProgressHUD.h"
#import "BIDBLEManager.h"
#import "BIDSideViewController.h"
#import "BIDSideViewController_iPad.h"
#import "MobClick.h"

@implementation BIDAppDelegate {
    BMKMapManager *baiduMapManager;
    UInt8 tryBaiduCount;
}

@synthesize managedObjectContext = _managedObjectContext;
@synthesize managedObjectModel = _managedObjectModel;
@synthesize persistentStoreCoordinator = _persistentStoreCoordinator;

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    NSLog(@"%s: launchOptions = %@", __func__, launchOptions);
    _runningMapUUID = [[NSMutableDictionary alloc] initWithCapacity:2];
    if (launchOptions) {
        NSArray *centralManagerIdentifiers = launchOptions[UIApplicationLaunchOptionsBluetoothCentralsKey];
        NSLog(@"centralManagerIdentifiers = %@", centralManagerIdentifiers);
        
        UILocalNotification * localNotif=[launchOptions objectForKey:UIApplicationLaunchOptionsLocalNotificationKey];
        NSLog(@"localNotif = %@", localNotif);
    }
    tryBaiduCount = 0;
    baiduMapManager = [[BMKMapManager alloc] init];
    // 如果要关注网络及授权验证事件，请设定generalDelegate参数
    BOOL ret = [baiduMapManager start:@"9VESn8zdUmSrxjrOFkZ58EFS" generalDelegate:self];//com.phobos
    if (!ret) {
        NSLog(@"baiduMapManager start failed!");
    }
    else {
        NSLog(@"baiduMapManager start Sucess!");
    }
    if (isPad) {
        NSLog(@"Yes, I'm iPad");
        UISplitViewController *splitViewController = (UISplitViewController *)self.window.rootViewController;
        UINavigationController *navigationController = [splitViewController.viewControllers lastObject];
        splitViewController.delegate = (id)navigationController.topViewController;
        NSLog(@"splitViewController.delegate = %@", splitViewController.delegate);
        _radarViewController = (BIDRadarViewController6iPad*)splitViewController.delegate;
    } else {
        NSLog(@"Yes, I'm iPhone");
    }
    
//    _baiduMapView = [[BMKMapView alloc] initWithFrame:CGRectMake(0, 0, 20, 20)];
//    _baiduMapView.delegate = self;
    //NSLog(@"_baiduMapView = %@", _baiduMapView);
//    if (!isPad) {
//        [self performSelector:@selector(setSideViewBaiduMapView) withObject:nil afterDelay:1];
//    }
    NSLog(@"applicationState = %d", application.applicationState);
    if (application.applicationState == UIApplicationStateBackground) {
        [self startBackgroundTask];
    }
#if defined(TEST_iBEACON)
    [self performSelector:@selector(testBeacon) withObject:nil afterDelay:3];
#endif
    [MobClick startWithAppkey:@"52b94cbb56240b31a7001647" reportPolicy:REALTIME channelId:@"Sun Temple"];
    [MobClick setLogEnabled:NO];
    
    //[self testBin];
    return YES;
}

//- (void)setSideViewBaiduMapView {
//    NSLog(@"%s, applicationState = %d", __func__, [UIApplication sharedApplication].applicationState);
//    if ([UIApplication sharedApplication].applicationState == UIApplicationStateBackground) {
//        //在后台时，不能对百度view进行初始化
//        NSLog(@"在后台时，不能对百度view进行初始化");
//        [self performSelector:@selector(setSideViewBaiduMapView) withObject:nil afterDelay:2];
//        return;
//    }
//    if (isPad) {
//        BIDSideViewController_iPad *sideViewController = _viewController;
//        if (sideViewController == nil) {
//            [self performSelector:@selector(setSideViewBaiduMapView) withObject:nil afterDelay:1];
//            return;
//        }
//        [sideViewController setMapViewForAppDelegate];
//    } else {
//        BIDSideViewController *sideViewController = _viewController;
//        if (sideViewController == nil) {
//            [self performSelector:@selector(setSideViewBaiduMapView) withObject:nil afterDelay:1];
//            return;
//        }
//        [sideViewController setMapViewForAppDelegate];
//    }
//}

- (void)applicationWillResignActive:(UIApplication *)application
{
    NSLog(@"%s", __func__);
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    NSLog(@"%s", __func__);
    appInBackground = true;
    //BIDBLEManager *bleManager = [BIDBLEManager sharedInstance];
    
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later. 
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    NSLog(@"%s", __func__);
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
    NSLog(@"%s", __func__);
    appInBackground = false;
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application
{
    NSLog(@"%s", __func__);
    // Saves changes in the application's managed object context before the application terminates.
    [self saveContext];
    [[BIDBLEManager sharedInstance] chuAlertAllNecessary];
//    BIDAppDelegate *app = [UIApplication sharedApplication].delegate;
//    [app saveContext];
}

- (void)saveContext
{
    NSLog(@"%s", __func__);
    NSError *error = nil;
    NSManagedObjectContext *managedObjectContext = self.managedObjectContext;
    if (managedObjectContext != nil) {
        if ([managedObjectContext hasChanges] && ![managedObjectContext save:&error]) {
             // Replace this implementation with code to handle the error appropriately.
             // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development. 
            NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
            abort();
        } 
    }
}

#pragma mark - Core Data stack

// Returns the managed object context for the application.
// If the context doesn't already exist, it is created and bound to the persistent store coordinator for the application.
- (NSManagedObjectContext *)managedObjectContext
{
    NSLog(@"%s", __func__);
    if (_managedObjectContext != nil) {
        return _managedObjectContext;
    }
    
    NSPersistentStoreCoordinator *coordinator = [self persistentStoreCoordinator];
    if (coordinator != nil) {
        _managedObjectContext = [[NSManagedObjectContext alloc] init];
        [_managedObjectContext setPersistentStoreCoordinator:coordinator];
    }
    return _managedObjectContext;
}

// Returns the managed object model for the application.
// If the model doesn't already exist, it is created from the application's model.
- (NSManagedObjectModel *)managedObjectModel
{
    NSLog(@"%s", __func__);
    if (_managedObjectModel != nil) {
        return _managedObjectModel;
    }
    NSURL *modelURL = [[NSBundle mainBundle] URLForResource:@"Phobos2" withExtension:@"momd"];
    _managedObjectModel = [[NSManagedObjectModel alloc] initWithContentsOfURL:modelURL];
    return _managedObjectModel;
}

// Returns the persistent store coordinator for the application.
// If the coordinator doesn't already exist, it is created and the application's store added to it.
- (NSPersistentStoreCoordinator *)persistentStoreCoordinator
{
    NSLog(@"%s", __func__);
    if (_persistentStoreCoordinator != nil) {
        return _persistentStoreCoordinator;
    }
    
    NSURL *storeURL = [[self applicationDocumentsDirectory] URLByAppendingPathComponent:@"Phobos2.sqlite"];
    
    NSDictionary *optionsDictionary = [NSDictionary dictionaryWithObjectsAndKeys:[NSNumber numberWithBool:YES],
                                       NSMigratePersistentStoresAutomaticallyOption, [NSNumber numberWithBool:YES],
                                       NSInferMappingModelAutomaticallyOption, nil];
    NSError *error = nil;
    _persistentStoreCoordinator = [[NSPersistentStoreCoordinator alloc] initWithManagedObjectModel:[self managedObjectModel]];
    if (![_persistentStoreCoordinator addPersistentStoreWithType:NSSQLiteStoreType configuration:nil URL:storeURL options:optionsDictionary error:&error]) {
        /*
         Replace this implementation with code to handle the error appropriately.
         
         abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development. 
         
         Typical reasons for an error here include:
         * The persistent store is not accessible;
         * The schema for the persistent store is incompatible with current managed object model.
         Check the error message to determine what the actual problem was.
         
         
         If the persistent store is not accessible, there is typically something wrong with the file path. Often, a file URL is pointing into the application's resources directory instead of a writeable directory.
         
         If you encounter schema incompatibility errors during development, you can reduce their frequency by:
         * Simply deleting the existing store:
         [[NSFileManager defaultManager] removeItemAtURL:storeURL error:nil]
         
         * Performing automatic lightweight migration by passing the following dictionary as the options parameter:
         @{NSMigratePersistentStoresAutomaticallyOption:@YES, NSInferMappingModelAutomaticallyOption:@YES}
         
         Lightweight migration will only work for a limited set of schema changes; consult "Core Data Model Versioning and Data Migration Programming Guide" for details.
         
         */
        NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
        abort();
    }    
    
    return _persistentStoreCoordinator;
}

#pragma mark - Application's Documents directory

// Returns the URL to the application's Documents directory.
- (NSURL *)applicationDocumentsDirectory
{
    NSLog(@"%s", __func__);
    return [[[NSFileManager defaultManager] URLsForDirectory:NSDocumentDirectory inDomains:NSUserDomainMask] lastObject];
}


#pragma mark -
- (void)application:(UIApplication *)application didReceiveLocalNotification:(UILocalNotification *)notification {
    NSLog(@"%s: notification = %@", __func__, notification);
    NSDictionary *info = notification.userInfo;
    NSString *name = info[@"name"];
    BIDBLEManager *bm = [BIDBLEManager sharedInstance];
    if ([info[@"chu"] boolValue]) {
        [MMProgressHUD showWithTitle:@"出围报警"
                              status:[NSString stringWithFormat:@"我终于失去了你……\"%@\"", name]
                 confirmationMessage:@"知道啦！！！"
                         cancelBlock:^{
                             NSLog(@"出围报警 cancelled!");
//                             [[BIDBLEManager sharedInstance] saveChuRuState:YES UUID:info[@"uuid"] churuState:NO];
                         }];
    } else {
        [bm connectPhobos:info[@"uuid"]];
        [MMProgressHUD showWithTitle:@"入围报警"
                              status:[NSString stringWithFormat:@"终于发现她了……\"%@\"", name]
                 confirmationMessage:@"取消报警？"
                         cancelBlock:^{
                                 NSLog(@"入围报警 cancelled!");
//                             [[BIDBLEManager sharedInstance] saveChuRuState:NO UUID:info[@"uuid"] churuState:NO];
                         }];
    }
    [bm playWinSound];
}

#pragma mark -
static bool appInBackground = false;
+ (BOOL)isBackground {
//    NSLog(@"%s, appInBackground = %d",__func__, appInBackground);
    if (appInBackground == true) {
        return YES;
    }
    return NO;
}

#pragma mark -
- (void)startBackgroundTask {
    NSLog(@"%@", NSStringFromSelector(_cmd));
    UIApplication *app = [UIApplication sharedApplication];
    __block UIBackgroundTaskIdentifier taskId;
    taskId = [app beginBackgroundTaskWithExpirationHandler:^{
        NSLog(@"Background task#%d ran out of time and was terminated.", taskId);
        NSLog(@"哦，彻底地离开了后台了……taskId = %d", taskId);
        [app endBackgroundTask:taskId];
        
    }];
    if (taskId == UIBackgroundTaskInvalid) {
        NSLog(@"Failed to start background task! taskId=%d", UIBackgroundTaskInvalid);
        return;
    } else {
        NSLog(@"taskId = %d, UIBackgroundTaskInvalid = %d", taskId, UIBackgroundTaskInvalid);
    }
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        NSLog(@"Starting background task#%d with %f seconds remaining", taskId, app.backgroundTimeRemaining);
        for (int i = 0; i < 7; i++) {
            [NSThread sleepForTimeInterval:1.0];
            NSLog(@" %d seconds passed... and task#%d with %f seconds remaining", i, taskId, app.backgroundTimeRemaining);
        }
        NSLog(@"Finishing background task#%d with %f seconds remaining", taskId, app.backgroundTimeRemaining);
        [app endBackgroundTask:taskId];
    });
}

- (void) testBlock {    
    __block BOOL found = NO;
    NSSet *aSet = [NSSet setWithObjects: @"Alpha", @"Beta", @"Gamma", @"X", nil];
    NSString *string = @"gamma";
    //we provide below a way of how to enumerate, using our own compare logic
    [aSet enumerateObjectsUsingBlock:^(id obj, BOOL *stop) {
        if ([obj localizedCaseInsensitiveCompare:string] == NSOrderedSame) {
            *stop = YES;
            found = YES;
        }
    }];

}

- (void)startBackgroundTask:(void (^)(double longitude, double latitude))tenSecondsCome {
    NSLog(@"%@", NSStringFromSelector(_cmd));
    UIApplication *app = [UIApplication sharedApplication];
    __block UIBackgroundTaskIdentifier taskId;
    taskId = [app beginBackgroundTaskWithExpirationHandler:^{
        NSLog(@"Background task#%d ran out of time and was terminated.", taskId);
        NSLog(@"哦，彻底地离开了后台了……taskId = %d", taskId);
        [app endBackgroundTask:taskId];
        
    }];
    if (taskId == UIBackgroundTaskInvalid) {
        NSLog(@"Failed to start background task! taskId=%d", UIBackgroundTaskInvalid);
        return;
    } else {
        NSLog(@"taskId = %d, UIBackgroundTaskInvalid = %d", taskId, UIBackgroundTaskInvalid);
    }
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        NSLog(@"Starting background task#%d with %f seconds remaining", taskId, app.backgroundTimeRemaining);
        for (int i = 0; i < 10; i++) {
            [NSThread sleepForTimeInterval:1.0];
            if (i == 5) {
                tenSecondsCome(_longitude, _latitude);
            }
            NSLog(@" %d seconds passed... and task#%d with %f seconds remaining", i, taskId, app.backgroundTimeRemaining);
        }
        NSLog(@"Finishing background task#%d with %f seconds remaining", taskId, app.backgroundTimeRemaining);
        [app endBackgroundTask:taskId];
    });
}

#if 0
/** 发现在后台时，苹果的MapView不上来数据，也就是说，在后台时不调用didUpdateUserLocation了
 所以只能把百度的加入
 */
- (void)beginAppleMapView {
    NSLog(@"%s", __func__);
    if (_appleMapView == nil) {
        _appleMapView = [[MKMapView alloc] initWithFrame:CGRectMake(0, 0, 20, 20)];
    }
    _appleMapView.delegate = self;
    //    CLLocationCoordinate2D coordinate = CLLocationCoordinate2DMake(39.9738, 116.443421);
    //    MKCoordinateRegion region = MKCoordinateRegionMakeWithDistance(coordinate, 1500, 1500);
    _appleMapView.showsUserLocation = NO;
    _appleMapView.userTrackingMode = MKUserTrackingModeNone;
    _appleMapView.showsUserLocation = YES;
}

- (void)stopAppleMapView {
    NSLog(@"%s", __func__);
    _appleMapView.userTrackingMode = MKUserTrackingModeNone;
    _appleMapView.showsUserLocation = NO;
}
#endif

- (void)beginBaiduMapView:(NSString*)uuid chuOrRu:(BOOL)flag {
    NSLog(@"%s: _baiduMapRunning = %@, flag = %@", __func__, _baiduMapRunning?@"YES":@"NO", flag?@"YES":@"NO");
    if (_baiduMapRunning) {
        [_runningMapUUID setObject:[NSNumber numberWithBool:flag] forKey:uuid];
        return;
    }
    _baiduMapRunning = YES;
    _locService = [[BMKLocationService alloc] init];
    _locService.delegate = self;
    [_locService startUserLocationService];
    NSLog(@"_locService = %@", _locService);
    
    [_runningMapUUID setObject:[NSNumber numberWithBool:flag] forKey:uuid];
    [self performSelector:@selector(stopBaiduMapView) withObject:nil afterDelay:7];
}

- (void)stopBaiduMapView {
    NSLog(@"%s: _baiduMapRunning = %@", __func__, _baiduMapRunning?@"YES":@"NO");
    if (!_baiduMapRunning) {
        return;
    }
    _baiduMapRunning = NO;
    [_locService stopUserLocationService];
    _locService.delegate = nil;
    _locService = nil;
    
    BIDBLEManager *bidBleManager = [BIDBLEManager sharedInstance];
    for (NSString *uuid in _runningMapUUID) {
        NSLog(@"uuid = %@, _longitude = %f, _latitude = %f", uuid, _longitude, _latitude);
        [bidBleManager saveZuoBiao:[_runningMapUUID[uuid] boolValue] UUID:uuid longitude:_longitude latitude:_latitude];
    }
    _runningMapUUID = [[NSMutableDictionary alloc] initWithCapacity:2];
}

#pragma mark - BMKLocationServiceDelegate
/**
 *用户位置更新后，会调用此函数
 *@param userLocation 新的用户位置
 */
- (void)didUpdateUserLocation:(BMKUserLocation *)userLocation {
    NSLog(@"%s, latitude = %f, longitude = %f", __func__, userLocation.location.coordinate.latitude, userLocation.location.coordinate.longitude);
    _latitude = userLocation.location.coordinate.latitude;
    _longitude = userLocation.location.coordinate.longitude;
}

/**
 *定位失败后，会调用此函数
 *@param error 错误号
 */
- (void)didFailToLocateUserWithError:(NSError *)error {
    NSLog(@"%s", __func__);
}


#pragma mark - BMKGeneralDelegate
- (void)onGetNetworkState:(int)iError {
    NSLog(@"%s: iError = %d", __func__, iError);
    if (0 == iError) {
        NSLog(@"联网成功");
    }
    else{
        NSLog(@"onGetNetworkState %d",iError);
    }
}

- (void)onGetPermissionState:(int)iError {
    NSLog(@"%s: iError = %d", __func__, iError);
    if (0 == iError) {
        NSLog(@"授权成功");
    }
    else {
        NSLog(@"onGetPermissionState = %d, tryBaiduCount = %d",iError, tryBaiduCount);
        if (tryBaiduCount == 0) {
            [self performSelector:@selector(tryBaiduMapManager) withObject:nil afterDelay:3.0];
        }
        else if (tryBaiduCount == 1) {
            [self performSelector:@selector(tryBaiduMapManager1) withObject:nil afterDelay:2.0];
        }
    }
}

- (void) tryBaiduMapManager {
    NSLog(@"%s", __func__);
//    baiduMapManager = nil;
//    baiduMapManager = [[BMKMapManager alloc] init];
    BOOL ret = [baiduMapManager start:@"xtp8PVr19jneTsnjsBFqZ3fy" generalDelegate:self];//com.eversino.icpslite
//    BOOL ret = [baiduMapManager start:@"LoeyYb5neO8iGhFw16PTM4N0" generalDelegate:self];//com.eversino.Phobos2
    tryBaiduCount++;
}

- (void)tryBaiduMapManager1 {
    NSLog(@"%s", __func__);
    BOOL ret = [baiduMapManager start:@"LoeyYb5neO8iGhFw16PTM4N0" generalDelegate:self];//com.eversino.Phobos2
    tryBaiduCount++;
}

#pragma mark -
#if defined(TEST_iBEACON)
- (void)testBeacon {
    NSLog(@"%s", __func__);
    _testBeacon = [[BIDTestBeacon alloc] init];
//    [_testBeacon performSelector:@selector(doTest) withObject:nil afterDelay:1];
    [_testBeacon doTest];
}
#endif

- (void)testBin {
    NSLog(@"%s", __func__);
    NSString *path = [[NSBundle mainBundle] pathForResource:@"PhobosCC2541ImgB" ofType:@"bin"];
    NSLog(@"path = %@", path);
    NSData *reader = [NSData dataWithContentsOfFile:path];
    int size = [reader length];
    NSLog(@"the file size is %d", size);
    Byte byteData[16];
    int i = 0;
    for(int n = 0; n < 3; n++) {
//        [reader getBytes:&iData length:4];
        [reader getBytes:byteData range:NSMakeRange(i, 16)];
//        NSLog(@"iData = 0x%02X", byteData);
        for (int j = 0; j < 16; j++) {
            printf("0x%02X ", byteData[j]);
        }
        printf("\n");
        i += 16;
    }
}
@end
