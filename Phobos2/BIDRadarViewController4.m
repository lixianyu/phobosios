//
//  ViewController.m
//  TestRadar
//
//  Created by Li Xianyu on 13-11-19.
//  Copyright (c) 2013年 Li Xianyu. All rights reserved.
//

#import "BIDRadarViewController4.h"
#import <CoreMotion/CoreMotion.h>

#define kUpdateInterval    (1.0f / 20.0f)

@interface BIDRadarViewController4 ()
@property (strong, nonatomic) CMMotionManager *motionManager;
@property (strong, nonatomic) NSOperationQueue *queue;
@property (strong, nonatomic) NSTimer *updateTimer;
@property (strong, nonatomic) NSTimer *breatheBallTimer;
@end

@implementation BIDRadarViewController4 {
    CGFloat minScaleHeight;
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    NSLog(@"%s", __func__);
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    CGRect appRect = [UIScreen mainScreen].applicationFrame;
    NSLog(@"appRect: x = %f, y = %f, width = %f, height = %f", appRect.origin.x, appRect.origin.y, appRect.size.width, appRect.size.height);
    CGRect statusBarRect = [[UIApplication sharedApplication] statusBarFrame];
    NSLog(@"statusBarRect: x = %f, y = %f, width = %f, height = %f", statusBarRect.origin.x, statusBarRect.origin.y, statusBarRect.size.width, statusBarRect.size.height);
    CGRect myRadarBounds = CGRectMake(0, statusBarRect.size.height+self.navigationController.navigationBar.bounds.size.height, self.view.bounds.size.width, self.view.bounds.size.height - self.tabBarController.tabBar.bounds.size.height - self.navigationController.navigationBar.bounds.size.height - statusBarRect.size.height);
    NSLog(@"myRadarBounds: x = %f, y = %f, width = %f, height = %f", myRadarBounds.origin.x, myRadarBounds.origin.y, myRadarBounds.size.width, myRadarBounds.size.height);
    _myscrollview = [[BIDBallScrollView alloc] initWithFrame:myRadarBounds];
//    _myscrollview = [[BIDBallScrollView alloc] initWithFrame:CGRectMake(0.0, 44, 320, 524)];
//    _myscrollview = [[UIScrollView alloc] initWithFrame:CGRectMake(0.0, 44, 320, 567)];
    
    UIImageView *imageViewBackground = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"1"]];
    NSLog(@"myimage.image.size.width = %f, myimage.image.size.height = %f", imageViewBackground.image.size.width, imageViewBackground.image.size.height);
    //[myimage setContentMode:UIViewContentModeScaleAspectFill];
//    [myimage setFrame:CGRectMake(0, 0, 1085, 1085)];
    [imageViewBackground setFrame:CGRectMake(0, 0, imageViewBackground.image.size.width, imageViewBackground.image.size.height)];
    [_myscrollview addSubview:imageViewBackground];
    _imageView = imageViewBackground;
    
    _myscrollview.directionalLockEnabled = NO; //只能一个方向滑动
    _myscrollview.pagingEnabled = NO; //是否翻页
    _myscrollview.backgroundColor = [UIColor grayColor];
    _myscrollview.showsVerticalScrollIndicator =NO; //垂直方向的滚动指示
    _myscrollview.indicatorStyle = UIScrollViewIndicatorStyleWhite;//滚动指示的风格
    _myscrollview.showsHorizontalScrollIndicator = NO;//水平方向的滚动指示
    _myscrollview.delegate = self;
    //CGSize newSize = CGSizeMake(self.view.frame.size.width, self.view.frame.size.height+1);
    //[_myscrollview setContentSize:newSize];
    CGFloat minScaleWidth = self.view.frame.size.width / imageViewBackground.image.size.width;
    NSLog(@"self.view.frame.size.width = %f, myimage.image.size.width = %f", self.view.frame.size.width, imageViewBackground.image.size.width);
    minScaleHeight = self.view.frame.size.height / imageViewBackground.image.size.width;
    NSLog(@"minScaleWidth = %f, minScaleHeight = %f", minScaleWidth, minScaleHeight);
    [_myscrollview setMinimumZoomScale:minScaleWidth];
    [_myscrollview setMaximumZoomScale:1.0];
    [_myscrollview setZoomScale:minScaleHeight animated:YES];
    
    [self.view addSubview:_myscrollview];
    //[self.myscrollview setContentOffset:CGPointMake(-1, -104)];
    
//    [self startAccelerometer];
}

- (void)viewWillAppear:(BOOL)animated
{
    NSLog(@"%s", __func__);
    [super viewWillAppear:animated];
    self.updateTimer = [NSTimer scheduledTimerWithTimeInterval:4.0
                                                      target:self
                                                      selector:@selector(drawRadar)
                                                      userInfo:nil
                                                      repeats:YES];
//    self.breatheBallTimer = [NSTimer scheduledTimerWithTimeInterval:1
//                                                        target:self
//                                                      selector:@selector(breatheTheBall)
//                                                      userInfo:nil
//                                                       repeats:YES];
}

- (void)breatheTheBall {
    static int iState = 0;
//    NSLog(@"%s: iState = %d", __func__, iState);
    if (self.myscrollview) {
        if (iState == 0) {
            self.myscrollview.ballAlpha = 0.6f;
            iState = 1;
        } else if (iState == 1) {
            self.myscrollview.ballAlpha = 0.7f;
            iState = 2;
        }
        else if (iState == 2) {
            self.myscrollview.ballAlpha = 0.7f;
            iState = 3;
        }
        else if (iState == 3) {
            self.myscrollview.ballAlpha = 0.8f;
            iState = 4;
        }
        else if (iState == 4) {
            self.myscrollview.ballAlpha = 0.9f;
            iState = 5;
        }
        else if (iState == 5) {
            self.myscrollview.ballAlpha = 0.9f;
            iState = 6;
        }
        else if (iState == 6) {
            self.myscrollview.ballAlpha = 1.0f;
            iState = 7;
        }
        else if (iState == 7) {
            self.myscrollview.ballAlpha = 1.0f;
            iState = 8;
        }
        else if (iState == 8) {
            self.myscrollview.ballAlpha = 1.0f;
            iState = 9;
        }
        else if (iState == 9) {
            self.myscrollview.ballAlpha = 1.0f;
            iState = 10;
        }
        else if (iState == 10) {
            self.myscrollview.ballAlpha = 1.0f;
            iState = 11;
        }
        else if (iState == 11) {
            self.myscrollview.ballAlpha = 0.9f;
            iState = 12;
        }
        else if (iState == 12) {
            self.myscrollview.ballAlpha = 0.9f;
            iState = 13;
        }
        else if (iState == 13) {
            self.myscrollview.ballAlpha = 0.9f;
            iState = 14;
        }
        else if (iState == 14) {
            self.myscrollview.ballAlpha = 0.9f;
            iState = 15;
        }
        else if (iState == 15) {
            self.myscrollview.ballAlpha = 0.8f;
            iState = 16;
        }
        else if (iState == 16) {
            self.myscrollview.ballAlpha = 0.8f;
            iState = 17;
        }
        else if (iState == 17) {
            self.myscrollview.ballAlpha = 0.7f;
            iState = 18;
        }
        else if (iState == 18) {
            self.myscrollview.ballAlpha = 0.6f;
            iState = 19;
        }
        else if (iState == 19) {
            self.myscrollview.ballAlpha = 0.5f;
            iState = 0;
        }
        [self.myscrollview performSelectorOnMainThread:@selector(updateDragging)
                                            withObject:nil
                                         waitUntilDone:NO];
    }
}

- (void)drawRadar {
    if (self.myscrollview) {
        [self.myscrollview drawRadar];
    }
}

- (void)viewDidDisappear:(BOOL)animated
{
    NSLog(@"%s", __func__);
    [super viewDidDisappear:animated];
    [self.updateTimer invalidate];
    self.updateTimer = nil;
    [self.breatheBallTimer invalidate];
    self.breatheBallTimer = nil;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark -
- (void)startAccelerometer {
    NSLog(@"%s", __func__);
    self.motionManager = [[CMMotionManager alloc] init];
    self.queue = [[NSOperationQueue alloc] init];
    self.motionManager.accelerometerUpdateInterval = kUpdateInterval;
    [self.motionManager startAccelerometerUpdatesToQueue:self.queue withHandler:
        ^(CMAccelerometerData *accelerometerData, NSError *error) {
            [(id)self.myscrollview setAcceleration:accelerometerData.acceleration];
            [self.myscrollview performSelectorOnMainThread:@selector(update)
                                     withObject:nil
                                  waitUntilDone:NO];
     }];

}

#pragma mark － UIScrollViewDelegate
//只要滚动了就会触发
- (void)scrollViewDidScroll:(UIScrollView *)scrollView;
{
    //    NSLog(@" scrollViewDidScroll");
    NSLog(@"scrollViewDidScroll: ContentOffset x=%f,y=%f", scrollView.contentOffset.x,scrollView.contentOffset.y);
//    NSLog(@"scrollViewDidScroll: contentInset left=%f,top=%f,right=%f,bottom=%f",scrollView.contentInset.left,scrollView.contentInset.top, scrollView.contentInset.right, scrollView.contentInset.bottom);
    [self.myscrollview performSelectorOnMainThread:@selector(updateDragging)
                                        withObject:nil
                                     waitUntilDone:NO];
}

//static CGFloat beginDraggingOffsetX;
//static CGFloat beginDraggingOffsetY;
//static CGFloat endDraggingOffsetX;
//static CGFloat endDraggingOffsetY;
//开始拖拽视图
- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView;
{
    NSLog(@"scrollViewWillBeginDragging");
//    beginDraggingOffsetX = scrollView.contentOffset.x;
//    beginDraggingOffsetY = scrollView.contentOffset.y;
    [self.myscrollview performSelectorOnMainThread:@selector(updateDragging)
                                        withObject:nil
                                     waitUntilDone:NO];
}
//完成拖拽
- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate;
{
    NSLog(@"scrollViewDidEndDragging, decelerate = %d", decelerate);
//    endDraggingOffsetX = scrollView.contentOffset.x;
//    endDraggingOffsetY = scrollView.contentOffset.y;
//    CGPoint point = CGPointMake(endDraggingOffsetX-beginDraggingOffsetX, endDraggingOffsetY-beginDraggingOffsetY);
    
//    [self.myscrollview setDraggingPoint:point];
    [self.myscrollview performSelectorOnMainThread:@selector(updateDragging)
                                        withObject:nil
                                     waitUntilDone:NO];
}

//将开始降速时
- (void)scrollViewWillBeginDecelerating:(UIScrollView *)scrollView;
{
    NSLog(@"scrollViewWillBeginDecelerating");
}

//减速停止了时执行，手触摸时执行执行
- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView;
{
    NSLog(@"scrollViewDidEndDecelerating");
}

//滚动动画停止时执行,代码改变时出发,也就是setContentOffset改变时
- (void)scrollViewDidEndScrollingAnimation:(UIScrollView *)scrollView;
{
    NSLog(@"scrollViewDidEndScrollingAnimation");
}


- (void)scrollViewDidZoom:(UIScrollView *)scrollView {
    NSLog(@"scrollViewDidZoom:  zoomScale = %f", scrollView.zoomScale);
    [self.myscrollview setCurrentScale:scrollView.zoomScale];
    [self.myscrollview performSelectorOnMainThread:@selector(update1)
                                        withObject:nil
                                     waitUntilDone:NO];
}

//设置放大缩小的视图，要是uiscrollview的subview
- (UIView *)viewForZoomingInScrollView:(UIScrollView *)scrollView;
{
    NSLog(@"viewForZoomingInScrollView");
    return _imageView;
}

//完成放大缩小时调用
- (void)scrollViewDidEndZooming:(UIScrollView *)scrollView withView:(UIView *)view atScale:(float)scale;
{
    NSLog(@"view=%@", view);
    NSLog(@"scale = %f, minScaleHeight = %f", scale, minScaleHeight);
//    _myscrollview.frame=CGRectMake(50,0,100,400);
    [scrollView setZoomScale:scale animated:YES];
    
    if (scale < minScaleHeight) {
        //[_myscrollview setContentOffset:CGPointMake((view.frame.size.width - 320)/2, (view.frame.size.height-524)/2) animated:NO];
        //[self performSelector:@selector(handleTimesup:) withObject:[NSNumber numberWithFloat:scale] afterDelay:0.11];
    }
    [self.myscrollview setCurrentScale:scale];
    [self.myscrollview performSelectorOnMainThread:@selector(update1)
                                        withObject:nil
                                     waitUntilDone:NO];
    NSLog(@"scrollViewDidEndZooming:scale between minimum and maximum. called after any 'bounce' animations");
    
}// scale between minimum and maximum. called after any 'bounce' animations

- (void)handleTimesup:(id)object {
    NSLog(@"%s", __func__);
    NSNumber *number = object;
    NSLog(@"object = %f", [number floatValue]);
    [_myscrollview setContentOffset:CGPointMake((self.imageView.frame.size.width - 320)/2, (self.imageView.frame.size.height-524)/2) animated:NO];
}

//如果你不是完全滚动到滚轴视图的顶部，你可以轻点状态栏，那个可视的滚轴视图会一直滚动到顶部，那是默认行为，你可以通过该方法返回NO来关闭它
- (BOOL)scrollViewShouldScrollToTop:(UIScrollView *)scrollView;
{
    NSLog(@"scrollViewShouldScrollToTop");
    return NO;
}

- (void)scrollViewDidScrollToTop:(UIScrollView *)scrollView;
{
    NSLog(@"scrollViewDidScrollToTop");
}

@end
