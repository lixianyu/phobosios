//
//  BIDRadarViewController5.h
//  Phobos2
//
//  Created by Li Xianyu on 13-11-21.
//  Copyright (c) 2013年 Li Xianyu. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BIDScrollView.h"

typedef NS_ENUM(NSInteger, RadarState){
    RadarStateAllBLE = 0, //显示周边所有未配对蓝牙BLE
    RadarStateAllPair,
    RadarStateSelected
};

@protocol BIDRadarViewController5Delegate
@optional
-(void) stateUpdate;
//@required
@end

@interface BIDRadarViewController5 : UIViewController <UIScrollViewDelegate>
//@property (nonatomic,assign) NSMutableArray <BIDRadarViewController5Delegate> *delegate;
@property (nonatomic, strong) NSMutableArray *delegate;

- (BOOL)isConnected:(NSString*)uuid;
@end
