//
//  BIDConstant.h
//  Phobos2
//
//  Created by Li Xianyu on 13-11-27.
//  Copyright (c) 2013年 Li Xianyu. All rights reserved.
//

#import <Foundation/Foundation.h>

const CGFloat gcenterX = 542.5f;
const CGFloat gcenterY = 542.0f;
const CGFloat gradius = 542.0f;

// Defines for the TI CC2540 keyfob peripheral
#define TI_KEYFOB_PROXIMITY_ALERT_UUID                      0x1802
#define TI_KEYFOB_PROXIMITY_ALERT_PROPERTY_UUID             0x2a06
#define TI_KEYFOB_PROXIMITY_ALERT_ON_VAL                    0x01
#define TI_KEYFOB_PROXIMITY_ALERT_OFF_VAL                   0x00
#define TI_KEYFOB_PROXIMITY_ALERT_WRITE_LEN                 1

#define TI_KEYFOB_LINK_LOSS_SERVICE_UUID                    0x1803
#define TI_KEYFOB_LINK_LOSS_PROPERTY_UUID                   0x2a06
#define TI_KEYFOB_LINK_LOSS_WRITE_LEN                       1


#define TI_KEYFOB_PROXIMITY_TX_PWR_SERVICE_UUID             0x1804
#define TI_KEYFOB_PROXIMITY_TX_PWR_NOTIFICATION_UUID        0x2A07
#define TI_KEYFOB_PROXIMITY_TX_PWR_NOTIFICATION_READ_LEN    1

#define TI_KEYFOB_BATT_SERVICE_UUID                         0x180F
#define TI_KEYFOB_LEVEL_SERVICE_UUID                        0x2A19
#define TI_KEYFOB_LEVEL_SERVICE_READ_LEN                    1

//#define TI_KEYFOB_ACCEL_SERVICE_UUID                        0xFFA0
//#define TI_KEYFOB_ACCEL_ENABLER_UUID                        0xFFA1
//#define TI_KEYFOB_ACCEL_RANGE_UUID                          0xFFA2
//#define TI_KEYFOB_ACCEL_READ_LEN                            1
//#define TI_KEYFOB_ACCEL_X_UUID                              0xFFA3
//#define TI_KEYFOB_ACCEL_Y_UUID                              0xFFA4
//#define TI_KEYFOB_ACCEL_Z_UUID                              0xFFA5
//
//#define TI_KEYFOB_KEYS_SERVICE_UUID                         0xFFE0
//#define TI_KEYFOB_KEYS_NOTIFICATION_UUID                    0xFFE1
//#define TI_KEYFOB_KEYS_NOTIFICATION_READ_LEN                1

#if defined(GET_TEMPERATURE_USE_BATTERY_SERVICE)
#define TI_KEYFOB_TEMPERATURE_SERVICE_UUID                  0x180F
#define TI_KEYFOB_TEMPERATURE_CHARACTERISTIC                0x2A1C
#else
#define TI_KEYFOB_TEMPERATURE_SERVICE_UUID                  0xFFB0
#define TI_KEYFOB_TEMPERATURE_CHARACTERISTIC                0xFFB1
#endif
#define TI_KEYFOB_TEMPERATURE_CHARACTERISTIC_READ_LEN       4

@interface BIDConstant : NSObject
+ (float)distanceFromPointX:(CGPoint)start distanceToPointY:(CGPoint)end;
@end
