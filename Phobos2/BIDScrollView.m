//
//  BIDScrollView.m
//  Phobos2
//
//  Created by Li Xianyu on 13-11-21.
//  Copyright (c) 2013年 Li Xianyu. All rights reserved.
//

#import "BIDScrollView.h"
#import <CoreMotion/CoreMotion.h>
#import "BIDBallLabelView.h"
#import "BIDConstant.h"

@interface BIDScrollView()
@property (assign, nonatomic) CGPoint currentPoint;
@property (assign, nonatomic) CGPoint previousPoint;
@property (assign, nonatomic) CMAcceleration acceleration;
@property (assign, nonatomic) CGFloat ballXVelocity;
@property (assign, nonatomic) CGFloat ballYVelocity;
@property (assign, nonatomic) CGPoint draggingPoint;
@property (strong, nonatomic) NSMutableDictionary *allBallDict;
@end

@implementation BIDScrollView {
    CGFloat centerX;
    CGFloat centerY;
    BIDBallLabelView *lastAddballLabelView;
}

//等比率缩放
- (UIImage *)scaleImage:(UIImage *)image toScale:(float)scaleSize
{
    UIGraphicsBeginImageContext(CGSizeMake(image.size.width * scaleSize, image.size.height * scaleSize));
    [image drawInRect:CGRectMake(0, 0, image.size.width * scaleSize, image.size.height * scaleSize)];
    UIImage *scaledImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return scaledImage;
}

- (void)commonInit
{
    NSLog(@"%s", __func__);
    centerX = gcenterX;
    centerY = gcenterY;

    //self.imageBall = [UIImage imageNamed:@"leash_rangeIndicatorActive"];
    //self.imageBall = [UIImage imageNamed:@"radar_BlueInsideCircle"];
    //self.imageBall = [UIImage imageNamed:@"radar_marker"];
#if 0
    self.currentPoint = CGPointMake((self.bounds.size.width / 2.0f) +
                                    (self.imageBall.size.width / 2.0f),
                                    (self.bounds.size.height / 2.0f) + (self.imageBall.size.height / 2.0f));
#else
    NSLog(@"self.bounds.size.width = %f, self.bounds.size.height = %f", self.bounds.size.width, self.bounds.size.height);
    //self.currentPoint = CGPointMake(542, 542);
#endif
    _allBallDict = [NSMutableDictionary dictionaryWithCapacity:10];
}

- (void)addABallLabelWithLabel:(NSString*)text pointOnMe:(CGPoint)point {
    NSLog(@"%s: point.x = %f, point.y = %f", __func__, point.x, point.y);
    BIDBallLabelView *ballLabelView = [[BIDBallLabelView alloc] initWithLabel:text thePoint:point unKnowBle:NO];
    NSLog(@"ballLabelView.frame.origin.x = %f, ballLabelView.frame.origin.y = %f", ballLabelView.frame.origin.x, ballLabelView.frame.origin.y);
    [self addSubview:ballLabelView];
    lastAddballLabelView = ballLabelView;
//    [self setNeedsDisplay];
}

- (void)addABallLabelWithLabel:(NSString*)text UUID:(NSString*)uuid pointOnMe:(CGPoint)point {
    NSLog(@"%s: point.x = %f, point.y = %f, uuid=%@", __func__, point.x, point.y, uuid);
    BIDBallLabelView *ballLabelView = [[BIDBallLabelView alloc] initWithLabel:text thePoint:point unKnowBle:NO UUID:uuid];
//    NSLog(@"ballLabelView.frame.origin.x = %f, ballLabelView.frame.origin.y = %f", ballLabelView.frame.origin.x, ballLabelView.frame.origin.y);
    [self addSubview:ballLabelView];
    lastAddballLabelView = ballLabelView;
    [_allBallDict setObject:ballLabelView forKey:uuid];
    NSLog(@"_allBallDict.count = %d", _allBallDict.count);
}

- (void)addABallLabelWithLabel:(NSString*)text UUID:(NSString*)uuid pointOnMe:(CGPoint)point unKnowBle:(BOOL)unKnowBle {
    BIDBallLabelView *ballLabelView = [[BIDBallLabelView alloc] initWithLabel:text thePoint:point unKnowBle:unKnowBle];
    [self addSubview:ballLabelView];
    lastAddballLabelView = ballLabelView;
    [_allBallDict setObject:ballLabelView forKey:uuid];
    NSLog(@"_allBallDict.count = %d", _allBallDict.count);
}

- (void)renameABall:(NSString*)uuid newName:(NSString*)newname {
    NSLog(@"%s, uuid = %@, newname = %@", __func__, uuid, newname);
    BIDBallLabelView *aBallView = _allBallDict[uuid];
    [aBallView rename:newname];
}

- (void)setABallAlpha:(CGFloat)alpha UUID:(NSString*)uuid flash:(BOOL)flash {
//    NSLog(@"%s, uuid = %@, alpha = %f", __func__, uuid, alpha);
//    for (BIDBallLabelView *aBallView in self.subviews) {
//        if ([aBallView isKindOfClass:[BIDBallLabelView class]]) {
//
//        }
//    }
    BIDBallLabelView *aBallView  = _allBallDict[uuid];
//    NSLog(@"aBallView = %@", aBallView);
    if (aBallView.alpha != alpha) {
        aBallView.alpha = alpha;
    } else {
//        NSLog(@"oh, aBallView.alpha == alpha, return now!");
        return;
    }

    if (flash) {
        CABasicAnimation *ani = [self opacityTimes_Animation:4 durTimes:0.4];
        [aBallView.layer addAnimation:ani forKey:uuid];
        [self performSelector:@selector(removeABallAnimation:) withObject:uuid afterDelay:2];
    }
}

- (void)removeABallAnimation:(NSString*)uuid {
//    NSLog(@"%s: uuid = %@", __func__, uuid);
    BIDBallLabelView *aBallView = _allBallDict[uuid];
    [aBallView.layer removeAnimationForKey:uuid];
    aBallView.alpha = 1.0f;
}

- (void)removeABallFromMe:(NSString*)uuid {
    BIDBallLabelView *aBallView = _allBallDict[uuid];
    [aBallView removeFromSuperview];
}

- (void)ssss {
    NSLog(@"Enter");
    CGFloat cenX = centerX * _currentScale;
    CGFloat cenY = centerY * _currentScale;
    NSLog(@"cenX = %f, cenY = %f", cenX, cenY);
    UIBezierPath *path = [UIBezierPath bezierPath];
    CGFloat r = _currentScale * [BIDConstant distanceFromPointX:lastAddballLabelView.curPoint distanceToPointY:CGPointMake(centerX , centerY)];
//    r1 = [self distanceFromPointX:CGPointMake(cenX, cenY) distanceToPointY:CGPointMake(lastAddballLabelView.curPoint.x*_currentScale, lastAddballLabelView.curPoint.y*_currentScale)];
//    CGFloat r2 = 100.0f;
    NSLog(@"r = %f", r);
    [path addArcWithCenter:CGPointMake(cenX, cenY) radius:r startAngle:0 endAngle:2*M_PI clockwise:YES];
    //创建一个Animation对象，把它的keypath属性的值设为"position"
    CAKeyframeAnimation *moveAnim = [CAKeyframeAnimation animationWithKeyPath:@"position"];
    moveAnim.path = path.CGPath;//用path实现
    
//    NSMutableArray *values=[NSMutableArray array];
//    for (int i=0; i<9; i++) {
//        x = cenX + (r1 * cos(M_PI*2.0/10*i));
//        y = cenY + (r1 * sin(M_PI*2.0/10*i));
//        [values addObject:[NSValue valueWithCGPoint:CGPointMake(x,y)]];
//    }
//    moveAnim.values = values;//用values实现
    moveAnim.duration = 5.0f;
    moveAnim.removedOnCompletion = NO;
//    [lastAddballLabelView.imageView.layer addAnimation:moveAnim forKey:nil];
//    [lastAddballLabelView.label.layer addAnimation:moveAnim forKey:nil];
//    [lastAddballLabelView.layer addAnimation:moveAnim forKey:nil];
    for (BIDBallLabelView *aBallView in self.subviews) {
        if ([aBallView isKindOfClass:[BIDBallLabelView class]]) {
            [aBallView.layer addAnimation:moveAnim forKey:nil];
        }
    }
    NSLog(@"Leave");
}

//获取一个随机整数，范围在[from,to]，包括from，包括to
- (u_int32_t)getRandomNumber:(u_int32_t)from to:(u_int32_t)to
{
    //double val = floorf(((double)arc4random() / ARC4RANDOM_MAX) * 100.0f); //0到100之间的浮点数
    return (u_int32_t)(from + (arc4random() % (to - from + 1)));
}

//保证得到的弧度都是0～2*M_PI
- (CGFloat)getAngle:(BIDBallLabelView*)aBallView {
    CGFloat angle = 0, x, y;
    CGPoint point = aBallView.curPoint;
    if ((point.x > centerX) && (point.y < centerY)) {//1象限
//        NSLog(@"Xiang Xian 1");
        x = point.x - centerX;
        y = centerY - point.y;
        angle = atanf(y/x);
//        angle = -angle;
        angle = (M_PI + M_PI) - angle;
    } else if (point.x > centerX && point.y > centerY) {//4
//        NSLog(@"Xiang Xian 4");
        x = point.x - centerX;
        y = centerY - point.y;
        angle = atanf(y/x);
        angle = -angle;
    } else if (point.x < centerX && point.y < centerY) {//2
//        NSLog(@"Xiang Xian 2");
        x = point.x - centerX;
        y = centerY - point.y;
        angle = atanf(y/x);
        angle = M_PI - angle;
    } else if ((point.x < centerX) && (point.y > centerY)) {//3
//        NSLog(@"Xiang Xian 3");
        x = point.x - centerX;
        y = centerY - point.y;
        angle = atanf(y/x);
        angle = M_PI - angle;
    } else {
        if (point.x == centerX) {
//            NSLog(@"point.x == centerX");
            if (point.y < centerY) {
                angle = M_PI_2 * 3;
            } else {
                angle = M_PI_2;
            }
        } else if (point.y == centerY) {
//            NSLog(@"point.y == centerY");
            if (point.x < centerX) {
                angle = M_PI;
            } else {
                angle = 0.0f;
            }
        }
    }
    return angle;
}

- (void)ssss1 {
    NSLog(@"%s", __func__);
    CGFloat cenX = centerX * _currentScale;
    CGFloat cenY = centerY * _currentScale;
    NSLog(@"cenX = %f, cenY = %f", cenX, cenY);
    CGFloat startAngle = 0.1;
    
    BOOL flag = YES;
    for (BIDBallLabelView *aBallView in self.subviews) {
        if ([aBallView isKindOfClass:[BIDBallLabelView class]]) {
            //            startAngle = tanf((ABS(aBallView.curPoint.x - centerX)) / (ABS(aBallView.curPoint.y - centerY)));
            //            startAngle = tan((aBallView.curPoint.x - centerX) / (aBallView.curPoint.y - centerY));
            startAngle = [self getAngle:aBallView];
            //            startAngle = tanf((300 - centerX) / (300 - centerY));
            //            startAngle -= M_PI_2;
            NSLog(@"x = %f, y = %f, centerX = %f, centerY = %f", aBallView.curPoint.x*_currentScale, aBallView.curPoint.y*_currentScale, centerX, centerY);
            NSLog(@"startAngle = %f, degrees = %f", startAngle, startAngle*180/M_PI);
            UIBezierPath *path = [UIBezierPath bezierPath];
            CGFloat r = _currentScale * [BIDConstant distanceFromPointX:aBallView.curPoint distanceToPointY:CGPointMake(centerX , centerY)];
            NSLog(@"r = %f", r);
            if (YES == flag) {
                [path addArcWithCenter:CGPointMake(cenX, cenY) radius:r startAngle:startAngle endAngle:startAngle-M_PI/360 clockwise:YES];
            } else {
                [path addArcWithCenter:CGPointMake(cenX, cenY) radius:r startAngle:startAngle endAngle:startAngle+M_PI/360 clockwise:NO];
            }
            CAKeyframeAnimation *moveAnim = [CAKeyframeAnimation animationWithKeyPath:@"position"];
            moveAnim.delegate = aBallView;
            moveAnim.path = path.CGPath;
            //            moveAnim.duration = [self getRandomNumber:5 to:30];
            moveAnim.duration = 5.0f;
            moveAnim.repeatCount = 1;
            moveAnim.removedOnCompletion = NO;
            moveAnim.fillMode = kCAFillModeForwards;
            [aBallView.layer addAnimation:moveAnim forKey:@"myArc"];
            
            //            startAngle += 0.5f;
            //            flag = !flag;
        }
    }
}

- (void)testBLErssi:(NSDictionary *)rssiWithUUID {
//    NSLog(@"%s: rssi = %@, uuid = %@", __func__, rssiWithUUID[@"rssi"], rssiWithUUID[@"uuid"]);
    BIDBallLabelView *aBallView = _allBallDict[rssiWithUUID[@"uuid"]];
    if (aBallView.angleUpdating) {
//        NSLog(@"angle updating ,so just return.");
//        [self performSelector:@selector(testBLErssi:) withObject:rssiWithUUID afterDelay:1];
//        [self performSelectorOnMainThread:@selector(testBLErssi:) withObject:rssiWithUUID waitUntilDone:NO];
        return;
    }
    if (aBallView.rssiUpdating) {
//        NSLog(@"Oh, myself rssi is updatting, so just return too.");
//        [self performSelector:@selector(testBLErssi:) withObject:rssiWithUUID afterDelay:1];
//        [self performSelectorOnMainThread:@selector(testBLErssi:) withObject:rssiWithUUID waitUntilDone:NO];
        return;
    }
//    NSLog(@"aBallView = %@", aBallView);
    if (aBallView.canMove == NO) {
        return;
    }
    aBallView.rssiUpdating = YES;
    CGFloat x, y;
    NSNumber *rssiNumber = rssiWithUUID[@"rssi"];
    NSDictionary *dXY = [self getXYFromRssi:rssiNumber ballView:aBallView];
    aBallView.curPoint = CGPointMake([dXY[@"x"] floatValue], [dXY[@"y"] floatValue]);
    x = aBallView.curPoint.x * self.currentScale - lastAddballLabelView.imageBall.size.width/2;
    y = aBallView.curPoint.y * self.currentScale - lastAddballLabelView.imageBall.size.height/2;

#if 0
    CABasicAnimation *ba = [self moveY:1.0 Y:[NSNumber numberWithFloat:y]];
    ba.delegate = aBallView;
    [aBallView.layer addAnimation:ba forKey:nil];
#elif 0
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDuration:1.0];
    [aBallView setFrame:CGRectMake(x, y, aBallView.frame.size.width, aBallView.frame.size.height)];
    [UIView commitAnimations];
#elif 0
    [UIView animateWithDuration:1.0f animations:^ {
        [aBallView setFrame:CGRectMake(x, y, aBallView.frame.size.width, aBallView.frame.size.height)];
    }];
#elif 0
    CGAffineTransform transform = CGAffineTransformMakeTranslation(x, y);
    [UIView animateWithDuration:1.0f animations:^ {
        aBallView.transform = transform;
    }];
#elif 0
    [UIView animateWithDuration:1.0f delay:0.0f options:UIViewAnimationOptionAllowUserInteraction animations:^{
        [aBallView setFrame:CGRectMake(x, y, aBallView.frame.size.width, aBallView.frame.size.height)];
    } completion:^(BOOL finish) {
//        NSLog(@"oh, animation complete!");
    }
     ];
#else
    [UIView animateWithDuration:3.0f delay:0.0f usingSpringWithDamping:1.7f initialSpringVelocity:0.0f options:UIViewAnimationOptionAllowUserInteraction animations:^(void) {
            [aBallView setFrame:CGRectMake(x, y, aBallView.frame.size.width, aBallView.frame.size.height)];
        }
                     completion:^(BOOL finished) {
//                         NSLog(@"Yes, the animation completed.");
                         aBallView.rssiUpdating = NO;
                     }];
#endif
//    NSLog(@"Leave %s", __func__);
}

- (void)testBLEArc:(NSDictionary*)angleWithUUID {
//    NSLog(@"%s", __func__);
    BIDBallLabelView *aBallView = _allBallDict[angleWithUUID[@"uuid"]];
    if (aBallView.rssiUpdating) {
        NSLog(@"rssi updating ,so just return.");
//        [self performSelector:@selector(testBLEArc:) withObject:angleWithUUID afterDelay:1];
//        [self performSelectorOnMainThread:@selector(testBLEArc:) withObject:angleWithUUID waitUntilDone:NO];
        return;
    }
    if (aBallView.angleUpdating) {
        NSLog(@"Oh, myself angle is updating, so return too.");
//        [self performSelector:@selector(testBLEArc:) withObject:angleWithUUID afterDelay:1];
//        [self performSelectorOnMainThread:@selector(testBLEArc:) withObject:angleWithUUID waitUntilDone:NO];
        return;
    }
    if (aBallView.canMove == NO) {
        return;
    }
    aBallView.angleUpdating = YES;
    CGFloat startAngle = [self getAngle:aBallView];//得到当前点的弧度
    CGFloat stopAngle = [angleWithUUID[@"angle"] floatValue];
//    NSLog(@"aBallView.curPoint.x = %f, aBallView.curPoint.y = %f, centerX = %f, centerY = %f", aBallView.curPoint.x, aBallView.curPoint.y, centerX, centerY);
//    NSLog(@"startAngle = %f, degrees = %f", startAngle, startAngle*180/M_PI);
//    NSLog(@"stopAngle = %f, degrees = %f", stopAngle, stopAngle*180/M_PI);
    UIBezierPath *path = [UIBezierPath bezierPath];
    CGFloat r = _currentScale * [BIDConstant distanceFromPointX:aBallView.curPoint distanceToPointY:CGPointMake(centerX , centerY)];
//    NSLog(@"r = %f", r);
    CGFloat cenX = centerX * _currentScale;
    CGFloat cenY = centerY * _currentScale;
    BOOL clockwiseflag = YES;
    if (startAngle >= stopAngle) {
        if (startAngle - stopAngle <= M_PI) {
            clockwiseflag = NO;
        } else {
            clockwiseflag = YES;
        }
    } else {
        if (stopAngle - startAngle <= M_PI) {
            clockwiseflag = YES;
        } else {
            clockwiseflag = NO;
        }
    }
    if (YES == clockwiseflag) {
        [path addArcWithCenter:CGPointMake(cenX, cenY) radius:r startAngle:startAngle endAngle:stopAngle clockwise:YES];
    } else {
        [path addArcWithCenter:CGPointMake(cenX, cenY) radius:r startAngle:startAngle endAngle:stopAngle clockwise:NO];
    }
    CAKeyframeAnimation *moveAnim = [CAKeyframeAnimation animationWithKeyPath:@"position"];
    moveAnim.delegate = aBallView;
    moveAnim.path = path.CGPath;
    moveAnim.duration = 1.5f;
    moveAnim.removedOnCompletion = NO;
    moveAnim.fillMode = kCAFillModeForwards;
    [aBallView.layer addAnimation:moveAnim forKey:@"myArc"];
    
//    NSNumber *angleNumber = angleWithUUID[@"angle"];
//    NSDictionary *dXY = [self getXYFromAngle:angleNumber ballView:aBallView];
//    aBallView.curPoint = CGPointMake([dXY[@"x"] floatValue], [dXY[@"y"] floatValue]);
//    x = aBallView.curPoint.x * self.currentScale - lastAddballLabelView.imageBall.size.width/2;
//    y = aBallView.curPoint.y * self.currentScale - lastAddballLabelView.imageBall.size.height/2;
}

- (NSDictionary *)getXYFromAngle:(NSNumber*)angleNumber ballView:(BIDBallLabelView*)aBallView {
//    CGFloat stopAngle = [angleNumber floatValue];
    
    return nil;
}

- (NSDictionary *)getXYFromRssi:(NSNumber*)rssiNumber ballView:(BIDBallLabelView*)aBallView {
    CGFloat bilv = ( (-[rssiNumber floatValue]) - 30) / (95 - 30);
//    NSLog(@"bilv = %f", bilv);
    
    CGFloat curR = bilv * gradius;
    CGFloat x = centerX + (curR * aBallView.curCos);
    CGFloat y = centerY + (curR * aBallView.curSin);
    NSDictionary *adict = @{@"x" : [NSNumber numberWithFloat:x],
                            @"y" : [NSNumber numberWithFloat:y]};
//    NSLog(@"adict = %@", adict);
    return adict;
}

//缩放之后调用此函数
- (void) updateZoom {
    NSLog(@"%s", __func__);
    CGFloat x , y;
    CGRect newRect;
    for (BIDBallLabelView *aBall in self.subviews) {
        if ([aBall isKindOfClass:[BIDBallLabelView class]]) {
            aBall.scaleNow = self.currentScale;
//            NSLog(@"aBall.curPoint.x = %f, aBall.curPoint.y = %f", aBall.curPoint.x, aBall.curPoint.y);
            x = aBall.curPoint.x * self.currentScale - lastAddballLabelView.imageBall.size.width/2;
            y = aBall.curPoint.y * self.currentScale - lastAddballLabelView.imageBall.size.height/2;
//            x = aBall.curPoint.x * self.currentScale ;
//            y = aBall.curPoint.y * self.currentScale ;

            //            NSLog(@"x = %f, y = %f", x, y);
            newRect = CGRectMake(x, y, aBall.frame.size.width, aBall.frame.size.height);
            //            NSLog(@"aBall.frame.size.width = %f, aBall.frame.size.height = %f", aBall.frame.size.width, aBall.frame.size.height);
            //            NSLog(@"aBall.bounds.size.width = %f, aBall.bounds.size.height = %f", aBall.bounds.size.width, aBall.bounds.size.height);
            [aBall setFrame:newRect];
            //            NSLog(@"Oh, YES! I'm a BIDBallLabelView class...");
        }
        else {
            //            NSLog(@"Oh, I'm not a BIDBallLabelView class...%@", aBall);
        }
    }
    //    [self setNeedsDisplay];
}



/*
 x = xo + r * cos(α)
 y = yo + r * sin(α)
 */

- (void)tttt {
    /* 
     * 已知圆心坐标P,圆上一个点坐标A,A-P直线以圆心旋转角度x后,求新的A点坐标
     P新.x = (A.x - P.x) cos((x/180)*PI) + P.x
     P新.y = (A.x - P.x) sin((x/180)*PI) + P.y
     */
}


- (CABasicAnimation *)opacityForever_Animation:(float)time //永久闪烁的动画
{
    CABasicAnimation *animation=[CABasicAnimation animationWithKeyPath:@"opacity"];
    animation.fromValue=[NSNumber numberWithFloat:1.0];
    animation.toValue=[NSNumber numberWithFloat:0.0];
    animation.autoreverses=YES;
    animation.duration=time;
    animation.repeatCount=FLT_MAX;
    animation.removedOnCompletion=NO;
    animation.fillMode=kCAFillModeForwards;
    return animation;
}

- (CABasicAnimation *)opacityTimes_Animation:(float)repeatTimes durTimes:(float)time; //有闪烁次数的动画
{
    CABasicAnimation *animation=[CABasicAnimation animationWithKeyPath:@"opacity"];
    animation.fromValue=[NSNumber numberWithFloat:0.1];
    animation.toValue=[NSNumber numberWithFloat:1.0];
    animation.repeatCount=repeatTimes;
    animation.duration=time;
    animation.removedOnCompletion=NO;
    animation.fillMode=kCAFillModeForwards;
    animation.timingFunction=[CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseIn];
    animation.autoreverses=YES;
    return  animation;
}

- (CABasicAnimation *)moveY:(float)time Y:(NSNumber *)y //纵向移动
{
    CABasicAnimation *animation=[CABasicAnimation animationWithKeyPath:@"transform.translation.y"];
    animation.toValue=y;
    animation.duration=time;
    animation.removedOnCompletion=NO;
    animation.fillMode=kCAFillModeForwards;
    return animation;
}

- (CABasicAnimation *)movepoint:(CGPoint )point //点移动
{
    CABasicAnimation *animation=[CABasicAnimation animationWithKeyPath:@"transform.translation"];
    animation.toValue=[NSValue valueWithCGPoint:point];
    animation.removedOnCompletion=NO;
    animation.fillMode=kCAFillModeForwards;
    return animation;
}

//-(void)doTranslate//位移
//{
//    //如果单次 只改变一次 如果连续 在以前基础上再次进行移位
//    CGAffineTransform transform = isSingle?CGAffineTransformMakeTranslation(10, 10):CGAffineTransformTranslate(bigImage.transform, 10, 10);
//
//    //    [UIView beginAnimations:nil context:nil];
//    //    bigImage.transform = transform;
//    //    [UIView commitAnimations];
//    [UIView animateWithDuration:1 animations:^{
//        bigImage.transform = transform;
//    }];
//}


- (id)initWithCoder:(NSCoder *)coder {
    NSLog(@"%s", __func__);
    if (self = [super initWithCoder:coder]) {
        [self commonInit];
    }
    return self;
}

- (id)initWithFrame:(CGRect)frame
{
    NSLog(@"%s", __func__);
    self = [super initWithFrame:frame];
    if (self) {
        [self commonInit];
        
    }
    return self;
}

- (void)drawRect:(CGRect)rect
{
    [super drawRect:rect];
    NSLog(@"%s: x = %f, y = %f", __func__, _currentPoint.x, _currentPoint.y);
    // Drawing code
    //    [self.imageBall drawAtPoint:self.currentPoint];
    //[self.imageBall drawAtPoint:self.currentPoint blendMode:kCGBlendModeNormal alpha:_ballAlpha];
}

#pragma mark -
- (void)setCurrentPoint:(CGPoint)newPoint {
    NSLog(@"%s: newPoint.x = %f, newPoint.y = %f", __func__, newPoint.x, newPoint.y);
#if 1
    _currentPoint = newPoint;
#else
    self.previousPoint = self.currentPoint;
    _currentPoint = newPoint;
    
    if (self.currentPoint.x < 0) {
        _currentPoint.x = 0;
        self.ballXVelocity = -self.ballXVelocity / 2;
    }
    if (self.currentPoint.y < 0){
        _currentPoint.y = 0;
        self.ballYVelocity = -self.ballYVelocity / 2;
    }
    if (self.currentPoint.x > self.bounds.size.width - self.imageBall.size.width) {
        _currentPoint.x = self.bounds.size.width - self.imageBall.size.width;
        self.ballXVelocity = -self.ballXVelocity / 2;
    }
    if (self.currentPoint.y > self.bounds.size.height - self.imageBall.size.height) {
        _currentPoint.y = self.bounds.size.height - self.imageBall.size.height;
        self.ballYVelocity = -self.ballYVelocity / 2;
    }
#endif
    [self setNeedsDisplay];
}

- (void)updateDragging {
    NSLog(@"%s", __func__);
    //    self.currentPoint = CGPointMake(_currentPoint.x - _draggingPoint.x, _currentPoint.y - _draggingPoint.y);
    //[self setNeedsDisplay];
}

#pragma mark -
- (void) drawRadar {
    CALayer *waveLayer=[CALayer layer];
    waveLayer.frame = CGRectMake(centerX*_currentScale-10/2, centerY*_currentScale-10/2, 10, 10);
//    int colorInt = arc4random()%9;
    
//    CGFloat alpha = (arc4random()%101) / 100;
    CGFloat red = (arc4random() % 256) / 255.0;
    CGFloat green = (arc4random() % 256) / 255.0;
    CGFloat blue = (arc4random() % 256) / 255.0;
//    NSLog(@"red = %f, green = %f, blue = %f", red, green, blue);
    UIColor *colorRandom =[UIColor colorWithRed:red
                    green:green
                     blue:blue
                    alpha:1.0];
    int colorInt = 10;
    switch (colorInt) {
        case 0:
            waveLayer.borderColor =[UIColor redColor].CGColor;
            break;
        case 1:
            waveLayer.borderColor =[UIColor grayColor].CGColor;
            break;
        case 2:
            waveLayer.borderColor =[UIColor purpleColor].CGColor;
            break;
        case 3:
            waveLayer.borderColor =[UIColor orangeColor].CGColor;
            break;
        case 4:
            waveLayer.borderColor =[UIColor yellowColor].CGColor;
            break;
        case 5:
            waveLayer.borderColor =[UIColor greenColor].CGColor;
            break;
        case 6:
            waveLayer.borderColor =[UIColor blueColor].CGColor;
            break;
        case 7:
            waveLayer.borderColor = [UIColor grayColor].CGColor;
            break;
        case 8:
            waveLayer.borderColor = [UIColor lightGrayColor].CGColor;
            break;
        case 9:
            waveLayer.borderColor = [UIColor lightTextColor].CGColor;
            break;
        case 10:
            waveLayer.borderColor = [UIColor darkGrayColor].CGColor;
            break;
        case 11:
            waveLayer.borderColor = colorRandom.CGColor;
            break;
        default:
            waveLayer.borderColor =[UIColor blackColor].CGColor;
            break;
    }
    waveLayer.borderWidth =0.7;
    waveLayer.cornerRadius =5.0;
    
    [self.layer addSublayer:waveLayer];
    [self scaleBegin:waveLayer];
}

-(void)scaleBegin:(CALayer *)aLayer
{
    const float maxScale=140.0;
    //    NSLog(@"transform.m11 = %f", aLayer.transform.m11);
    if (aLayer.transform.m11 < maxScale) {
        if (aLayer.transform.m11==1.0) {
            [aLayer setTransform:CATransform3DMakeScale( 1.1, 1.1, 5.0)];
            
        }else{
            [aLayer setTransform:CATransform3DScale(aLayer.transform, 1.1, 1.1, 1.0)];
        }
        if (aLayer.transform.m11 < 10) {
            [self performSelector:_cmd withObject:aLayer afterDelay:0.005];
        }
        else if (aLayer.transform.m11 < 20) {
            [self performSelector:_cmd withObject:aLayer afterDelay:0.01];
        }
        else if (aLayer.transform.m11 < 30) {
            [self performSelector:_cmd withObject:aLayer afterDelay:0.02];
        }
        else if (aLayer.transform.m11 < 40) {
            [self performSelector:_cmd withObject:aLayer afterDelay:0.03];
        }
        else if (aLayer.transform.m11 < 50) {
            [self performSelector:_cmd withObject:aLayer afterDelay:0.04];
        }
        else if (aLayer.transform.m11 < 60) {
            [self performSelector:_cmd withObject:aLayer afterDelay:0.05];
        }
        else {
            [self performSelector:_cmd withObject:aLayer afterDelay:0.06];
        }
        
    } else {
        [aLayer removeFromSuperlayer];
    }
}

#pragma mark - 
- (NSString*)getUUID:(BIDBallLabelView*)ballView {
    NSLog(@"%s", __func__);
    for (NSString *uuid in _allBallDict) {
        if ([_allBallDict[uuid] isEqual:ballView]) {
            NSLog(@"got it : %@", ballView);
            return uuid;
        }
    }
    return nil;
}
@end
