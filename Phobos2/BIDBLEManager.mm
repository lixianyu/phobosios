//
//  BIDBLEManager.m
//  Phobos2
//
//  Created by Li Xianyu on 13-12-1.
//  Copyright (c) 2013年 Li Xianyu. All rights reserved.
//

#import "BIDBLEManager.h"
#import <Corebluetooth/CBCentralManager.h>
#import <AudioToolbox/AudioToolbox.h>
#import "BIDBLEiOS6Delegate.h"
#import "BIDBLEiOS7Delegate.h"
#import "BIDAppDelegate.h"
#import "MMProgressHUD.h"
#import "BIDConstant.h"
#import "BIDRadarViewController6.h"
#import "BIDRadarViewController6iPad.h"

typedef NS_ENUM(NSInteger, CommunicateState){
    CommunicateStateIdle = 0,
    CommunicateStateBuzzer,
    CommunicateStateTemperature,
    CommunicateStateBattery,
    CommunicateStateLinkLoss,
    CommunicateStateChangeName
};

@interface BIDBLEManager() <CLLocationManagerDelegate>
@property (assign, nonatomic) CommunicateState communicateState;
@property (assign, nonatomic) NSInteger retValue;
@property (strong, nonatomic) NSString *buzzerUUID;
@property (strong, nonatomic) NSString *changeNameUUID;
@property (strong, nonatomic) NSString *changeNameName;
@property (strong, nonatomic) CBService *buzzerService;
@property (strong, nonatomic) NSString *linkLossUUID;
@property (strong, nonatomic) CBService *linkLossService;
@property (strong, nonatomic) CBService *batteryService;
@property (strong, nonatomic) CBService *temperatureService;
@property (strong, nonatomic) CBCharacteristic *temperatureCharacteristic;
@property (strong, nonatomic) CBService *changeNameService;

@property (strong, nonatomic) NSString *batteryUUID;
@property (strong, nonatomic) NSString *temperatureUUID;
@property (strong, nonatomic) CLLocationManager *locationManager;
//@property (strong, nonatomic) CLLocation *curLocation;

@property (strong, nonatomic) NSMutableArray *delegate;
@property (strong, nonatomic) NSMutableSet *actionList;
@property (strong, nonatomic) CBCentralManager *cbManager;
@property (strong, nonatomic) NSMutableDictionary *everConnectedUUID; //所有曾经已连接过的
@property (retain, nonatomic) NSMutableDictionary *knownPeripheralsDict;//所有曾经已连接过的
//@property (strong, nonatomic) NSArray *knownPeripherals;
@property (strong, nonatomic) NSTimer *stateCheckTimer; //每隔5秒检查一下状态
@end

@implementation BIDBLEManager {
    SystemSoundID winSoundID;
    Byte linkLossValue;
    UInt32 count1;
}

static CGFloat systemVersion;
+ (BIDBLEManager *)sharedInstance {
    static dispatch_once_t onceToken;
    static BIDBLEManager *sSharedInstance;
    dispatch_once(&onceToken, ^{
        systemVersion = [[[UIDevice currentDevice] systemVersion] floatValue];
        NSLog(@"systemVersion = %f", systemVersion);
        if (!sSharedInstance) {
            if (systemVersion < 7.0f && systemVersion >= 6.0f) {
                sSharedInstance = [[BIDBLEiOS6Delegate alloc] init];
            } else if (systemVersion >= 7.0f){
                sSharedInstance = [[BIDBLEiOS7Delegate alloc] init];
            } else {
                //不支持iOS6.0以下系统
    //            sSharedInstance = nil;
                sSharedInstance = [[BIDBLEiOS6Delegate alloc] init];
            }
        }
    });
    NSLog(@"%s: sSharedInstance = %@", __func__, sSharedInstance);
    return sSharedInstance;
}

- (void)bleJustForATest {
    NSLog(@"Enter %s", __func__);
#if 0
    UInt16 uint16Value = 0x0902;
    NSLog(@"uint16Value = 0x%04X", uint16Value);
    UInt8 a = (uint16Value >> 8) & 0x00FF;
    UInt8 b = uint16Value & 0x00FF;
    NSLog(@"a = %d, b = %d", a, b);
#endif
    if (count1 > 100) {
        return;
    }
    UInt32 iRandom = arc4random();
    NSLog(@"iRadom = 0x%08X", iRandom);
    [self performSelector:@selector(bleJustForATest) withObject:nil afterDelay:0.2];
    count1++;
}

- (void)initBLEManagerContinue {
    NSLog(@"%s, _cbManager = %@", __func__, _cbManager);
    NSMutableDictionary *dict;
    for (NSUUID *aUUID in _everConnectedUUID) {
        NSLog(@"aUUID = %@", aUUID);
        dict = [self churuAlert:[aUUID UUIDString]];
        if ([dict[@"chu"] boolValue]) {
            [self saveChuRuState:YES UUID:dict[@"uuid"] churuState:NO];
        }
        if ([dict[@"ru"] boolValue]) {
            [self connectPhobos:dict[@"uuid"]];
        }
    }
    count1 = 0;
//    [self bleJustForATest];
}

- (void)initBLEManager {
    NSLog(@"%s, _cbManager = %@, _communicateState = %d", __func__, _cbManager, _communicateState);
    _communicateState = CommunicateStateIdle;
    if (_locationManager == nil) {
        _locationManager = [[CLLocationManager alloc] init];
        _locationManager.delegate = self;
    }

    if (_cbManager == nil) {
        
        if (systemVersion >= 7.0) {
            _cbManager = [[CBCentralManager alloc] initWithDelegate:self queue:nil options:@{CBCentralManagerOptionRestoreIdentifierKey : @"phobosCentralManagerIdentifier"}];
        } else {
            _cbManager = [[CBCentralManager alloc] initWithDelegate:self queue:nil];
        }
        
        NSLog(@"_cbManager = %@", _cbManager);
        NSNotificationCenter *center = [NSNotificationCenter defaultCenter];
        [center addObserver:self selector:@selector(handleHaveDeleteOneConnectedPeripheral:) name:@"Notification_haveDeleteOneConnectedPeripheral" object:nil];
        
        [self initEverConnectedUUID];
        
        _delegate = [NSMutableArray arrayWithCapacity:2];
        _actionList = [NSMutableSet setWithCapacity:1];
//        _allConnectPeripheral = [NSMutableDictionary dictionaryWithCapacity:5];
//        [self performSelector:@selector(testit) withObject:nil afterDelay:2.1];
//        [self testit];
//        [self testit1];
        
        // iOS8注册本地通知
        if ([UIApplication instancesRespondToSelector:@selector(registerUserNotificationSettings:)]) {
            [[UIApplication sharedApplication] registerUserNotificationSettings:[UIUserNotificationSettings settingsForTypes:UIUserNotificationTypeAlert|UIUserNotificationTypeBadge|UIUserNotificationTypeSound categories:nil]];
        }

        if ([UIApplication sharedApplication].applicationState == UIApplicationStateBackground) {
            NSLog(@"Background, so return!!!!");
            return;
        }
    }
    NSLog(@"Leave %s", __func__);
}

- (void)initEverConnectedUUID {
    NSLog(@"%s", __func__);
    NSString *filePath = [self dataFilePath];
    _everConnectedUUID = [NSMutableDictionary dictionaryWithCapacity:5];
    if ([[NSFileManager defaultManager] fileExistsAtPath:filePath]) {
        NSArray *array = [[NSArray alloc] initWithContentsOfFile:filePath];
        for (NSString *uuid in array) {
            [_everConnectedUUID setObject:uuid forKey:[[NSUUID alloc] initWithUUIDString:uuid]];
        }
    }
    NSArray *knownPeripherals = [_cbManager retrievePeripheralsWithIdentifiers:[self.everConnectedUUID allKeys]];
    NSLog(@"%s: knownPeripherals = %@", __func__, knownPeripherals);
    self.knownPeripheralsDict = [NSMutableDictionary dictionaryWithCapacity:3];
    
    for (CBPeripheral *aPeripheral in knownPeripherals) {
        [self.knownPeripheralsDict setObject:aPeripheral forKey:aPeripheral.identifier.UUIDString];
        aPeripheral.delegate = self;
    }
    NSLog(@"%s: knownPeripheralsDict = %@", __func__, self.knownPeripheralsDict);
}

- (void)handleHaveDeleteOneConnectedPeripheral: (NSNotification*)aNotification {
    NSLog(@"%s, aNotification = %@", __func__, aNotification);
    NSString *uuid = [aNotification object];
//    if (_allConnectPeripheral) {
//        if ([_allConnectPeripheral objectForKey:uuid] != nil) {
//            [_cbManager cancelPeripheralConnection:_allConnectPeripheral[uuid]];
//            [_allConnectPeripheral removeObjectForKey:uuid];
//        }
//    }
    CBPeripheral *perihperal = self.knownPeripheralsDict[uuid];
    if (perihperal) {
        [_cbManager cancelPeripheralConnection:perihperal];
    }
    NSLog(@"_everConnectedUUID.count = %d", _everConnectedUUID.count);
    if (_everConnectedUUID) {
        [_everConnectedUUID removeObjectForKey:[[NSUUID alloc] initWithUUIDString:uuid]];
    }
    if (_knownPeripheralsDict) {
        [_knownPeripheralsDict removeObjectForKey:uuid];
    }
    NSLog(@"_everConnectedUUID.count = %d", _everConnectedUUID.count);
    [self saveEverConnectedUUID:nil];
    
    for (id targetDelegate in _delegate) {
        if ([targetDelegate respondsToSelector:@selector(didDeleteOne:)]) {
            [targetDelegate didDeleteOne:uuid];
        }
    }
    NSLog(@"Leave %s", __func__);
}

- (void)saveEverConnectedUUID:(NSString*)uuid {
    NSLog(@"%s", __func__);
    NSMutableArray *uuidString = [NSMutableArray arrayWithCapacity:5];
    for (NSUUID *aNSUUID in _everConnectedUUID) {
        [uuidString addObject:aNSUUID.UUIDString];
    }
    [uuidString writeToFile:[self dataFilePath] atomically:YES];
}

//在配对新Phobos时候，才会走到这里
- (void)saveIt:(CBPeripheral*)peripheral {
    NSLog(@"Enter %s", __func__);
    [_everConnectedUUID setObject:peripheral.identifier.UUIDString forKey:peripheral.identifier];
    [_knownPeripheralsDict setObject:peripheral forKey:peripheral.identifier.UUIDString];
    peripheral.delegate = self;
    NSLog(@"_everConnectedUUID.count = %d", _everConnectedUUID.count);
    [self saveEverConnectedUUID:nil];
    NSLog(@"delegate.count = %d", _delegate.count);
    for (id targetDelegate in _delegate) {
        [self updateTheState:targetDelegate];
    }
    [self performSelector:@selector(initEverConnectedUUID) withObject:nil afterDelay:1.0];
    NSLog(@"Leave %s", __func__);
}

- (NSString *)dataFilePath {
    NSLog(@"%s", __func__);
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    return [documentsDirectory stringByAppendingPathComponent:@"dataPhobosEver.plist"];
}

#define DEBUG_BLE_STATE
- (void)testit {
#ifdef DEBUG_BLE_STATE
    if (systemVersion >= 7.0) {
//        NSArray *knownPeripherals1 = [_cbManager retrievePeripheralsWithIdentifiers:[_everConnectedUUID allKeys]];
        NSLog(@"%s: Already Known Peripherals = %@", __func__, _knownPeripheralsDict);
        [self performSelector:@selector(testit) withObject:nil afterDelay:3.1];
    }
#else
    // Nothing
#endif
}

- (void)testit1 {
//    UILocalNotification *notification=[[UILocalNotification alloc] init];
//    if (notification!=nil)
//    {
//        NSDate *now=[NSDate new];
//        //notification.fireDate=[now addTimeInterval:period];
//        notification.fireDate = [now dateByAddingTimeInterval:10];
//        NSLog(@"%d",10);
//        notification.timeZone=[NSTimeZone defaultTimeZone];
//        notification.soundName = @"Bell.mp3";
//        //notification.alertBody=@"TIME！";
//        notification.alertBody = [NSString stringWithFormat:@"@%时间到了!", @"起床"];
//        [[UIApplication sharedApplication] scheduleLocalNotification:notification];
//    }
}

- (void)doConnectPhobos:(CBPeripheral*)aPeripheral {
    NSLog(@"%s, uuid = %@", __func__, aPeripheral.identifier.UUIDString);
//    NSDictionary *connectOptions = @{CBConnectPeripheralOptionNotifyOnConnectionKey : [NSNumber numberWithBool:NO],
//                                     CBConnectPeripheralOptionNotifyOnDisconnectionKey : [NSNumber numberWithBool:NO],
//                                     CBConnectPeripheralOptionNotifyOnNotificationKey : [NSNumber numberWithBool:YES]};
    [_cbManager connectPeripheral:aPeripheral options:nil];
//    [_cbManager connectPeripheral:aPeripheral options:connectOptions];
}

- (void)cancelConnectPhobos:(NSString*)uuid {
    NSLog(@"%s", __func__);
    CBPeripheral *perihperal = self.knownPeripheralsDict[uuid];
    if (perihperal) {
        [_cbManager cancelPeripheralConnection:perihperal];
    }
}

/* 只有当调用connectPeripheral时，程序切到后台后，蓝牙4.0才会工作，
 * 系统负责时不时地去连接Phobos，
 * 入围报警的产生，是因为与Phobos建立了连接，当入围报警发生后就没有必要一直连着Phobos，
 * 一直连着会非常费电的
 */
- (void)cancelConnectPhobosRuAlert:(NSString*)uuid {
    NSLog(@"%s", __func__);
    if ([BIDAppDelegate isBackground]) {
        CBPeripheral *perihperal = self.knownPeripheralsDict[uuid];
        if (perihperal) {
            [_cbManager cancelPeripheralConnection:perihperal];
        }
    }
}

- (BOOL)isInRadarView {
    NSLog(@"%s", __func__);
    if (isPad) { //iPad的界面，雷达界面是一直显示的
        NSLog(@"Yes, I'm iPad.");
        return YES;
    } else {
        NSLog(@"Yes, I'm iPhone.");
        return [BIDRadarViewController6 isInRadarView];
    }
}

- (BOOL)ifNeedConnect:(NSString*)uuid {
    NSLog(@"%s", __func__);
    if (![self isInRadarView]) {
        return NO;
    }
    
    for (id aObserver in _delegate) {
        if ([aObserver isKindOfClass:[BIDRadarViewController6 class]]) {
            NSLog(@"Yes, got BIDRadarViewController6");
            BIDRadarViewController6 *radarViewController = (BIDRadarViewController6*)aObserver;
            if ([radarViewController isChecked:uuid]) {
                NSLog(@"isChecked return YES");
                return YES;
            } else {
                NSLog(@"isChecked return NO");
            }
            break;
        }
        if ([aObserver isKindOfClass:[BIDRadarViewController6iPad class]]) {
            NSLog(@"Yes, got BIDRadarViewController6iPad");
            BIDRadarViewController6iPad *radarViewController = (BIDRadarViewController6iPad*)aObserver;
            if ([radarViewController isChecked:uuid]) {
                NSLog(@"isChecked return YES");
                return YES;
            } else {
                NSLog(@"isChecked return NO");
            }
            break;
        }
    }
    return NO;
}

- (void)insertToActionList:(NSDictionary*)info {
    NSLog(@"%s", __func__);
    NSNumber *numberState = info[@"state"];
    CommunicateState state = (CommunicateState)[numberState integerValue];
    NSString *uuid = info[@"uuid"];
    
    NSNumber *numberState1;
    CommunicateState state1;
    NSString *uuid1 = nil;
    
    BOOL hadEqualUUID = NO;
    BOOL hadEqualState = NO;
    for (NSMutableDictionary *aInfo in _actionList) {
        uuid1 = aInfo[@"uuid"];
        if ([uuid1 isEqualToString:uuid]) {
            hadEqualUUID = YES;
            numberState1 = aInfo[@"state"];
            state1 = (CommunicateState)[numberState1 integerValue];
            if (state1 == state) {
                hadEqualState = YES;
            }
        }
    }
    
    if (hadEqualUUID) {
        //虽然UUID一样，但是只要state不一样，则是两个不同的事件。比如对相同uuid的设备读电池电量、soundBuzzer等
        if (!hadEqualState) {
            [_actionList addObject:info];
        }
    }
    else {
        [_actionList addObject:info];
    }
}

- (void)checkActionList {
    NSLog(@"%s, _actionList.count = %d", __func__, _actionList.count);
    if (_actionList.count == 0) {
        NSLog(@"_actionList.count == 0, so just return.");
        return;
    }
    if (_communicateState == CommunicateStateIdle) {
        NSMutableDictionary *info =[_actionList anyObject];
        NSString *nextUUID = info[@"uuid"];
        NSLog(@"nextUUID = %@", nextUUID);

        NSNumber *numberState = info[@"state"];
        CommunicateState state = (CommunicateState)[numberState integerValue];
        switch (state) {
            case CommunicateStateChangeName:
            {
                NSString *name = info[@"name"];
                [self continueChangePhobosName:name uuid:nextUUID];
                break;
            }
            
            case CommunicateStateBattery:
                [self readBattery:nextUUID];
                break;
                
            case CommunicateStateBuzzer:
                [self soundBuzzer:nextUUID];
                break;
                
            case CommunicateStateLinkLoss:
            {
                NSNumber *numberByte = info[@"value"];
                Byte linkLoss = [numberByte unsignedCharValue];
                [self setLinkLoss:nextUUID value:linkLoss];
                break;
            }
            
            case CommunicateStateTemperature:
                [self readTemperature:nextUUID];
                break;
                
            default:
                break;
        }
        [_actionList removeObject:info];
    }
    else { // Other communicate state.
        [self performSelector:@selector(checkActionList) withObject:nil afterDelay:3.0];
    }
}

- (void)continueChangePhobosName:(NSString*)name uuid:(NSString*)uuid {
    NSLog(@"%s", __func__);
    CBPeripheral *peripheral = self.knownPeripheralsDict[uuid];
    NSLog(@"peripheral = %@", peripheral);
    if (peripheral) {
        if (peripheral.state == CBPeripheralStateConnected) {
            NSString *peripheralName = peripheral.name;
            NSLog(@"name = %@, peripheralName = %@", name, peripheralName);
            if (![name isEqualToString:peripheralName]) {
                [self changeName:uuid name:name];
            }
            else {
                NSLog(@"Oh, the name is same! No need to change.");
            }
        }
    }
}

- (void)changePhobosName:(NSMutableDictionary *)info {
    NSLog(@"%s, _communicateState = %d", __func__, _communicateState);
    NSString *uuid = info[@"uuid"];
    NSString *name = info[@"name"];
    
    // 如果当前状态是IDLE，说明当前BLEManager没有和PHOBOS通讯
    if (_communicateState == CommunicateStateIdle) {
        [self continueChangePhobosName:name uuid:uuid];
    }
    else {
        NSDictionary *infoA = @{@"state" : [NSNumber numberWithInteger:CommunicateStateChangeName],
                               @"uuid" : uuid,
                               @"name" : name};
        [self insertToActionList:infoA];
        [self performSelector:@selector(checkActionList) withObject:nil afterDelay:3.0];
    }
}

- (void)connectPhobos:(NSString *)uuid {
    NSLog(@"%s", __func__);
    if ([self isInRadarView]) {
        NSLog(@"Yes, I'm in radar view.");
        [self scanPeripherals];
    } else {
        NSLog(@"Yes, I'm not in radar view, so do not scan.");
    }
    
    CBPeripheral *peripheral = self.knownPeripheralsDict[uuid];
    NSLog(@"peripheral = %@", peripheral);
    
    if (peripheral) {
        if (peripheral.state == CBPeripheralStateConnected) {
            [self performSelector:@selector(doConnectPhobos:) withObject:peripheral afterDelay:2.0];
//            [self doConnectPhobos:peripheral];
//            for (id targetDelegate in _delegate) {
//                if ([targetDelegate respondsToSelector:@selector(didConnectPeripheral:)]) {
//                    [targetDelegate didConnectPeripheral:peripheral.identifier.UUIDString];
//                }
//                [self updateTheState:targetDelegate];
//            }
        }
        else {
            [self doConnectPhobos:peripheral];
        }
    }
}

- (void)scanPeripherals {
    NSLog(@"%s", __func__);
    NSDictionary *optionsdict = @{CBCentralManagerScanOptionAllowDuplicatesKey : [NSNumber numberWithBool:YES]};
    [_cbManager scanForPeripheralsWithServices:nil options:optionsdict];
    
//    NSArray *arrayUUID = [NSArray arrayWithObjects:[CBUUID UUIDWithString:@"853C5886-90EA-7499-847B-96A29C28D2C8"],
//    [CBUUID UUIDWithString:@"1802"], nil];
//    [_bleManager scanForPeripheralsWithServices:arrayUUID options:nil];
//    [_cbManager scanForPeripheralsWithServices:nil options:nil];
}

- (void)stopScanPeripherals {
    NSLog(@"%s", __func__);
    [_cbManager stopScan];
}

- (void)connectAllPhobos {
    NSLog(@"%s", __func__);
    
    for (NSString *uuid in _knownPeripheralsDict) {
        [self doConnectPhobos:_knownPeripheralsDict[uuid]];
    }
}

- (BOOL)isConnected:(NSString*)uuid {
    CBPeripheral *peripheral = _knownPeripheralsDict[uuid];
    if (peripheral) {
        if (peripheral.state == CBPeripheralStateConnected) {
            NSLog(@"%s, uuid = %@, Connected", __func__, uuid);
            return YES;
        }
        else {
            NSLog(@"%s, uuid = %@, Disconnected", __func__, uuid);
            return NO;
        }
    }
    return NO;
}

- (void)readRSSI {
//    NSLog(@"%s", __func__);
    CBPeripheral *peripheral;
    for (NSString *uuid in _knownPeripheralsDict) {
        peripheral = _knownPeripheralsDict[uuid];
        if (peripheral.state == CBPeripheralStateConnected) {
            [peripheral readRSSI];
        }
    }
}

- (void)readRSSIwithUUID:(NSString*)uuid {
    NSLog(@"%s, uuid = %@, _knownPeripheralsDict = %@", __func__, uuid, _knownPeripheralsDict);
    CBPeripheral *peripheral = _knownPeripheralsDict[uuid];
    NSLog(@"peripheral = %@", peripheral);
    if (peripheral) {
        if (peripheral.state == CBPeripheralStateConnected) {
            [peripheral readRSSI];
        }
    }
}

- (void)updateTheState:(id)targetDelegate {
    if ([targetDelegate respondsToSelector:@selector(stateUpdate)]) {
        [targetDelegate stateUpdate];
    }
}

- (NSMutableDictionary*)churuAlert:(NSString*)uuid {
    NSLog(@"%s: uuid = %@", __func__, uuid);
    BIDAppDelegate *appDelegate = (BIDAppDelegate*)[UIApplication sharedApplication].delegate;
    NSManagedObjectContext *context = [appDelegate managedObjectContext];
    NSError *error;
    NSFetchRequest *request = [[NSFetchRequest alloc] initWithEntityName:kPhobosEntityName];
    NSPredicate *pred = [NSPredicate predicateWithFormat:@"%K = %@", kPhobosDeviceUUIDKey, uuid];
    [request setPredicate:pred];
    NSArray *objects = [context executeFetchRequest:request error:&error];
    if (objects == nil) {
        NSLog(@"%s : There was an error!", __func__);
        return NULL;
    }
    if ([objects count] <= 0) {
        return NULL;
    }
    
    NSManagedObject *thePhobos = [objects objectAtIndex:0];
    UInt32 majorminor = (UInt32)[[thePhobos valueForKey:kPhobosMajorMinorKey] longLongValue];
    CLBeaconMajorValue major = (majorminor >> 16);
    CLBeaconMinorValue minor = majorminor & 0xFFFF;
    BOOL chuSwitch = [[thePhobos valueForKey:kPhobosChuAlertKey] boolValue];
    BOOL ruSwitch = [[thePhobos valueForKey:kPhobosRuAlertKey] boolValue];
    int chuSegment = [[thePhobos valueForKey:kPhobosChuAlertTypeKey] intValue];
    NSString *name = [thePhobos valueForKey:kPhobosNameKey];
    NSLog(@"chuSwitch = %@, ruSwitch = %@", chuSwitch?@"YES":@"NO", ruSwitch?@"YES":@"NO");
    
    NSMutableDictionary *aDictionary = [NSMutableDictionary dictionaryWithDictionary:@{
                                @"chu": [NSNumber numberWithBool:chuSwitch],
                                @"ru" : [NSNumber numberWithBool:ruSwitch],
                                @"name" : name,
                                @"uuid" : uuid,
                                @"major" : [NSNumber numberWithUnsignedInt:major],
                                @"minor" : [NSNumber numberWithUnsignedInt:minor],
                                @"chuSegment":[NSNumber numberWithInt:chuSegment]}];
    return aDictionary;
}

/** 只有未连接的Phobos才会进入到这里
 */
- (void)ruOpened:(NSString*)uuid MajorMinor:(UInt32)majorminor {
    NSLog(@"%s", __func__);
#if defined(RU_WEI_USE_IBEACON)
    CLBeaconMajorValue major = (majorminor >> 16);
    CLBeaconMinorValue minor = majorminor & 0xFFFF;
    NSLog(@"major = 0x%04X, minor = 0x%04X", major, minor);
//    CLBeaconRegion *region = [[CLBeaconRegion alloc] initWithProximityUUID:[[NSUUID alloc] initWithUUIDString:@"915FF9B4-AB65-47B7-BC1B-A4A39BA2BFA6"] identifier:uuid];
    CLBeaconRegion *region = [[CLBeaconRegion alloc] initWithProximityUUID:[[NSUUID alloc] initWithUUIDString:@"915FF9B4-AB65-47B7-BC1B-A4A39BA2BFA6"] major:major minor:minor identifier:uuid];
//    CLBeaconRegion *region = [[CLBeaconRegion alloc] initWithProximityUUID:[[NSUUID alloc] initWithUUIDString:@"E2C56DB5-DFFB-48D2-B060-D0F5A71096E0"] identifier:uuid];
    region.notifyOnEntry = YES;
    region.notifyOnExit = YES;
    region.notifyEntryStateOnDisplay = YES;
    [_locationManager startMonitoringForRegion:region];
#endif
    [self connectPhobos:uuid];
}

- (void)ruClosed:(NSString*)uuid MajorMinor:(UInt32)majorminor {
    NSLog(@"%s", __func__);
#if defined(RU_WEI_USE_IBEACON)
    CLBeaconMajorValue major = (majorminor >> 16);
    CLBeaconMinorValue minor = majorminor & 0xFFFF;
    NSLog(@"major = 0x%04X, minor = 0x%04X", major, minor);
//    CLBeaconRegion *region = [[CLBeaconRegion alloc] initWithProximityUUID:[NSUUID UUID] identifier:uuid];
    CLBeaconRegion *region = [[CLBeaconRegion alloc] initWithProximityUUID:[[NSUUID alloc] initWithUUIDString:@"915FF9B4-AB65-47B7-BC1B-A4A39BA2BFA6"] major:major minor:minor identifier:uuid];
    [_locationManager stopMonitoringForRegion:region];
#endif
    [self cancelConnectPhobos:uuid];
}

#if 0
- (void)appEnterBackground {
    NSLog(@"Enter %s", __func__);
    NSArray * knownPeripherals2 = [_cbManager retrievePeripheralsWithIdentifiers:[_everConnectedUUID allKeys]];
    NSLog(@"knownPeripherals2 = %@", knownPeripherals2);
    if (knownPeripherals2.count > 0) {
        NSDictionary *churuDict;
        for (CBPeripheral *aPeripheral in knownPeripherals2) {
            churuDict = [self churuAlert:aPeripheral.identifier.UUIDString];
            NSLog(@"churuDict = %@", churuDict);
            if (aPeripheral.state == CBPeripheralStateConnected) {
                if (![churuDict[@"chu"] boolValue]) {//如果没有打开“出围报警”则立即断开蓝牙连接
                    [_cbManager cancelPeripheralConnection:aPeripheral];
                }
            } else {
                if (aPeripheral.state == CBPeripheralStateDisconnected) {
                    if ([churuDict[@"ru"] boolValue]) {
                        [self doConnectPhobos:aPeripheral];
                    }
                } else if (aPeripheral.state == CBPeripheralStateConnecting) {
                    if (![churuDict[@"ru"] boolValue]) {
                        [_cbManager cancelPeripheralConnection:aPeripheral];
                    }
                }
            }
        }
    }
    NSLog(@"Leave %s", __func__);
}
#else
- (void)appEnterBackground {
    NSLog(@"Enter %s", __func__);
    BOOL ifStopScan = YES;
    
    NSDictionary *churuDict;
    CBPeripheral *aPeripheral;
    for (NSString *uuid in _knownPeripheralsDict) {
        aPeripheral = _knownPeripheralsDict[uuid];
        churuDict = [self churuAlert:aPeripheral.identifier.UUIDString];
        NSLog(@"churuDict = %@, aPeripheral.state = %d", churuDict, aPeripheral.state);
        if (aPeripheral.state == CBPeripheralStateConnected) {
            if (![churuDict[@"chu"] boolValue]) {//如果没有打开“出围报警”则立即断开蓝牙连接
                [_cbManager cancelPeripheralConnection:aPeripheral];
            }
        }
        else if (aPeripheral.state == CBPeripheralStateConnecting) {
            if (![churuDict[@"ru"] boolValue]) {
                [_cbManager cancelPeripheralConnection:aPeripheral];
            }
            else {
                ifStopScan = NO;//只要有一个Phobos的“入围报警”是开着的，就不停止后台扫描
            }
        }
    }

    if (ifStopScan) {
        [self stopScanPeripherals];
    }
    else {
        [self scanPeripherals];
    }
    NSLog(@"Leave %s", __func__);
}
#endif

- (void)ruAlertNow:(NSString*)uuid Major:(CLBeaconMajorValue)major Minor:(CLBeaconMinorValue)minor {
    NSLog(@"%s, major = 0x%02X, minor = 0x%02X", __func__, major, minor);
    NSMutableDictionary *churuDict = [self churuAlert:uuid];
    NSLog(@"churuDict = %@", churuDict);
    if ([churuDict[@"ru"] boolValue]) {
#if defined(RU_WEI_USE_IBEACON)
        CLBeaconMajorValue major1 = [churuDict[@"major"] unsignedIntValue];
        CLBeaconMinorValue minor1 = [churuDict[@"minor"] unsignedIntValue];
        if ((major != major1) || (minor != minor1)) {
            if ((major == 0) && (minor == 0)) {
                //
            }
            else {
                return;
            }
        }
        CLBeaconRegion *region = [[CLBeaconRegion alloc] initWithProximityUUID:[[NSUUID alloc] initWithUUIDString:@"915FF9B4-AB65-47B7-BC1B-A4A39BA2BFA6"] major:major minor:minor identifier:uuid];
        [_locationManager stopMonitoringForRegion:region];
#endif
        [self saveChuRuState:NO UUID:uuid churuState:NO];
        [self setLocalNotification:churuDict chuOrRu:NO TimeInterval:0.5];
        BIDAppDelegate *appDelegate = (BIDAppDelegate*)[UIApplication sharedApplication].delegate;
//        if ([UIApplication sharedApplication].applicationState == UIApplicationStateBackground) {
//            //得到20秒的后台运行机会
//            [appDelegate startBackgroundTask:^(double longitude, double latitude){
//                NSLog(@"oh after ten seconds passed.....longitude = %f, latitude = %f", longitude, latitude);
//                [appDelegate stopBaiduMapView];
//                [self saveZuoBiao:NO UUID:uuid longitude:longitude latitude:latitude];
//            }];
//            [appDelegate beginBaiduMapView];
//        } else {
//            
//        }
        [appDelegate startBackgroundTask];// 得到7秒的后台运行机会
        [appDelegate beginBaiduMapView:uuid chuOrRu:NO];
        
        [self performSelector:@selector(cancelConnectPhobosRuAlert:) withObject:uuid afterDelay:7];
    }
}

- (void)chuAlertNow:(NSString*)uuid {
    NSLog(@"%s", __func__);
    NSMutableDictionary *churuDict = [self churuAlert:uuid];
    NSLog(@"churuDict = %@", churuDict);
    if ([churuDict[@"chu"] boolValue]) {
        [self saveChuRuState:YES UUID:uuid churuState:NO];
        int segment = [churuDict[@"chuSegment"] intValue];
        NSLog(@"segment = %d", segment);
        if (segment == 0 || segment == 1) {
            [self setLocalNotification:churuDict chuOrRu:YES TimeInterval:0.5];
        }
        BIDAppDelegate *appDelegate = (BIDAppDelegate*)[UIApplication sharedApplication].delegate;
//        //得到20秒的后台运行机会
//        [appDelegate startBackgroundTask:^(double longitude, double latitude){
//            NSLog(@"oh after ten seconds passed.....longitude = %f, latitude = %f", longitude, latitude);
//            [appDelegate stopBaiduMapView];
//            [self saveZuoBiao:YES UUID:uuid longitude:longitude latitude:latitude];
//        }];
//        [appDelegate beginBaiduMapView];
        [appDelegate startBackgroundTask];
        [appDelegate beginBaiduMapView:uuid chuOrRu:YES];
    }
}

- (void)chuAlertAllNecessary {
    NSLog(@"%s", __func__);
    NSMutableDictionary *churuDict;
    NSTimeInterval timeDelay = 1;
    CBPeripheral *aPeripheral;
    for (NSString *uuid in _knownPeripheralsDict) {
        aPeripheral = _knownPeripheralsDict[uuid];
        churuDict = [self churuAlert:aPeripheral.identifier.UUIDString];
        if ([churuDict[@"chu"] boolValue]) {
            [self setLocalNotification:churuDict chuOrRu:YES TimeInterval:timeDelay];
            timeDelay += 2;
        }
    }
}

/**
 @param chuOrRu YES means chu alert, and NO means ru alert.
 @param time 延迟多少秒后触发
 */
- (void)setLocalNotification:(NSMutableDictionary*)info chuOrRu:(BOOL)chuOrRu TimeInterval:(NSTimeInterval)time {
    UILocalNotification *notification = [[UILocalNotification alloc] init];
    if (notification != nil) {
        NSDate *now = [NSDate new];
        notification.fireDate = [now dateByAddingTimeInterval:0.5];
        notification.timeZone=[NSTimeZone defaultTimeZone];
        notification.soundName = @"Bell1.wav";
        if (chuOrRu) {
            notification.alertBody = [NSString stringWithFormat:@"出围报警：%@", info[@"name"]];
            [info setObject:[NSNumber numberWithBool:NO] forKey:@"ru"];
        } else {
            notification.alertBody = [NSString stringWithFormat:@"入围报警：%@", info[@"name"]];
            [info setObject:[NSNumber numberWithBool:NO] forKey:@"chu"];
        }
        notification.userInfo = info;
        [[UIApplication sharedApplication] scheduleLocalNotification:notification];
    }
}

/** 播放报警音乐
 */
- (void)playWinSound {
    NSLog(@"%s", __func__);
    if (winSoundID == 0) {
        NSURL *soundURL = [[NSBundle mainBundle] URLForResource:@"Bell1"
                                                  withExtension:@"wav"];
        AudioServicesCreateSystemSoundID((__bridge CFURLRef)soundURL,
                                         &winSoundID);
    }
    AudioServicesPlaySystemSound(winSoundID);
}

/**
 @param flag YES means chu alert, and NO means ru alert.
 */
- (void)saveChuRuState:(BOOL)flag UUID:(NSString*)uuid churuState:(BOOL)churuState {
    NSLog(@"%s, uuid = %@", __func__, uuid);
    BIDAppDelegate *appDelegate = (BIDAppDelegate*)[UIApplication sharedApplication].delegate;
    NSManagedObjectContext *context = [appDelegate managedObjectContext];
    NSError *error;
    NSFetchRequest *request = [[NSFetchRequest alloc] initWithEntityName:kPhobosEntityName];
    NSPredicate *pred = [NSPredicate predicateWithFormat:@"%K = %@", kPhobosDeviceUUIDKey, uuid];
    [request setPredicate:pred];
    NSArray *objects = [context executeFetchRequest:request error:&error];
    if (objects == nil) {
        NSLog(@"%s : There was an error!", __func__);
    }
    if ([objects count] <= 0) {
        NSLog(@"objects count <= 0 , so return.");
        return;
    }
    NSManagedObject *thePhobos = [objects objectAtIndex:0];
    if (flag) {
        [thePhobos setValue:[NSNumber numberWithBool:churuState] forKey:kPhobosChuAlertKey];
        
    } else {
        [thePhobos setValue:[NSNumber numberWithBool:churuState] forKey:kPhobosRuAlertKey];
    }
    [appDelegate saveContext];
}

/** return 2014-08-24 13:58:35
 */
- (NSString*)getTime {
    //获得系统时间
    NSDate *  senddate=[NSDate date];
    NSDateFormatter  *dateformatter=[[NSDateFormatter alloc] init];
//    [dateformatter setDateFormat:@"HH:mm"];
//    NSString *  locationString=[dateformatter stringFromDate:senddate];
    [dateformatter setDateFormat:@"YYYY-MM-dd HH:mm:ss"];
    NSString *  morelocationString=[dateformatter stringFromDate:senddate];
    NSLog(@"morelocationString=%@", morelocationString);
    return morelocationString;
}

/**
 @param flag YES means chu alert, and NO means ru alert.
 */
- (void)saveZuoBiao:(BOOL)flag UUID:(NSString*)uuid longitude:(double)longitude latitude:(double)latitude {
    NSLog(@"%s: uuid = %@", __func__, uuid);
    BIDAppDelegate *appDelegate = (BIDAppDelegate*)[UIApplication sharedApplication].delegate;
    NSManagedObjectContext *context = [appDelegate managedObjectContext];
    NSError *error;
    NSFetchRequest *request = [[NSFetchRequest alloc] initWithEntityName:kPhobosEntityName];
    NSPredicate *pred = [NSPredicate predicateWithFormat:@"%K = %@", kPhobosDeviceUUIDKey, uuid];
    [request setPredicate:pred];
    NSArray *objects = [context executeFetchRequest:request error:&error];
    if (objects == nil) {
        NSLog(@"%s : There was an error!", __func__);
    }
    if ([objects count] <= 0) {
        NSLog(@"objects count <= 0 , so return.");
        return;
    }
    NSManagedObject *thePhobos = [objects objectAtIndex:0];
    if (flag) {
        [thePhobos setValue:[NSNumber numberWithDouble:longitude] forKey:kPhobosLongitudeChuKey];
        [thePhobos setValue:[NSNumber numberWithDouble:latitude] forKey:kPhobosLatitudeChuKey];
        [thePhobos setValue:[self getTime] forKey:kPhobosChuAlertTime];
    } else {
        [thePhobos setValue:[NSNumber numberWithDouble:longitude] forKey:kPhobosLongitudeRuKey];
        [thePhobos setValue:[NSNumber numberWithDouble:latitude] forKey:kPhobosLatitudeRuKey];
        [thePhobos setValue:[self getTime] forKey:kPhobosRuAlertTime];
    }
    
    [appDelegate saveContext];
}

- (void)addDelegateObserver:(id)observer {
    NSLog(@"%s", __func__);
    for (id aObserver in _delegate) {
        if ([aObserver isEqual:observer]) {
            NSLog(@"oh, aleready have : %@, so return", observer);
            return;
        }
    }
    [_delegate addObject:observer];
}

- (void)removeDelegateObserver:(id)observer {
    NSLog(@"%s", __func__);
    [_delegate removeObject:observer];
}

#pragma mark - CBCentralManagerDelegate
- (void)centralManagerDidUpdateState:(CBCentralManager *)central {
    NSLog(@"%s", __func__);
    NSLog(@"state = %d", central.state);
    switch (central.state) {
        case CBCentralManagerStatePoweredOn: {
            NSLog(@"CBCentralManagerStatePoweredOn");
            [self performSelector:@selector(initBLEManagerContinue) withObject:nil afterDelay:0.1];
            break;
        }
        case CBCentralManagerStatePoweredOff:
            NSLog(@"CBCentralManagerStatePoweredOff");
            break;
        case CBCentralManagerStateResetting:
            NSLog(@"CBCentralManagerStateResetting");
            break;
        case CBCentralManagerStateUnauthorized:
            NSLog(@"CBCentralManagerStateUnauthorized");
            break;
        case CBCentralManagerStateUnknown:
            NSLog(@"CBCentralManagerStateUnknown");
            break;
        case CBCentralManagerStateUnsupported:
            NSLog(@"CBCentralManagerStateUnsupported");
            break;
        default:
            break;
    }
}

- (void)centralManager:(CBCentralManager *)central willRestoreState:(NSDictionary *)dict {
    NSLog(@"%s: self = %@", __func__, self);
    NSLog(@"central is : %@", central);
    id idInstance = [BIDBLEManager sharedInstance];
    NSLog(@"idInstance = %@", idInstance);
    NSArray *peripherals = dict[CBCentralManagerRestoredStatePeripheralsKey];
    NSLog(@"restored peripherals = %@", peripherals);
    BIDAppDelegate *appDelegate = (BIDAppDelegate*)[UIApplication sharedApplication].delegate;
    [appDelegate startBackgroundTask];// 得到7秒的后台运行机会
//    NSMutableDictionary *churuDict;
//    for (CBPeripheral *aPeripheral in peripherals) {
//        aPeripheral.delegate = self;
//        churuDict = [self churuAlert:aPeripheral.identifier.UUIDString];
//        if ([churuDict[@"ru"] boolValue]) {
//            [self connectPhobos:aPeripheral.identifier.UUIDString];
//        }
//    }
}

- (void)centralManager:(CBCentralManager *)central didRetrievePeripherals:(NSArray *)peripherals {
    NSLog(@"%s", __func__);
}

- (void)centralManager:(CBCentralManager *)central didRetrieveConnectedPeripherals:(NSArray *)peripherals {
    NSLog(@"%s", __func__);
}

- (void)centralManager:(CBCentralManager *)central didDiscoverPeripheral:(CBPeripheral *)peripheral advertisementData:(NSDictionary *) advertisementData RSSI:(NSNumber *)RSSI {
    NSLog(@"%s: uuid = %@, peripheral.name = %@", __func__, peripheral.identifier.UUIDString, peripheral.name);

    if (_knownPeripheralsDict[peripheral.identifier.UUIDString]) {
        if ([self ifNeedConnect:peripheral.identifier.UUIDString]) {
            NSLog(@"Yes, need connect %@", peripheral.identifier.UUIDString);
            peripheral.delegate = self;
//            [self doConnectPhobos:peripheral];
            [self performSelector:@selector(doConnectPhobos:) withObject:peripheral afterDelay:4.7];
        }
    }
}

- (void)centralManager:(CBCentralManager *)central didConnectPeripheral:(CBPeripheral *)peripheral {
    NSLog(@"%s: uuid = %@, peripheral.name = %@", __func__, peripheral.identifier.UUIDString, peripheral.name);

    [self ruAlertNow:peripheral.identifier.UUIDString Major:0 Minor:0];
    for (id targetDelegate in _delegate) {
        if ([targetDelegate respondsToSelector:@selector(didConnectPeripheral:)]) {
            [targetDelegate didConnectPeripheral:peripheral.identifier.UUIDString];
        }
        [self updateTheState:targetDelegate];
    }
    NSLog(@"applicationState = %d", [UIApplication sharedApplication].applicationState);
}

- (void)centralManager:(CBCentralManager *)central didFailToConnectPeripheral:(CBPeripheral *)peripheral error:(NSError *)error {
    NSLog(@"%s: error = %@", __func__, error);

    for (id targetDelegate in _delegate) {
        if ([targetDelegate respondsToSelector:@selector(didFailToConnectPeripheral:)]) {
            [targetDelegate didFailToConnectPeripheral:peripheral.identifier.UUIDString];
        }
        [self updateTheState:targetDelegate];
    }
}

- (void)centralManager:(CBCentralManager *)central didDisconnectPeripheral:(CBPeripheral *)peripheral error:(NSError *)error {
    NSLog(@"%s: error = %@", __func__, error);

    [self chuAlertNow:peripheral.identifier.UUIDString];
    for (id targetDelegate in _delegate) {
        if ([targetDelegate respondsToSelector:@selector(didDisconnectPeripheral:)]) {
            [targetDelegate didDisconnectPeripheral:peripheral.identifier.UUIDString];
        }
        [self updateTheState:targetDelegate];
    }
}

#pragma mark - CBPeripheralDelegate
- (void)peripheralDidUpdateName:(CBPeripheral *)peripheral {//NS_AVAILABLE(NA, 6_0);
    NSLog(@"%s", __func__);
}

- (void)peripheralDidUpdateRSSI:(CBPeripheral *)peripheral error:(NSError *)error {
//    NSLog(@"%s: error = %@", __func__, error);
    if (error) {
        NSLog(@"error = %@", error);
        return;
    }
    if (peripheral.RSSI == nil) {
        return;
    }
    for (id targetDelegate in _delegate) {
        if ([targetDelegate respondsToSelector:@selector(peripheralDidUpdateRSSI:RSSI:)]) {
            [targetDelegate peripheralDidUpdateRSSI:peripheral.identifier.UUIDString RSSI:peripheral.RSSI];
        }
    }
}

- (void)peripheral:(CBPeripheral *)peripheral didDiscoverServices:(NSError *)error {
    NSLog(@"%s, _communicateState = %d", __func__, _communicateState);
    CBUUID *aCBUUID;
    if (_communicateState == CommunicateStateBuzzer) {
        aCBUUID = [CBUUID UUIDWithString:@"1802"];
    } else if (_communicateState == CommunicateStateLinkLoss) {
        aCBUUID = [CBUUID UUIDWithString:@"1803"];
    } else if (_communicateState == CommunicateStateBattery) {
        aCBUUID = [CBUUID UUIDWithString:@"180f"];
    } else if (_communicateState == CommunicateStateTemperature) {
#ifdef GET_TEMPERATURE_USE_BATTERY_SERVICE
        aCBUUID = [CBUUID UUIDWithString:@"ffb0"];sssssssss
#else
        aCBUUID = [CBUUID UUIDWithString:@"013d8e3b-1877-4d5c-bc59-aaa7e5082346"];
#endif
    } else if (_communicateState == CommunicateStateChangeName) {
        aCBUUID = [CBUUID UUIDWithString:@"013d8e3b-1877-4d5c-bc59-aaa7e5082346"];
    }
    for (CBService *service in peripheral.services) {
        NSLog(@"service = %@", service);
        if (1 == [self compareCBUUID:service.UUID UUID2:aCBUUID]) {
            NSLog(@"Yes, got it.");
            if (_communicateState == CommunicateStateBuzzer) {
                NSLog(@"Yes, got 1802.");
                _buzzerService = service;
                [self performSelector:@selector(soundBuzzer2) withObject:nil afterDelay:0.1];
            } else if (_communicateState == CommunicateStateLinkLoss) {
                NSLog(@"Yes, got 1803.");
                _linkLossService = service;
                [self performSelector:@selector(setLinkLoss2) withObject:nil afterDelay:0.1];
            } else if (_communicateState == CommunicateStateBattery) {
                NSLog(@"Yes, got 180f.");
                _batteryService = service;
                [self performSelector:@selector(readBattery2) withObject:nil afterDelay:0.1];
            } else if (_communicateState == CommunicateStateTemperature) {
                _temperatureService = service;
                [self performSelector:@selector(readTemperature2) withObject:nil afterDelay:0.1];
            } else if (_communicateState == CommunicateStateChangeName) {
                NSLog(@"Yes, got 013d8e3b-1877-4d5c-bc59-aaa7e5082346.");
                _changeNameService = service;
                [self performSelector:@selector(changeName2) withObject:nil afterDelay:0.1];
            }
            return;
        }
    }
}

- (void)peripheral:(CBPeripheral *)peripheral didDiscoverIncludedServicesForService:(CBService *)service error:(NSError *)error {
    NSLog(@"%s", __func__);
}

- (void)peripheral:(CBPeripheral *)peripheral didDiscoverCharacteristicsForService:(CBService *)service error:(NSError *)error {
    NSLog(@"%s: service = %@, error = %@, _communicateState = %d", __func__, service, error, _communicateState);
    if (_communicateState == CommunicateStateBuzzer) {
        [self performSelector:@selector(soundBuzzer3) withObject:nil afterDelay:0.1];
    } else if (_communicateState == CommunicateStateLinkLoss) {
        [self performSelector:@selector(setLinkLoss3) withObject:nil afterDelay:0.1];
    } else if (_communicateState == CommunicateStateBattery) {
        [self performSelector:@selector(readBattery3) withObject:nil afterDelay:0.1];
    } else if (_communicateState == CommunicateStateTemperature) {
        for ( CBCharacteristic *characteristic in service.characteristics ) {
            if ([characteristic.UUID isEqual:[CBUUID UUIDWithString:@"7fc69075-e330-4db6-8c46-8a8219eb7448"]]) {
                _temperatureCharacteristic = characteristic;
                [self performSelector:@selector(readTemperature3) withObject:nil afterDelay:0.1];
            }
        }
    } else if (_communicateState == CommunicateStateChangeName) {
        [self performSelector:@selector(changeName3) withObject:nil afterDelay:0.1];
    }
}

- (void)peripheral:(CBPeripheral *)peripheral didUpdateValueForCharacteristic:(CBCharacteristic *)characteristic error:(NSError *)error {
    NSLog(@"%s: characteristic = %@, error = %@", __func__, characteristic, error);
    if (!error) {
        if ([characteristic.UUID isEqual:[CBUUID UUIDWithString:@"7fc69075-e330-4db6-8c46-8a8219eb7448"]]) {
            unsigned char data[characteristic.value.length];
            [characteristic.value getBytes:&data];
            NSLog(@"data4 = 0x%x", data[4]);
            NSLog(@"data5 = 0x%x", data[5]);
            for (int i = 0; i < characteristic.value.length; i++) {
                NSLog(@"0x%x", data[i]);
            }
            int16_t temperature = (data[4] << 8) | data[5];
            NSLog(@"temperature = 0x%x", temperature);
            _tempLevel = (temperature - 1340) / 10.0;
            NSLog(@"_tempLevel = %f", _tempLevel);
            for (id targetDelegate in _delegate) {
                if ([targetDelegate respondsToSelector:@selector(didUpdateTemperature:)]) {
                    [targetDelegate didUpdateTemperature:_tempLevel];
                }
            }
            return;
        }
        UInt16 characteristicUUID = [self CBUUIDToInt:characteristic.UUID];
        switch (characteristicUUID) {
            case TI_KEYFOB_LEVEL_SERVICE_UUID: {
                char batlevel;
                [characteristic.value getBytes:&batlevel length:TI_KEYFOB_LEVEL_SERVICE_READ_LEN];
                _batteryLevel = (float)batlevel;
                NSLog(@"_batteryLevel = %f", _batteryLevel);
                for (id targetDelegate in _delegate) {
                    if ([targetDelegate respondsToSelector:@selector(didUpdateBattery:UUID:)]) {
                        [targetDelegate didUpdateBattery:_batteryLevel UUID:_batteryUUID];
                    }
                }
                break;
            }
                
            case TI_KEYFOB_TEMPERATURE_CHARACTERISTIC: {
                SInt32 tempLevel;
                [characteristic.value getBytes:&tempLevel length:TI_KEYFOB_TEMPERATURE_CHARACTERISTIC_READ_LEN];
                float tempHight, tempLow;
                tempHight = (float)(tempLevel >> 16);
                tempLow = (float)(tempLevel & 0x0000FFFF);
                NSLog(@"tempHight = %f, tempLow = %f", tempHight, tempLow);
                _tempLevel = tempHight + (tempLow/10);
                NSLog(@"temperature = %f", _tempLevel);
                for (id targetDelegate in _delegate) {
                    if ([targetDelegate respondsToSelector:@selector(didUpdateTemperature:)]) {
                        [targetDelegate didUpdateTemperature:_tempLevel];
                    }
                }
                break;
            }
            default:
                break;
        }
    }
}

- (void)peripheral:(CBPeripheral *)peripheral didWriteValueForCharacteristic:(CBCharacteristic *)characteristic error:(NSError *)error {
    NSLog(@"%s, peripheral=%@, characteristic=%@, error=%@", __func__, peripheral, characteristic, error);
}

- (void)peripheral:(CBPeripheral *)peripheral didUpdateNotificationStateForCharacteristic:(CBCharacteristic *)characteristic error:(NSError *)error {
    NSLog(@"%s", __func__);
}

- (void)peripheral:(CBPeripheral *)peripheral didDiscoverDescriptorsForCharacteristic:(CBCharacteristic *)characteristic error:(NSError *)error {
    NSLog(@"%s", __func__);
}

- (void)peripheral:(CBPeripheral *)peripheral didUpdateValueForDescriptor:(CBDescriptor *)descriptor error:(NSError *)error {
    NSLog(@"%s", __func__);
}

- (void)peripheral:(CBPeripheral *)peripheral didWriteValueForDescriptor:(CBDescriptor *)descriptor error:(NSError *)error {
    NSLog(@"%s", __func__);
}

#pragma mark - Read&Write
/*
 *  @method compareCBUUID
 *
 *  @param UUID1 UUID 1 to compare
 *  @param UUID2 UUID 2 to compare
 *
 *  @returns 1 (equal) 0 (not equal)
 *
 *  @discussion compareCBUUID compares two CBUUID's to each other and returns 1 if they are equal and 0 if they are not
 *
 */

-(int) compareCBUUID:(CBUUID *)UUID1 UUID2:(CBUUID *)UUID2 {
    char b1[16];
    char b2[16];
    [UUID1.data getBytes:b1];
    [UUID2.data getBytes:b2];
    if (memcmp(b1, b2, UUID1.data.length) == 0) return 1;
    else return 0;
}

/*
 *  @method CBUUIDToInt
 *
 *  @param UUID1 UUID 1 to convert
 *
 *  @returns UInt16 representation of the CBUUID
 *
 *  @discussion CBUUIDToInt converts a CBUUID to a Uint16 representation of the UUID
 *
 */
-(UInt16) CBUUIDToInt:(CBUUID *) UUID {
    char b1[16];
    [UUID.data getBytes:b1];
    return ((b1[0] << 8) | b1[1]);
}

/*!
 *  @method swap:
 *
 *  @param s Uint16 value to byteswap
 *
 *  @discussion swap byteswaps a UInt16
 *
 *  @return Byteswapped UInt16
 */

-(UInt16) swap:(UInt16)s {
    UInt16 temp = s << 8;
    temp |= (s >> 8);
    return temp;
}

/*
 *  @method CBUUIDToString
 *
 *  @param UUID UUID to convert to string
 *
 *  @returns Pointer to a character buffer containing UUID in string representation
 *
 *  @discussion CBUUIDToString converts the data of a CBUUID class to a character pointer for easy printout using printf()
 *
 */
-(const char *) CBUUIDToString:(CBUUID *) UUID {
    return [[UUID.data description] cStringUsingEncoding:NSStringEncodingConversionAllowLossy];
}


/*
 *  @method UUIDToString
 *
 *  @param UUID UUID to convert to string
 *
 *  @returns Pointer to a character buffer containing UUID in string representation
 *
 *  @discussion UUIDToString converts the data of a CFUUIDRef class to a character pointer for easy printout using printf()
 *
 */
-(const char *) UUIDToString:(CFUUIDRef)UUID {
    if (!UUID) return "NULL";
    CFStringRef s = CFUUIDCreateString(NULL, UUID);
    return CFStringGetCStringPtr(s, 0);
    
}

/*
 *  @method findServiceFromUUID:
 *
 *  @param UUID CBUUID to find in service list
 *  @param p Peripheral to find service on
 *
 *  @return pointer to CBService if found, nil if not
 *
 *  @discussion findServiceFromUUID searches through the services list of a peripheral to find a
 *  service with a specific UUID
 *
 */
-(CBService *) findServiceFromUUID:(CBUUID *)UUID p:(CBPeripheral *)p {
    for(int i = 0; i < p.services.count; i++) {
        CBService *s = [p.services objectAtIndex:i];
        if ([self compareCBUUID:s.UUID UUID2:UUID]) return s;
    }
    return nil; //Service not found on this peripheral
}

/*
 *  @method findCharacteristicFromUUID:
 *
 *  @param UUID CBUUID to find in Characteristic list of service
 *  @param service Pointer to CBService to search for charateristics on
 *
 *  @return pointer to CBCharacteristic if found, nil if not
 *
 *  @discussion findCharacteristicFromUUID searches through the characteristic list of a given service
 *  to find a characteristic with a specific UUID
 *
 */
-(CBCharacteristic *) findCharacteristicFromUUID:(CBUUID *)UUID service:(CBService*)service {
    for(int i=0; i < service.characteristics.count; i++) {
        CBCharacteristic *c = [service.characteristics objectAtIndex:i];
        if ([self compareCBUUID:c.UUID UUID2:UUID]) return c;
    }
    return nil; //Characteristic not found on this service
}

/*!
 *  @method writeValue:
 *
 *  @param serviceUUID Service UUID to write to (e.g. 0x2400)
 *  @param characteristicUUID Characteristic UUID to write to (e.g. 0x2401)
 *  @param data Data to write to peripheral
 *  @param p CBPeripheral to write to
 *  @param flag If true, then write with CBCharacteristicWriteWithResponse
 *
 *  @discussion Main routine for writeValue request, writes without feedback. It converts integer into
 *  CBUUID's used by CoreBluetooth. It then searches through the peripherals services to find a
 *  suitable service, it then checks that there is a suitable characteristic on this service.
 *  If this is found, value is written. If not nothing is done.
 *
 */

- (NSInteger)writeValue:(int)serviceUUID characteristicUUID:(int)characteristicUUID p:(CBPeripheral *)p data:(NSData *)data Response:(BOOL)flag{
    UInt16 s = [self swap:serviceUUID];
    UInt16 c = [self swap:characteristicUUID];
    NSData *sd = [[NSData alloc] initWithBytes:(char *)&s length:2];
    NSData *cd = [[NSData alloc] initWithBytes:(char *)&c length:2];
    CBUUID *su = [CBUUID UUIDWithData:sd];
    CBUUID *cu = [CBUUID UUIDWithData:cd];
    CBService *service = [self findServiceFromUUID:su p:p];
    if (!service) {
        NSLog(@"Could not find service with UUID %s on peripheral with UUID %@\r\n",(char*)[self CBUUIDToString:su],p.identifier.UUIDString);
        return 1;
    }
    CBCharacteristic *characteristic = [self findCharacteristicFromUUID:cu service:service];
    if (!characteristic) {
        NSLog(@"Could not find characteristic with UUID %s on service with UUID %s on peripheral with UUID %@\r\n", (char*)[self CBUUIDToString:cu],[self CBUUIDToString:su], p.identifier.UUIDString);
        return 2;
    }
    if (flag) {
        [p writeValue:data forCharacteristic:characteristic type:CBCharacteristicWriteWithResponse];
    } else {
        [p writeValue:data forCharacteristic:characteristic type:CBCharacteristicWriteWithoutResponse];
    }
    return 0;
}


/*!
 *  @method readValue:
 *
 *  @param serviceUUID Service UUID to read from (e.g. 0x2400)
 *  @param characteristicUUID Characteristic UUID to read from (e.g. 0x2401)
 *  @param p CBPeripheral to read from
 *
 *  @discussion Main routine for read value request. It converts integers into
 *  CBUUID's used by CoreBluetooth. It then searches through the peripherals services to find a
 *  suitable service, it then checks that there is a suitable characteristic on this service.
 *  If this is found, the read value is started. When value is read the didUpdateValueForCharacteristic
 *  routine is called.
 *
 *  @see didUpdateValueForCharacteristic
 */

- (NSInteger)readValue:(int)serviceUUID characteristicUUID:(int)characteristicUUID p:(CBPeripheral *)p {
    UInt16 s = [self swap:serviceUUID];
    UInt16 c = [self swap:characteristicUUID];
    NSData *sd = [[NSData alloc] initWithBytes:(char *)&s length:2];
    NSData *cd = [[NSData alloc] initWithBytes:(char *)&c length:2];
    CBUUID *su = [CBUUID UUIDWithData:sd];
    CBUUID *cu = [CBUUID UUIDWithData:cd];
    CBService *service = [self findServiceFromUUID:su p:p];
    if (!service) {
        printf("Could not find service with UUID %s on peripheral with UUID %s\r\n",[self CBUUIDToString:su],[self UUIDToString:p.UUID]);
        return 1;
    }
    CBCharacteristic *characteristic = [self findCharacteristicFromUUID:cu service:service];
    if (!characteristic) {
        printf("Could not find characteristic with UUID %s on service with UUID %s on peripheral with UUID %s\r\n",[self CBUUIDToString:cu],[self CBUUIDToString:su], [self UUIDToString:p.UUID]);
        return 2;
    }
    
//    CBMutableCharacteristic *rCharacteristic = [[CBMutableCharacteristic alloc] initWithType:characteristic.UUID properties:CBCharacteristicPropertyRead|CBCharacteristicPropertyNotifyEncryptionRequired value:characteristic.value permissions:CBAttributePermissionsReadEncryptionRequired];
    
//    [p readValueForCharacteristic:rCharacteristic];
    
    [p readValueForCharacteristic:characteristic];
    return 0;
}

/*!
 *  @method soundBuzzer:
 *
 *  @param buzVal The data to write
 *  @param p CBPeripheral to write to
 *
 *  @discussion Sound the buzzer on a TI keyfob. This method writes a value to the proximity alert service
 *
 */
- (void)soundBuzzer:(Byte)buzVal peripheral:(CBPeripheral *)peripheral {
    NSLog(@"%s", __func__);
    NSData *d = [[NSData alloc] initWithBytes:&buzVal length:TI_KEYFOB_PROXIMITY_ALERT_WRITE_LEN];
    [self writeValue:TI_KEYFOB_PROXIMITY_ALERT_UUID characteristicUUID:TI_KEYFOB_PROXIMITY_ALERT_PROPERTY_UUID p:peripheral data:d Response:NO];
    [self performSelector:@selector(stopBuzzer) withObject:nil afterDelay:10];
}

- (void)soundBuzzer:(NSString *)uuid {
    NSLog(@"%s: uuid = %@", __func__, uuid);
    // 如果当前状态不是IDLE，说明当前BLEManager正在和PHOBOS通讯
    if (!(_communicateState == CommunicateStateIdle)) {
        NSDictionary *info = @{@"state" : [NSNumber numberWithInteger:CommunicateStateBuzzer],
                                @"uuid" : uuid};
        [self insertToActionList:info];
        [self performSelector:@selector(checkActionList) withObject:nil afterDelay:3.0];
        return;
    }
    
    _communicateState = CommunicateStateBuzzer;
    CBPeripheral *peripheral = _knownPeripheralsDict[uuid];
    _buzzerUUID = uuid;
    Byte buzVal = 0x02;
    NSData *d = [[NSData alloc] initWithBytes:&buzVal length:TI_KEYFOB_PROXIMITY_ALERT_WRITE_LEN];
    NSInteger ret = [self writeValue:TI_KEYFOB_PROXIMITY_ALERT_UUID characteristicUUID:TI_KEYFOB_PROXIMITY_ALERT_PROPERTY_UUID p:peripheral data:d Response:NO];
    if (ret == 0) {
        [self performSelector:@selector(stopBuzzer) withObject:nil afterDelay:10];
        return;
    }
    NSArray *sUUID = @[[CBUUID UUIDWithString:@"1802"]];
    [peripheral discoverServices:sUUID];
}

- (void)soundBuzzer2 {
    NSLog(@"%s", __func__);
    CBPeripheral *peripheral = _knownPeripheralsDict[_buzzerUUID];
    [peripheral discoverCharacteristics:nil forService:_buzzerService];
}

- (void)soundBuzzer3 {
    NSLog(@"%s", __func__);
    CBPeripheral *peripheral = _knownPeripheralsDict[_buzzerUUID];
    [self soundBuzzer:0x02 peripheral:peripheral];
}

- (void)stopBuzzer {
    NSLog(@"%s", __func__);
    CBPeripheral *peripheral = _knownPeripheralsDict[_buzzerUUID];
    Byte buzVal = 0x00;
    NSData *data = [[NSData alloc] initWithBytes:&buzVal length:TI_KEYFOB_PROXIMITY_ALERT_WRITE_LEN];
    [self writeValue:TI_KEYFOB_PROXIMITY_ALERT_UUID characteristicUUID:TI_KEYFOB_PROXIMITY_ALERT_PROPERTY_UUID p:peripheral data:data Response:NO];
    [self performSelector:@selector(communicateStateToIdle) withObject:nil afterDelay:2.0];
}
//-------------------------------------------------------------------------------------------------------------------
- (void)changeName:(NSString *)uuid name:(NSString*)name{
    NSLog(@"%s: uuid = %@, name = %@", __func__, uuid, name);
    _communicateState = CommunicateStateChangeName;
    CBPeripheral *peripheral = _knownPeripheralsDict[uuid];
    _changeNameUUID = uuid;
    NSArray *sUUID = @[[CBUUID UUIDWithString:@"013d8e3b-1877-4d5c-bc59-aaa7e5082346"]];
    [peripheral discoverServices:sUUID];
    _changeNameName = name;
}

- (void)changeName2 {
    NSLog(@"%s", __func__);
    CBPeripheral *peripheral = _knownPeripheralsDict[_changeNameUUID];
    NSArray *cUUID = @[[CBUUID UUIDWithString:@"34e0bd91-5e47-42e2-acd4-d5b4c25070af"]];
    [peripheral discoverCharacteristics:cUUID forService:_changeNameService];
}

- (void)changeName3 {
    NSLog(@"%s", __func__);
    CBPeripheral *peripheral = _knownPeripheralsDict[_changeNameUUID];
    //蓝牙协议规定名字的最大长度是21字节，考虑到每个汉字3个字节，所以上层UI得规定汉字不能多于7个
    //为了保险起见，汉字不能多于6个
    UInt8 name[20];
    memset(name, 0, sizeof(name));
    
    NSUInteger nameLength = _changeNameName.length;
    NSLog(@"nameLength = %d", nameLength);
    NSRange range = NSMakeRange(0, nameLength);
    NSUInteger useBuffer = 18;
    
    [_changeNameName getBytes:name maxLength:sizeof(name) usedLength:&useBuffer encoding:NSUTF8StringEncoding options:NSStringEncodingConversionAllowLossy range:range remainingRange:NULL];
    NSLog(@"usedLength = %d", useBuffer);
    NSData *nameData = [NSData dataWithBytes:name length:18];
    CBUUID *cCBUUID = [CBUUID UUIDWithString:@"34e0bd91-5e47-42e2-acd4-d5b4c25070af"];
    for ( CBCharacteristic *characteristic in _changeNameService.characteristics ) {
        if ([characteristic.UUID isEqual:cCBUUID]) {
            [peripheral writeValue:nameData forCharacteristic:characteristic type:CBCharacteristicWriteWithResponse];
            break;
        }
    }
    [self performSelector:@selector(communicateStateToIdle) withObject:nil afterDelay:2.0];
}

//-------------------------------------------------------------------------------------------------------------------
- (void)setLinkLoss:(Byte)buzVal peripheral:(CBPeripheral *)peripheral {
    NSLog(@"%s", __func__);
    NSData *d = [[NSData alloc] initWithBytes:&buzVal length:TI_KEYFOB_LINK_LOSS_WRITE_LEN];
    [self writeValue:TI_KEYFOB_LINK_LOSS_SERVICE_UUID characteristicUUID:TI_KEYFOB_LINK_LOSS_PROPERTY_UUID p:peripheral data:d Response:YES];
    [self performSelector:@selector(communicateStateToIdle) withObject:nil afterDelay:2.0];
}

/** 0:都提醒
  * 1:仅电话提醒
  * 2:仅Phobos提醒
 */
- (void)setLinkLoss:(NSString *)uuid value:(Byte)buzVal {
    NSLog(@"%s: uuid = %@, buzVal = %d", __func__, uuid, buzVal);
    // 如果当前状态不是IDLE，说明当前BLEManager正在和PHOBOS通讯
    if (!(_communicateState == CommunicateStateIdle)) {
        NSDictionary *info = @{@"state" : [NSNumber numberWithInteger:CommunicateStateLinkLoss],
                                @"value" : [NSNumber numberWithUnsignedChar:buzVal],
                                @"uuid"  : uuid};
        [self insertToActionList:info];
        [self performSelector:@selector(checkActionList) withObject:nil afterDelay:3.0];
        return;
    }
    
    _communicateState = CommunicateStateLinkLoss;
    CBPeripheral *peripheral = _knownPeripheralsDict[uuid];
    _linkLossUUID = uuid;
    NSData *d = [[NSData alloc] initWithBytes:&buzVal length:TI_KEYFOB_LINK_LOSS_WRITE_LEN];
    NSInteger ret = [self writeValue:TI_KEYFOB_LINK_LOSS_SERVICE_UUID characteristicUUID:TI_KEYFOB_LINK_LOSS_PROPERTY_UUID p:peripheral data:d Response:YES];
    NSLog(@"setLinkLoss ret = %d", ret);
    if (ret == 0) {
        [self performSelector:@selector(communicateStateToIdle) withObject:nil afterDelay:2.0];
        return;
    }
    linkLossValue = buzVal;
    NSArray *sUUID = @[[CBUUID UUIDWithString:@"1803"]];
    [peripheral discoverServices:sUUID];
}

- (void)setLinkLoss2 {
    NSLog(@"%s", __func__);
    CBPeripheral *peripheral = _knownPeripheralsDict[_linkLossUUID];
    [peripheral discoverCharacteristics:nil forService:_linkLossService];
}

- (void)setLinkLoss3 {
    NSLog(@"%s", __func__);
    CBPeripheral *peripheral = _knownPeripheralsDict[_linkLossUUID];
    [self setLinkLoss:linkLossValue peripheral:peripheral];
}

//-------------------------------------------------------------------------------------------------------------------
/*!
 *  @method readBattery:
 *
 *  @param p CBPeripheral to read from
 *
 *  @discussion Start a battery level read cycle from the battery level service
 *
 */
- (void)readBatteryPeripheral:(CBPeripheral *)peripheral {
    NSLog(@"%s", __func__);
    [self readValue:TI_KEYFOB_BATT_SERVICE_UUID characteristicUUID:TI_KEYFOB_LEVEL_SERVICE_UUID p:peripheral];
    [self performSelector:@selector(communicateStateToIdle) withObject:nil afterDelay:2.0];
}

- (void)readBattery:(NSString*)uuid {
    NSLog(@"%s", __func__);
    // 如果当前状态不是IDLE，说明当前BLEManager正在和PHOBOS通讯
    if (!(_communicateState == CommunicateStateIdle)) {
        NSDictionary *info = @{@"state" : [NSNumber numberWithInteger:CommunicateStateBattery],
                                @"uuid"  : uuid};
        [self insertToActionList:info];
        [self performSelector:@selector(checkActionList) withObject:nil afterDelay:3.0];
        return;
    }
    
    _communicateState = CommunicateStateBattery;
    CBPeripheral *peripheral = _knownPeripheralsDict[uuid];
    _batteryUUID = uuid;
    _retValue = [self readValue:TI_KEYFOB_BATT_SERVICE_UUID characteristicUUID:TI_KEYFOB_LEVEL_SERVICE_UUID p:peripheral];
    NSLog(@"_retValue = %d", _retValue);
    if (_retValue == 0) {
        [self performSelector:@selector(communicateStateToIdle) withObject:nil afterDelay:2.0];
        return;
    }
    
    NSArray *sUUID = @[[CBUUID UUIDWithString:@"180f"]];
    [peripheral discoverServices:sUUID];
}

- (void)readBattery2 {
    NSLog(@"%s", __func__);
    CBPeripheral *peripheral = _knownPeripheralsDict[_batteryUUID];
    [peripheral discoverCharacteristics:nil forService:_batteryService];
}

- (void)readBattery3 {
    NSLog(@"%s", __func__);
    CBPeripheral *peripheral = _knownPeripheralsDict[_batteryUUID];
    [self readBatteryPeripheral:peripheral];
}

#if defined(GET_TEMPERATURE_USE_BATTERY_SERVICE)
/*!
 *  @method readTemperaturePeripheral:
 *
 *  @param p CBPeripheral to read from
 *
 *  @discussion Start a battery level read cycle from the battery level service
 *
 */
- (void)readTemperaturePeripheral:(CBPeripheral *)peripheral {
    NSLog(@"%s", __func__);
    [self readValue:TI_KEYFOB_TEMPERATURE_SERVICE_UUID characteristicUUID:TI_KEYFOB_TEMPERATURE_CHARACTERISTIC p:peripheral];
}

- (void)readTemperature:(NSString*)uuid {
    NSLog(@"%s: uuid = %@", __func__, uuid);
    _communicateState = CommunicateStateTemperature;
    CBPeripheral *peripheral = _knownPeripheralsDict[uuid];
    _temperatureUUID = uuid;
    _retValue = [self readValue:TI_KEYFOB_TEMPERATURE_SERVICE_UUID characteristicUUID:TI_KEYFOB_TEMPERATURE_CHARACTERISTIC p:peripheral];
    NSLog(@"_retValue = %d", _retValue);
    if (_retValue == 0) {
        return;
    } else { // Just for test.
//        [self performSelector:@selector(testTemperature) withObject:nil afterDelay:1];
//        return;
    }
    
    NSArray *sUUID = @[[CBUUID UUIDWithString:@"ffb0"]];
    [peripheral discoverServices:sUUID];
}

- (void)readTemperature2 {
    NSLog(@"%s", __func__);
    CBPeripheral *peripheral = _knownPeripheralsDict[_temperatureUUID];
    [peripheral discoverCharacteristics:nil forService:_temperatureService];
}

- (void)readTemperature3 {
    NSLog(@"%s", __func__);
    CBPeripheral *peripheral = _knownPeripheralsDict[_temperatureUUID];
    [self readTemperaturePeripheral:peripheral];
}
#else // To use Gliese service
/*!
 *  @method readTemperaturePeripheral:
 *
 *  @param p CBPeripheral to read from
 *
 *  @discussion Start a battery level read cycle from the battery level service
 *
 */
- (void)readTemperaturePeripheral:(CBPeripheral *)peripheral {
    NSLog(@"%s", __func__);
    [self readValue:TI_KEYFOB_TEMPERATURE_SERVICE_UUID characteristicUUID:TI_KEYFOB_TEMPERATURE_CHARACTERISTIC p:peripheral];
}

- (void)readTemperature:(NSString*)uuid {
    NSLog(@"%s: uuid = %@", __func__, uuid);
    // 如果当前状态不是IDLE，说明当前BLEManager正在和PHOBOS通讯
    if (!(_communicateState == CommunicateStateIdle)) {
        NSDictionary *info = @{@"state" : [NSNumber numberWithInteger:CommunicateStateTemperature],
                               @"uuid"  : uuid};
        [self insertToActionList:info];
        [self performSelector:@selector(checkActionList) withObject:nil afterDelay:3.0];
        return;
    }
    
    _communicateState = CommunicateStateTemperature;
    CBPeripheral *peripheral = _knownPeripheralsDict[uuid];
    _temperatureUUID = uuid;

    CBService *service = nil;
    CBCharacteristic *characteristic = nil;
    for (CBService *s in peripheral.services) {
        if ([s.UUID isEqual:[CBUUID UUIDWithString:@"013d8e3b-1877-4d5c-bc59-aaa7e5082346"]]) {
            service = s;
            break;
        }
    }
    for ( CBCharacteristic *aCharacteristic in service.characteristics ) {
        if ([aCharacteristic.UUID isEqual:[CBUUID UUIDWithString:@"7fc69075-e330-4db6-8c46-8a8219eb7448"]]) {
            characteristic = aCharacteristic;
        }
    }
    if (service != nil && characteristic != nil) {
        NSLog(@"oh, let us read temperature directly");
        [peripheral readValueForCharacteristic:characteristic];
        [self performSelector:@selector(communicateStateToIdle) withObject:nil afterDelay:2.0];
        return;
    }

    NSArray *sUUID = @[[CBUUID UUIDWithString:@"013d8e3b-1877-4d5c-bc59-aaa7e5082346"]];
    [peripheral discoverServices:sUUID];
}

- (void)readTemperature2 {
    NSLog(@"%s", __func__);
    CBPeripheral *peripheral = _knownPeripheralsDict[_temperatureUUID];
    [peripheral discoverCharacteristics:nil forService:_temperatureService];
}

- (void)readTemperature3 {
    NSLog(@"%s", __func__);
    CBPeripheral *peripheral = _knownPeripheralsDict[_temperatureUUID];
//    [self readTemperaturePeripheral:peripheral];
    [peripheral readValueForCharacteristic:_temperatureCharacteristic];
    [self performSelector:@selector(communicateStateToIdle) withObject:nil afterDelay:2.0];
}

- (void)testTemperature {
    NSLog(@"%s", __func__);
    for (id targetDelegate in _delegate) {
        if ([targetDelegate respondsToSelector:@selector(didUpdateTemperature:)]) {
            [targetDelegate didUpdateTemperature:23.9];
        }
    }
}
#endif

- (void)communicateStateToIdle {
    NSLog(@"%s", __func__);
    _communicateState = CommunicateStateIdle;
}

#pragma mark - CLLocationManagerDelegate
- (void)locationManager:(CLLocationManager *)manager didDetermineState:(CLRegionState)state forRegion:(CLRegion *)region {
    NSLog(@"%s: state = %d, region = %@", __func__, state, region);
    CLBeaconRegion *beaconRegion = nil;
    CLBeaconMajorValue major;
    CLBeaconMinorValue minor;
    if ([region isKindOfClass:[CLBeaconRegion class]]) {
        beaconRegion = (CLBeaconRegion*)region;
        major = [beaconRegion.major unsignedShortValue];
        minor = [beaconRegion.minor unsignedShortValue];
    }
    if (beaconRegion == nil) {
        return;
    }
    if(state == CLRegionStateInside) {
        [self ruAlertNow:region.identifier Major:major Minor:minor];
        for (id targetDelegate in _delegate) {
            if ([targetDelegate respondsToSelector:@selector(didConnectPeripheral:)]) {
                [targetDelegate didConnectPeripheral:region.identifier];
            }
            [self updateTheState:targetDelegate];
        }
    }
}

// 0x91, 0x5F, 0xF9, 0xB4, 0xAB, 0x65, 0x47, 0xB7, 0xBC, 0x1B, 0xA4, 0xA3, 0x9B, 0xA2, 0xBF, 0xA6
- (void)locationManager:(CLLocationManager *)manager didEnterRegion:(CLRegion *)region {
    NSLog(@"%s: region = %@", __func__, region);
}

- (void)locationManager:(CLLocationManager *)manager didExitRegion:(CLRegion *)region {
    NSLog(@"%s: region = %@", __func__, region);
}

- (void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error {
    NSLog(@"%s: region = %@", __func__, error);
}
@end