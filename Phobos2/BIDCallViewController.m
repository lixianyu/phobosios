//
//  BIDCallViewController.m
//  Phobos2
//
//  Created by Li Xianyu on 13-11-23.
//  Copyright (c) 2013年 Li Xianyu. All rights reserved.
//

#import "BIDCallViewController.h"

@interface BIDCallViewController ()
@property (strong, nonatomic) NSString *titleCall;
@end

@implementation BIDCallViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    NSLog(@"%s", __func__);
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (id)initWithTitile:(NSString*)title {
    self = [[BIDCallViewController alloc] init];
    self.titleCall = title;
    return self;
}

- (void)viewDidLoad
{
    NSLog(@"%s", __func__);
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    self.title = _titleCall;
}

- (void)didReceiveMemoryWarning
{
    NSLog(@"%s", __func__);
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
