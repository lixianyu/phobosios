//
//  BIDRadarViewController5.m
//  Phobos2
//
//  Created by Li Xianyu on 13-11-21.
//  Copyright (c) 2013年 Li Xianyu. All rights reserved.
//

#import "BIDRadarViewController5.h"
#import "BIDBallLabelView.h"
#import "BIDRadarTableViewController.h"
#import "FPPopoverController.h"
#import "BIDAddNewPhobosViewController.h"
#import "BIDSideViewController.h"
#import "BIDAppDelegate.h"
#import "BIDConstant.h"
#import <CoreBluetooth/CoreBluetooth.h>
//#import "CBCentralManager+BIDBLEControl.h"
//#import "BIDBLEiOS6Delegate.h"
//#import "BIDBLEiOS7Delegate.h"

@interface BIDRadarViewController5 () <FPPopoverControllerDelegate, CBCentralManagerDelegate, CBPeripheralDelegate>
@property (nonatomic,retain) BIDScrollView *myscrollview;
@property (nonatomic, strong) UIImageView *imageView;

@property (strong, nonatomic) FPPopoverController *popover;
@property (strong, nonatomic) BIDRadarTableViewController *tableController;
@property (strong, nonatomic) NSTimer *updateTimer;
@property (strong, nonatomic) NSTimer *testTimer;
@property (strong, nonatomic) NSTimer *rssiTimer;
@property (strong, nonatomic) NSTimer *angleTimer;

@property (assign, nonatomic) RadarState radarState;
@property (assign, nonatomic) CGRect myRadarBounds;
@property (assign, nonatomic) BOOL pauseBallCanMove;

@property (strong, nonatomic) CBCentralManager *bleManager;
@property (strong, nonatomic) NSNumber *RSSInumber;
@property (strong, nonatomic) NSMutableDictionary *unknowBLEs;
@property (strong, nonatomic) NSMutableDictionary *allConnectPeripheral;
@property (strong, nonatomic) NSMutableDictionary *everConnectedUUID;
@property (strong, nonatomic) NSArray *knownPeripherals;

- (IBAction)chooseAPhobos:(id)sender;
@end

@implementation BIDRadarViewController5 {
    CGFloat minScaleHeight;
    CGFloat minScaleWidth;
}

- (id)initWithCoder:(NSCoder *)aDecoder {
    NSLog(@"%s", __func__);
    self = [super initWithCoder:aDecoder];
    if (self) {
//        _delegate = (NSMutableArray <BIDRadarViewController5Delegate> *)[NSMutableArray arrayWithCapacity:2];
        _delegate = [NSMutableArray arrayWithCapacity:2];
        NSNotificationCenter *center = [NSNotificationCenter defaultCenter];
        [center addObserver:self selector:@selector(handleHaveDeleteOneConnectedPeripheral:) name:@"Notification_haveDeleteOneConnectedPeripheral" object:nil];
        [self initEverConnectedUUID];
    }
    return self;
}

- (void)handleHaveDeleteOneConnectedPeripheral: (NSNotification*)aNotification {
    NSLog(@"%s, aNotification = %@", __func__, aNotification);
    NSString *uuid = [aNotification object];
    if (_allConnectPeripheral) {
        if ([_allConnectPeripheral objectForKey:uuid] != nil) {
            [_bleManager cancelPeripheralConnection:_allConnectPeripheral[uuid]];
            [_allConnectPeripheral removeObjectForKey:uuid];
        }
    }
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    NSLog(@"%s", __func__);
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (CGFloat)getiOSVersion {
    CGFloat systemVersion = [[[UIDevice currentDevice] systemVersion] floatValue];
    NSLog(@"iOS version = %f", systemVersion);
    return systemVersion;
}

- (NSString *)dataFilePath {
    NSLog(@"%s", __func__);
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    return [documentsDirectory stringByAppendingPathComponent:@"dataPhobos.plist"];
}

- (void)saveEverConnectedUUID:(NSString*)uuid {
    NSLog(@"%s", __func__);
    NSMutableArray *uuidString = [NSMutableArray arrayWithCapacity:5];
    for (NSUUID *aNSUUID in _everConnectedUUID) {
        [uuidString addObject:aNSUUID.UUIDString];
    }
    [uuidString writeToFile:[self dataFilePath] atomically:YES];
}

- (void)initEverConnectedUUID {
    NSLog(@"%s", __func__);
    NSString *filePath = [self dataFilePath];
    _everConnectedUUID = [NSMutableDictionary dictionaryWithCapacity:5];
    if ([[NSFileManager defaultManager] fileExistsAtPath:filePath]) {
        NSArray *array = [[NSArray alloc] initWithContentsOfFile:filePath];
        for (NSString *uuid in array) {
            [_everConnectedUUID setObject:uuid forKey:[[NSUUID alloc] initWithUUIDString:uuid]];
        }
    }
}

- (void)initBLE {
    NSLog(@"%s", __func__);
    CGFloat systemVersion = [[[UIDevice currentDevice] systemVersion] floatValue];
    NSLog(@"systemVersion = %f", systemVersion);

    id manager = _bleManager;
    NSLog(@"_bleManager = %@, _unknowBLEs = %@", _bleManager, _unknowBLEs);
    if (_bleManager == nil) {
        _bleManager = [[CBCentralManager alloc] initWithDelegate:self queue:nil options:@{CBCentralManagerOptionRestoreIdentifierKey : @"myCentralManagerIdentifier"}];
    }
    if (_unknowBLEs == nil) {
        _unknowBLEs = [[NSMutableDictionary alloc] initWithCapacity:10];
    }
    NSLog(@"_radarState = %d", _radarState);
    CGFloat randomX = arc4random() % 1000;
    CGFloat randomY = [self getRandomYFromX:randomX];
    if (_radarState == RadarStateAllBLE) {
        NSDictionary *optionsdict = @{CBCentralManagerScanOptionAllowDuplicatesKey : [NSNumber numberWithBool:YES]};
        if (manager != nil) {
            [_bleManager scanForPeripheralsWithServices:nil options:optionsdict];
        }
        NSArray *allKey = [_unknowBLEs allKeys];
        for (NSString *uuid in allKey) {
            [self.myscrollview addABallLabelWithLabel:@"未知" UUID:uuid pointOnMe:CGPointMake(randomX, randomY) unKnowBle:YES];
            randomX = arc4random() % 1000;
            randomY = [self getRandomYFromX:randomX];
        }
        [self letUsHaveAZoom];
        [self CheckRSSI];
    } else {
        NSArray *allKey = [_allConnectPeripheral allKeys];
        NSLog(@"_allConnectPeripheral's allKey.count = %d", allKey.count);
        for (NSString *uuid in allKey) {
            [_myscrollview setABallAlpha:1.0f UUID:uuid flash:NO];
        }
        if (allKey.count > 0) {
            if ([self getAllCheckCount] > allKey.count) {
                if (manager != nil) {
                    [_bleManager scanForPeripheralsWithServices:nil options:nil];
                }
            }
            [self CheckRSSI];
            return;
        }
        
        if (manager != nil) {
            
            if ([self getiOSVersion] >= 7.0) {
                _knownPeripherals = [_bleManager retrievePeripheralsWithIdentifiers:[_everConnectedUUID allKeys]];
            }
            NSLog(@"_knownPeripherals = %@", _knownPeripherals);
            if (_knownPeripherals.count > 0) {
                for (CBPeripheral *aPeripheral in _knownPeripherals) {
                    [self connectPhobos:aPeripheral];
                }
                if ([self getAllCheckCount] > _knownPeripherals.count) {
                    [self performSelector:@selector(scanPhobos) withObject:nil afterDelay:10];
                }
            } else {
                [_bleManager scanForPeripheralsWithServices:nil options:nil];
//            NSArray *arrayUUID = [NSArray arrayWithObjects:[CBUUID UUIDWithString:@"853C5886-90EA-7499-847B-96A29C28D2C8"],
//                                  [CBUUID UUIDWithString:@"180F"], nil];
//            [_bleManager scanForPeripheralsWithServices:arrayUUID options:nil];
            }
        }
        
//        [self performSelector:@selector(timeoutScan) withObject:nil afterDelay:5];
    }
}

- (BOOL)isConnected:(NSString*)uuid {
    NSLog(@"%s,uuid=%@", __func__, uuid);
    if ([_allConnectPeripheral objectForKey:uuid] == nil) {
        return NO;
    }
    return YES;
}

- (void)scanPhobos {
    NSLog(@"%s", __func__);
    [_bleManager scanForPeripheralsWithServices:nil options:nil];
}

- (void)stopScanPhobos {
    NSLog(@"%s", __func__);
    [_bleManager stopScan];
}

- (void)connectPhobos:(CBPeripheral*)aPeripheral {
    NSDictionary *connectOptions = @{CBConnectPeripheralOptionNotifyOnConnectionKey : [NSNumber numberWithBool:NO],
                                     CBConnectPeripheralOptionNotifyOnDisconnectionKey : [NSNumber numberWithBool:NO],
                                     CBConnectPeripheralOptionNotifyOnNotificationKey : [NSNumber numberWithBool:YES]};
//    [_bleManager connectPeripheral:aPeripheral options:nil];
    [_bleManager connectPeripheral:aPeripheral options:connectOptions];
}

- (NSInteger)getAllCheckCount {
    NSInteger section = [_tableController getSectionNumber];
    NSInteger count = 0;
    if (section == 1) {
        count =  _tableController.uuids.count;
    } else if (section == 2) {
        for (NSNumber *aNumber in _tableController.ifChecked) {
            if ([aNumber boolValue]) {
                count++;
            }
        }
        
    }
    NSLog(@"%s: count = %d", __func__, count);
    return count;
}

- (void)timeoutScan {
    NSLog(@"%s", __func__);
    NSLog(@"_bleManager.state = %d", _bleManager.state);
    [self performSelector:@selector(timeoutScan) withObject:nil afterDelay:5];
}

- (void)viewDidLoad
{
    NSLog(@"%s", __func__);
    [super viewDidLoad];

	// Do any additional setup after loading the view.
    //CGRect appRect = [UIScreen mainScreen].applicationFrame;
    //NSLog(@"appRect: x = %f, y = %f, width = %f, height = %f", appRect.origin.x, appRect.origin.y, appRect.size.width, appRect.size.height);
    CGRect statusBarRect = [[UIApplication sharedApplication] statusBarFrame];
    NSLog(@"statusBarRect: x = %f, y = %f, width = %f, height = %f", statusBarRect.origin.x, statusBarRect.origin.y, statusBarRect.size.width, statusBarRect.size.height);
//    _myRadarBounds = CGRectMake(0, statusBarRect.size.height+self.navigationController.navigationBar.bounds.size.height, self.view.bounds.size.width, self.view.bounds.size.height - self.tabBarController.tabBar.bounds.size.height - self.navigationController.navigationBar.bounds.size.height - statusBarRect.size.height);
    NSLog(@"myRadarBounds: x = %f, y = %f, width = %f, height = %f", _myRadarBounds.origin.x, _myRadarBounds.origin.y, _myRadarBounds.size.width, _myRadarBounds.size.height);
    _myRadarBounds = CGRectMake(0, statusBarRect.size.height, self.view.bounds.size.width, self.view.bounds.size.height - statusBarRect.size.height);
    _myscrollview = [[BIDScrollView alloc] initWithFrame:_myRadarBounds];
    
    UIImageView *imageViewBackground = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"1"]];
    NSLog(@"myimage.image.size.width = %f, myimage.image.size.height = %f", imageViewBackground.image.size.width, imageViewBackground.image.size.height);
    [imageViewBackground setFrame:CGRectMake(0, 0, imageViewBackground.image.size.width, imageViewBackground.image.size.height)];
    [_myscrollview addSubview:imageViewBackground];
    _imageView = imageViewBackground;
    
    _myscrollview.directionalLockEnabled = NO; //只能一个方向滑动
    _myscrollview.pagingEnabled = NO; //是否翻页
    _myscrollview.backgroundColor = [UIColor darkGrayColor];
    _myscrollview.showsVerticalScrollIndicator = YES; //垂直方向的滚动指示
    _myscrollview.indicatorStyle = UIScrollViewIndicatorStyleWhite;//滚动指示的风格
    _myscrollview.showsHorizontalScrollIndicator = YES;//水平方向的滚动指示
    _myscrollview.delegate = self;
    
    minScaleWidth = self.view.frame.size.width / imageViewBackground.image.size.width;
    NSLog(@"self.view.frame.size.width = %f, myimage.image.size.width = %f", self.view.frame.size.width, imageViewBackground.image.size.width);
    minScaleHeight = self.view.frame.size.height / imageViewBackground.image.size.width;
    NSLog(@"minScaleWidth = %f, minScaleHeight = %f", minScaleWidth, minScaleHeight);
    [_myscrollview setMinimumZoomScale:minScaleWidth];
    [_myscrollview setMaximumZoomScale:1.0];
    [_myscrollview setZoomScale:minScaleHeight animated:YES];
    
    [self.view addSubview:_myscrollview];
    [self.myscrollview setContentOffset:CGPointMake(126.5f, 81.0f)];//根据trace观察定下来的坐标
    
    //    [self startAccelerometer];
    UITapGestureRecognizer * oneTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(oneTaple:)];
    [oneTap setNumberOfTapsRequired:1];
    [self.myscrollview addGestureRecognizer:oneTap];
    
    _allConnectPeripheral = [NSMutableDictionary dictionaryWithCapacity:5];
    
    NSNotificationCenter *center = [NSNotificationCenter defaultCenter];
    [center addObserver:self selector:@selector(applicationDidEnterBackground) name:UIApplicationDidEnterBackgroundNotification object:nil];
}

- (void)viewWillAppear:(BOOL)animated
{
    NSLog(@"%s", __func__);
    [super viewWillAppear:animated];
    NSNotificationCenter *center = [NSNotificationCenter defaultCenter];
    [center addObserver:self selector:@selector(applicationDidBecomeActive) name:UIApplicationDidBecomeActiveNotification object:nil];
    [center addObserver:self selector:@selector(applicationWillEnterForeground) name:UIApplicationWillEnterForegroundNotification object:nil];
    [self addBallOnView];
    [self initBLE];
    
    [self performSelector:@selector(drawRadar) withObject:nil afterDelay:0.5];
    self.updateTimer = [NSTimer scheduledTimerWithTimeInterval:4.0
                                                        target:self
                                                      selector:@selector(drawRadar)
                                                      userInfo:nil
                                                       repeats:YES];
//    self.testTimer = [NSTimer scheduledTimerWithTimeInterval:1.2
//                                                        target:self
//                                                      selector:@selector(testSsss)
//                                                      userInfo:nil
//                                                       repeats:YES];
}

- (void)viewDidAppear:(BOOL)animated {
    NSLog(@"%s", __func__);
    [super viewDidAppear:animated];
    [self hideTabBar:YES];
    _pauseBallCanMove = YES;
}

- (void)viewWillDisappear:(BOOL)animated {
    NSLog(@"%s", __func__);
    [super viewWillDisappear:animated];
    [NSObject cancelPreviousPerformRequestsWithTarget:self selector:@selector(hideTabBarByPerform:) object:[NSNumber numberWithBool:YES]];
    [NSObject cancelPreviousPerformRequestsWithTarget:self selector:@selector(timeoutScan) object:nil];
    [self hideTabBar:NO];
    gtabNavFlag = YES;
    [_bleManager stopScan];
    NSNotificationCenter *center = [NSNotificationCenter defaultCenter];
    [center removeObserver:self name:UIApplicationDidBecomeActiveNotification object:nil];
    [center removeObserver:self name:UIApplicationWillEnterForegroundNotification object:nil];
}

- (void)viewDidDisappear:(BOOL)animated
{
    NSLog(@"%s", __func__);
    [super viewDidDisappear:animated];
    
    [self.updateTimer invalidate];
    self.updateTimer = nil;
    [self.testTimer invalidate];
    self.testTimer = nil;
    [self.rssiTimer invalidate];
    self.rssiTimer = nil;
    [self.angleTimer invalidate];
    self.angleTimer = nil;
    for (UIView *aView in self.myscrollview.subviews) {
        if ([aView isKindOfClass:[BIDBallLabelView class]]) {
            NSLog(@"remove one");
            [aView removeFromSuperview];
        }
    }
}

- (BOOL)chuAlert:(NSString*)uuid {
    NSLog(@"%s: uuid = %@", __func__, uuid);
    BIDAppDelegate *appDelegate = (BIDAppDelegate*)[UIApplication sharedApplication].delegate;
    NSManagedObjectContext *context = [appDelegate managedObjectContext];
    NSError *error;
    NSFetchRequest *request = [[NSFetchRequest alloc] initWithEntityName:kPhobosEntityName];
    NSPredicate *pred = [NSPredicate predicateWithFormat:@"%K = %@", kPhobosDeviceUUIDKey, uuid];
    [request setPredicate:pred];
    NSArray *objects = [context executeFetchRequest:request error:&error];
    if (objects == nil) {
        NSLog(@"%s : There was an error!", __func__);
        return NO;
    }
    if ([objects count] <= 0) {
        return NO;
    }
    
    NSManagedObject *thePhobos = [objects objectAtIndex:0];
    BOOL chuSwitch = [[thePhobos valueForKey:kPhobosChuAlertKey] boolValue];
    //    ruSwitch = [[thePhobos valueForKey:kPhobosRuAlertKey] boolValue];
    int chuSegment = [[thePhobos valueForKey:kPhobosChuAlertTypeKey] intValue];
    NSLog(@"chuSwitch = %@", chuSwitch?@"YES":@"NO");
    return chuSwitch;
}

- (void)applicationDidEnterBackground {
    NSLog(@"%s", __func__);
    [self stopCheckRSSI];
    for (NSString *uuid in _allConnectPeripheral) {
        NSLog(@"aCBPeripheral uuid = %@", uuid);
        if (![self chuAlert:uuid]) {
            [_bleManager cancelPeripheralConnection:_allConnectPeripheral[uuid]];
        }
        
//        [_myscrollview setABallAlpha:0.3 UUID:uuid flash:NO];
    }
}

- (void)applicationWillEnterForeground {
    NSLog(@"%s", __func__);
    if (_myscrollview) {
        for (BIDBallLabelView *aBallView in _myscrollview.subviews) {
            if ([aBallView isKindOfClass:[BIDBallLabelView class]]) {
                aBallView.alpha = 0.3f;
            }
        }
    }
}

- (void)applicationDidBecomeActive {
    NSLog(@"%s", __func__);
    [self initBLE];
    [self hideTabBar:NO];
    [self performSelector:@selector(hideTabBarByPerform:) withObject:[NSNumber numberWithBool:YES] afterDelay:2.0f];
}

//0到100之间的浮点数
- (CGFloat)getRandomFloat
{
    #define ARC4RANDOM_MAX      0x100000000
    CGFloat val = floorf(((CGFloat)arc4random() / ARC4RANDOM_MAX) * 100.0f);
    return val;
//    return (u_int32_t)(from + (arc4random() % (to - from + 1)));
}

- (void)CheckRSSI {
    NSLog(@"%s", __func__);
    if (_rssiTimer) {
        [_rssiTimer invalidate];
        _rssiTimer = nil;
    }
    if (_angleTimer) {
        [_angleTimer invalidate];
        _angleTimer = nil;
    }
    self.rssiTimer = [NSTimer scheduledTimerWithTimeInterval:1.5
                                                        target:self
                                                    selector:@selector(beginRssiUpdate)
                                                      userInfo:nil
                                                       repeats:YES];

//    self.angleTimer = [NSTimer scheduledTimerWithTimeInterval:5.0f
//                                                       target:self
//                                                     selector:@selector(angleUpdate)
//                                                     userInfo:nil
//                                                      repeats:YES];
}

- (void)beginRssiUpdate {
    NSLog(@"%s, _radarState = %d", __func__, _radarState);
    if (_radarState == RadarStateAllPair || _radarState == RadarStateSelected) {
        for (NSString *uuid in _allConnectPeripheral) {
            [_allConnectPeripheral[uuid] readRSSI];
        }
    } else {
        NSString *uuid = [self getAUUID];
        if (uuid == nil) {
            return;
        }
        CGFloat realRssi;
        realRssi = [_RSSInumber floatValue];
        NSLog(@"realRssi = %f", realRssi);
        if (realRssi == 127) {
            return;
        }
        
        NSDictionary *aDict = @{@"uuid" : uuid,
                                @"rssi" : [NSNumber numberWithFloat:realRssi]};
        [self.myscrollview performSelectorOnMainThread:@selector(testBLErssi:) withObject:aDict waitUntilDone:NO];
    }
}

- (void)stopCheckRSSI {
    NSLog(@"%s", __func__);
    [self.rssiTimer invalidate];
    self.rssiTimer = nil;
    [self.angleTimer invalidate];
    self.angleTimer = nil;
}

- (void)angleUpdate {
//    NSLog(@"%s", __func__);
    if (_pauseBallCanMove == NO) {
        return;
    }
    NSString *uuid = [self getAUUID];
    NSLog(@"uuid = %@", uuid);
    if (uuid == nil) {
        return;
    }
    CGFloat degree = arc4random() % 361;
    CGFloat angle = degree * M_PI / 180;
    
    NSDictionary *aDict = @{@"uuid" : uuid,
                            @"angle" : [NSNumber numberWithFloat:angle]};
    [self.myscrollview performSelectorOnMainThread:@selector(testBLEArc:) withObject:aDict waitUntilDone:NO];
}

- (void)rssiUpdate {
    //    NSLog(@"%s", __func__);
    if (_pauseBallCanMove == NO) {
        return;
    }
    NSString *uuid = [self getAUUID];
    if (uuid == nil) {
        return;
    }
    CGFloat realRssi, rssi;
    if (_bleManager) {
        realRssi = [_RSSInumber floatValue];
        NSLog(@"realRssi0 = %f", realRssi);
        if (realRssi == 127) {
            return;
        }
    } else {
        rssi = [self getRandomFloat];
        while (YES) {
            if (rssi < 37.1) {
                rssi = [self getRandomFloat];
            } else if (rssi > 95.1) {
                rssi = [self getRandomFloat];
            } else {
                break;
            }
        }
        realRssi = -rssi;
        NSLog(@"realRssi1 = %f", realRssi);
    }
    
    NSDictionary *aDict = @{@"uuid" : uuid,
                            @"rssi" : [NSNumber numberWithFloat:realRssi]};
    [self.myscrollview performSelectorOnMainThread:@selector(testBLErssi:) withObject:aDict waitUntilDone:NO];
}

- (NSString *)getAUUID {
    static int unknowBleCount = 0;
    NSInteger section = [_tableController getSectionNumber];
    NSLog(@"%s: section = %d, unknowBleCount = %d", __func__, section, unknowBleCount);
    if (section == 0) {
        NSArray *allkey = [_unknowBLEs allKeys];
        if (allkey.count <= 0) {
            return nil;
        }
        if (unknowBleCount >= allkey.count) {
            unknowBleCount = 0;
        }
        return allkey[unknowBleCount++];
    }
    NSMutableArray * workingBall = [[NSMutableArray alloc] initWithCapacity:10];
    for (int i = 0; i < _tableController.uuids.count; i++) {
        if (section == 1) {
            [workingBall addObject:_tableController.uuids[i]];
        } else if ([_tableController.ifChecked[i] boolValue]) {
            [workingBall addObject:_tableController.uuids[i]];
        }
    }
    u_int32_t mo = workingBall.count;
    NSLog(@"mo = %d", mo);
    if (workingBall.count <= 1) {
        if (workingBall.count == 1) {
            return workingBall[0];
        } else {
            return nil;
        }
    }
    u_int32_t randomI = arc4random() % mo;
    return workingBall[randomI];
}

//cos(π/2-α) = sinα
- (CGFloat)getRandomYFromX:(CGFloat)x {
    if (x > 950.0) {
        x = 950.0f;
    } else if (x < 50.0) {
        x = 50.0f;
    }
    CGFloat y;
    CGFloat maxY;
    while (YES) {
        y = arc4random() % 1000;
        maxY = sqrtf((gradius * gradius) - (ABS((x-gcenterX) * (x - gcenterX))));
        if (y >= gcenterY - maxY && y <= gcenterY + maxY) {
            break;
        } else {
            NSInteger iY = maxY;
            y = arc4random() % (iY*2);
            y += gcenterY - maxY;
            break;
        }
    }
    return y;
}

- (void)letUsHaveAZoom {
    NSLog(@"%s", __func__);
    static BOOL flag = YES;
    CGFloat tmpScale;
    if (flag) {
        NSLog(@"flag = YES");
        tmpScale = 0.01f;
    } else {
        NSLog(@"flag = NO");
        tmpScale = -0.01f;
    }
    flag = !flag;
    if (_myscrollview.minimumZoomScale == _myscrollview.zoomScale) {
        [self.myscrollview setZoomScale:self.myscrollview.zoomScale+0.01f animated:YES];
    } else {
        [self.myscrollview setZoomScale:self.myscrollview.zoomScale+tmpScale animated:YES];
    }
    NSLog(@"minimumZoomScale = %f, zoomScale = %f", _myscrollview.minimumZoomScale, _myscrollview.zoomScale);
}

- (void)addBallOnView {
    NSLog(@"%s", __func__);
    _tableController = [[BIDRadarTableViewController alloc] init];
    [_tableController getCoreData];
    if ([BIDSideViewController comeFromMe]) {//只要能从BIDSideViewController过来，说明肯定曾经配对过一个Phobos
        NSLog(@"YES, I come from 'BIDSideViewController'");
        _radarState = RadarStateSelected;
        [_tableController setSectionNumber:2];
        [BIDSideViewController resetComeToRadar];
        _tableController.comeFromSideViewController = YES;
        
        NSDictionary *aDict = [self getBallTextUuid];
        NSLog(@"ballText = %@", aDict[@"name"]);
        CGFloat randomX = arc4random() % 1000;
        CGFloat randomY = [self getRandomYFromX:randomX];
        [self.myscrollview addABallLabelWithLabel:aDict[@"name"] UUID:aDict[@"uuid"] pointOnMe:CGPointMake(randomX, randomY)];
//        [self performSelector:@selector(CheckRSSI) withObject:nil afterDelay:0.5];
    } else {
        _tableController.comeFromSideViewController = NO;
        
        NSLog(@"_tableController.dwarves.count = %d", _tableController.dwarves.count);
        if (_tableController.dwarves.count == 0) {
            //显示周边所有未配对蓝牙BLE
            _radarState = RadarStateAllBLE;
            [_tableController setSectionNumber:0];
//            for (NSString *uuid in _allConnectPeripheral) {
//                NSLog(@"aCBPeripheral uuid = %@", uuid);
//                [_bleManager cancelPeripheralConnection:_allConnectPeripheral[uuid]];
//                [_myscrollview setABallAlpha:0.3 UUID:uuid flash:NO];
//            }
//            _allConnectPeripheral = [NSMutableDictionary dictionaryWithCapacity:5];
            return;
        }
        
        NSInteger section = [_tableController getSectionNumber];
        NSLog(@"section = %d", section);
        if (section == 0) {
            //显示周边所有未配对蓝牙BLE
            _radarState = RadarStateAllBLE;
            return;
        }
        
        CGFloat randomX = arc4random() % 1000;
        CGFloat randomY = [self getRandomYFromX:randomX];
        for (int i = 0; i < _tableController.dwarves.count; i++) {
            if (section == 2) {
                if (YES == [_tableController.ifChecked[i] boolValue]) {
                    [_myscrollview addABallLabelWithLabel:_tableController.dwarves[i] UUID:_tableController.uuids[i] pointOnMe:CGPointMake(randomX, randomY)];
                }
            } else if (section == 1) {
                [_myscrollview addABallLabelWithLabel:_tableController.dwarves[i] UUID:_tableController.uuids[i] pointOnMe:CGPointMake(randomX, randomY)];
            }
            randomX = arc4random() % 1000;
            randomY = [self getRandomYFromX:randomX];
        }
        if (section == 2) {
            _radarState = RadarStateSelected;
        } else if (section == 1) {
            _radarState = RadarStateAllPair;
        }
//        [self performSelector:@selector(CheckRSSI) withObject:nil afterDelay:3.0];
    }
    [self letUsHaveAZoom];
}

- (void)reAddBallsOnView {
    NSLog(@"%s", __func__);
    for (UIView *aView in self.myscrollview.subviews) {
        if ([aView isKindOfClass:[BIDBallLabelView class]]) {
            NSLog(@"remove one");
            [aView removeFromSuperview];
        }
    }

    NSInteger section = [_tableController getSectionNumber];
    NSLog(@"section = %d", section);
    if (section == 0) {
        _radarState = RadarStateAllBLE;
        [self initBLE];
        return;
    }
    CGFloat randomX = arc4random() % 1000;
    CGFloat randomY = [self getRandomYFromX:randomX];
    NSLog(@"_tableController.dwarves.count = %d", _tableController.dwarves.count);
    for (int i = 0; i < _tableController.dwarves.count; i++) {
        if (section == 2) {
            if (YES == [_tableController.ifChecked[i] boolValue]) {
                [self.myscrollview addABallLabelWithLabel:_tableController.dwarves[i] UUID:_tableController.uuids[i] pointOnMe:CGPointMake(randomX, randomY)];
            }
        } else if (section == 1) {
            [_myscrollview addABallLabelWithLabel:_tableController.dwarves[i] UUID:_tableController.uuids[i] pointOnMe:CGPointMake(randomX, randomY)];
        }
        randomX = arc4random() % 1000;
        randomY = [self getRandomYFromX:randomX];
    }
    if (section == 2) {
        _radarState = RadarStateSelected;
    } else if (section == 1) {
        _radarState = RadarStateAllPair;
    }
    [self letUsHaveAZoom];
    [self initBLE];
}

- (NSDictionary *)getBallTextUuid {
    NSLog(@"%s", __func__);
    for (int i = 0; i < _tableController.ifChecked.count; i++) {
        if ([_tableController.ifChecked[i] boolValue]) {
            NSDictionary *adic = @{@"name" : _tableController.dwarves[i],
                                   @"uuid" : _tableController.uuids[i]};
            return adic;
        }
    }
    return nil;
}

- (void)testRotate {
    NSLog(@"Enter");
    static int i = 1;
    CGAffineTransform rotate = CGAffineTransformMakeRotation( i++ / 180.0 * 3.14 );
    [_imageView setTransform:rotate];
}

- (void)testSsss {
    [self.myscrollview performSelectorOnMainThread:@selector(ssss1) withObject:nil waitUntilDone:NO];
    [self.testTimer invalidate];
    self.testTimer = nil;
}

- (void)drawRadar {
    if (self.myscrollview) {
        [self.myscrollview drawRadar];
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark － UIScrollViewDelegate
//只要滚动了就会触发
- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    //    NSLog(@" scrollViewDidScroll");
//    NSLog(@"scrollViewDidScroll: ContentOffset x=%f,y=%f", scrollView.contentOffset.x,scrollView.contentOffset.y);
    //    NSLog(@"scrollViewDidScroll: contentInset left=%f,top=%f,right=%f,bottom=%f",scrollView.contentInset.left,scrollView.contentInset.top, scrollView.contentInset.right, scrollView.contentInset.bottom);
//    [self.myscrollview performSelectorOnMainThread:@selector(updateDragging)
//                                        withObject:nil
//                                     waitUntilDone:NO];
}

//static CGFloat beginDraggingOffsetX;
//static CGFloat beginDraggingOffsetY;
//static CGFloat endDraggingOffsetX;
//static CGFloat endDraggingOffsetY;
//开始拖拽视图
- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView
{
//    NSLog(@"scrollViewWillBeginDragging");
    //    beginDraggingOffsetX = scrollView.contentOffset.x;
    //    beginDraggingOffsetY = scrollView.contentOffset.y;
//    [self.myscrollview performSelectorOnMainThread:@selector(updateDragging)
//                                        withObject:nil
//                                     waitUntilDone:NO];
}
//完成拖拽
- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate
{
//    NSLog(@"scrollViewDidEndDragging, decelerate = %d", decelerate);
    if (decelerate) {
        return;
    } else {
        [self updateOffset:scrollView];
    }
}

//将开始降速时
- (void)scrollViewWillBeginDecelerating:(UIScrollView *)scrollView
{
//    NSLog(@"scrollViewWillBeginDecelerating");
}

//减速停止了时执行，手触摸时执行执行
- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView
{
//    NSLog(@"scrollViewDidEndDecelerating");
    [self updateOffset:scrollView];
}

//滚动动画停止时执行,代码改变时出发,也就是setContentOffset改变时
- (void)scrollViewDidEndScrollingAnimation:(UIScrollView *)scrollView
{
//    NSLog(@"scrollViewDidEndScrollingAnimation");
}

- (void)scrollViewDidZoom:(UIScrollView *)scrollView {
//    NSLog(@"scrollViewDidZoom:  zoomScale = %f", scrollView.zoomScale);
    [self.myscrollview setCurrentScale:scrollView.zoomScale];
    [self.myscrollview performSelectorOnMainThread:@selector(updateZoom)
                                        withObject:nil
                                     waitUntilDone:NO];
}

//设置放大缩小的视图，要是uiscrollview的subview
- (UIView *)viewForZoomingInScrollView:(UIScrollView *)scrollView
{
//    NSLog(@"viewForZoomingInScrollView");
    return _imageView;
}

//完成放大缩小时调用
- (void)scrollViewDidEndZooming:(UIScrollView *)scrollView withView:(UIView *)view atScale:(float)scale
{
//    NSLog(@"view=%@", view);
    NSLog(@"scale = %f, minScaleHeight = %f", scale, minScaleHeight);
    [scrollView setZoomScale:scale animated:YES];
    
    if (scale < minScaleHeight) {
        //[_myscrollview setContentOffset:CGPointMake((view.frame.size.width - 320)/2, (view.frame.size.height-524)/2) animated:NO];
        //[self performSelector:@selector(handleTimesup:) withObject:[NSNumber numberWithFloat:scale] afterDelay:0.11];
    }
//    [self.myscrollview setCurrentScale:scale];
//    [self.myscrollview performSelectorOnMainThread:@selector(updateZoom)
//                                        withObject:nil
//                                     waitUntilDone:NO];
//    NSLog(@"scrollViewDidEndZooming:scale between minimum and maximum. called after any 'bounce' animations");
    [self updateOffset:scrollView];
}

//如果你不是完全滚动到滚轴视图的顶部，你可以轻点状态栏，那个可视的滚轴视图会一直滚动到顶部，那是默认行为，你可以通过该方法返回NO来关闭它
- (BOOL)scrollViewShouldScrollToTop:(UIScrollView *)scrollView
{
    NSLog(@"scrollViewShouldScrollToTop");
    return NO;
}

- (void)scrollViewDidScrollToTop:(UIScrollView *)scrollView
{
    NSLog(@"scrollViewDidScrollToTop");
}

#pragma mark -
- (void)updateOffset:(UIScrollView *)scrollView {
    for (BIDBallLabelView *aBall in self.myscrollview.subviews) {
        if ([aBall isKindOfClass:[BIDBallLabelView class]]) {
            aBall.offsetX = scrollView.contentOffset.x;
            aBall.offsetY = scrollView.contentOffset.y;
        }
        else {
            
        }
    }
}

#pragma mark -
- (IBAction)chooseAPhobos:(id)sender {
    NSLog(@"%s: sender = %@", __func__, sender);
    _pauseBallCanMove = NO;
    [_bleManager stopScan];
    if ([BIDAddNewPhobosViewController ifAddAPhobos] == YES) {
        _tableController = [[BIDRadarTableViewController alloc] init];
        [BIDAddNewPhobosViewController resetIfAddAPhobosFlag];
        [_tableController getCoreData];
    } else if ([BIDSideViewController ifDeleteAPhobos] == YES) {
        _tableController = [[BIDRadarTableViewController alloc] init];
        [BIDSideViewController resetIfDeleteAPhobosFlag];
        [_tableController getCoreData];
    }
    if (_tableController == nil) {
        _tableController = [[BIDRadarTableViewController alloc] init];
        [_tableController getCoreData];
    }
    
    _popover = [[FPPopoverController alloc] initWithViewController:_tableController];
    _popover.delegate = self;
    _popover.tint = FPPopoverLightGrayTint;
    
//    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
    {
        _popover.contentSize = CGSizeMake(200, 400);
    }
    _popover.arrowDirection = FPPopoverArrowDirectionUp;
    
//    [_popover presentPopoverFromView:sender];
    [_popover presentPopoverFromPoint:CGPointMake(290, 28) ifUsePoint:YES];
}

#pragma mark - FPPopoverControllerDelegate
- (void)popoverControllerDidDismissPopover:(FPPopoverController *)popoverController {
    NSLog(@"%s", __func__);
    [_tableController savePhobos];
    [_popover removeObservers];
//    _popover = nil;
    
    [self reAddBallsOnView];
    _pauseBallCanMove = YES;
}

- (void)presentedNewPopoverController:(FPPopoverController *)newPopoverController
          shouldDismissVisiblePopover:(FPPopoverController*)visiblePopoverController {
    NSLog(@"%s", __func__);
}

#pragma mark -
static BOOL gtabNavFlag = YES;
-(void)oneTaple:(UITapGestureRecognizer*)gesture {
//    NSLog(@"%s: gtabNavFlag = %@", __func__, gtabNavFlag ? @"YES" : @"NO");
    if (gtabNavFlag) {
        [NSObject cancelPreviousPerformRequestsWithTarget:self selector:@selector(hideTabBarByPerform:) object:[NSNumber numberWithBool:YES]];
        [self hideTabBar:NO];
        [self performSelector:@selector(hideTabBarByPerform:) withObject:[NSNumber numberWithBool:YES] afterDelay:5];
    } else {
        [NSObject cancelPreviousPerformRequestsWithTarget:self selector:@selector(hideTabBarByPerform:) object:[NSNumber numberWithBool:YES]];
        [self hideTabBar:YES];
    }
    gtabNavFlag = !gtabNavFlag;
}

- (void)hideTabBar:(BOOL)hidden {
//    NSLog(@"%s, hidden = %@", __func__, hidden?@"YES":@"NO");
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDuration:1.0];
    for(UIView *view in self.tabBarController.view.subviews)
    {
//        NSLog(@"tabbar subview : %@", view);
        if([view isKindOfClass:[UITabBar class]])
        {
            if (hidden) {
                view.alpha = 0.0f;
                
            } else {
                view.alpha = 1.0f;
                self.navigationController.navigationBar.alpha = 1.0f;
            }
        }
    }
    if (hidden) {
        self.navigationController.navigationBar.alpha = 0.0f;
    } else {
        self.navigationController.navigationBar.alpha = 1.0f;
    }
    [UIView commitAnimations];
}

- (void)hideTabBarByPerform:(BOOL)hidden {
//    NSLog(@"%s, hidden = %@", __func__, hidden?@"YES":@"NO");
    gtabNavFlag = YES;
    [self hideTabBar:hidden];
}

- (void)updateDelegates {
    NSLog(@"%s, _delegate.count = %d", __func__, _delegate.count);
    for (id targetDelegate in _delegate) {
        [targetDelegate stateUpdate];
    }
}

#pragma mark - CBCentralManagerDelegate
- (void)centralManagerDidUpdateState:(CBCentralManager *)central {
    NSLog(@"%s", __func__);
    NSLog(@"state = %d", central.state);
    switch (central.state) {
        case CBCentralManagerStatePoweredOn: {
            NSLog(@"CBCentralManagerStatePoweredOn, _radarState = %d", _radarState);
            if (_radarState == RadarStateAllBLE) {
                NSDictionary *optionsdict = @{CBCentralManagerScanOptionAllowDuplicatesKey : [NSNumber numberWithBool:YES]};
                [_bleManager scanForPeripheralsWithServices:nil options:optionsdict];
            } else {
                [_bleManager scanForPeripheralsWithServices:nil options:nil];
//                NSArray *arrayUUID = [NSArray arrayWithObjects:[CBUUID UUIDWithString:@"853C5886-90EA-7499-847B-96A29C28D2C8"],
//                                      [CBUUID UUIDWithString:@"1802"], nil];
//                [_bleManager scanForPeripheralsWithServices:arrayUUID options:nil];
            }
            break;
        }
        case CBCentralManagerStatePoweredOff:
            NSLog(@"CBCentralManagerStatePoweredOff");
            break;
        case CBCentralManagerStateResetting:
            NSLog(@"CBCentralManagerStateResetting");
            break;
        case CBCentralManagerStateUnauthorized:
            NSLog(@"CBCentralManagerStateUnauthorized");
            break;
        case CBCentralManagerStateUnknown:
            NSLog(@"CBCentralManagerStateUnknown");
            break;
        case CBCentralManagerStateUnsupported:
            NSLog(@"CBCentralManagerStateUnsupported");
            break;
        default:
            break;
    }
}

- (void)centralManager:(CBCentralManager *)central willRestoreState:(NSDictionary *)dict {
    NSLog(@"%s", __func__);
    NSArray *peripherals = dict[CBCentralManagerRestoredStatePeripheralsKey];
    NSLog(@"peripherals = %@", peripherals);
}

- (void)centralManager:(CBCentralManager *)central didRetrievePeripherals:(NSArray *)peripherals {
    NSLog(@"%s", __func__);
}

- (void)centralManager:(CBCentralManager *)central didRetrieveConnectedPeripherals:(NSArray *)peripherals {
    NSLog(@"%s", __func__);
}

- (void)centralManager:(CBCentralManager *)central didDiscoverPeripheral:(CBPeripheral *)peripheral advertisementData:(NSDictionary *)advertisementData RSSI:(NSNumber *)RSSI {
    NSLog(@"%s", __func__);
    NSLog(@"Did discover peripheral. peripheral: %@, rssi: %@, UUID: %@, advertisementData: %@", peripheral, RSSI, peripheral.identifier.UUIDString, advertisementData);
//    NSLog(@"UUID = %@", peripheral.identifier.UUIDString);
    _RSSInumber = RSSI;

    NSLog(@"_radarState = %d", _radarState);
    if (_radarState == RadarStateAllBLE) {
        if ([_unknowBLEs objectForKey:peripheral.identifier.UUIDString] == nil) {
            for (NSString *uuid in _tableController.uuids) {
                if ([uuid isEqualToString:peripheral.identifier.UUIDString]) {
                    NSLog(@"%@ alreay in Phobos array, so just return.", uuid);
                    return;
                }
            }
            NSLog(@"哦，发现了一个新的未知BLE设备！");
            [_unknowBLEs setObject:peripheral.name forKey:peripheral.identifier.UUIDString];
            CGFloat randomX = arc4random() % 1000;
            CGFloat randomY = [self getRandomYFromX:randomX];
            NSLog(@"_tableController.dwarves.count = %d", _tableController.dwarves.count);
            [self.myscrollview addABallLabelWithLabel:@"未知" UUID:peripheral.identifier.UUIDString pointOnMe:CGPointMake(randomX, randomY) unKnowBle:YES];
            
            [self letUsHaveAZoom];
            
            NSDictionary *aDict = @{@"uuid" : peripheral.identifier.UUIDString,
                                    @"rssi" : RSSI};
            [self.myscrollview performSelectorOnMainThread:@selector(testBLErssi:) withObject:aDict waitUntilDone:NO];
        } else {
            NSDictionary *aDict = @{@"uuid" : peripheral.identifier.UUIDString,
                                    @"rssi" : RSSI};
            [self.myscrollview performSelectorOnMainThread:@selector(testBLErssi:) withObject:aDict waitUntilDone:NO];
        }
    } else if (_radarState == RadarStateAllPair) {
        for (NSString *uuid in _tableController.uuids) {
            if ([uuid isEqualToString:peripheral.identifier.UUIDString]) {
                if ([_allConnectPeripheral objectForKey:peripheral.identifier.UUIDString] == nil) {
                    [_allConnectPeripheral setObject:peripheral forKey:peripheral.identifier.UUIDString];
                    NSLog(@"Let us connect %@", peripheral.identifier.UUIDString);
                    [self connectPhobos:peripheral];
                }
                break;
            }
        }
    } else if (_radarState == RadarStateSelected) {
        for (int i = 0; i < _tableController.uuids.count; i++) {
            if ([_tableController.uuids[i] isEqualToString:peripheral.identifier.UUIDString]) {
                if ([_tableController.ifChecked[i] boolValue]) {
                    if ([_allConnectPeripheral objectForKey:peripheral.identifier.UUIDString] == nil) {
                        [_allConnectPeripheral setObject:peripheral forKey:peripheral.identifier.UUIDString];
                        NSLog(@"Let us connect %@", peripheral.identifier.UUIDString);
                        [self connectPhobos:peripheral];
                    }
                    break;
                }
            }
        }
    }
}

- (void)centralManager:(CBCentralManager *)central didConnectPeripheral:(CBPeripheral *)peripheral {
    NSLog(@"%s: peripheral = %@", __func__, peripheral);
    
    peripheral.delegate = self;
    [_myscrollview setABallAlpha:1.0f UUID:peripheral.identifier.UUIDString flash:YES];
    [_allConnectPeripheral setObject:peripheral forKey:peripheral.identifier.UUIDString];
    NSLog(@"_allConnectPeripheral.count = %d", _allConnectPeripheral.count);
    if (_allConnectPeripheral.count == [self getAllCheckCount]) {
        NSLog(@"将要连接之数量等于已连接数量了，所以stopScan");
        [self stopScanPhobos];
    }
    [self updateDelegates];
#if 0
    if (_allConnectPeripheral.count == 1) {
        [self CheckRSSI];
    }
#else
    [_everConnectedUUID setObject:peripheral.identifier.UUIDString forKey:peripheral.identifier];
    NSLog(@"_everConnectedUUID.count = %d", _everConnectedUUID.count);
    [self saveEverConnectedUUID:nil];
    [self CheckRSSI];
#endif
}

- (void)centralManager:(CBCentralManager *)central didFailToConnectPeripheral:(CBPeripheral *)peripheral error:(NSError *)error {
    NSLog(@"%s, error = %@", __func__, error);
    [_allConnectPeripheral removeObjectForKey:peripheral.identifier.UUIDString];
    if (_allConnectPeripheral.count < [self getAllCheckCount]) {
        [self scanPhobos];
    }
    [_myscrollview setABallAlpha:0.3 UUID:peripheral.identifier.UUIDString flash:NO];
}

- (void)centralManager:(CBCentralManager *)central didDisconnectPeripheral:(CBPeripheral *)peripheral error:(NSError *)error {
    NSLog(@"%s, error = %@", __func__, error);
    [_allConnectPeripheral removeObjectForKey:peripheral.identifier.UUIDString];
    NSLog(@"_allConnectPeripheral.count = %d", _allConnectPeripheral.count);
    [self updateDelegates];
    [_myscrollview setABallAlpha:0.3 UUID:peripheral.identifier.UUIDString flash:NO];
}

#pragma mark - CBPeripheralDelegate
- (void)peripheralDidUpdateName:(CBPeripheral *)peripheral {//NS_AVAILABLE(NA, 6_0);
    NSLog(@"%s", __func__);
}

- (void)peripheralDidInvalidateServices:(CBPeripheral *)peripheral {//NS_DEPRECATED(NA, NA, 6_0, 7_0);
    NSLog(@"%s", __func__);
}

- (void)peripheral:(CBPeripheral *)peripheral didModifyServices:(NSArray *)invalidatedServices {//NS_AVAILABLE(NA, 7_0);
    NSLog(@"%s", __func__);
}

- (void)peripheralDidUpdateRSSI:(CBPeripheral *)peripheral error:(NSError *)error {
    NSLog(@"%s, error = %@", __func__, error);
    if (error) {
        return;
    }
    if (peripheral.RSSI == nil) {
        return;
    }
    NSLog(@"RSSI = %@", peripheral.RSSI);
    NSDictionary *aDict = @{@"uuid" : peripheral.identifier.UUIDString,
                            @"rssi" : peripheral.RSSI};
    [self.myscrollview performSelectorOnMainThread:@selector(testBLErssi:) withObject:aDict waitUntilDone:NO];
}

- (void)peripheral:(CBPeripheral *)peripheral didDiscoverServices:(NSError *)error {
    NSLog(@"%s, error = %@", __func__, error);
}

- (void)peripheral:(CBPeripheral *)peripheral didDiscoverIncludedServicesForService:(CBService *)service error:(NSError *)error {
    NSLog(@"%s, error = %@", __func__, error);
}

- (void)peripheral:(CBPeripheral *)peripheral didDiscoverCharacteristicsForService:(CBService *)service error:(NSError *)error {
    NSLog(@"%s, error = %@", __func__, error);
}

- (void)peripheral:(CBPeripheral *)peripheral didUpdateValueForCharacteristic:(CBCharacteristic *)characteristic error:(NSError *)error {
    NSLog(@"%s, error = %@", __func__, error);
}

- (void)peripheral:(CBPeripheral *)peripheral didWriteValueForCharacteristic:(CBCharacteristic *)characteristic error:(NSError *)error {
    NSLog(@"%s, error = %@", __func__, error);
}

- (void)peripheral:(CBPeripheral *)peripheral didUpdateNotificationStateForCharacteristic:(CBCharacteristic *)characteristic error:(NSError *)error {
    NSLog(@"%s, error = %@", __func__, error);
}

- (void)peripheral:(CBPeripheral *)peripheral didDiscoverDescriptorsForCharacteristic:(CBCharacteristic *)characteristic error:(NSError *)error {
    NSLog(@"%s, error = %@", __func__, error);
}

- (void)peripheral:(CBPeripheral *)peripheral didUpdateValueForDescriptor:(CBDescriptor *)descriptor error:(NSError *)error {
    NSLog(@"%s, error = %@", __func__, error);
}

- (void)peripheral:(CBPeripheral *)peripheral didWriteValueForDescriptor:(CBDescriptor *)descriptor error:(NSError *)error {
    NSLog(@"%s, error = %@", __func__, error);
}
@end
