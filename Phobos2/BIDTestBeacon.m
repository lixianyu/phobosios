//
//  BIDTestBeacon.m
//  Phobos2
//
//  Created by Li Xianyu on 13-12-24.
//  Copyright (c) 2013年 Li Xianyu. All rights reserved.
//

#import "BIDTestBeacon.h"

@interface BIDTestBeacon() <CLLocationManagerDelegate>
@property (strong, nonatomic) CLLocationManager *locationManager;
@end

@implementation BIDTestBeacon
- (void)doTest {
    NSLog(@"%s", __func__);
    UIApplication *app = [UIApplication sharedApplication];
    _locationManager = [[CLLocationManager alloc] init];
    _locationManager.delegate = self;
    if ([CLLocationManager isMonitoringAvailableForClass:[app class]]) {
        NSLog(@"isMonitoringAvailableForClass for BIDTestBeacon available.");
    } else {
        NSLog(@"isMonitoringAvailableForClass for BIDTestBeacon not available.");
    }
    
    CLAuthorizationStatus authorizationStatus = [CLLocationManager authorizationStatus];
    NSLog(@"authorizationStatus = %d", authorizationStatus);
    
    UIBackgroundRefreshStatus backgroundRefreshStatus = [app backgroundRefreshStatus];
    NSLog(@"backgroundRefreshStatus = %d", backgroundRefreshStatus);
}
@end
