//
//  BIDSideViewController2.h
//  Phobos2
//
//  Created by Li Xianyu on 13-11-11.
//  Copyright (c) 2013年 Li Xianyu. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef NS_ENUM(NSInteger, BIDDrawerSection){
    BIDDrawerSectionViewSelection,
    
};

@interface BIDSideViewController2 : UIViewController <UITableViewDataSource,UITableViewDelegate, UIAlertViewDelegate>
@property (nonatomic, strong) UITableView * tableView;

@property (strong, nonatomic) NSArray *names;

@property (copy, nonatomic) NSDictionary *selection;
@property (weak, nonatomic) id delegate;

- (IBAction)tappedButton:(UIButton *)sender;
@end
