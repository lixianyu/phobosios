//
//  BIDRadarViewController2.h
//  Phobos2
//
//  Created by Li Xianyu on 13-11-18.
//  Copyright (c) 2013年 Li Xianyu. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MRZoomScrollView.h"

@interface BIDRadarViewController2 : UIViewController <UIScrollViewDelegate>
@property (nonatomic, retain) UIScrollView      *scrollView;
@property (nonatomic, retain) MRZoomScrollView  *zoomScrollView;
@end
