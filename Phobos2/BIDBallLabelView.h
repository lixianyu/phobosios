//
//  BIDBallLabelView.h
//  Phobos2
//
//  Created by Li Xianyu on 13-11-21.
//  Copyright (c) 2013年 Li Xianyu. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <QuartzCore/CAAnimation.h>
#import "BIDBLEManager.h"

@interface BIDBallLabelView : UIControl
@property (strong, nonatomic) UIImage *imageBall;
@property (assign, nonatomic) CGPoint curPoint;//此点是背景雷达图片的绝对坐标
@property (assign, nonatomic) CGFloat scaleNow;
@property (assign, nonatomic) CGFloat offsetX;
@property (assign, nonatomic) CGFloat offsetY;
@property (assign, nonatomic) BOOL canMove;
@property (assign, nonatomic) CGFloat curSin;
@property (assign, nonatomic) CGFloat curCos;
@property (assign, nonatomic) BOOL rssiUpdating;
@property (assign, nonatomic) BOOL angleUpdating;

- (id)initWithLabel:(NSString *)label thePoint:(CGPoint)point unKnowBle:(BOOL)unKnowBle;
- (id)initWithLabel:(NSString *)label thePoint:(CGPoint)point unKnowBle:(BOOL)unKnowBle UUID:(NSString*)uuid;
- (void)rename:(NSString*)newname;
@end
