//
//  BIDRadarViewController6.m
//  Phobos2
//
//  Created by Li Xianyu on 13-12-9.
//  Copyright (c) 2013年 Li Xianyu. All rights reserved.
//

#import "BIDRadarViewController6iPad.h"
#import "BIDScrollView.h"
#import "BIDBallLabelView.h"
#import "BIDRadarTableViewController.h"
#import "BIDAddNewPhobosViewController.h"
#import "BIDSideViewController_iPad.h"
#import "BIDAppDelegate.h"
#import "BIDConstant.h"
#import "FPPopoverController.h"
#import "BIDBLEManager.h"
#import <CoreBluetooth/CoreBluetooth.h>
#import "Animations.h"
#import "BIDSettingViewController.h"
#import "BIDTestBeacon.h"

typedef NS_ENUM(NSInteger, RadarState){
    RadarStateAllBLE = 0, //显示周边所有未配对蓝牙BLE
    RadarStateAllPair,
    RadarStateSelected
};

@interface BIDRadarViewController6iPad () <FPPopoverControllerDelegate,UIScrollViewDelegate, BIDBLEManagerDelegate,CBCentralManagerDelegate,UISplitViewControllerDelegate, UIPopoverControllerDelegate>
@property (nonatomic,retain) BIDScrollView *myscrollview;
@property (nonatomic, strong) UIImageView *imageView;
@property (nonatomic, strong) UIImageView *batteryView;
@property (nonatomic, strong) UILabel *temperatureLable;

@property (strong, nonatomic) FPPopoverController *popover;
@property (strong, nonatomic) UIPopoverController *uiPopover;
@property (strong, nonatomic) BIDRadarTableViewController *tableController;
@property (strong, nonatomic) NSTimer *updateTimer;
@property (strong, nonatomic) NSTimer *rssiTimer;

@property (assign, nonatomic) RadarState radarState;
@property (assign, nonatomic) CGRect myRadarBounds;
@property (assign, nonatomic) BOOL pauseBallCanMove;
@property (strong, nonatomic) CBCentralManager *cbCentralManager;
@property (strong, nonatomic) NSMutableDictionary *unknowBLEs;
@property (strong, nonatomic) BIDBLEManager *phobosManager;

@property (strong, nonatomic) UIButton *buttonClose;
@property (strong, nonatomic) BMKPointAnnotation *pointAnnotation;

- (IBAction)choosePhobos1:(id)sender;
@end

@implementation BIDRadarViewController6iPad {
    CGFloat minScaleHeightPortrait;
    CGFloat minScaleWidthPortrait;
    CGFloat minScaleHeightLandscape;
    CGFloat minScaleWidthLandscape;
    CGFloat screenWidth;
    CGFloat screenHeight;
    CGFloat systemVersion;
    BOOL IAmVisible;
    BOOL onlyShowTemperature;
}

- (id)initWithCoder:(NSCoder *)aDecoder {
    NSLog(@"%s", __func__);
    self = [super initWithCoder:aDecoder];
    if (self) {
        systemVersion = [[[UIDevice currentDevice] systemVersion] floatValue];
        _phobosManager = [BIDBLEManager sharedInstance];
        [_phobosManager addDelegateObserver:self];
        NSLog(@"_phobosManager = %@", _phobosManager);
        NSNotificationCenter *center = [NSNotificationCenter defaultCenter];
        [center addObserver:self selector:@selector(applicationDidEnterBackground) name:UIApplicationDidEnterBackgroundNotification object:nil];
        [center addObserver:self selector:@selector(renameFinished:) name:@"Notification_renameFinished" object:nil];
        [center addObserver:self selector:@selector(justHadAddedOnePhobos:) name:@"Notification_justAddOnePhobos" object:nil];
    }
    return self;
}

- (void)justHadAddedOnePhobos: (NSNotification*)aNotification {
    NSLog(@"%s, _radarState = %d", __func__, _radarState);
    if (_radarState == RadarStateSelected) {
        return;
    }
    NSDictionary *aDictionary = [aNotification object];
    NSString *uuid = aDictionary[@"uuid"];
    NSString *name = aDictionary[@"name"];
    if (_radarState == RadarStateAllBLE) {
        [_myscrollview removeABallFromMe:uuid];
        return;
    }
    
    [_tableController getCoreData];
    [_tableController reLoadData];

    CGFloat randomX = arc4random() % 1000;
    CGFloat randomY = [self getRandomYFromX:randomX];
    [self.myscrollview addABallLabelWithLabel:name UUID:uuid pointOnMe:CGPointMake(randomX, randomY)];
}

- (void)renameFinished: (NSNotification*)aNotification {
    NSLog(@"%s", __func__);
    NSDictionary *aDictionary = [aNotification object];
    NSString *uuid = aDictionary[@"uuid"];
    NSString *name = aDictionary[@"name"];
    [_myscrollview renameABall:uuid newName:name];
    [_tableController getCoreData];
    [_tableController reLoadData];
}

- (void)initBLE {
    NSLog(@"%s", __func__);
    NSLog(@"_phobosManager = %@, _unknowBLEs = %@", _phobosManager, _unknowBLEs);
    [_phobosManager initBLEManager];
    
    if (_unknowBLEs == nil) {
        _unknowBLEs = [[NSMutableDictionary alloc] initWithCapacity:3];
    }
    NSLog(@"_radarState = %d", _radarState);
    if (_radarState == RadarStateAllBLE) {//对于未知BLE设备，只能通过设备发出的广播中的RSSI来确定远近
        [_phobosManager removeDelegateObserver:self];
        NSDictionary *optionsdict = @{CBCentralManagerScanOptionAllowDuplicatesKey : [NSNumber numberWithBool:YES]};
        if (_cbCentralManager == nil) {
            _cbCentralManager = [[CBCentralManager alloc] initWithDelegate:self queue:nil];
        } else {
            [_cbCentralManager scanForPeripheralsWithServices:nil options:optionsdict];
        }
        [self letUsHaveAZoom];
    } else {
        [_phobosManager addDelegateObserver:self];
        if (_radarState == RadarStateAllPair) {
            for (NSString *uuid in _tableController.uuids) {
                [_phobosManager connectPhobos:uuid];
            }
        } else if (_radarState == RadarStateSelected) {
            for (int i = 0; i < _tableController.uuids.count; i++) {
                if ([_tableController.ifChecked[i] boolValue]) {
                    [_phobosManager connectPhobos:_tableController.uuids[i]];
                }
            }
        }
    }
}

- (void)viewDidLoad
{
    NSLog(@"%s", __func__);
    [super viewDidLoad];
    
	// Do any additional setup after loading the view.
//    screenWidth = self.view.bounds.size.width;
//    screenHeight = self.view.bounds.size.height;
    CGRect rx = [UIScreen mainScreen ].bounds;//屏幕尺寸
    screenWidth = rx.size.width;
    screenHeight = rx.size.height;
    NSLog(@"screenWidth = %f, screenHeight = %f", screenWidth, screenHeight);
    _myRadarBounds = [self getRadarBounds:NO];
    _myscrollview = [[BIDScrollView alloc] initWithFrame:_myRadarBounds];
    
    UIImageView *imageViewBackground = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"1"]];
    NSLog(@"myimage.image.size.width = %f, myimage.image.size.height = %f", imageViewBackground.image.size.width, imageViewBackground.image.size.height);
    [imageViewBackground setFrame:CGRectMake(0, 0, imageViewBackground.image.size.width, imageViewBackground.image.size.height)];
    [_myscrollview addSubview:imageViewBackground];
    _imageView = imageViewBackground;
    
    _myscrollview.directionalLockEnabled = NO; //只能一个方向滑动
    _myscrollview.pagingEnabled = NO; //是否翻页
    _myscrollview.backgroundColor = [UIColor darkGrayColor];
    _myscrollview.showsVerticalScrollIndicator = YES; //垂直方向的滚动指示
    _myscrollview.indicatorStyle = UIScrollViewIndicatorStyleWhite;//滚动指示的风格
    _myscrollview.showsHorizontalScrollIndicator = YES;//水平方向的滚动指示
    _myscrollview.delegate = self;

    CGRect statusBarRect = [[UIApplication sharedApplication] statusBarFrame];
    if (systemVersion >= 8.0) {
        if (UIInterfaceOrientationIsLandscape([[UIApplication sharedApplication] statusBarOrientation])) {// Landscape
            minScaleWidthLandscape = (screenWidth - 320) / imageViewBackground.image.size.width;
            minScaleHeightLandscape = (screenHeight - self.navigationController.navigationBar.bounds.size.height) / imageViewBackground.image.size.width;
        }
        else { //如果当前是竖屏
            minScaleWidthLandscape = (screenHeight - 320) / imageViewBackground.image.size.width;
            minScaleHeightLandscape = (screenWidth - self.navigationController.navigationBar.bounds.size.height) / imageViewBackground.image.size.width;
        }
    }
    else {
        minScaleWidthLandscape = (screenHeight - 320) / imageViewBackground.image.size.width;
        minScaleHeightLandscape = (screenWidth - self.navigationController.navigationBar.bounds.size.height - statusBarRect.size.width) / imageViewBackground.image.size.width;
    }
    
    if (UIInterfaceOrientationIsLandscape([[UIApplication sharedApplication] statusBarOrientation])) {// Landscape
        minScaleWidthPortrait = screenHeight / imageViewBackground.image.size.width;
        minScaleHeightPortrait = screenWidth / imageViewBackground.image.size.width;
    }
    else {
        minScaleWidthPortrait = screenWidth / imageViewBackground.image.size.width;
        minScaleHeightPortrait = screenHeight / imageViewBackground.image.size.width;
    }
    NSLog(@"self.view.frame.size.width = %f, myimage.image.size.width = %f", self.view.frame.size.width, imageViewBackground.image.size.width);
    NSLog(@"minScaleWidthLandscape = %f, minScaleHeightLandscape = %f", minScaleWidthLandscape, minScaleHeightLandscape);
    NSLog(@"minScaleWidthPortrait = %f, minScaleHeightPortrait = %f", minScaleWidthPortrait, minScaleHeightPortrait);
    if (UIInterfaceOrientationIsLandscape([[UIApplication sharedApplication] statusBarOrientation])) { // Landscape
        [_myscrollview setMinimumZoomScale:minScaleWidthLandscape];
        [_myscrollview setMaximumZoomScale:1.0];
        [_myscrollview setZoomScale:minScaleWidthLandscape animated:YES];
    } else {
        [_myscrollview setMinimumZoomScale:minScaleWidthPortrait];
        [_myscrollview setMaximumZoomScale:1.0];
        [_myscrollview setZoomScale:minScaleWidthPortrait animated:YES];
    }
    
    [self.view addSubview:_myscrollview];
    [self.myscrollview setContentOffset:CGPointMake(126.5f, 81.0f)];//根据trace观察定下来的坐标
    
//    UITapGestureRecognizer * oneTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(oneTaple:)];
//    [oneTap setNumberOfTapsRequired:1];
//    [self.myscrollview addGestureRecognizer:oneTap];
    [self setMapView];
    
    if (UIInterfaceOrientationIsLandscape([[UIApplication sharedApplication] statusBarOrientation])) { //Landscape.
        _batteryView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"battery8.png"]];
        if (systemVersion >= 8.0) {
            [_batteryView setFrame:CGRectMake(screenWidth-320-39, screenHeight-55, 45, 55)];
        }
        else {
            [_batteryView setFrame:CGRectMake(screenHeight-320-39, screenWidth-55, 45, 55)];
        }
        [self.view addSubview:_batteryView];
        _batteryView.alpha = 0.0f;
        
        _temperatureLable = [[UILabel alloc] initWithFrame:CGRectMake(7, screenWidth-45, 65, 55)];
        _temperatureLable.text = @"温度：";
        _temperatureLable.textColor = [UIColor whiteColor];
        _temperatureLable.alpha = 0.0;
        [self.view addSubview:_temperatureLable];
    } else {//Portrait
        _batteryView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"battery8.png"]];
        [_batteryView setFrame:CGRectMake(screenWidth-39, screenHeight-55, 45, 55)];
        [self.view addSubview:_batteryView];
        _batteryView.alpha = 0.0f;
        
        _temperatureLable = [[UILabel alloc] initWithFrame:CGRectMake(7, screenHeight-45, 65, 55)];
        _temperatureLable.text = @"温度：";
        _temperatureLable.textColor = [UIColor whiteColor];
        _temperatureLable.alpha = 0.0;
        [self.view addSubview:_temperatureLable];
    }
    
//    [self testBeacon];
}

- (void)viewWillAppear:(BOOL)animated
{
    NSLog(@"%s", __func__);
    [super viewWillAppear:animated];
    IAmVisible = YES;

    NSNotificationCenter *center = [NSNotificationCenter defaultCenter];
    [center addObserver:self selector:@selector(applicationDidBecomeActive) name:UIApplicationDidBecomeActiveNotification object:nil];
    [center addObserver:self selector:@selector(applicationWillEnterForeground) name:UIApplicationWillEnterForegroundNotification object:nil];
    [self addBallOnView];
    
    [self performSelector:@selector(drawRadar) withObject:nil afterDelay:0.5];
    self.updateTimer = [NSTimer scheduledTimerWithTimeInterval:4.0
                                                        target:self
                                                      selector:@selector(drawRadar)
                                                      userInfo:nil
                                                       repeats:YES];
}

- (void)viewDidAppear:(BOOL)animated {
    NSLog(@"%s", __func__);
    [super viewDidAppear:animated];
    _pauseBallCanMove = YES;
}

- (void)viewWillDisappear:(BOOL)animated {
    NSLog(@"%s", __func__);
    [super viewWillDisappear:animated];
    [_cbCentralManager stopScan];
}

- (void)viewDidDisappear:(BOOL)animated
{
    NSLog(@"%s", __func__);
    [super viewDidDisappear:animated];
    IAmVisible = NO;
    [self.updateTimer invalidate];
    self.updateTimer = nil;
    [self.rssiTimer invalidate];
    self.rssiTimer = nil;
    for (UIView *aView in self.myscrollview.subviews) {
        if ([aView isKindOfClass:[BIDBallLabelView class]]) {
            NSLog(@"remove one");
            [aView removeFromSuperview];
        }
    }
}

- (void)didReceiveMemoryWarning
{
    NSLog(@"%s", __func__);
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - About Baidu Map
- (void)setMapView {
    NSLog(@"%s: _customMapView = %@", __func__, _customMapView);
    if (_customMapView == nil) {
        if (UIInterfaceOrientationIsLandscape([[UIApplication sharedApplication] statusBarOrientation])) { //Landscape.
            _customMapView = [[BMKMapView alloc] initWithFrame:CGRectMake(0, 64, 703, 703)];
        } else {
            _customMapView = [[BMKMapView alloc] initWithFrame:CGRectMake(0, 64, screenWidth, screenHeight-64)];
        }
    }
    _customMapView.delegate = (BIDAppDelegate*)[UIApplication sharedApplication].delegate;
    _customMapView.scrollEnabled = YES;
    [_customMapView setBackgroundColor:[UIColor brownColor]];
    _customMapView.alpha = 0.0f;
    _customMapView.layer.cornerRadius = 6.0f;
    
    if (_buttonClose == nil) {
        _buttonClose = [UIButton buttonWithType:UIButtonTypeCustom];
        _buttonClose.frame = CGRectMake(10, 10, 60, 50);
        [_buttonClose setTitle:@"X" forState:UIControlStateNormal];
        [_buttonClose setTitleColor:[UIColor redColor] forState:UIControlStateNormal];
        [_buttonClose sizeToFit];
        [_buttonClose addTarget:self
                         action:@selector(tappedButtonClose:)
               forControlEvents:UIControlEventTouchUpInside];
    }
    [_customMapView addSubview:_buttonClose];
    [self.view addSubview:_customMapView];
//    [self.view addSubview:_buttonClose];
    NSLog(@"self.view.subviews.count = %d", self.view.subviews.count);
    NSLog(@"_customMapView.subviews.count = %d", _customMapView.subviews.count);
}

- (void)tappedButtonClose:(UIButton *)sender {
    NSLog(@"%s", __func__);
    _customMapView.delegate = nil;
    [self dismissCustomIBActionPanel];
}

- (void)showTheMap:(BOOL)flag managedObject:(NSManagedObject *)theManagedObjectPhobos {
    NSLog(@"%s", __func__);

    [Animations fadeIn:_customMapView andAnimationDuration:1.0 alpha:0.91 andWait:YES];
    double longitude, latitude;
    if (flag) {
        longitude = [[theManagedObjectPhobos valueForKey:kPhobosLongitudeChuKey] doubleValue];
        latitude = [[theManagedObjectPhobos valueForKey:kPhobosLatitudeChuKey] doubleValue];
    } else {
        longitude = [[theManagedObjectPhobos valueForKey:kPhobosLongitudeRuKey] doubleValue];
        latitude = [[theManagedObjectPhobos valueForKey:kPhobosLatitudeRuKey] doubleValue];
    }
    CLLocationCoordinate2D coordinate = CLLocationCoordinate2DMake(latitude, longitude);
    
    if (_pointAnnotation == nil) {
        _pointAnnotation = [[BMKPointAnnotation alloc] init];
    } else {
        [_customMapView removeAnnotation:_pointAnnotation];
    }
    _pointAnnotation.coordinate = coordinate;
    NSString *alertime;
    if (flag) {
        _pointAnnotation.title = @"上次出围报警";
        alertime = [theManagedObjectPhobos valueForKey:kPhobosChuAlertTime];
    } else {
        _pointAnnotation.title = @"上次入围报警";
        alertime = [theManagedObjectPhobos valueForKey:kPhobosRuAlertTime];
    }
    _pointAnnotation.subtitle = alertime;
    [_customMapView addAnnotation:_pointAnnotation];
    [_customMapView selectAnnotation:_pointAnnotation animated:YES];
    
    BMKCoordinateRegion region = BMKCoordinateRegionMakeWithDistance(coordinate, 250, 250);
    _customMapView.showsUserLocation = NO;
    //    _customMapView.userTrackingMode = BMKUserTrackingModeNone;
    //    _customMapView.showsUserLocation = YES;
    [_customMapView setRegion:region animated:YES];
    NSLog(@"latitude = %f, longitude = %f", latitude, longitude);
    [_customMapView addSubview:_buttonClose];
//    [_customMapView performSelector:@selector(addSubview:) withObject:_buttonClose afterDelay:1];
    NSLog(@"_buttonClose = %@", _buttonClose);
//    NSDictionary *aDict = @{@"flag" : [NSNumber numberWithBool:flag],
//                            @"object" : theManagedObjectPhobos};
//    [self performSelector:@selector(showMapAgain:) withObject:aDict afterDelay:1];
//    [self.view addSubview:_buttonClose];
    
}
#if 1
- (void)showMapAgain:(NSDictionary*)aDict {
    NSLog(@"%s", __func__);
    _buttonClose = [UIButton buttonWithType:UIButtonTypeCustom];
    _buttonClose.frame = CGRectMake(20, 20, 50, 50);
    [_buttonClose setTitle:@"X" forState:UIControlStateNormal];
    [_buttonClose setTitleColor:[UIColor redColor] forState:UIControlStateNormal];
    [_buttonClose sizeToFit];
    [_buttonClose addTarget:self
                     action:@selector(tappedButtonClose:)
           forControlEvents:UIControlEventTouchUpInside];
    [_customMapView addSubview:_buttonClose];
}
#else
- (void)showMapAgain:(NSDictionary*)aDict {
    NSLog(@"%s", __func__);
    [self setMapView];
    double longitude, latitude;
    if ([aDict[@"flag"] boolValue]) {
        longitude = [[aDict[@"object"] valueForKey:kPhobosLongitudeChuKey] doubleValue];
        latitude = [[aDict[@"object"] valueForKey:kPhobosLatitudeChuKey] doubleValue];
    } else {
        longitude = [[aDict[@"object"] valueForKey:kPhobosLongitudeRuKey] doubleValue];
        latitude = [[aDict[@"object"] valueForKey:kPhobosLatitudeRuKey] doubleValue];
    }
    CLLocationCoordinate2D coordinate = CLLocationCoordinate2DMake(latitude, longitude);
    
    if (_pointAnnotation == nil) {
        _pointAnnotation = [[BMKPointAnnotation alloc] init];
    } else {
        [_customMapView removeAnnotation:_pointAnnotation];
    }
    _pointAnnotation.coordinate = coordinate;
    _pointAnnotation.title = @"报警";
    if ([aDict[@"flag"] boolValue]) {
        _pointAnnotation.subtitle = @"上次出围报警位置";
    } else {
        _pointAnnotation.subtitle = @"上次入围报警位置";
    }
    [_customMapView addAnnotation:_pointAnnotation];
    [_customMapView selectAnnotation:_pointAnnotation animated:YES];
    
    BMKCoordinateRegion region = BMKCoordinateRegionMakeWithDistance(coordinate, 250, 250);
    _customMapView.showsUserLocation = NO;
    //    _customMapView.userTrackingMode = BMKUserTrackingModeNone;
    //    _customMapView.showsUserLocation = YES;
    [_customMapView setRegion:region animated:YES];
    NSLog(@"latitude = %f, longitude = %f", latitude, longitude);
}
#endif

- (void)dismissCustomIBActionPanel {
    NSLog(@"%s", __func__);
    [UIView animateWithDuration:0.6f
                     animations:^() {
                         _customMapView.alpha = 0.0f;
                     }];
}

/** 开始后台经纬度定位
 */
//- (void)beginBaiduMapViewUpdate {
//    NSLog(@"%s: _customMapView = %@", __func__, _customMapView);
//    _customMapView.delegate = (BIDAppDelegate*)[UIApplication sharedApplication].delegate;
//    _customMapView.showsUserLocation = NO;//先关闭显示的定位图层
//    _customMapView.userTrackingMode = BMKUserTrackingModeNone;//设置定位的状态
//    _customMapView.showsUserLocation = YES;//显示定位图层
//}
//
//- (void)stopBaiduMapViewUpdate {
//    NSLog(@"%s", __func__);
//    _customMapView.userTrackingMode = BMKUserTrackingModeNone;
//    _customMapView.showsUserLocation = NO;
//    _customMapView.delegate = nil;
//}

#pragma mark -
- (void)testBeacon {
    NSLog(@"%s", __func__);
    UIApplication *app = [UIApplication sharedApplication];
    BIDTestBeacon *testBeacon = [[BIDTestBeacon alloc] init];
    [testBeacon performSelector:@selector(doTest) withObject:nil afterDelay:1];
}

- (CGRect)getRadarBounds80:(BOOL)flag {
    NSLog(@"%s", __func__);
    CGRect radarBounds;
    CGRect statusBarRect = [[UIApplication sharedApplication] statusBarFrame];
    CGRect rx = [UIScreen mainScreen ].bounds;//屏幕尺寸
    NSLog(@"self.navigationController.navigationBar.bounds.size.width = %f, self.navigationController.navigationBar.bounds.size.height = %f", self.navigationController.navigationBar.bounds.size.width, self.navigationController.navigationBar.bounds.size.height);
    NSLog(@"statusBarRect.size.width=%f, statusBarRect.size.height=%f, statusBarRect.origin.x=%f, statusBarRect.origin.y=%f", statusBarRect.size.width,statusBarRect.size.height,statusBarRect.origin.x,statusBarRect.origin.y);
    NSLog(@"screenWidth = %f, screenHeight = %f", screenWidth, screenHeight);
    NSLog(@"rx.size.width = %f, rx.size.height = %f", rx.size.width, rx.size.height);
    if (flag) {
        goto Portrait;
    }
    if (UIInterfaceOrientationIsLandscape([[UIApplication sharedApplication] statusBarOrientation])) {// Landscape bounds.
        NSLog(@"Landscape!");

        /*radarBounds = CGRectMake(0,
                                 statusBarRect.size.height + self.navigationController.navigationBar.bounds.size.height,
                                 screenWidth - 320,
                                 screenHeight - self.navigationController.navigationBar.bounds.size.height - statusBarRect.size.height);
         */
        /*
        radarBounds = CGRectMake(0,
                                 self.navigationController.navigationBar.bounds.size.height,
                                 screenWidth - 320,
                                 screenHeight - self.navigationController.navigationBar.bounds.size.height);
         */
        radarBounds = CGRectMake(0,
                                 1,
                                 rx.size.width - 320,
                                 rx.size.height);
    } else { // Portrait bounds.
    Portrait:
        NSLog(@"Portrait!");
        /*
        radarBounds = CGRectMake(0,
                                 statusBarRect.size.height + self.navigationController.navigationBar.bounds.size.height,
                                 screenWidth,
                                 screenHeight - self.navigationController.navigationBar.bounds.size.height - statusBarRect.size.height);
         */
        radarBounds = CGRectMake(0,
                                 1,
                                 rx.size.width,
                                 rx.size.height);
    }
    CGRect selfViewBounds = self.view.bounds;
    NSLog(@"selfViewBounds.origin.x = %f, selfViewBounds.origin.y = %f, selfViewBounds.size.width = %f, selfViewBounds.size.height = %f", selfViewBounds.origin.x, selfViewBounds.origin.y,selfViewBounds.size.width, selfViewBounds.size.height);
    CGRect appRect = [UIScreen mainScreen].applicationFrame;
    NSLog(@"appRect: x = %f, y = %f, width = %f, height = %f", appRect.origin.x, appRect.origin.y, appRect.size.width, appRect.size.height);
    
    NSLog(@"statusBarRect: x = %f, y = %f, width = %f, height = %f", statusBarRect.origin.x, statusBarRect.origin.y, statusBarRect.size.width, statusBarRect.size.height);
    NSLog(@"radarBounds: x = %f, y = %f, width = %f, height = %f", radarBounds.origin.x, radarBounds.origin.y, radarBounds.size.width, radarBounds.size.height);
    return radarBounds;
}

- (CGRect)getRadarBounds:(BOOL)flag {
    NSLog(@"%s", __func__);
    if (systemVersion >= 8.0) {
        return [self getRadarBounds80:flag];
    }
    CGRect radarBounds;
    CGRect statusBarRect = [[UIApplication sharedApplication] statusBarFrame];
    NSLog(@"self.navigationController.navigationBar.bounds.size.width = %f, self.navigationController.navigationBar.bounds.size.height = %f", self.navigationController.navigationBar.bounds.size.width, self.navigationController.navigationBar.bounds.size.height);
    NSLog(@"statusBarRect.size.width=%f, statusBarRect.size.height=%f, statusBarRect.origin.x=%f, statusBarRect.origin.y=%f", statusBarRect.size.width,statusBarRect.size.height,statusBarRect.origin.x,statusBarRect.origin.y);
    if (flag) {
        goto Portrait;
    }
    if (UIInterfaceOrientationIsLandscape([[UIApplication sharedApplication] statusBarOrientation])) {// Landscape bounds.
        NSLog(@"Landscape!");
        radarBounds = CGRectMake(0,
                                 statusBarRect.size.width + self.navigationController.navigationBar.bounds.size.height,
                                 screenHeight - 320,
                                 screenWidth - self.navigationController.navigationBar.bounds.size.height - statusBarRect.size.width);
    } else { // Portrait bounds.
Portrait:
        NSLog(@"Portrait!");
        radarBounds = CGRectMake(0,
                                 statusBarRect.size.height + self.navigationController.navigationBar.bounds.size.height,
                                 screenWidth,
                                 screenHeight - self.navigationController.navigationBar.bounds.size.height - statusBarRect.size.height);
    }
    CGRect selfViewBounds = self.view.bounds;
    NSLog(@"selfViewBounds.origin.x = %f, selfViewBounds.origin.y = %f, selfViewBounds.size.width = %f, selfViewBounds.size.height = %f", selfViewBounds.origin.x, selfViewBounds.origin.y,selfViewBounds.size.width, selfViewBounds.size.height);
    CGRect appRect = [UIScreen mainScreen].applicationFrame;
    NSLog(@"appRect: x = %f, y = %f, width = %f, height = %f", appRect.origin.x, appRect.origin.y, appRect.size.width, appRect.size.height);
    
    NSLog(@"statusBarRect: x = %f, y = %f, width = %f, height = %f", statusBarRect.origin.x, statusBarRect.origin.y, statusBarRect.size.width, statusBarRect.size.height);
    NSLog(@"radarBounds: x = %f, y = %f, width = %f, height = %f", radarBounds.origin.x, radarBounds.origin.y, radarBounds.size.width, radarBounds.size.height);
    return radarBounds;
}

- (void)renamePhobos:(NSMutableDictionary*)dictionary {
    NSLog(@"%s", __func__);
    BIDBLEManager *bleManager = [BIDBLEManager sharedInstance];
    [bleManager changePhobosName:dictionary];
}

- (void)readBattery:(NSString*)uuidBattery {
    NSLog(@"%s", __func__);
    BIDBLEManager *bleManager = [BIDBLEManager sharedInstance];
    [bleManager readBattery:uuidBattery];
}

- (void)readTemperature:(NSString*)uuid {
    BIDBLEManager *bleManager = [BIDBLEManager sharedInstance];
    [bleManager readTemperature:uuid];
}

- (void)stopCheckRSSI {
    NSLog(@"%s", __func__);
    [self.rssiTimer invalidate];
    self.rssiTimer = nil;
}

- (void)beginCheckRSSI {
    NSLog(@"%s, _tableController.dwarves.count = %d", __func__, _tableController.dwarves.count);
    if (_tableController.dwarves.count == 0) {
        return;
    }
    if (_rssiTimer) {
        [_rssiTimer invalidate];
        _rssiTimer = nil;
    }
    self.rssiTimer = [NSTimer scheduledTimerWithTimeInterval:1.5
                                                      target:self
                                                    selector:@selector(beginRssiUpdate)
                                                    userInfo:nil
                                                     repeats:YES];
}

- (void)beginRssiUpdate {
    NSLog(@"%s, _radarState = %d", __func__, _radarState);

    if (_radarState == RadarStateAllPair) {
        [_phobosManager readRSSI];
    }
    else if (_radarState == RadarStateSelected) {
        for (int i = 0; i < _tableController.ifChecked.count; i++) {
            if ([_tableController.ifChecked[i] boolValue]) {
                [_phobosManager readRSSIwithUUID:_tableController.uuids[i]];
            }
        }
    }
}

//cos(π/2-α) = sinα
- (CGFloat)getRandomYFromX:(CGFloat)x {
    if (x > 950.0) {
        x = 950.0f;
    } else if (x < 50.0) {
        x = 50.0f;
    }
    CGFloat y;
    CGFloat maxY;
    while (YES) {
        y = arc4random() % 1000;
        maxY = sqrtf((gradius * gradius) - (ABS((x-gcenterX) * (x - gcenterX))));
        if (y >= gcenterY - maxY && y <= gcenterY + maxY) {
            break;
        } else {
            NSInteger iY = maxY;
            y = arc4random() % (iY*2);
            y += gcenterY - maxY;
            break;
        }
    }
    return y;
}

- (void)letUsHaveAZoom {
    NSLog(@"%s", __func__);
    static BOOL flag = YES;
    CGFloat tmpScale;
    if (flag) {
        NSLog(@"flag = YES");
        tmpScale = 0.01f;
    } else {
        NSLog(@"flag = NO");
        tmpScale = -0.01f;
    }
    flag = !flag;
    if (_myscrollview.minimumZoomScale == _myscrollview.zoomScale) {
        [self.myscrollview setZoomScale:self.myscrollview.zoomScale+0.01f animated:YES];
    } else {
        [self.myscrollview setZoomScale:self.myscrollview.zoomScale+tmpScale animated:YES];
    }
    NSLog(@"minimumZoomScale = %f, zoomScale = %f", _myscrollview.minimumZoomScale, _myscrollview.zoomScale);
}

-(void) addOneBallForSideViewController {
    NSLog(@"%s", __func__);
    NSLog(@"YES, I come from 'BIDSideViewController_iPad'");
    for (UIView *aView in self.myscrollview.subviews) {
        if ([aView isKindOfClass:[BIDBallLabelView class]]) {
            NSLog(@"remove one");
            [aView removeFromSuperview];
        }
    }
    [_tableController getCoreData];
    _radarState = RadarStateSelected;
    [_tableController setSectionNumber:2];
    if ([BIDSideViewController_iPad onlyShowTemperature]) {
        onlyShowTemperature = YES;
    }
    else {
        onlyShowTemperature = NO;
    }
    
    [BIDSideViewController_iPad resetComeToRadar];
    _tableController.comeFromSideViewController = YES;
    [_tableController reLoadData];
    
    NSDictionary *aDict = [self getBallTextUuid];
    NSLog(@"ballText = %@", aDict[@"name"]);
    CGFloat randomX = arc4random() % 1000;
    CGFloat randomY = [self getRandomYFromX:randomX];
    [self.myscrollview addABallLabelWithLabel:aDict[@"name"] UUID:aDict[@"uuid"] pointOnMe:CGPointMake(randomX, randomY)];
    [self letUsHaveAZoom];
    [self initBLE];
    [self beginCheckRSSI];
}

- (void)addBallOnView {
    NSLog(@"%s", __func__);
    _tableController = [[BIDRadarTableViewController alloc] init];
    [_tableController getCoreData];
    if ([BIDSideViewController_iPad comeFromMe]) {//只要能从BIDSideViewController过来，说明肯定曾经配对过一个Phobos
        NSLog(@"YES, I come from 'BIDSideViewController_iPad'");
        if ([BIDSideViewController_iPad onlyShowTemperature]) {
            onlyShowTemperature = YES;
        }
        else {
            onlyShowTemperature = NO;
        }
        NSLog(@"onlyShowTemperature = %@", onlyShowTemperature ? @"YES" : @"NO");

        _radarState = RadarStateSelected;
        [_tableController setSectionNumber:2];
        [BIDSideViewController_iPad resetComeToRadar];
        _tableController.comeFromSideViewController = YES;
        
        NSDictionary *aDict = [self getBallTextUuid];
        NSLog(@"ballText = %@", aDict[@"name"]);
        CGFloat randomX = arc4random() % 1000;
        CGFloat randomY = [self getRandomYFromX:randomX];
        [self.myscrollview addABallLabelWithLabel:aDict[@"name"] UUID:aDict[@"uuid"] pointOnMe:CGPointMake(randomX, randomY)];
    } else {
        _tableController.comeFromSideViewController = NO;
        
        NSLog(@"_tableController.dwarves.count = %d", _tableController.dwarves.count);
        if (_tableController.dwarves.count == 0) {
            //显示周边所有未配对蓝牙BLE
            _radarState = RadarStateAllBLE;
            [_tableController setSectionNumber:0];
            return;
        }
        
        NSInteger section = [_tableController getSectionNumber];
        NSLog(@"section = %d", section);
        if (section == 0) {
            //显示周边所有未配对蓝牙BLE
            _radarState = RadarStateAllBLE;
            return;
        }
        
        CGFloat randomX = arc4random() % 1000;
        CGFloat randomY = [self getRandomYFromX:randomX];
        for (int i = 0; i < _tableController.dwarves.count; i++) {
            if (section == 2) {
                if (YES == [_tableController.ifChecked[i] boolValue]) {
                    [_myscrollview addABallLabelWithLabel:_tableController.dwarves[i] UUID:_tableController.uuids[i] pointOnMe:CGPointMake(randomX, randomY)];
                }
            } else if (section == 1) {
                [_myscrollview addABallLabelWithLabel:_tableController.dwarves[i] UUID:_tableController.uuids[i] pointOnMe:CGPointMake(randomX, randomY)];
            }
            randomX = arc4random() % 1000;
            randomY = [self getRandomYFromX:randomX];
        }
        if (section == 2) {
            _radarState = RadarStateSelected;
        } else if (section == 1) {
            _radarState = RadarStateAllPair;
        }
    }
    [self letUsHaveAZoom];
}

- (void)reAddBallsOnView {
    NSLog(@"%s", __func__);
    _batteryView.alpha = 0.0;
    _temperatureLable.alpha = 0.0;
    for (UIView *aView in self.myscrollview.subviews) {
        if ([aView isKindOfClass:[BIDBallLabelView class]]) {
            NSLog(@"remove one");
            [aView removeFromSuperview];
        }
    }
    
    NSInteger section = [_tableController getSectionNumber];
    NSLog(@"section = %d", section);
    if (section == 0) {
        _unknowBLEs = nil;
        _radarState = RadarStateAllBLE;
        [self stopCheckRSSI];
        [self initBLE];
        return;
    }
    CGFloat randomX = arc4random() % 1000;
    CGFloat randomY = [self getRandomYFromX:randomX];
    NSLog(@"_tableController.dwarves.count = %d", _tableController.dwarves.count);
    for (int i = 0; i < _tableController.dwarves.count; i++) {
        if (section == 2) {
            if (YES == [_tableController.ifChecked[i] boolValue]) {
                [self.myscrollview addABallLabelWithLabel:_tableController.dwarves[i] UUID:_tableController.uuids[i] pointOnMe:CGPointMake(randomX, randomY)];
            }
        } else if (section == 1) {
            [_myscrollview addABallLabelWithLabel:_tableController.dwarves[i] UUID:_tableController.uuids[i] pointOnMe:CGPointMake(randomX, randomY)];
        }
        randomX = arc4random() % 1000;
        randomY = [self getRandomYFromX:randomX];
    }
    if (section == 2) {
        _radarState = RadarStateSelected;
    } else if (section == 1) {
        _radarState = RadarStateAllPair;
    }
    [self letUsHaveAZoom];
    [self initBLE];
    [self beginCheckRSSI];
}

- (NSDictionary *)getBallTextUuid {
    NSLog(@"%s: count = %d", __func__, _tableController.ifChecked.count);
    for (int i = 0; i < _tableController.ifChecked.count; i++) {
        if ([_tableController.ifChecked[i] boolValue]) {
            NSDictionary *adic = @{@"name" : _tableController.dwarves[i],
                                   @"uuid" : _tableController.uuids[i]};
            return adic;
        }
    }
    return nil;
}

- (NSString *)getNameByUuid:(NSString*)uuid {
    NSLog(@"%s", __func__);
    for (int i = 0; i < _tableController.ifChecked.count; i++) {
        if ([_tableController.uuids[i] isEqualToString:uuid]) {
            NSLog(@"got name : %@", _tableController.dwarves[i]);
            return _tableController.dwarves[i];
        }
    }
    return nil;
}

- (NSInteger)getAllCheckCount {
    NSInteger section = [_tableController getSectionNumber];
    NSInteger count = 0;
    if (section == 1) {
        count =  _tableController.uuids.count;
    } else if (section == 2) {
        if (_tableController.uuids.count != 0) {
            for (NSNumber *aNumber in _tableController.ifChecked) {
                if ([aNumber boolValue]) {
                    count++;
                }
            }
        }
    }
    NSLog(@"%s: count = %d, section = %d", __func__, count, section);
    return count;
}

/** 检查uuid是否正在显示
 */
- (BOOL)isChecked:(NSString*)uuid {
    NSInteger section = [_tableController getSectionNumber];
    
    if (section == 1) {
        return YES;
    } else if (section == 2) {
        for (int i = 0; i < _tableController.ifChecked.count; i++) {
            if ([_tableController.ifChecked[i] boolValue]) {
                if ([_tableController.uuids[i] isEqualToString:uuid]) {
                    return YES;
                }
            }
        }
    }
    return NO;
}

- (void)drawRadar {
    static NSInteger iCheckBallState = 0;
    NSLog(@"%s", __func__);
    if (self.myscrollview) {
        [self.myscrollview drawRadar];
    }
    iCheckBallState++;
    if (iCheckBallState >= 3) {
        [self checkBallState];
        iCheckBallState = 0;
    }
}

- (void)checkBallState {
    NSLog(@"%s: _radarState = %d", __func__, _radarState);
    if (_radarState == RadarStateAllBLE) {
        return;
    }
    if (_radarState == RadarStateAllPair) {
        for (NSString *uuid in _tableController.uuids) {
            if ([_phobosManager isConnected:uuid]) {
                [_myscrollview setABallAlpha:1.0 UUID:uuid flash:NO];
            } else {
                [_myscrollview setABallAlpha:0.3 UUID:uuid flash:NO];
            }
        }
    } else {
        for (int i = 0; i < _tableController.uuids.count; i++) {
            if ([_tableController.ifChecked[i] boolValue]) {
                if ([_phobosManager isConnected:_tableController.uuids[i]]) {
                    [_myscrollview setABallAlpha:1.0 UUID:_tableController.uuids[i] flash:NO];
                } else {
                    [_myscrollview setABallAlpha:0.3 UUID:_tableController.uuids[i] flash:NO];
                }
            }
        }
    }
}

- (void)startBackgroundTask {
    NSLog(@"%@", NSStringFromSelector(_cmd));
    UIApplication *app = [UIApplication sharedApplication];
    __block UIBackgroundTaskIdentifier taskId;
    taskId = [app beginBackgroundTaskWithExpirationHandler:^{
        NSLog(@"Background task#%d ran out of time and was terminated.", taskId);
        NSLog(@"哦，彻底地离开了后台了……taskId = %d", taskId);
        [app endBackgroundTask:taskId];
        
    }];
    if (taskId == UIBackgroundTaskInvalid) {
        NSLog(@"Failed to start background task! taskId=%d", UIBackgroundTaskInvalid);
        return;
    } else {
        NSLog(@"taskId = %d, UIBackgroundTaskInvalid = %d", taskId, UIBackgroundTaskInvalid);
    }
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        NSLog(@"Starting background task#%d with %f seconds remaining", taskId, app.backgroundTimeRemaining);
        [_cbCentralManager stopScan];
        [self stopCheckRSSI];
        [_phobosManager appEnterBackground];
//        for (int i = 1; i < 10; i++) {
//            [NSThread sleepForTimeInterval:1.0];
//            NSLog(@" %d seconds passed... and task#%d with %f seconds remaining", i, taskId, app.backgroundTimeRemaining);
//        }
        NSLog(@"Finishing background task#%d with %f seconds remaining", taskId, app.backgroundTimeRemaining);
        [app endBackgroundTask:taskId];
    });
}

- (void)applicationDidEnterBackground {
    NSLog(@"%s", __func__);
//    [self.updateTimer invalidate];
//    self.updateTimer = nil;
//    [self startBackgroundTask];
    [_cbCentralManager stopScan];
    [self stopCheckRSSI];
    [_phobosManager appEnterBackground];
}

- (void)applicationWillEnterForeground {
    NSLog(@"%s", __func__);
    if (!(_radarState == RadarStateAllBLE)) {
        if (_myscrollview) {
            for (BIDBallLabelView *aBallView in _myscrollview.subviews) {
                if ([aBallView isKindOfClass:[BIDBallLabelView class]]) {
                    aBallView.alpha = 0.3f;
                }
            }
        }
    }
//    self.updateTimer = [NSTimer scheduledTimerWithTimeInterval:4.0
//                                                        target:self
//                                                      selector:@selector(drawRadar)
//                                                      userInfo:nil
//                                                       repeats:YES];
}

- (void)applicationDidBecomeActive {
    NSLog(@"%s", __func__);
    [self initBLE];
//    [self hideTabBar:NO];
//    [self performSelector:@selector(hideTabBarByPerform:) withObject:[NSNumber numberWithBool:YES] afterDelay:2.0f];
}

#pragma mark - UIScrollViewDelegate
//完成拖拽
- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate
{
//    NSLog(@"scrollViewDidEndDragging, decelerate = %d", decelerate);
    if (decelerate) {
        return;
    } else {
        [self updateOffset:scrollView];
    }
}

//减速停止了时执行，手触摸时执行执行
- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView
{
//    NSLog(@"scrollViewDidEndDecelerating");
    [self updateOffset:scrollView];
}

- (void)scrollViewDidZoom:(UIScrollView *)scrollView {
//    NSLog(@"scrollViewDidZoom:  zoomScale = %f", scrollView.zoomScale);
    [self.myscrollview setCurrentScale:scrollView.zoomScale];
    [self.myscrollview performSelectorOnMainThread:@selector(updateZoom)
                                        withObject:nil
                                     waitUntilDone:NO];
}

//设置放大缩小的视图，要是uiscrollview的subview
- (UIView *)viewForZoomingInScrollView:(UIScrollView *)scrollView
{
//    NSLog(@"viewForZoomingInScrollView");
    return _imageView;
}

//完成放大缩小时调用
- (void)scrollViewDidEndZooming:(UIScrollView *)scrollView withView:(UIView *)view atScale:(float)scale
{
//    NSLog(@"view=%@", view);
//    NSLog(@"scale = %f, minScaleHeight = %f", scale, minScaleHeight);
    [scrollView setZoomScale:scale animated:YES];
    [self updateOffset:scrollView];
}

#if 0
//滚动动画停止时执行,代码改变时出发,也就是setContentOffset改变时
- (void)scrollViewDidEndScrollingAnimation:(UIScrollView *)scrollView
{
//    NSLog(@"scrollViewDidEndScrollingAnimation");
}

//将开始降速时
- (void)scrollViewWillBeginDecelerating:(UIScrollView *)scrollView
{
//    NSLog(@"scrollViewWillBeginDecelerating");
}

//只要滚动了就会触发
- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
}

//开始拖拽视图
- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView
{
}

//如果你不是完全滚动到滚轴视图的顶部，你可以轻点状态栏，那个可视的滚轴视图会一直滚动到顶部，那是默认行为，你可以通过该方法返回NO来关闭它
- (BOOL)scrollViewShouldScrollToTop:(UIScrollView *)scrollView
{
    NSLog(@"scrollViewShouldScrollToTop");
    return NO;
}

- (void)scrollViewDidScrollToTop:(UIScrollView *)scrollView
{
    NSLog(@"scrollViewDidScrollToTop");
}
#endif

#pragma mark -
- (void)updateOffset:(UIScrollView *)scrollView {
    for (BIDBallLabelView *aBall in self.myscrollview.subviews) {
        if ([aBall isKindOfClass:[BIDBallLabelView class]]) {
            aBall.offsetX = scrollView.contentOffset.x;
            aBall.offsetY = scrollView.contentOffset.y;
        }
        else {
            
        }
    }
}

#pragma mark -
static BOOL gtabNavFlag = YES;
-(void)oneTaple:(UITapGestureRecognizer*)gesture {
//    NSLog(@"%s: gtabNavFlag = %@", __func__, gtabNavFlag ? @"YES" : @"NO");
    if (gtabNavFlag) {
        [NSObject cancelPreviousPerformRequestsWithTarget:self selector:@selector(hideTabBarByPerform:) object:[NSNumber numberWithBool:YES]];
        [self hideTabBar:NO];
        [self performSelector:@selector(hideTabBarByPerform:) withObject:[NSNumber numberWithBool:YES] afterDelay:5];
    } else {
        [NSObject cancelPreviousPerformRequestsWithTarget:self selector:@selector(hideTabBarByPerform:) object:[NSNumber numberWithBool:YES]];
        [self hideTabBar:YES];
    }
    gtabNavFlag = !gtabNavFlag;
}

- (void)hideTabBar:(BOOL)hidden {
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDuration:1.0];
    for(UIView *view in self.tabBarController.view.subviews)
    {
        if([view isKindOfClass:[UITabBar class]])
        {
            if (hidden) {
                view.alpha = 0.0f;
                
            } else {
                view.alpha = 1.0f;
                self.navigationController.navigationBar.alpha = 1.0f;
            }
        }
    }
    if (hidden) {
        self.navigationController.navigationBar.alpha = 0.0f;
    } else {
        self.navigationController.navigationBar.alpha = 1.0f;
    }
    [UIView commitAnimations];
}

- (void)hideTabBarByPerform:(BOOL)hidden {
    gtabNavFlag = YES;
    [self hideTabBar:hidden];
}



#pragma mark -
- (IBAction)choosePhobos:(id)sender {
    NSLog(@"%s: sender = %@", __func__, sender);
    _pauseBallCanMove = NO;
    [_cbCentralManager stopScan];
    if ([BIDAddNewPhobosViewController ifAddAPhobos] == YES) {
        _tableController = [[BIDRadarTableViewController alloc] init];
        [BIDAddNewPhobosViewController resetIfAddAPhobosFlag];
        [_tableController getCoreData];
    } else if ([BIDSideViewController_iPad ifDeleteAPhobos] == YES) {
        _tableController = [[BIDRadarTableViewController alloc] init];
        [BIDSideViewController_iPad resetIfDeleteAPhobosFlag];
        [_tableController getCoreData];
    }
    if (_tableController == nil) {
        _tableController = [[BIDRadarTableViewController alloc] init];
        [_tableController getCoreData];
    }
    
    _popover = [[FPPopoverController alloc] initWithViewController:_tableController];
    _popover.delegate = self;
    _popover.tint = FPPopoverLightGrayTint;
    
    //    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
    {
        _popover.contentSize = CGSizeMake(200, 400);
    }
    _popover.arrowDirection = FPPopoverArrowDirectionUp;
    
    //    [_popover presentPopoverFromView:sender];
    [_popover presentPopoverFromPoint:CGPointMake(290, 28) ifUsePoint:YES];
}

- (IBAction)choosePhobos1:(id)sender {
    NSLog(@"%s: sender = %@", __func__, sender);
    _pauseBallCanMove = NO;
    [_cbCentralManager stopScan];
    if ([BIDAddNewPhobosViewController ifAddAPhobos] == YES) {
        _tableController = [[BIDRadarTableViewController alloc] init];
        [BIDAddNewPhobosViewController resetIfAddAPhobosFlag];
        [_tableController getCoreData];
    } else if ([BIDSideViewController_iPad ifDeleteAPhobos] == YES) {
        _tableController = [[BIDRadarTableViewController alloc] init];
        [BIDSideViewController_iPad resetIfDeleteAPhobosFlag];
        [_tableController getCoreData];
    }
    if (_tableController == nil) {
        _tableController = [[BIDRadarTableViewController alloc] init];
        [_tableController getCoreData];
    }
    if (_uiPopover == nil) {
        _uiPopover = [[UIPopoverController alloc]
                                    initWithContentViewController:_tableController];
        _uiPopover.popoverContentSize = CGSizeMake(250, 404.5);
        _uiPopover.delegate = self;
    }
    [_uiPopover presentPopoverFromBarButtonItem:sender
                permittedArrowDirections:UIPopoverArrowDirectionAny
                                animated:YES];
//        [poc presentPopoverFromRect:<#(CGRect)#> inView:<#(UIView *)#> permittedArrowDirections:<#(UIPopoverArrowDirection)#> animated:<#(BOOL)#>]
}

#pragma mark - UIPopoverControllerDelegate
- (BOOL)popoverControllerShouldDismissPopover:(UIPopoverController *)popoverController {
    NSLog(@"%s", __func__);
    return YES;
}

- (void)popoverControllerDidDismissPopover:(UIPopoverController *)popoverController {
    NSLog(@"%s", __func__);
    [_tableController savePhobos];
    [self reAddBallsOnView];
    _pauseBallCanMove = YES;
    _uiPopover = nil;
}

//#pragma mark - FPPopoverControllerDelegate
//- (void)popoverControllerDidDismissPopover:(FPPopoverController *)popoverController {
//    NSLog(@"%s", __func__);
//    [_tableController savePhobos];
//    [_popover removeObservers];
//    //    _popover = nil;
//    
//    [self reAddBallsOnView];
//    _pauseBallCanMove = YES;
//}
//
//- (void)presentedNewPopoverController:(FPPopoverController *)newPopoverController
//          shouldDismissVisiblePopover:(FPPopoverController*)visiblePopoverController {
//    NSLog(@"%s", __func__);
//}

#pragma mark - CBCentralManagerDelegate
- (void)centralManagerDidUpdateState:(CBCentralManager *)central {
    NSLog(@"%s: state = %d", __func__, central.state);
    switch (central.state) {
        case CBCentralManagerStatePoweredOn: {
            NSLog(@"CBCentralManagerStatePoweredOn, _radarState = %d", _radarState);
            if (_radarState == RadarStateAllBLE) {//显示周边所有未知蓝牙BLE设备
                NSDictionary *optionsdict = @{CBCentralManagerScanOptionAllowDuplicatesKey : [NSNumber numberWithBool:YES]};
                [_cbCentralManager scanForPeripheralsWithServices:nil options:optionsdict];
            }
            break;
        }
        case CBCentralManagerStatePoweredOff:
            NSLog(@"CBCentralManagerStatePoweredOff");
            break;
        case CBCentralManagerStateResetting:
            NSLog(@"CBCentralManagerStateResetting");
            break;
        case CBCentralManagerStateUnauthorized:
            NSLog(@"CBCentralManagerStateUnauthorized");
            break;
        case CBCentralManagerStateUnknown:
            NSLog(@"CBCentralManagerStateUnknown");
            break;
        case CBCentralManagerStateUnsupported:
            NSLog(@"CBCentralManagerStateUnsupported");
            break;
        default:
            break;
    }
}

- (void)centralManager:(CBCentralManager *)central didDiscoverPeripheral:(CBPeripheral *)peripheral advertisementData:(NSDictionary *)advertisementData RSSI:(NSNumber *)RSSI {
    NSLog(@"%s: _radarState = %d", __func__, _radarState);
    NSLog(@"Did discover peripheral : %@, rssi: %@, UUID: %@, advertisementData: %@", peripheral, RSSI, peripheral.identifier.UUIDString, advertisementData);
    if (_radarState == RadarStateAllBLE) {
        if ([_unknowBLEs objectForKey:peripheral.identifier.UUIDString] == nil) {
//            for (NSString *uuid in _tableController.uuids) {
//                if ([uuid isEqualToString:peripheral.identifier.UUIDString]) {
//                    NSLog(@"%@ alreay in Phobos array, so just return.", uuid);
//                    return;
//                }
//            }
            NSLog(@"哦，发现了一个新的未知BLE设备！");
            if (peripheral.name) {
                NSString *theName = [self getNameByUuid:peripheral.identifier.UUIDString];
                NSLog(@"theName = %@", theName);
                if (theName) {
                    [_unknowBLEs setObject:theName forKey:peripheral.identifier.UUIDString];
                }
                else {
                    [_unknowBLEs setObject:peripheral.name forKey:peripheral.identifier.UUIDString];
                }
            }
            else {
                [_unknowBLEs setObject:@"未知" forKey:peripheral.identifier.UUIDString];
            }
            CGFloat randomX = arc4random() % 1000;
            CGFloat randomY = [self getRandomYFromX:randomX];
            NSLog(@"_tableController.dwarves.count = %d", _tableController.dwarves.count);
            [self.myscrollview addABallLabelWithLabel:_unknowBLEs[peripheral.identifier.UUIDString] UUID:peripheral.identifier.UUIDString pointOnMe:CGPointMake(randomX, randomY) unKnowBle:YES];
            
            [self letUsHaveAZoom];
            
            NSDictionary *aDict = @{@"uuid" : peripheral.identifier.UUIDString,
                                    @"rssi" : RSSI};
            [self.myscrollview performSelectorOnMainThread:@selector(testBLErssi:) withObject:aDict waitUntilDone:NO];
        } else {
            NSDictionary *aDict = @{@"uuid" : peripheral.identifier.UUIDString,
                                    @"rssi" : RSSI};
            [self.myscrollview performSelectorOnMainThread:@selector(testBLErssi:) withObject:aDict waitUntilDone:NO];
        }
    }
}

#pragma mark - BIDBLEManagerDelegate
- (void)didConnectPeripheral:(NSString *)uuid {
    NSLog(@"%s: IAmVisible = %@", __func__, IAmVisible?@"YES":@"NO");
    if (IAmVisible && (![BIDAppDelegate isBackground])) {
#if defined(RU_WEI_USE_IBEACON)
        BIDBLEManager *bleManager = [BIDBLEManager sharedInstance];
        if (![bleManager isConnected:uuid]) {
            [bleManager performSelector:@selector(connectPhobos:) withObject:uuid afterDelay:1.0];
            return;
        }
#endif
        [self beginCheckRSSI];
        [_myscrollview setABallAlpha:1.0f UUID:uuid flash:YES];
        if (1 == [self getAllCheckCount]) {
            if (onlyShowTemperature) {
                [self readTemperature:uuid];
            }
            else {
                [self readBattery:uuid];
            }
            NSString *name = [self getNameByUuid:uuid];
            if (!name) {
                NSDictionary *aDictionary = @{@"uuid" : uuid,
                                          @"name" : name};
                [self performSelector:@selector(renamePhobos:) withObject:aDictionary afterDelay:0.1];
            }
        }
    }
}

- (void)didFailToConnectPeripheral:(NSString *)uuid {
    NSLog(@"%s: IAmVisible = %@", __func__, IAmVisible?@"YES":@"NO");
    if (IAmVisible && (![BIDAppDelegate isBackground])) {
        [_myscrollview setABallAlpha:0.3 UUID:uuid flash:NO];
    }
}

- (void)didDisconnectPeripheral:(NSString *)uuid {
    NSLog(@"%s: IAmVisible = %@", __func__, IAmVisible?@"YES":@"NO");
    if (IAmVisible && (![BIDAppDelegate isBackground])) {
        [_myscrollview setABallAlpha:0.3 UUID:uuid flash:NO];
        NSLog(@"OH, app is not in background!!!");

        if ([self isChecked:uuid]) {
            NSLog(@"oh, let's reconnect : %@", uuid);
            [_phobosManager connectPhobos:uuid];
        }
    }
}

- (void)peripheralDidUpdateRSSI:(NSString *)uuid RSSI:(NSNumber *)rssi {
    NSLog(@"%s", __func__);
    NSLog(@"RSSI = %@", rssi);
    if (IAmVisible && (![BIDAppDelegate isBackground])) {
        NSDictionary *aDict = @{@"uuid" : uuid,
                                @"rssi" : rssi};
        [self.myscrollview performSelectorOnMainThread:@selector(testBLErssi:) withObject:aDict waitUntilDone:NO];
    }
}

- (void)didDeleteOne:(NSString*)uuid {
    NSLog(@"%s: uuid = %@", __func__, uuid);
    [_myscrollview removeABallFromMe:uuid];
    if (IAmVisible) {
        [_tableController getCoreData];
        if (_tableController.uuids.count < 1) {
            [self stopCheckRSSI];
        }
    }
}

- (void)didUpdateBattery:(float)batteryLevel UUID:(NSString *)uuid {
    NSLog(@"%s", __func__);
    NSString *imageName;
    if (batteryLevel > 90 && batteryLevel <= 100) {
        imageName = @"battery8.png";
    } else if (batteryLevel > 80 && batteryLevel <= 90) {
        imageName = @"battery7.png";
    } else if (batteryLevel > 70 && batteryLevel <= 80) {
        imageName = @"battery6.png";
    } else if (batteryLevel > 60 && batteryLevel <= 70) {
        imageName = @"battery5.png";
    } else if (batteryLevel > 50 && batteryLevel <= 60) {
        imageName = @"battery4.png";
    } else if (batteryLevel > 40 && batteryLevel <= 50) {
        imageName = @"battery3.png";
    } else if (batteryLevel > 30 && batteryLevel <= 40) {
        imageName = @"battery2.png";
    } else if (batteryLevel > 20 && batteryLevel <= 30) {
        imageName = @"battery1.png";
    } else if (batteryLevel > 10 && batteryLevel <= 20) {
        imageName = @"battery0.png";
    } else {
        imageName = @"battery0.png";
    }
    [_batteryView setImage:[UIImage imageNamed:imageName]];
    [UIView animateWithDuration:1.9 animations:^{
        _batteryView.alpha = 1.0;
    }];
}

- (void)didUpdateTemperature:(float)temperature {
    NSLog(@"%s: temperature = %f", __func__, temperature);
    if ([BIDSettingViewController getWendu] == 1) {
        _temperatureLable.text = [NSString stringWithFormat:@"%.1f ", temperature];
        _temperatureLable.text = [_temperatureLable.text stringByAppendingString:@"摄氏度"];
    } else {
        _temperatureLable.text = [NSString stringWithFormat:@"%.1f ", temperature];
        _temperatureLable.text = [_temperatureLable.text stringByAppendingString:@"华氏度"];
    }
    [_temperatureLable sizeToFit];
    [UIView animateWithDuration:1.0 animations:^{
        _temperatureLable.alpha = 1.0;
    }];
}

#pragma mark - 旋转
- (void)willAnimateRotationToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration {
    NSLog(@"%s: toInterfaceOrientation = %d, duration = %f", __func__, toInterfaceOrientation, duration);
    if (systemVersion >= 8.0) {
        if (UIInterfaceOrientationIsPortrait(toInterfaceOrientation)) {
            NSLog(@"To Portrait");
            CGRect rx = [UIScreen mainScreen ].bounds;//屏幕尺寸
            screenWidth = rx.size.width;
            screenHeight = rx.size.height;
            _myRadarBounds = [self getRadarBounds:YES];
            [_myscrollview setFrame:_myRadarBounds];
            [_myscrollview setMinimumZoomScale:minScaleWidthPortrait];
            if (_myscrollview.zoomScale < minScaleWidthPortrait) {
                [_myscrollview setZoomScale:minScaleWidthPortrait animated:YES];
            }
            [_batteryView setFrame:CGRectMake(screenWidth-39, screenHeight-55, 45, 55)];
            [_temperatureLable setFrame:CGRectMake(7, screenHeight-45, 100, 55)];
            [_customMapView setFrame:CGRectMake(0, 64, screenWidth, screenHeight-64)];
            [_customMapView addSubview:_buttonClose];
        } else { //To Landscape
            NSLog(@"To Landscape");
            CGRect rx = [UIScreen mainScreen ].bounds;//屏幕尺寸
            screenWidth = rx.size.width;
            screenHeight = rx.size.height;
            _myRadarBounds = [self getRadarBounds:NO];
            [_myscrollview setFrame:_myRadarBounds];
            [_myscrollview setMinimumZoomScale:minScaleWidthLandscape];
            if (_myscrollview.zoomScale <= minScaleWidthPortrait) {
                [_myscrollview setZoomScale:minScaleWidthLandscape animated:YES];
            }
            [_batteryView setFrame:CGRectMake(screenWidth-320-39, screenHeight-55, 45, 55)];
            [_temperatureLable setFrame:CGRectMake(7, screenWidth-45, 100, 55)];
            [_customMapView setFrame:CGRectMake(0, 64, 703, 703)];
            [_customMapView addSubview:_buttonClose];
        }
        return;
    }
    if (UIInterfaceOrientationIsPortrait(toInterfaceOrientation)) {
        NSLog(@"To Portrait");
        _myRadarBounds = [self getRadarBounds:YES];
        [_myscrollview setFrame:_myRadarBounds];
        [_myscrollview setMinimumZoomScale:minScaleWidthPortrait];
        if (_myscrollview.zoomScale < minScaleWidthPortrait) {
            [_myscrollview setZoomScale:minScaleWidthPortrait animated:YES];
        }
        [_batteryView setFrame:CGRectMake(screenWidth-39, screenHeight-55, 45, 55)];
        [_temperatureLable setFrame:CGRectMake(7, screenHeight-45, 100, 55)];
        [_customMapView setFrame:CGRectMake(0, 64, screenWidth, screenHeight-64)];
        [_customMapView addSubview:_buttonClose];
    } else { //To Landscape
        NSLog(@"To Landscape");
        _myRadarBounds = [self getRadarBounds:NO];
        [_myscrollview setFrame:_myRadarBounds];
        [_myscrollview setMinimumZoomScale:minScaleWidthLandscape];
        if (_myscrollview.zoomScale <= minScaleWidthPortrait) {
            [_myscrollview setZoomScale:minScaleWidthLandscape animated:YES];
        }
        [_batteryView setFrame:CGRectMake(screenHeight-320-39, screenWidth-55, 45, 55)];
        [_temperatureLable setFrame:CGRectMake(7, screenWidth-45, 100, 55)];
        [_customMapView setFrame:CGRectMake(0, 64, 703, 703)];
        [_customMapView addSubview:_buttonClose];
    }
//    
//    if (UIInterfaceOrientationIsLandscape([[UIApplication sharedApplication] statusBarOrientation])) { //Landscape.
//        _customMapView = [[BMKMapView alloc] initWithFrame:CGRectMake(0, 64, 703, 703)];
//    } else {
//        _customMapView = [[BMKMapView alloc] initWithFrame:CGRectMake(0, 64, screenWidth, screenHeight-64)];
//    }
}

#pragma mark - Split view
- (void)splitViewController:(UISplitViewController *)splitController willHideViewController:(UIViewController *)viewController withBarButtonItem:(UIBarButtonItem *)barButtonItem forPopoverController:(UIPopoverController *)popoverController
{
    barButtonItem.title = NSLocalizedString(@"众里寻她", @"众里寻她");
    [self.navigationItem setLeftBarButtonItem:barButtonItem animated:NO];
//    self.masterPopoverController = popoverController;
}

- (void)splitViewController:(UISplitViewController *)splitController willShowViewController:(UIViewController *)viewController invalidatingBarButtonItem:(UIBarButtonItem *)barButtonItem
{
    // Called when the view is shown again in the split view, invalidating the button and popover controller.
    [self.navigationItem setLeftBarButtonItem:nil animated:YES];
//    self.masterPopoverController = nil;
}
@end
