//
//  BIDOAD.h
//  Phobos2
//
//  Created by Li Xianyu on 14-1-19.
//  Copyright (c) 2014年 Li Xianyu. All rights reserved.
//

#import <Foundation/Foundation.h>

#define IMAGE_A
#define IMAGE_B

/*
 c4 12 ff ff 07 00 20 7c 42 42 42 42 ff ff ff ff
 */
// The Image Header will not be encrypted, but it will be included in a Signature.
typedef struct {
#if defined FEATURE_OAD_SECURE
    // Secure OAD uses the Signature for image validation instead of calculating a CRC, but the use
    // of CRC==CRC-Shadow for quick boot-up determination of a validated image is still used.
    UInt16 crc0;       // CRC must not be 0x0000 or 0xFFFF.
#endif
    UInt16 crc1;       // CRC-shadow must be 0xFFFF.
    // User-defined Image Version Number - default logic uses simple a '!=' comparison to start an OAD.
    UInt16 ver;
    UInt16 len;        // Image length in 4-byte blocks (i.e. HAL_FLASH_WORD_SIZE blocks).
    Byte  uid[4];     // User-defined Image Identification bytes.
    Byte  res[4];     // Reserved space for future use.
} img_hdr_t;

/**
 
 */
@interface BIDOAD : NSObject

@end
