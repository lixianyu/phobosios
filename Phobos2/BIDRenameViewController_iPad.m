//
//  BIDRenameViewController.m
//  Phobos2
//
//  Created by Li Xianyu on 13-11-18.
//  Copyright (c) 2013年 Li Xianyu. All rights reserved.
//

#import "BIDRenameViewController_iPad.h"
#import "BIDAppDelegate.h"

@interface BIDRenameViewController_iPad ()

@end

@implementation BIDRenameViewController_iPad

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    NSLog(@"%s", __func__);
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (id)initWithCoder:(NSCoder *)aDecoder {
    NSLog(@"%s", __func__);
    self = [super initWithCoder:aDecoder];
    if (self) {
        //
    }
    return self;
}

- (void)viewDidLoad
{
    NSLog(@"%s", __func__);
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    self.title = @"请重新命名";
    self.textField.text = self.name;
//    [self.textField becomeFirstResponder];
//    [self.textField performSelector:@selector(becomeFirstResponder) withObject:nil afterDelay:0.2];
    NSLog(@"uuid = %@", _uuid);
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    [self.textField performSelector:@selector(becomeFirstResponder) withObject:nil afterDelay:0.2];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)buttonOK:(id)sender {
    NSLog(@"%s", __func__);
    [self.textField endEditing:YES];
    NSString *newName = self.textField.text;
    NSLog(@"newName = %@", newName);
    [self.popupRename dismissPopoverAnimated:YES];
    self.popupRename = nil;
    
    BIDAppDelegate *appDelegate = (BIDAppDelegate*)[UIApplication sharedApplication].delegate;
    NSManagedObjectContext *context = [appDelegate managedObjectContext];
    NSError *error;
    NSFetchRequest *request = [[NSFetchRequest alloc] initWithEntityName:kPhobosEntityName];
    NSPredicate *pred = [NSPredicate predicateWithFormat:@"%K = %@", kPhobosDeviceUUIDKey, _uuid];
    [request setPredicate:pred];
    NSArray *objects = [context executeFetchRequest:request error:&error];
    if (objects == nil) {
        NSLog(@"%s : There was an error!", __func__);
    }
    if ([objects count] <= 0) {
        return;
    }
    NSManagedObject *thePhobos = [objects objectAtIndex:0];
    [thePhobos setValue:newName forKey:kPhobosNameKey];
    [appDelegate saveContext];
    
    if (self.delegate) {
        [self.delegate renameFinished:newName];
    }
}
@end
