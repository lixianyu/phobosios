//
//  BIDPlace.h
//  WhereAmI My
//
//  Created by Li Xianyu on 13-10-30.
//  Copyright (c) 2013年 Li Xianyu. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <MapKit/MapKit.h>

@interface BIDPlace : NSObject <MKAnnotation>
@property (copy, nonatomic) NSString *title;
@property (copy, nonatomic) NSString *subtitle;
@property (assign, nonatomic) CLLocationCoordinate2D coordinate;

@end
