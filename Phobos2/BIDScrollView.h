//
//  BIDScrollView.h
//  Phobos2
//
//  Created by Li Xianyu on 13-11-21.
//  Copyright (c) 2013年 Li Xianyu. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BIDScrollView : UIScrollView
@property (assign, nonatomic) CGFloat currentScale;

- (void)updateZoom;
- (void)updateDragging;
- (void)testBLErssi:(NSDictionary *)rssiWithUUID;
- (void)testBLEArc:(NSDictionary*)angleWithUUID;
- (void)drawRadar;
- (void)addABallLabelWithLabel:(NSString*)text pointOnMe:(CGPoint)point;
- (void)addABallLabelWithLabel:(NSString*)text UUID:(NSString*)uuid pointOnMe:(CGPoint)point;
- (void)addABallLabelWithLabel:(NSString*)text UUID:(NSString*)uuid pointOnMe:(CGPoint)point unKnowBle:(BOOL)unKnowBle;
- (void)setABallAlpha:(CGFloat)alpha UUID:(NSString*)uuid flash:(BOOL)flash;
- (void)removeABallFromMe:(NSString*)uuid;
- (void)renameABall:(NSString*)uuid newName:(NSString*)newname;
@end
