//
//  BIDRadarViewController.h
//  Phobos2
//
//  Created by Li Xianyu on 13-11-10.
//  Copyright (c) 2013年 Li Xianyu. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MRZoomScrollView.h"

@interface BIDRadarViewController : UIViewController <UIScrollViewDelegate>
@property (strong, nonatomic) IBOutlet UIScrollView *scrollView;
@property (retain,nonatomic) IBOutlet UIImageView *imageView;
@end
