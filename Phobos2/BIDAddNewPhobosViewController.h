//
//  BIDAddNewPhobosViewController.h
//  Phobos2
//
//  Created by Li Xianyu on 13-11-15.
//  Copyright (c) 2013年 Li Xianyu. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BIDBeginViewController2.h"

@interface BIDAddNewPhobosViewController : UIViewController <UITableViewDataSource, UITableViewDelegate>
@property (weak, nonatomic) BIDBeginViewController2 *beginViewController;
@property (assign, nonatomic) BOOL ifHaveJustAddAPhobos;
@property (strong, nonatomic) NSString *deviceUUID;
@property (strong, nonatomic) IBOutlet UILabel *labelPlease;
@property (strong, nonatomic) IBOutlet UITextField *textField;
@property (strong, nonatomic) IBOutlet UITableView *tableView;
@property (strong, nonatomic) IBOutlet UIView *inputANameView;
- (IBAction)cancelButton:(id)sender;
- (IBAction)okButton:(id)sender;
- (IBAction)backgroundTap:(id)sender;
- (IBAction)textFieldDoneEditing:(id)sender;

+ (BOOL)ifAddAPhobos;
+ (void)resetIfAddAPhobosFlag;
- (void)scanNewPhobos;
- (void)closeMe;
@end
