//
//  BIDRadarViewController6.h
//  Phobos2
//
//  Created by Li Xianyu on 13-12-9.
//  Copyright (c) 2013年 Li Xianyu. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef NS_ENUM(NSInteger, RadarState){
    RadarStateAllBLE = 0, //显示周边所有未配对蓝牙BLE
    RadarStateAllPair,
    RadarStateSelected
};

@interface BIDRadarViewController6 : UIViewController
+ (BOOL)isInRadarView;
- (BOOL)isChecked:(NSString*)uuid;
@end
