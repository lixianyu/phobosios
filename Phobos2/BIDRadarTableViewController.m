//
//  BIDRadarTableViewController.m
//  Phobos2
//
//  Created by Li Xianyu on 13-11-23.
//  Copyright (c) 2013年 Li Xianyu. All rights reserved.
//

#import "BIDRadarTableViewController.h"
#import "MMLogoView.h"
#import "MMSideDrawerSectionHeaderView.h"
#import "BIDAppDelegate.h"

static NSString *CellIdentifier = @"RadarTableIdentifier";

@interface BIDRadarTableViewController ()
@property (strong, nonatomic) UITableView *tableView;
@end

@implementation BIDRadarTableViewController

- (id)initWithCoder:(NSCoder *)aDecoder {
    NSLog(@"%s", __func__);
    self = [super initWithCoder:aDecoder];
    if (self) {
        //
    }
    return self;
}

- (void)viewDidLoad
{
    NSLog(@"%s: _comeFromSideViewController = %@", __func__, _comeFromSideViewController?@"YES":@"NO");
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    if (_comeFromSideViewController) {
        _comeFromSideViewController = NO;
        [self setSectionNumber:2];
        [self getCoreData];
    }
    _tableView = [[UITableView alloc] initWithFrame:self.view.bounds style:UITableViewStylePlain];
    [self.tableView setDelegate:self];
    [self.tableView setDataSource:self];

    [self.tableView setAutoresizingMask:UIViewAutoresizingFlexibleWidth|UIViewAutoresizingFlexibleHeight];
    [self.tableView setSeparatorColor:[UIColor colorWithRed:49.0/255.0
                                                      green:54.0/255.0
                                                       blue:57.0/255.0
                                                      alpha:1.0]];
    [self.tableView setBackgroundColor:[UIColor colorWithRed:77.0/255.0
                                                       green:79.0/255.0
                                                        blue:80.0/255.0
                                                       alpha:1.0]];
    CGSize logoSize = CGSizeMake(58, 62);
    MMLogoView * logo = [[MMLogoView alloc] initWithFrame:CGRectMake(CGRectGetMidX(self.tableView.bounds)-logoSize.width/2.0,
                                                                     -logoSize.height-logoSize.height/4.0,
                                                                     logoSize.width,
                                                                     logoSize.height)];
    [logo setAutoresizingMask:UIViewAutoresizingFlexibleLeftMargin|UIViewAutoresizingFlexibleRightMargin];
    //NSLog(@"tableView = %@, view = %@", self.tableView, self.view);
    [self.tableView addSubview:logo];
    [self.tableView registerClass:[UITableViewCell class] forCellReuseIdentifier:CellIdentifier];
//    [self.tableView setFrame:CGRectMake(0, 0, 320, 568)];
    self.title = @"连接选择";
    [self.view addSubview:self.tableView];

    //[self getCoreData];
    [self setupAccessory];
    
//    NSNotificationCenter *center = [NSNotificationCenter defaultCenter];
//    [center addObserver:self selector:@selector(applicationDidEnterBackground) name:UIApplicationDidEnterBackgroundNotification object:nil];
//    [center addObserver:self selector:@selector(applicationWillResignActive) name:UIApplicationWillResignActiveNotification object:nil];
//    [self setupButton];
}

- (void)setupButton {
    UIButton *buttonClose = [UIButton buttonWithType:UIButtonTypeCustom];
    buttonClose.frame = CGRectMake(0, 300, 180, 30);
//    buttonClose.bounds = CGRectMake(0, 0, 200, 25);
    
    UIImage *buttonUpImage = [UIImage imageNamed:@"button_up.png"];
    UIImage *buttonDownImage = [UIImage imageNamed:@"button_down.png"];
    [buttonClose setBackgroundImage:buttonUpImage
                           forState:UIControlStateNormal];
    [buttonClose setBackgroundImage:buttonDownImage
                           forState:UIControlStateHighlighted];
    [buttonClose setTitle:@"确定" forState:UIControlStateNormal];
//    [buttonClose setTitleColor:[UIColor redColor] forState:UIControlStateNormal];
//    [buttonClose sizeToFit];
    [buttonClose addTarget:self
                    action:@selector(tappedButtonOK:)
          forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:buttonClose];
}

- (void)tappedButtonOK:(UIButton *)sender
{
    NSLog(@"%s", __func__);
}

- (NSInteger)getCoreData {
    NSLog(@"%s", __func__);
    BIDAppDelegate *appDelegate = (BIDAppDelegate*)[UIApplication sharedApplication].delegate;
    NSManagedObjectContext *context = [appDelegate managedObjectContext];
    NSFetchRequest *request = [[NSFetchRequest alloc] initWithEntityName:kPhobosEntityName];
    NSError *error;
    NSArray *objects = [context executeFetchRequest:request error:&error];
    NSLog(@"Phobos count = %d", [objects count]);
    if (objects == nil) {
        NSLog(@"There was an error!");
        return 0;
    }
    if ([objects count] <= 0) {
        [_dwarves removeAllObjects];
        [_uuids removeAllObjects];
        [_ifChecked removeAllObjects];
        return 0;
    }
    _dwarves = [[NSMutableArray alloc] init];
    _uuids = [[NSMutableArray alloc] init];
    _ifChecked = [[NSMutableArray alloc] init];
    for (NSManagedObject *oneObject in objects) {
        NSString *name = [oneObject valueForKey:kPhobosNameKey];
        NSString *deviceUUID = [oneObject valueForKey:kPhobosDeviceUUIDKey];
        NSNumber *checked = [oneObject valueForKey:kPhobosIfCheckedKey];
        [_dwarves addObject:name];
        [_uuids addObject:deviceUUID];
        [_ifChecked addObject:checked];
    }
    return [objects count];
}

- (void)reLoadData {
    NSLog(@"%s", __func__);
    [_tableView reloadData];
}

- (void)viewWillDisappear:(BOOL)animated {
    NSLog(@"%s", __func__);
    [super viewWillDisappear:animated];
}

- (void)viewDidAppear:(BOOL)animated {
    NSLog(@"%s", __func__);
    [super viewDidAppear:animated];
//    [self setupAccessory];
//    [_tableView reloadData];
}

- (void)viewWillAppear:(BOOL)animated {
    NSLog(@"%s", __func__);
    [super viewWillAppear:animated];
}

- (void)setupAccessory {
    NSLog(@"%s", __func__);
    NSNumber *sectionNumber = [[NSUserDefaults standardUserDefaults] objectForKey:@"selectedSection"];
    if (self.dwarves) {
        if (sectionNumber) {
            NSInteger selectedSection = [sectionNumber intValue];
            if (selectedSection == 0 || selectedSection == 1) {
                //Do nothing.
            } else if (selectedSection == 2) {
                BOOL ifHaveCheck = NO;
                for (NSNumber *aNumber in _ifChecked) {
                    BOOL ifChecked = [aNumber boolValue];
                    if (ifChecked) {
                        ifHaveCheck = YES;
                        break;
                    }
                }
                if (ifHaveCheck == NO) {
                    [self setSectionNumber:0];
                }
            }
        } else {
            [self setSectionNumber:0];
        }
    }
}

/** 0:显示所有未知的BLE；1：显示所有Phobos；2：按多项选择所选的显示
 *
 */
- (NSInteger)getSectionNumber {
    NSNumber *sectionNumber = [[NSUserDefaults standardUserDefaults] objectForKey:@"selectedSection"];
    if (sectionNumber == nil) {
        return 0;
    }
    return [sectionNumber intValue];
}

- (void)setSectionNumber:(NSInteger)section {
    NSLog(@"%s: section = %d", __func__, section);
    [[NSUserDefaults standardUserDefaults] setInteger:section forKey:@"selectedSection"];
}

#if 0
#pragma mark -
- (void)applicationWillResignActive {
    NSLog(@"%s", __func__);
    [self savePhobos];
}
#endif

#pragma mark - Table view data source
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    NSLog(@"%s", __func__);
    // Return the number of sections.
    return 3;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    NSLog(@"%s: _dwarves.count = %d", __func__, _dwarves.count);
    // Return the number of rows in the section.
    if (section == 0) {
        return 1;
    } else if (section == 1) {
        return 1;
    } else {
        return _dwarves.count;
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    NSLog(@"%s: indexPath.row = %d, section = %d", __func__, indexPath.row, indexPath.section);

    NSInteger lastSelectedSection = [self getSectionNumber];
    NSLog(@"lastSelectedSection = %d", lastSelectedSection);
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    }
    if (indexPath.section == 0) {
        cell.textLabel.text = @"附近全部";
        if (lastSelectedSection == 0) {
            cell.accessoryType = UITableViewCellAccessoryCheckmark;
        } else {
            cell.accessoryType = UITableViewCellAccessoryNone;
        }
    } else if (indexPath.section == 1) {
        cell.textLabel.text = @"所有Phobos";
        if (lastSelectedSection == 1) {
            cell.accessoryType = UITableViewCellAccessoryCheckmark;
        } else {
            cell.accessoryType = UITableViewCellAccessoryNone;
        }
    } else {
        cell.textLabel.text = self.dwarves[indexPath.row];
        if (lastSelectedSection == 2) {
            if ([_ifChecked[indexPath.row] boolValue] == YES) {
                cell.accessoryType = UITableViewCellAccessoryCheckmark;
            } else {
                cell.accessoryType = UITableViewCellAccessoryNone;
            }
        } else {
            cell.accessoryType = UITableViewCellAccessoryNone;
        }
    }
    [self setCell:cell];
    [cell setSelectionStyle:UITableViewCellSelectionStyleBlue];
    cell.textLabel.font = [UIFont boldSystemFontOfSize:20];
    return cell;
}

- (void)setCell:(UITableViewCell *)cell {
    NSLog(@"%s", __func__);
    UIView * backgroundView = [[UIView alloc] initWithFrame:cell.bounds];
    [backgroundView setAutoresizingMask:UIViewAutoresizingFlexibleHeight|UIViewAutoresizingFlexibleWidth];
    [backgroundView setBackgroundColor:[UIColor colorWithRed:77.0/255.0
                                                       green:79.0/255.0
                                                        blue:80.0/255.0
                                                       alpha:1.0]];
    [cell setBackgroundView:backgroundView];
    
    [cell.textLabel setBackgroundColor:[UIColor clearColor]];
    [cell.textLabel setTextColor:[UIColor
                                  colorWithRed:230.0/255.0
                                  green:236.0/255.0
                                  blue:242.0/255.0
                                  alpha:1.0]];
    [cell.textLabel setShadowColor:[[UIColor blackColor] colorWithAlphaComponent:.5]];
    [cell.textLabel setShadowOffset:CGSizeMake(0, 1)];
    [cell.textLabel setFont:[UIFont boldSystemFontOfSize:16.0]];
}


-(NSString*)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    NSLog(@"%s: section = %d", __func__, section);
    switch (section) {
        case 0:
            return @"显示周边所有设备";
            break;
        case 1:
            return @"尝试连接所有Phobos";
            break;
        case 2:
            return @"多项选择";
            break;
        default:
            return nil;
    }
    return nil;
}

-(UIView*)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    NSLog(@"%s", __func__);
    MMSideDrawerSectionHeaderView * headerView =  [[MMSideDrawerSectionHeaderView alloc] initWithFrame:CGRectMake(0, 0, CGRectGetWidth(tableView.bounds), 20.0f)];
    [headerView setAutoresizingMask:UIViewAutoresizingFlexibleHeight|UIViewAutoresizingFlexibleWidth];
    [headerView setTitle:[tableView.dataSource tableView:tableView titleForHeaderInSection:section]];
    return headerView;
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    NSLog(@"%s", __func__);
    return 23.0;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    NSLog(@"%s", __func__);
    return 40.0;
}

#pragma mark - Table view delegate
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    NSLog(@"%s: section = %d", __func__, indexPath.section);
    [self setSectionNumber:indexPath.section];
    
    UITableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
    if (indexPath.section == 0 || indexPath.section == 1) {
        if (cell.accessoryType == UITableViewCellAccessoryNone) {
            cell.accessoryType = UITableViewCellAccessoryCheckmark;
            NSInteger section;
            if (indexPath.section == 0) {
                section = 1;
            } else {
                section = 0;
            }
            UITableViewCell *aCell = [tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:section]];
            aCell.accessoryType = UITableViewCellAccessoryNone;
            
            for (int i = 0; i < _ifChecked.count; i++) {
                _ifChecked[i] = [NSNumber numberWithBool:NO];
            }
        }
    }  else {
        if ([_ifChecked[indexPath.row] boolValue] == NO) {
            _ifChecked[indexPath.row] = [NSNumber numberWithBool:YES];
            UITableViewCell *aCell0 = [tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0]];
            UITableViewCell *aCell1 = [tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:1]];
            if (aCell0.accessoryType == UITableViewCellAccessoryCheckmark) {
                aCell0.accessoryType = UITableViewCellAccessoryNone;
            }
            if (aCell1.accessoryType == UITableViewCellAccessoryCheckmark) {
                aCell1.accessoryType = UITableViewCellAccessoryNone;
            }
        } else {
            BOOL onlyMe = YES;
            int i;
            for (i = 0; i < _ifChecked.count; i++) {
                if (i == indexPath.row) {
                    continue;
                }
                if ([_ifChecked[i] boolValue]) {
                    onlyMe = NO;
                    break;
                }
            }
            NSLog(@"i = %d", i);
            if (onlyMe == NO) {
                NSLog(@"onlyMe is NO");
                _ifChecked[indexPath.row] = [NSNumber numberWithBool:NO];
            } else {
                NSLog(@"onlyMe is YES");
            }
        }
    }
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    [tableView reloadData];
}

//当界面消失时，一次保存
- (void)savePhobos {
    NSLog(@"%s", __func__);
    BIDAppDelegate *appDelegate = (BIDAppDelegate*)[UIApplication sharedApplication].delegate;
    NSManagedObjectContext *context = [appDelegate managedObjectContext];
    NSFetchRequest *request = [[NSFetchRequest alloc] initWithEntityName:kPhobosEntityName];
    NSError *error;
    NSArray *objects = [context executeFetchRequest:request error:&error];
    NSLog(@"Phobos count = %d", [objects count]);
    if (objects == nil) {
        NSLog(@"There was an error!");
        return;
    }
    if ([objects count] <= 0) {
        return;
    }

    int i = 0;
    NSLog(@"_ifChecked.count = %d", _ifChecked.count);
    for (NSManagedObject *oneObject in objects) {
        [oneObject setValue:_ifChecked[i++] forKey:kPhobosIfCheckedKey];
    }
    NSLog(@"i = %d", i);
    [appDelegate saveContext];
}
@end
