//
//  BIDRadarViewController3.m
//  Phobos2
//
//  Created by Li Xianyu on 13-11-18.
//  Copyright (c) 2013年 Li Xianyu. All rights reserved.
//

#import "BIDRadarViewController3.h"
#import "ImageCropperView.h"

@interface BIDRadarViewController3 ()
@property (strong, nonatomic) IBOutlet ImageCropperView *cropper;

@end

@implementation BIDRadarViewController3

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self.view setBackgroundColor:[UIColor whiteColor]];
	// Do any additional setup after loading the view.
    //_cropper.layer.borderWidth = 1.0;
    //_cropper.layer.borderColor = [UIColor blueColor].CGColor;
    [_cropper setup];
    _cropper.image = [UIImage imageNamed:@"bg_radarRings2"];
    [_cropper setBackgroundColor:[UIColor darkGrayColor]];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
