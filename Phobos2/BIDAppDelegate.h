//
//  BIDAppDelegate.h
//  Phobos2
//
//  Created by Li Xianyu on 13-11-10.
//  Copyright (c) 2013年 Li Xianyu. All rights reserved.
//
#import "BIDRadarViewController6iPad.h"
#import <UIKit/UIKit.h>
#import <MapKit/MapKit.h>
#import "BMKBaseComponent.h"
#import "BMKLocationComponent.h"
#import "BIDTestBeacon.h"


#define isPad (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)

static NSString * const kPhobosEntityName = @"Phobos";

static NSString * const kPhobosDeviceUUIDKey = @"deviceUUID";
static NSString * const kPhobosNameKey = @"name";
static NSString * const kPhobosSerialNumberKey = @"serialNumber";

static NSString * const kPhobosChuAlertKey = @"chuAlert";
static NSString * const kPhobosChuAlertTime = @"chuAlertTime";
static NSString * const kPhobosChuAlertTypeKey = @"chuAlertType";
static NSString * const kPhobosChuAltitudeKey = @"chuHaiba";
static NSString * const kPhobosLatitudeChuKey = @"latitudeChu";
static NSString * const kPhobosLongitudeChuKey = @"longitudeChu";

static NSString * const kPhobosRuAlertKey = @"ruAlert";
static NSString * const kPhobosRuAlertTime = @"ruAlertTime";
static NSString * const kPhobosRuAltitudeKey = @"ruHaiba";
static NSString * const kPhobosLatitudeRuKey = @"latitudeRu";
static NSString * const kPhobosLongitudeRuKey = @"longitudeRu";

static NSString * const kPhobosIfCheckedKey = @"selectedInRadarView";
static NSString * const kPhobosMajorMinorKey = @"majorminor";

@interface BIDAppDelegate : UIResponder <UIApplicationDelegate, BMKGeneralDelegate, BMKLocationServiceDelegate>

@property (strong, nonatomic) UIWindow *window;
#if defined(TEST_iBEACON)
@property (strong, nonatomic) BIDTestBeacon *testBeacon;
#endif
@property (readonly, strong, nonatomic) NSManagedObjectContext *managedObjectContext;
@property (readonly, strong, nonatomic) NSManagedObjectModel *managedObjectModel;
@property (readonly, strong, nonatomic) NSPersistentStoreCoordinator *persistentStoreCoordinator;
//@property (strong, nonatomic) BMKMapView *baiduMapView;
//@property (strong, nonatomic) MKMapView *appleMapView;
//@property (strong, nonatomic) id viewController;
@property (strong, nonatomic) BIDRadarViewController6iPad *radarViewController;
@property (assign, nonatomic) CLLocationDegrees latitude;
@property (assign, nonatomic) CLLocationDegrees longitude;

@property (assign, nonatomic) BOOL baiduMapRunning;
@property (strong, nonatomic) NSMutableDictionary *runningMapUUID;
@property (strong, nonatomic) BMKLocationService* locService;

- (void)saveContext;
- (NSURL *)applicationDocumentsDirectory;
+ (BOOL)isBackground;
- (void)startBackgroundTask;
- (void)startBackgroundTask:(void (^)(double longitude, double latitude))tenSecondsCome;
- (void)beginBaiduMapView:(NSString*)uuid chuOrRu:(BOOL)flag;
- (void)stopBaiduMapView;
@end
