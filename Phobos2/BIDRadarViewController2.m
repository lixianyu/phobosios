//
//  BIDRadarViewController2.m
//  Phobos2
//
//  Created by Li Xianyu on 13-11-18.
//  Copyright (c) 2013年 Li Xianyu. All rights reserved.
//

#import "BIDRadarViewController2.h"

@interface BIDRadarViewController2 ()

@end

@implementation BIDRadarViewController2

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    _scrollView = [[UIScrollView alloc]initWithFrame:CGRectMake(0, 0, 320, 568)];
    _scrollView.delegate = self;
    _scrollView.pagingEnabled = YES;
    _scrollView.userInteractionEnabled = YES;
    _scrollView.showsHorizontalScrollIndicator = YES;
    _scrollView.showsVerticalScrollIndicator = YES;
    [self.view addSubview:_scrollView];
    
    _zoomScrollView = [[MRZoomScrollView alloc]init];
    CGRect frame = self.scrollView.frame;
    _zoomScrollView.frame = frame;
    _zoomScrollView.imageView.image = [UIImage imageNamed:@"bg_radarRings2"];
    [_zoomScrollView.imageView sizeToFit];
    [_zoomScrollView setBackgroundColor:[UIColor grayColor]];
    [self.scrollView addSubview:_zoomScrollView];
    [_scrollView setContentSize:CGSizeMake(_zoomScrollView.imageView.image.size.width, _zoomScrollView.imageView.image.size.height)];
    [_zoomScrollView setContentSize:CGSizeMake(_zoomScrollView.imageView.image.size.width, _zoomScrollView.imageView.image.size.height)];
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
