//
//  BIDRadarViewController.m
//  Phobos2
//
//  Created by Li Xianyu on 13-11-10.
//  Copyright (c) 2013年 Li Xianyu. All rights reserved.
//

#import "BIDRadarViewController.h"

#define MRScreenWidth      CGRectGetWidth([UIScreen mainScreen].applicationFrame)
#define MRScreenHeight     CGRectGetHeight([UIScreen mainScreen].applicationFrame)

@interface BIDRadarViewController ()

@end

@implementation BIDRadarViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    NSLog(@"%s", __func__);
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

-(id)initWithCoder:(NSCoder *)aDecoder {
    NSLog(@"%s", __func__);
    self = [super initWithCoder:aDecoder];
    if (self) {
        //[self initImageView];
    }
    return self;
}

- (void)viewDidLoad
{
    NSLog(@"%s", __func__);
    [super viewDidLoad];
	_scrollView.pagingEnabled = NO;
    _scrollView.userInteractionEnabled = YES;
    _scrollView.showsHorizontalScrollIndicator = YES;
    _scrollView.showsVerticalScrollIndicator = YES;
    
    NSLog(@"%s: _imageView = %@", __func__, _imageView);
    [self initImageView];
    //    [_scrollView setContentSize:CGSizeMake(320 * 3, 460)];
    _imageView.image = [UIImage imageNamed:@"bg_radarRings2"];
//    [self.scrollView addSubview:_zoomScrollView];
}

- (void)didReceiveMemoryWarning
{
    NSLog(@"%s", __func__);
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)initImageView
{
    NSLog(@"%s", __func__);

    // The imageView can be zoomed largest size
 //   imageView.frame = CGRectMake(0, 0, MRScreenWidth * 2.5, MRScreenHeight * 2.5);
    _imageView.userInteractionEnabled = YES;
    //[self addSubview:imageView];
    NSLog(@"%s: _imageView.frame = %@", __func__ ,_imageView);
    // Add gesture,double tap zoom imageView.
    UITapGestureRecognizer *doubleTapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self
                                                                                       action:@selector(handleDoubleTap:)];
    [doubleTapGesture setNumberOfTapsRequired:2];
    [_scrollView addGestureRecognizer:doubleTapGesture];
    
    float minimumScale = _scrollView.frame.size.width / _imageView.frame.size.width;
    [_scrollView setMinimumZoomScale:minimumScale];
    [_scrollView setZoomScale:minimumScale];
}


#pragma mark - Zoom methods

- (void)handleDoubleTap:(UIGestureRecognizer *)gesture
{
    NSLog(@"%s", __func__);
    float newScale = _scrollView.zoomScale * 1.5;
    CGRect zoomRect = [self zoomRectForScale:newScale withCenter:[gesture locationInView:gesture.view]];
    [_scrollView zoomToRect:zoomRect animated:YES];
    [_imageView sizeToFit];
}

- (CGRect)zoomRectForScale:(float)scale withCenter:(CGPoint)center
{
    NSLog(@"%s", __func__);
    CGRect zoomRect;
    zoomRect.size.height = _scrollView.frame.size.height / scale;
    zoomRect.size.width  = _scrollView.frame.size.width  / scale;
    zoomRect.origin.x = center.x - (zoomRect.size.width  / 2.0);
    zoomRect.origin.y = center.y - (zoomRect.size.height / 2.0);
    return zoomRect;
}


#pragma mark - UIScrollViewDelegate

- (UIView *)viewForZoomingInScrollView:(UIScrollView *)scrollView
{
    NSLog(@"%s", __func__);
    return _imageView;
}

- (void)scrollViewDidEndZooming:(UIScrollView *)scrollView withView:(UIView *)view atScale:(float)scale
{
    NSLog(@"%s", __func__);
    [scrollView setZoomScale:scale animated:NO];
}

@end
