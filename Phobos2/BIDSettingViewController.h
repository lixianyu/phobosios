//
//  BIDSettingViewController.h
//  Phobos2
//
//  Created by Li Xianyu on 13-11-10.
//  Copyright (c) 2013年 Li Xianyu. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BIDSettingViewController : UITableViewController
@property (strong, nonatomic) NSArray *names;

+ (NSInteger)getWendu;
@end
