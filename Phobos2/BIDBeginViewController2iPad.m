//
//  BIDBeginViewController2.m
//  Phobos2
//
//  Created by Li Xianyu on 13-11-15.
//  Copyright (c) 2013年 Li Xianyu. All rights reserved.
//

#import "BIDBeginViewController2iPad.h"
#import "BIDRadarViewController6iPad.h"
#import "MMLogoView.h"
#import "Animations.h"
#import "BIDAppDelegate.h"
#import "BIDAddNewPhobosViewController.h"
#import "BIDBLEManager.h"
#import "BMKLocationService.h"
#import "MMProgressHUD.h"

static NSString *CellIdentifier = @"Cell";

@interface BIDBeginViewController2iPad () <UIGestureRecognizerDelegate, BIDBLEManagerDelegate,UIAlertViewDelegate,BMKLocationServiceDelegate>
@property (strong, nonatomic) BIDAddNewPhobosViewController *addController;
@property (strong, nonatomic) BIDBLEManager *phobosManager;
@end

@implementation BIDBeginViewController2iPad {
    BOOL IAmVisible;
    BMKLocationService* _locService;
    CLLocationManager *_clLocationManager;
}

- (void)awakeFromNib
{
    NSLog(@"%s", __func__);
//    self.clearsSelectionOnViewWillAppear = NO;
    self.preferredContentSize = CGSizeMake(320.0, 600.0);
    [super awakeFromNib];
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    NSLog(@"%s", __func__);
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (id)initWithCoder:(NSCoder *)aDecoder {
    NSLog(@"%s", __func__);
    self = [super initWithCoder:aDecoder];
    if (self) {
        NSNotificationCenter *center = [NSNotificationCenter defaultCenter];
        //[center addObserver:self selector:@selector(applicationWillResignActive) name:UIApplicationWillResignActiveNotification object:nil];
        [center addObserver:self selector:@selector(applicationDidBecomeActive) name:UIApplicationDidBecomeActiveNotification object:nil];
        [center addObserver:self selector:@selector(applicationDidEnterBackground) name:UIApplicationDidEnterBackgroundNotification object:nil];
        //[center addObserver:self selector:@selector(applicationWillEnterForeground) name:UIApplicationWillEnterForegroundNotification object:nil];
        [center addObserver:self selector:@selector(getNewPhobos:) name:@"Notification_GetNewPhobos" object:nil];
        [center addObserver:self selector:@selector(reLoadTableView:) name:@"Notification_reloadTableView" object:nil];
        [center addObserver:self selector:@selector(reLoadTableViewOneRow:) name:@"Notification_reloadTableViewOneRow" object:nil];
        _phobosManager = [BIDBLEManager sharedInstance];
        [_phobosManager initBLEManager];
        [_phobosManager addDelegateObserver:self];
    }
    return self;
}

- (void) reLoadTableViewOneRow: (NSNotification*) aNotification {
    NSDictionary *aDictionary = [aNotification object];
    NSString *name = aDictionary[@"name"];
    NSIndexPath *indexpath = aDictionary[@"indexPath"];
    [_dwarves replaceObjectAtIndex:indexpath.row withObject:name];
    [self.tableView reloadRowsAtIndexPaths:@[indexpath] withRowAnimation:UITableViewRowAnimationAutomatic];
    [self.tableView selectRowAtIndexPath:indexpath animated:YES scrollPosition:UITableViewScrollPositionMiddle];
}

- (void) reLoadTableView: (NSNotification*) aNotification {
    NSLog(@"%s", __func__);
    NSInteger count = [self getCoreData];
    if (count == 0) {
        _dwarves = nil;
        _uuids = nil;
    }
    [self.tableView reloadData];
    NSIndexPath * indexpath = [aNotification object];
    NSInteger row = indexpath.row;
    NSLog(@"dwarves count = %d", [_dwarves count]);
    if ([_dwarves count] > 0) {
        if (indexpath.row > [_dwarves count] - 1) {
            row -= 1;
        }
        [self.tableView selectRowAtIndexPath:[NSIndexPath indexPathForRow:row inSection:0] animated:YES scrollPosition:UITableViewScrollPositionMiddle];
    }
    _barButtonZongshu.title = @"总数:";
    _barButtonZongshu.title = [_barButtonZongshu.title stringByAppendingFormat:@"%d", _dwarves.count];
}

- (void) getNewPhobos: (NSNotification*) aNotification {
    NSLog(@"%s", __func__);
    NSArray *array = [aNotification object];
    NSString *name = array[0];
    NSString *uuid = array[1];
    NSLog(@"name = %@, uuid = %@", name, uuid);
    if (_dwarves == nil) {
        _dwarves = [[NSMutableArray alloc] init];
    }
    if (_uuids == nil) {
        _uuids = [[NSMutableArray alloc] init];
    }
    [self.dwarves addObject:name];
    [self.uuids addObject:uuid];
    [self.tableView reloadData];
    NSLog(@"count = %d", [self.dwarves count]);
    NSInteger row = [self.dwarves count];
    if (row > 0) {
        row--;
    }
    [self.tableView selectRowAtIndexPath:[NSIndexPath indexPathForRow:row inSection:0] animated:YES scrollPosition:UITableViewScrollPositionMiddle];
    if ([_dwarves count] == 1) {
    }
    _barButtonZongshu.title = @"总数:";
    _barButtonZongshu.title = [_barButtonZongshu.title stringByAppendingFormat:@"%d", _dwarves.count];
    
    [self performSelector:@selector(dismissTheAddNewView) withObject:nil afterDelay:0.1];
}

- (void)viewDidLoad
{
    NSLog(@"%s", __func__);
    [super viewDidLoad];
    self.navigationItem.leftBarButtonItem = self.editButtonItem;
    
    UIBarButtonItem *addButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemAdd target:self action:@selector(addAPhobos:)];
    self.navigationItem.rightBarButtonItem = addButton;
    self.detailViewController = (BIDRadarViewController6iPad *)[[self.splitViewController.viewControllers lastObject] topViewController];
	CGFloat systemVersion = [[[UIDevice currentDevice] systemVersion] floatValue];
    if (systemVersion < 7.0) {
        [self alertNow];
    }
    CGRect myTableBounds = CGRectMake(0, 20+self.navigationController.navigationBar.bounds.size.height, self.view.bounds.size.width, self.view.bounds.size.height - self.tabBarController.tabBar.bounds.size.height - self.navigationController.navigationBar.bounds.size.height - 20);

    [self.tableView setAutoresizingMask:UIViewAutoresizingFlexibleWidth|UIViewAutoresizingFlexibleHeight];
    [self.tableView setSeparatorColor:[UIColor colorWithRed:49.0/255.0
                                                      green:54.0/255.0
                                                       blue:57.0/255.0
                                                      alpha:1.0]];
    [self.tableView setBackgroundColor:[UIColor colorWithRed:77.0/255.0
                                                       green:79.0/255.0
                                                        blue:80.0/255.0
                                                       alpha:1.0]];
    
    [self.view setBackgroundColor:[UIColor colorWithRed:66.0/255.0
                                                  green:69.0/255.0
                                                   blue:71.0/255.0
                                                  alpha:1.0]];
    
    [self setupAddNewPhobosView];
    CGSize logoSize = CGSizeMake(58, 62);
    MMLogoView * logo = [[MMLogoView alloc] initWithFrame:CGRectMake(CGRectGetMidX(self.tableView.bounds)-logoSize.width/2.0,
                                                                     -logoSize.height-logoSize.height/4.0,
                                                                     logoSize.width,
                                                                     logoSize.height)];
    [logo setAutoresizingMask:UIViewAutoresizingFlexibleLeftMargin|UIViewAutoresizingFlexibleRightMargin];
    [self.tableView addSubview:logo];

    [self getCoreData];

    _barButtonZongshu = [[UIBarButtonItem alloc] initWithTitle:@"" style:UIBarButtonItemStyleBordered target:nil action:nil];
    _barButtonZongshu.title = NSLocalizedString(@"总数:", @"总数:");
    _barButtonZongshu.title = [_barButtonZongshu.title stringByAppendingFormat:@"%d", _dwarves.count];
    _barButtonZongshu.tintColor = [UIColor blackColor];
    [self.navigationItem setLeftBarButtonItem:_barButtonZongshu animated:NO];
}

- (NSInteger)getCoreData {
    NSLog(@"%s", __func__);
    BIDAppDelegate *appDelegate = (BIDAppDelegate*)[UIApplication sharedApplication].delegate;
    NSManagedObjectContext *context = [appDelegate managedObjectContext];
    NSFetchRequest *request = [[NSFetchRequest alloc] initWithEntityName:kPhobosEntityName];
    NSError *error;
    NSArray *objects = [context executeFetchRequest:request error:&error];
    NSLog(@"objects count = %d", [objects count]);
    if (objects == nil) {
        NSLog(@"There was an error!");
        return 0;
    }
    if ([objects count] <= 0) {
        return [objects count];
    }
    _dwarves = [[NSMutableArray alloc] init];
    _uuids = [[NSMutableArray alloc] init];
    for (NSManagedObject *oneObject in objects) {
        NSString *name = [oneObject valueForKey:kPhobosNameKey];
        NSString *deviceUUID = [oneObject valueForKey:kPhobosDeviceUUIDKey];
        [_dwarves addObject:name];
        [_uuids addObject:deviceUUID];
    }
    return [objects count];
}

- (void)applicationDidEnterBackground {
    NSLog(@"%s", __func__);
    NSInteger selectedIndex = [self.tableView indexPathForSelectedRow].row;
    [[NSUserDefaults standardUserDefaults] setInteger:selectedIndex forKey:@"selectedIndex"];
}

- (void)applicationDidBecomeActive {
    NSLog(@"%s", __func__);
    [self.tableView reloadData];
    NSNumber *indexNumber = [[NSUserDefaults standardUserDefaults] objectForKey:@"selectedIndex"];
    NSInteger selectedIndex = [indexNumber intValue];
    [self.tableView selectRowAtIndexPath:[NSIndexPath indexPathForRow:selectedIndex inSection:0] animated:NO scrollPosition:UITableViewScrollPositionNone];
}

- (void)dismissTheAddNewView {
    NSLog(@"%s", __func__);
    [_addController.textField resignFirstResponder];
    [_addController closeMe];
    [UIView animateWithDuration:0.5f
                     animations:^() {
                         self.semiTransparentView.alpha = 0.0f;
                         self.customAddNewView.alpha = 0.0f;
                         self.tableView.alpha = 1.0f;
                     }];
    _addController = nil;
}

- (void)setupAddNewPhobosView {
    UIStoryboard *storyBoard = [UIStoryboard storyboardWithName:@"Main_iPhone" bundle:nil];
    NSLog(@"%s : storyBoard = %@...", __func__, storyBoard);
    _addController = [storyBoard instantiateViewControllerWithIdentifier:@"addnewPhobos"];
    _addController.beginViewController = self;
    self.customAddNewView = _addController.view;
    NSLog(@"customAddNewView = %@", self.customAddNewView);
    self.customAddNewView.alpha = 0.0f;
    self.customAddNewView.layer.cornerRadius = 6.0f;
    self.semiTransparentView = [[UIView alloc] initWithFrame:[UIScreen mainScreen].bounds];
    NSLog(@"semiTransparentView = %@", self.semiTransparentView);
    UITapGestureRecognizer *cancelTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(dismissTheAddNewView)];
    [self.semiTransparentView addGestureRecognizer:cancelTap];
    self.semiTransparentView.backgroundColor = [UIColor blackColor];
    self.semiTransparentView.alpha = 0.0f;
    
    float halfOfWidth = 320.0 / 2.0;
    float height = 768;

    self.semiTransparentView.frame = CGRectMake(0, 0, halfOfWidth * 2.0, height);
    self.semiTransparentView.center = CGPointMake(halfOfWidth, height / 2.0);
    
    self.customAddNewView.frame = CGRectMake(0, 0, 320 - 40, 320);
    self.customAddNewView.center = CGPointMake(halfOfWidth, height / 2.0 - 50);
    NSLog(@"customAddNewView = %@", self.customAddNewView);
    
    UIButton *buttonClose = [UIButton buttonWithType:UIButtonTypeCustom];
    buttonClose.frame = CGRectMake(6, 6, 40, 40);
    [buttonClose setTitle:@"x" forState:UIControlStateNormal];
    [buttonClose setTitleColor:[UIColor redColor] forState:UIControlStateNormal];
    [buttonClose sizeToFit];
    [buttonClose addTarget:self
                    action:@selector(dismissTheAddNewView)
          forControlEvents:UIControlEventTouchUpInside];
    [self.customAddNewView addSubview:buttonClose];
    [self.view addSubview:self.customAddNewView];
    [self.view insertSubview:self.semiTransparentView belowSubview:self.customAddNewView];
}

- (void)tappedButtonOK:(UIButton *)sender {
    NSLog(@"%s", __func__);
}

- (void)tappedButtonCancel:(UIButton *)sender {
    NSLog(@"%s", __func__);
}

- (void)viewWillAppear:(BOOL)animated {
    NSLog(@"%s", __func__);
    [super viewWillAppear:animated];
    [self performSelector:@selector(startLocation) withObject:nil afterDelay:1.1];
}

- (void)viewWillDisappear:(BOOL)animated {
    NSLog(@"%s", __func__);
    [super viewWillDisappear:animated];
    NSInteger selectedIndex = [self.tableView indexPathForSelectedRow].row;
    [[NSUserDefaults standardUserDefaults] setInteger:selectedIndex forKey:@"selectedIndex"];
}

- (void)viewDidAppear:(BOOL)animated {
    NSLog(@"%s", __func__);
    if ([UIApplication sharedApplication].applicationState == UIApplicationStateBackground) {
        NSLog(@"UIApplicationStateBackground, so just return!!!");
        return;
    }
    [super viewDidAppear:animated];
    IAmVisible = YES;
    NSNumber *indexNumber = [[NSUserDefaults standardUserDefaults] objectForKey:@"selectedIndex"];
    if (self.dwarves) {
        if (indexNumber) {
            NSInteger selectedIndex = [indexNumber intValue];
            if (selectedIndex >= _dwarves.count) {
                selectedIndex = _dwarves.count - 1;
            }
            [self.tableView selectRowAtIndexPath:[NSIndexPath indexPathForRow:selectedIndex inSection:0] animated:YES scrollPosition:UITableViewScrollPositionMiddle];
            [[NSUserDefaults standardUserDefaults] setInteger:selectedIndex forKey:@"selectedIndex"];
        } else {
            [self.tableView selectRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0] animated:YES scrollPosition:UITableViewScrollPositionMiddle];
        }
    }
    _barButtonZongshu.title = @"总数:";
    _barButtonZongshu.title = [_barButtonZongshu.title stringByAppendingFormat:@"%d", _dwarves.count];
    [self stateUpdate];
}

- (void)viewDidDisappear:(BOOL)animated {
    NSLog(@"%s", __func__);
    [super viewDidDisappear:animated];
    IAmVisible = NO;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    NSLog(@"%s", __func__);
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    NSLog(@"%s, count = %d", __func__, [self.dwarves count]);
    NSLog(@"dwarves = %@", self.dwarves);
    // Return the number of rows in the section.
    return [self.dwarves count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSLog(@"%s", __func__);
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
    NSLog(@"cell = %@", cell);

    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    }
    cell.textLabel.text = self.dwarves[indexPath.row];
    if ([_phobosManager isConnected:_uuids[indexPath.row]]) {
        UIImage *image = [UIImage imageNamed:@"radar_BlueInsideCircle"];
        cell.imageView.image = image;
    } else {
        UIImage *image = [UIImage imageNamed:@"radar_GreyInsideCircle"];
        cell.imageView.image = image;
    }
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    NSLog(@"%s: indexPath = %@", __func__, indexPath);

}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    NSLog(@"%s", __func__);
    UIViewController * destinationController = segue.destinationViewController;
    NSLog(@"destinationController = %@", destinationController);
    if ([destinationController respondsToSelector:@selector(setDelegate:)]) {
        [destinationController setValue:self forKey:@"delegate"];
    }
    if ([destinationController respondsToSelector:@selector(setDetailViewController:)]) {
        [destinationController setValue:_detailViewController forKey:@"detailViewController"];
    }
    if ([destinationController respondsToSelector:@selector(setSelection:)]) {
        NSIndexPath *indexPath = [self.tableView indexPathForCell:sender];
        NSDictionary *selection = @{@"indexPath" : indexPath,
                                    @"name" : _dwarves[indexPath.row],
                                    @"deviceUUID" : _uuids[indexPath.row],
                                    };
        [destinationController setValue:selection forKey:@"selection"];
    }
}

- (IBAction)addAPhobos:(id)sender {
    NSLog(@"%s", __func__);
    if (_addController == nil) {
        [self setupAddNewPhobosView];
    }
    [UIView animateWithDuration:0.5f
                     animations:^() {
                         self.semiTransparentView.alpha = 0.4f;
                         self.customAddNewView.alpha = 1.0f;
                         
                         _addController.labelPlease.alpha = 1.0f;
                         _addController.tableView.alpha = 1.0f;
                         _addController.inputANameView.alpha = 0.0f;
                     }];
    [_addController scanNewPhobos];
}

#pragma mark - UIAlertViewDelegate
- (void) alertNow {
    UIAlertView * alert = [[UIAlertView alloc] initWithTitle:@"无法使用" message:@"iOS系统版本太低，请升级iOS系统到7.0" delegate:self cancelButtonTitle:nil otherButtonTitles:@"知道了", nil];
    alert.alertViewStyle = UIAlertViewStyleDefault;
    [alert show];
}

-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    NSLog(@"%s: buttonIndex = %d", __func__, buttonIndex);
    if (buttonIndex == 0) {
        //cancel - do nothing.
        [self performSelector:@selector(alertNow) withObject:nil afterDelay:0.2];
    } else if (buttonIndex == 1) {
        //ok
//        exit(0);
    }
}

#pragma mark - BIDBLEManagerDelegate
- (void)stateUpdate {
    NSLog(@"%s", __func__);
    if (IAmVisible && (![BIDAppDelegate isBackground])) {
        NSIndexPath *indexpath = [self.tableView indexPathForSelectedRow];
        [self.tableView reloadData];
        [self.tableView selectRowAtIndexPath:indexpath animated:YES scrollPosition:UITableViewScrollPositionNone];
    }
}

#pragma mark - BMKLocationServiceDelegate
/**
 *在将要启动定位时，会调用此函数
 */
- (void)willStartLocatingUser {
    NSLog(@"%s", __func__);
}

/**
 *在停止定位后，会调用此函数
 */
- (void)didStopLocatingUser {
    NSLog(@"%s", __func__);
}

/**
 *用户位置更新后，会调用此函数
 *@param userLocation 新的用户位置
 */
- (void)didUpdateUserLocation:(BMKUserLocation *)userLocation {
    NSLog(@"%s, latitude = %f, longitude = %f", __func__, userLocation.location.coordinate.latitude, userLocation.location.coordinate.longitude);
    [self performSelector:@selector(stopLocation) withObject:nil afterDelay:1.0];
}

/**
 *定位失败后，会调用此函数
 *@param error 错误号
 */
- (void)didFailToLocateUserWithError:(NSError *)error {
    NSLog(@"%s, error = %@", __func__, error);
    [self shouldOpenLocation];
    [_locService stopUserLocationService];
    _locService = nil;
}

- (void)startLocation {
    NSLog(@"%s", __func__);
    CGFloat systemVersion = [[[UIDevice currentDevice] systemVersion] floatValue];
    
    if (systemVersion < 8) {
        _locService = [[BMKLocationService alloc] init];
        _locService.delegate = self;
        [_locService startUserLocationService];
    }
    else {
        NSLog(@"authorizationStatus = %d", [CLLocationManager authorizationStatus]);
        if ([CLLocationManager authorizationStatus] != kCLAuthorizationStatusAuthorizedAlways) {
            _clLocationManager = [[CLLocationManager alloc] init];
//            _clLocationManager.delegate = self;
//            [_clLocationManager startUpdatingLocation];
//            [_clLocationManager requestWhenInUseAuthorization];
            [_clLocationManager requestAlwaysAuthorization];
            [self performSelector:@selector(stopAppleLocation) withObject:nil afterDelay:40.0];
        }
    }
}

- (void)stopAppleLocation {
    NSLog(@"%s", __func__);
    _clLocationManager = nil;
}

//停止定位
- (void)stopLocation {
    NSLog(@"%s, _locService = %@", __func__, _locService);
    if (_locService) {
        [_locService stopUserLocationService];
        _locService.delegate = nil;
        _locService = nil;
    }
}

- (void)shouldOpenLocation {
    NSString *titleString, *statusString;
    
    titleString = @"定位服务";
    statusString = @"请进入设置、隐私，打开定位服务";
    [MMProgressHUD setPresentationStyle:MMProgressHUDPresentationStyleExpand];
    
    UIImage *_staticImage = [UIImage imageNamed:@"smiley.png"];
    [MMProgressHUD showWithTitle:titleString
                          status:statusString
                           image:_staticImage];
    double delayInSeconds = 2.5;
    dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, delayInSeconds * NSEC_PER_SEC);
    dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
        [MMProgressHUD dismissWithSuccess:@"祝您今天好心情！"];
    });
}
@end