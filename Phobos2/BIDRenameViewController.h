//
//  BIDRenameViewController.h
//  Phobos2
//
//  Created by Li Xianyu on 13-11-18.
//  Copyright (c) 2013年 Li Xianyu. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "FPPopoverController.h"

@protocol BIDRenameViewControllerDelegate <NSObject>
@optional
- (void)renameFinished:(NSString*)name;
@end

@interface BIDRenameViewController : UIViewController
@property(nonatomic, assign) id<BIDRenameViewControllerDelegate> delegate;

@property (copy, nonatomic) NSString *name;
@property (copy, nonatomic) NSString *uuid;
@property (retain, nonatomic) FPPopoverController *popupRename;
@property (strong, nonatomic) IBOutlet UITextField *textField;

- (IBAction)buttonOK:(id)sender;
@end
