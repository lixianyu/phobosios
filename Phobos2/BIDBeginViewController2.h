//
//  BIDBeginViewController2.h
//  Phobos2
//
//  Created by Li Xianyu on 13-11-15.
//  Copyright (c) 2013年 Li Xianyu. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BIDBeginViewController2 : UIViewController <UITableViewDataSource,UITableViewDelegate>
@property (nonatomic, strong) UITableView * tableView;

@property UIView *semiTransparentView;
@property (strong, nonatomic) UIView *customAddNewView;
@property (strong, nonatomic) UIView *customAddNewViewReal;
@property (strong, nonatomic) NSMutableArray *dwarves;
@property (strong, nonatomic) NSMutableArray *uuids;
@property (strong, nonatomic) IBOutlet UIBarButtonItem *barButtonZongshu;
- (IBAction)addAPhobos:(id)sender;
@end
