//
//  BIDTabBarViewController.m
//  Phobos2
//
//  Created by Li Xianyu on 13-11-10.
//  Copyright (c) 2013年 Li Xianyu. All rights reserved.
//

#import "BIDTabBarViewController.h"
#import "MMExampleLeftSideDrawerViewController.h"
#import "MMExampleCenterTableViewController.h"
#import "MMExampleRightSideDrawerViewController.h"
#import "MMExampleDrawerVisualStateManager.h"
#import "BIDBeginViewController.h"
#import "BIDSideViewController2.h"

@interface BIDTabBarViewController ()

@end

@implementation BIDTabBarViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    NSLog(@"%s", __func__);
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        
    }
    return self;
}

- (id)initWithCoder:(NSCoder *)aDecoder {
    NSLog(@"%s", __func__);
    //NSLog(@"%s:self = %@", __func__, self);
    //NSLog(@"controllers[0] = %@", controllers[0]);
    //[self changeTheFirstController];
    self = [super initWithCoder:aDecoder];
    //NSLog(@"%s:self = %@", __func__, self);
    NSArray *controllers = [self viewControllers];
    NSLog(@"controllers[0] = %@", controllers[0]);
    [self changeTheFirstController1];
    controllers = [self viewControllers];
    NSLog(@"controllers[0] = %@", controllers[0]);
    if (self) {
        
        NSUInteger counts = [controllers count];
        NSLog(@"controller count is : %u", counts);
    }
    UIStoryboard *sb = [self storyboard];
    if (sb) {
        NSLog(@"%s:Story board is not nil", __func__);
    } else {
        NSLog(@"%s:Story board is nil", __func__);
        //        return self;
    }
    return self;
}

- (void)viewDidLoad
{
    NSLog(@"%s", __func__);
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    UIStoryboard *sb = [self storyboard];
    if (sb) {
        NSLog(@"%s:Story board is not nil", __func__);
    } else {
        NSLog(@"%s:Story board is nil", __func__);
        //        return self;
    }
}

- (void)didReceiveMemoryWarning
{
    NSLog(@"%s", __func__);
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Init
//Change the first controller
//- (void)changeTheFirstController {
//    NSLog(@"%s", __func__);
//    UIViewController * leftSideDrawerViewController = [[MMExampleLeftSideDrawerViewController alloc] init];
//    
//    UIViewController * centerViewController = [[MMExampleCenterTableViewController alloc] initWithStyle:UITableViewStyleGrouped];
//    
//    //UIViewController * rightSideDrawerViewController = [[MMExampleRightSideDrawerViewController alloc] init];
//    
//    UINavigationController * navigationController = [[UINavigationController alloc] initWithRootViewController:centerViewController];
//    UIStoryboard *sb = [self storyboard];
//    if (sb) {
//        NSLog(@"%s:Story board is not nil", __func__);
//    } else {
//        NSLog(@"%s:Story board is nil", __func__);
//        //        return self;
//    }
//    MMDrawerController * drawerController = [[MMDrawerController alloc]
//                                             initWithCenterViewController:navigationController
//                                             leftDrawerViewController:leftSideDrawerViewController
//                                             rightDrawerViewController:nil];
//    [drawerController setMaximumLeftDrawerWidth:250.0];
//    [drawerController setOpenDrawerGestureModeMask:MMOpenDrawerGestureModeAll];
//    [drawerController setCloseDrawerGestureModeMask:MMCloseDrawerGestureModeAll];
//    drawerController.tabBarItem.title = @"开始";
//    [drawerController
//     setDrawerVisualStateBlock:^(MMDrawerController *drawerController, MMDrawerSide drawerSide, CGFloat percentVisible) {
//         MMDrawerControllerDrawerVisualStateBlock block;
//         block = [[MMExampleDrawerVisualStateManager sharedManager]
//                  drawerVisualStateBlockForDrawerSide:drawerSide];
//         if(block){
//             block(drawerController, drawerSide, percentVisible);
//         }
//     }];
//    NSMutableArray *controllers = (NSMutableArray*)[self viewControllers];
//    controllers[0] = drawerController;
//    [self setViewControllers:controllers];
//}

- (void)changeTheFirstController1 {
    NSLog(@"%s", __func__);
    UIStoryboard *storyBoard = [UIStoryboard storyboardWithName:@"Main_iPhone" bundle:nil];
    NSLog(@"%s : storyBoard = %@...", __func__, storyBoard);

    UIViewController *leftSideDrawerViewController = [storyBoard instantiateViewControllerWithIdentifier:@"lxy2"];
    //UIViewController *leftSideDrawerViewController = [[BIDSideViewController2 alloc] init];
    UINavigationController *navigationController = [storyBoard instantiateViewControllerWithIdentifier:@"lxy1"];

    MMDrawerController *drawerController = [[MMDrawerController alloc]
                                             initWithCenterViewController:navigationController
                                             leftDrawerViewController:leftSideDrawerViewController
                                             rightDrawerViewController:nil];
    [drawerController setMaximumLeftDrawerWidth:280.0];
    [drawerController setOpenDrawerGestureModeMask:MMOpenDrawerGestureModeAll];
    [drawerController setCloseDrawerGestureModeMask:MMCloseDrawerGestureModeAll];
    drawerController.tabBarItem.title = @"开始";
    drawerController.tabBarItem.image = [UIImage imageNamed:@"house"];
    [drawerController
     setDrawerVisualStateBlock:^(MMDrawerController *drawerController, MMDrawerSide drawerSide, CGFloat percentVisible) {
         MMDrawerControllerDrawerVisualStateBlock block;
         block = [[MMExampleDrawerVisualStateManager sharedManager]
                  drawerVisualStateBlockForDrawerSide:drawerSide];
         if(block){
             block(drawerController, drawerSide, percentVisible);
         }
     }];
    NSMutableArray *controllers = (NSMutableArray*)[self viewControllers];
    controllers[0] = drawerController;
    [self setViewControllers:controllers];
    
    //[MMDrawerController ]
}
@end
