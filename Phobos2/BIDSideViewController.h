//
//  BIDSideViewController.h
//  Phobos2
//
//  Created by Li Xianyu on 13-11-11.
//  Copyright (c) 2013年 Li Xianyu. All rights reserved.
//

#import <UIKit/UIKit.h>
//#import <MapKit/MapKit.h>
#import "BMapKit.h"
//#import "BIDPlace.h"
#import "BIDRenameViewController.h"

typedef NS_ENUM(NSInteger, BIDDrawerSection){
    BIDDrawerSectionViewSelection,
    
};

@interface BIDSideViewController : UITableViewController <UIAlertViewDelegate,BIDRenameViewControllerDelegate>
@property (strong, nonatomic) BMKMapView *customMapView;

- (IBAction)ruSwitchChanged:(UISwitch *)sender;
- (IBAction)chuSwitchChanged:(UISwitch *)sender;
- (IBAction)segmentChanged:(UISegmentedControl *)sender;

+ (BOOL)ifDeleteAPhobos;
+ (void)resetIfDeleteAPhobosFlag;
+ (BOOL)comeFromMe;
+ (void)resetComeToRadar;
+ (BOOL)onlyShowTemperature;

- (void)beginBaiduMapView;
- (void)stopBaiduMapView;
- (id)getSideViewController;
//- (void)setMapViewForAppDelegate;
@end
