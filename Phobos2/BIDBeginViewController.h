//
//  BIDBeginViewController.h
//  Phobos2
//
//  Created by Li Xianyu on 13-11-10.
//  Copyright (c) 2013年 Li Xianyu. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BIDAddNewPhobosViewController.h"

@interface BIDBeginViewController : UITableViewController
@property (strong, nonatomic) BIDAddNewPhobosViewController *addController;
@property UIView *semiTransparentView;
@property (strong, nonatomic) UIView *customIBActionSheetView;

@property (copy, nonatomic) NSArray *dwarves;
- (IBAction)addAPhobos:(id)sender;
@end
