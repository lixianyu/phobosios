//
//  BIDBLEManager7.m
//  Phobos2
//
//  Created by Li Xianyu on 13-12-1.
//  Copyright (c) 2013年 Li Xianyu. All rights reserved.
//

#import "BIDBLEiOS7Delegate.h"

@implementation BIDBLEiOS7Delegate

#pragma mark - CBPeripheralDelegate
- (void)peripheral:(CBPeripheral *)peripheral didModifyServices:(NSArray *)invalidatedServices {//NS_AVAILABLE(NA, 7_0);
    NSLog(@"%s", __func__);
}
@end
