//
//  BIDSettingViewController.m
//  Phobos2
//
//  Created by Li Xianyu on 13-11-10.
//  Copyright (c) 2013年 Li Xianyu. All rights reserved.
//

#import "BIDSettingViewController.h"

static NSString *CellIdentifier = @"CellSetting";

@interface BIDSettingViewController ()

@end

@implementation BIDSettingViewController

- (id)initWithStyle:(UITableViewStyle)style
{
    NSLog(@"%s", __func__);
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    NSLog(@"%s", __func__);
    [super viewDidLoad];

    self.names = @[@"温度单位", @"校准温度", @"购买更多", @"帮助"];
    //设置背景图片
    UIImageView *imageview = [[UIImageView alloc] initWithFrame:self.view.bounds];
    [imageview setImage:[UIImage imageNamed:@"settings_bg"]];
    [self.tableView setBackgroundView:imageview];
    
//    [self.tableView registerClass:[UITableViewCell class] forCellReuseIdentifier:CellIdentifier];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 2;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    if (section == 0) {
        return 0;
    }
    return 2;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSLog(@"%s: indexPath.section = %d, indexPath.row = %d", __func__, indexPath.section, indexPath.row);
    static NSString *CellIdentifier = @"plainCell";
    if (indexPath.section == 0) {
        if (indexPath.row == 0) {
            CellIdentifier = @"labelCell";
        } else {
            CellIdentifier = @"plainCell";
        }
    } else {
        CellIdentifier = @"plainCell";
    }
    UITableViewCell *cell = (UITableViewCell*)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];

    // Configure the cell...
    if (indexPath.section == 0) {
        cell.textLabel.text = _names[indexPath.row];
        if (indexPath.row == 0) {
            UILabel *label = (UILabel*)[cell viewWithTag:1];
            NSLog(@"label = %@", label);
            if ([BIDSettingViewController getWendu] == 1) {
                label.text = @"摄氏度";
            } else {
                label.text = @"华氏度";
            }
        }
    } else {
        cell.textLabel.text = _names[indexPath.row + 2];
    }
    cell.layer.cornerRadius = 7.0f;
    CGRect myFrame = cell.frame;
    myFrame.size.width = myFrame.size.width - 20;
    [cell setFrame:myFrame];
    NSLog(@"%s: cell = %@", __func__, cell);
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return 15;
}

#pragma mark - UITableViewDelegate
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    NSLog(@"%s", __func__);
    if (indexPath.section == 0 && indexPath.row == 0) {
        UITableViewCell *aCell = [tableView cellForRowAtIndexPath:indexPath];
        UILabel *label = (UILabel*)[aCell viewWithTag:1];
        NSLog(@"label = %@", label);
        [UIView animateWithDuration:0.9 animations:^(void) {
            if ([BIDSettingViewController getWendu] == 0) {
                label.text = @"摄氏度";
                [self setWendu:1];
            } else {
                label.text = @"华氏度";
                [self setWendu:0];
            }
        }];
    }
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}

#pragma mark -
/**
 @return 0,华氏度, 1,摄氏度
 */
+ (NSInteger)getWendu {
    NSLog(@"%s", __func__);
    NSNumber *oldNumber = [[NSUserDefaults standardUserDefaults] objectForKey:@"wendudanwei"];
    if (oldNumber == nil) {
        [[NSUserDefaults standardUserDefaults] setInteger:1 forKey:@"wendudanwei"];
        return 1;
    }
    NSInteger counter = [oldNumber integerValue];
    return counter;
}

- (void)setWendu:(NSInteger)wendu {
    [[NSUserDefaults standardUserDefaults] setInteger:wendu forKey:@"wendudanwei"];
}
@end
