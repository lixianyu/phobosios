//
//  BIDRenameViewController.m
//  Phobos2
//
//  Created by Li Xianyu on 13-11-18.
//  Copyright (c) 2013年 Li Xianyu. All rights reserved.
//

#import "BIDRenameViewController.h"
#import "BIDAppDelegate.h"

@interface BIDRenameViewController ()

@end

@implementation BIDRenameViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    NSLog(@"%s", __func__);
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (id)initWithCoder:(NSCoder *)aDecoder {
    NSLog(@"%s", __func__);
    self = [super initWithCoder:aDecoder];
    if (self) {
        //
    }
    return self;
}

- (void)viewDidLoad
{
    NSLog(@"%s", __func__);
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    self.title = @"请重新命名";
    self.textField.text = self.name;
    [self.textField becomeFirstResponder];
    NSLog(@"uuid = %@", _uuid);
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)textFieldDoneEditting:(id)sender {
    NSLog(@"%s", __func__);
    [sender resignFirstResponder];
}

- (IBAction)buttonOK:(id)sender {
    NSLog(@"%s", __func__);
    [self.textField endEditing:YES];
    NSString *newName = self.textField.text;
    NSLog(@"newName = %@", newName);
    [self.popupRename dismissPopoverAnimated:YES];
    self.popupRename = nil;
    
    BIDAppDelegate *appDelegate = (BIDAppDelegate*)[UIApplication sharedApplication].delegate;
    NSManagedObjectContext *context = [appDelegate managedObjectContext];
    NSError *error;
    NSFetchRequest *request = [[NSFetchRequest alloc] initWithEntityName:kPhobosEntityName];
    NSPredicate *pred = [NSPredicate predicateWithFormat:@"%K = %@", kPhobosDeviceUUIDKey, _uuid];
    [request setPredicate:pred];
    NSArray *objects = [context executeFetchRequest:request error:&error];
    if (objects == nil) {
        NSLog(@"%s : There was an error!", __func__);
    }
    if ([objects count] <= 0) {
        return;
    }
    NSManagedObject *thePhobos = [objects objectAtIndex:0];
    [thePhobos setValue:newName forKey:kPhobosNameKey];
    [appDelegate saveContext];
    
    if (self.delegate) {
        [self.delegate renameFinished:newName];
    }
}
@end
