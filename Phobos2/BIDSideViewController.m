//
//  BIDSideViewController.m
//  Phobos2
//
//  Created by Li Xianyu on 13-11-11.
//  Copyright (c) 2013年 Li Xianyu. All rights reserved.
//

#import "BIDSideViewController.h"
#import "MMDrawerController.h"
#import "MMLogoView.h"
#import "MMSideDrawerTableViewCell.h"
#import "MMSideDrawerSectionHeaderView.h"
#import "Animations.h"
#import "BIDAppDelegate.h"
#import "BIDRenameViewController.h"
#import "FPPopoverController.h"
#import "BIDBLEManager.h"
#import "MMProgressHUD.h"

static NSString *CellIdentifier = @"mySimpleTableIdentifier";

@interface BIDSideViewController () <BIDBLEManagerDelegate>
@property (strong, nonatomic) BIDBLEManager * phobosManager;
//@property (strong, nonatomic) CLLocationManager *locationManager;
//@property (retain, nonatomic) BIDPlace *lastPlace;
//@property (strong, nonatomic) MKMapView *customMapView;

@property (strong, nonatomic) UIButton *buttonClose;
//@property UIView *semiTransparentView;

@property (strong, nonatomic) NSArray *names;

@property (copy, nonatomic) NSDictionary *selection;
@property (weak, nonatomic) id delegate;

@property (strong, nonatomic) NSManagedObject *theManagedObjectPhobos;
@property (strong, nonatomic) BMKPointAnnotation *pointAnnotation;
@end

@implementation BIDSideViewController {
    BOOL chuSwitch;
    BOOL ruSwitch;
    int chuSegment;
    BOOL IAmVisible;
    BOOL mapButtonTag;
    BOOL onlyone;
}

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    NSLog(@"%s", __func__);
    [super viewDidLoad];

    [self.tableView setAutoresizingMask:UIViewAutoresizingFlexibleWidth|UIViewAutoresizingFlexibleHeight];
    [self.tableView setSeparatorColor:[UIColor colorWithRed:49.0/255.0
                                                      green:54.0/255.0
                                                       blue:57.0/255.0
                                                      alpha:1.0]];
    [self.tableView setBackgroundColor:[UIColor colorWithRed:77.0/255.0
                                                       green:79.0/255.0
                                                        blue:80.0/255.0
                                                       alpha:1.0]];
    
    [self.view setBackgroundColor:[UIColor colorWithRed:66.0/255.0
                                                  green:69.0/255.0
                                                   blue:71.0/255.0
                                                  alpha:1.0]];
    
    CGSize logoSize = CGSizeMake(58, 62);
    MMLogoView * logo = [[MMLogoView alloc] initWithFrame:CGRectMake(CGRectGetMidX(self.tableView.bounds)-logoSize.width/2.0,
                                                                     -logoSize.height-logoSize.height/4.0,
                                                                     logoSize.width,
                                                                     logoSize.height)];
    [logo setAutoresizingMask:UIViewAutoresizingFlexibleLeftMargin|UIViewAutoresizingFlexibleRightMargin];
    NSLog(@"tableView = %@, view = %@", self.tableView, self.view);
    [self.tableView addSubview:logo];
    //[self.view setBackgroundColor:[UIColor clearColor]];
    [self.tableView registerClass:[UITableViewCell class] forCellReuseIdentifier:CellIdentifier];
    self.names = @[@"删除",@"重命名", @"出围报警", @"入围报警", @"雷达"];
    onlyone = YES;
}

//- (void)tellAppDelegateMe {
//    NSLog(@"%s", __func__);
//    BIDAppDelegate *appDelegate = (BIDAppDelegate*)[UIApplication sharedApplication].delegate;
//    appDelegate.viewController = self;
//}

//- (void)setMapViewForAppDelegate {
//    NSLog(@"%s: _customMapView = %@", __func__, _customMapView);
//    if (_customMapView == nil) {
//        _customMapView = [[BMKMapView alloc] initWithFrame:CGRectMake(0, 20, 279, 500)];
//    }
//}

- (void)setMapView {
    NSLog(@"%s: _customMapView = %@", __func__, _customMapView);
    if (_customMapView == nil) {
        _customMapView = [[BMKMapView alloc] initWithFrame:CGRectMake(0, 20, 279, 500)];
    }
//    _customMapView.delegate = (BIDAppDelegate*)[UIApplication sharedApplication].delegate;
    //_customMapView.delegate = self;
    _customMapView.scrollEnabled = YES;
    [_customMapView setBackgroundColor:[UIColor brownColor]];
    _customMapView.alpha = 1.0f;
    _customMapView.layer.cornerRadius = 6.0f;
    
    if (_buttonClose == nil) {
        _buttonClose = [UIButton buttonWithType:UIButtonTypeCustom];
        _buttonClose.frame = CGRectMake(10, 10, 40, 40);
        [_buttonClose setTitle:@"X" forState:UIControlStateNormal];
        [_buttonClose setTitleColor:[UIColor redColor] forState:UIControlStateNormal];
        [_buttonClose sizeToFit];
        [_buttonClose addTarget:self
                        action:@selector(tappedButtonClose:)
              forControlEvents:UIControlEventTouchUpInside];
    }
    [_customMapView addSubview:_buttonClose];
#if 0
    self.semiTransparentView = [[UIView alloc] initWithFrame:[UIScreen mainScreen].bounds];
    UITapGestureRecognizer *cancelTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(dismissCustomIBActionPanel)];
    [self.semiTransparentView addGestureRecognizer:cancelTap];
    self.semiTransparentView.backgroundColor = [UIColor blackColor];
    self.semiTransparentView.alpha = 0.0f;
    [self.view addSubview:_customMapView];
    [self.view insertSubview:self.semiTransparentView belowSubview:_customMapView];
#else
    [self.view addSubview:_customMapView];
#endif
    NSLog(@"self.view.subviews.count = %d", self.view.subviews.count);
    NSLog(@"_customMapView.subviews.count = %d", _customMapView.subviews.count);
}

- (void)setSwitchAndSeg {
    NSLog(@"%s", __func__);
    NSString *uuid = self.selection[@"deviceUUID"];
    NSLog(@"uuid here is : %@", uuid);
    BIDAppDelegate *appDelegate = (BIDAppDelegate*)[UIApplication sharedApplication].delegate;
    NSManagedObjectContext *context = [appDelegate managedObjectContext];
    NSError *error;
    NSFetchRequest *request = [[NSFetchRequest alloc] initWithEntityName:kPhobosEntityName];
    NSPredicate *pred = [NSPredicate predicateWithFormat:@"%K = %@", kPhobosDeviceUUIDKey, uuid];
    [request setPredicate:pred];
    NSArray *objects = [context executeFetchRequest:request error:&error];
    if (objects == nil) {
        NSLog(@"%s : There was an error!", __func__);
        _theManagedObjectPhobos = nil;
    }
    if ([objects count] <= 0) {
        _theManagedObjectPhobos = nil;
        return;
    }
    
    NSManagedObject *thePhobos = [objects objectAtIndex:0];
    _theManagedObjectPhobos = thePhobos;
    chuSwitch = [[thePhobos valueForKey:kPhobosChuAlertKey] boolValue];
    ruSwitch = [[thePhobos valueForKey:kPhobosRuAlertKey] boolValue];
    chuSegment = [[thePhobos valueForKey:kPhobosChuAlertTypeKey] intValue];

    //根据当前是否已连接来设置enabled
    if (_phobosManager == nil) {
        _phobosManager = [BIDBLEManager sharedInstance];
        [_phobosManager addDelegateObserver:self];
    }
    NSIndexPath *indexpath = [NSIndexPath indexPathForRow:0 inSection:1];
    UITableViewCell *cell = [self.tableView cellForRowAtIndexPath:indexpath];
    UISwitch *switchitChu = (UISwitch*)[cell viewWithTag:2];
    [switchitChu setOn:chuSwitch animated:NO];
    UISegmentedControl *segmentChu = (UISegmentedControl*)[cell viewWithTag:3];
    //Begin 根据当前是否已连接来设置enabled
    if ([_phobosManager isConnected:uuid]) {
        switchitChu.enabled = YES;
        segmentChu.enabled = YES;
    } else {
        switchitChu.enabled = NO;
        segmentChu.enabled = NO;
    }
    //End 根据当前是否已连接来设置enabled
    if (chuSwitch) {
        [segmentChu setSelectedSegmentIndex:chuSegment];
        segmentChu.enabled = YES;
    } else {
        [segmentChu setSelectedSegmentIndex:chuSegment];
        segmentChu.enabled = NO;
    }
    
    indexpath = [NSIndexPath indexPathForRow:1 inSection:1];
    cell = [self.tableView cellForRowAtIndexPath:indexpath];
    UISwitch *switchitRu = (UISwitch *)[cell viewWithTag:2];
    [switchitRu setOn:ruSwitch animated:NO];
    //根据当前是否已连接来设置enabled
    if ([_phobosManager isConnected:uuid]) {
        switchitRu.enabled = NO;
    } else {
        switchitRu.enabled = YES;
    }
}

-(void)viewWillAppear:(BOOL)animated {
    NSLog(@"%s", __func__);
    [super viewWillAppear:animated];
    NSNotificationCenter *center = [NSNotificationCenter defaultCenter];
    [center addObserver:self selector:@selector(applicationDidBecomeActive) name:UIApplicationDidBecomeActiveNotification object:nil];
    [self setSwitchAndSeg];
//    if (_customMapView) {
//        if (_customMapView.alpha == 1.0) {
//            _customMapView.delegate = (BIDAppDelegate*)[UIApplication sharedApplication].delegate;
//        }
//    }
}

- (void)viewDidAppear:(BOOL)animated {
    NSLog(@"%s", __func__);
    [super viewDidAppear:animated];
    IAmVisible = YES;
}

- (void)viewWillDisappear:(BOOL)animated {
    NSLog(@"%s", __func__);
    [super viewWillDisappear:animated];
    NSNotificationCenter *center = [NSNotificationCenter defaultCenter];
    [center removeObserver:self name:UIApplicationDidBecomeActiveNotification object:nil];
//    [self.locationManager stopUpdatingLocation];
    _customMapView.delegate = nil;
//    self.locationManager = nil;
//    self.lastPlace = nil;
    [self dismissCustomIBActionPanel];
    _customMapView = nil;
    onlyone = YES;
}

- (void)viewDidDisappear:(BOOL)animated {
    NSLog(@"%s", __func__);
    [super viewDidDisappear:animated];
    IAmVisible = NO;
}

- (void)applicationDidBecomeActive {
    NSLog(@"%s", __func__);
    [self setSwitchAndSeg];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (id)getMMDrawerController {
    NSLog(@"%s", __func__);
    UIApplication *app = [UIApplication sharedApplication];
    UITabBarController *root = (UITabBarController*)[app.windows[0] rootViewController];
    NSLog(@"root = %@", root);
    NSMutableArray *controllers = (NSMutableArray*)[root viewControllers];
    NSLog(@"controllers[1] = %@", controllers[1]);
    for (id aID in controllers) {
        NSLog(@"aID = %@", aID);
    }
    return controllers[0];
}

- (void)timesup {
    NSLog(@"%s", __func__);
    [UIView animateWithDuration:0.5f animations:^() {
        NSLog(@"animations");
        self.tableView.alpha = 0.0f;
        self.customMapView.alpha = 1.0f;
    }completion:^(BOOL finish) {
        NSLog(@"completion, finish = %hhd", finish);
        self.tableView.alpha = 0.0f;
        self.customMapView.alpha = 1.0f;
    }];
}

//点击了“地图”按钮
- (void)tappedButton:(UIButton *)sender
{
    NSLog(@"%s, sender.tag = %d", __func__, sender.tag);
    NSLog(@"sender = %@", sender);
    NSLog(@"applicationState = %d", [UIApplication sharedApplication].applicationState);
    if (sender.tag == 0) {
        mapButtonTag = YES;//出围报警
    } else {
        mapButtonTag = NO;
    }
    double longitude, latitude;
    if (mapButtonTag) {
        longitude = [[_theManagedObjectPhobos valueForKey:kPhobosLongitudeChuKey] doubleValue];
        latitude = [[_theManagedObjectPhobos valueForKey:kPhobosLatitudeChuKey] doubleValue];
    } else {
        longitude = [[_theManagedObjectPhobos valueForKey:kPhobosLongitudeRuKey] doubleValue];
        latitude = [[_theManagedObjectPhobos valueForKey:kPhobosLatitudeRuKey] doubleValue];
    }
    if (longitude == 0 || latitude == 0) {
        NSLog(@"ho, show progress HUD....");
        NSString *titleString, *statusString;
        if (mapButtonTag) {
            titleString = @"出围报警";
            statusString = @"还未发生哦";
            [MMProgressHUD setPresentationStyle:MMProgressHUDPresentationStyleExpand];
        } else {
            titleString = @"入围报警";
            statusString = @"还未发生喔";
            [MMProgressHUD setPresentationStyle:MMProgressHUDPresentationStyleShrink];
        }
        UIImage *_staticImage = [UIImage imageNamed:@"smiley.png"];
        [MMProgressHUD showWithTitle:titleString
                              status:statusString
                               image:_staticImage];
        double delayInSeconds = 2.5;
        dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, delayInSeconds * NSEC_PER_SEC);
        dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
            [MMProgressHUD dismissWithSuccess:@"祝您今天好心情！"];
        });
        return;
    }
    
    [self setMapView];
    [Animations fadeIn:_customMapView andAnimationDuration:1.0 andWait:YES];
    
    [self startLocationMap];
    if (onlyone) {
        [self performSelector:@selector(tappedButtonAgain) withObject:nil afterDelay:1];
        onlyone = NO;
    }
    
}

- (void)tappedButtonAgain {
    NSLog(@"%s", __func__);
    [self setMapView];
    [self startLocationMap];
}

- (void)tappedButtonClose:(UIButton *)sender {
//    [self.locationManager stopUpdatingLocation];
    _customMapView.delegate = nil;
//    self.locationManager = nil;
//    self.lastPlace = nil;
    [self dismissCustomIBActionPanel];
}

- (void)dismissCustomIBActionPanel {
    
    [UIView animateWithDuration:0.5f
                     animations:^() {
//                         self.semiTransparentView.alpha = 0.0f;
                         _customMapView.alpha = 0.0f;
//                         self.tableView.alpha = 1.0f;
                     }];
}

#pragma mark - Table view data source
- (void)setCell:(UITableViewCell *)cell {
    UIView * backgroundView = [[UIView alloc] initWithFrame:cell.bounds];
    [backgroundView setAutoresizingMask:UIViewAutoresizingFlexibleHeight|UIViewAutoresizingFlexibleWidth];
    [backgroundView setBackgroundColor:[UIColor colorWithRed:77.0/255.0
                                                       green:79.0/255.0
                                                        blue:80.0/255.0
                                                       alpha:1.0]];
    [cell setBackgroundView:backgroundView];
    
    [cell.textLabel setBackgroundColor:[UIColor clearColor]];
    [cell.textLabel setTextColor:[UIColor
                                  colorWithRed:230.0/255.0
                                  green:236.0/255.0
                                  blue:242.0/255.0
                                  alpha:1.0]];
    [cell.textLabel setShadowColor:[[UIColor blackColor] colorWithAlphaComponent:.5]];
    [cell.textLabel setShadowOffset:CGSizeMake(0, 1)];
    [cell.textLabel setFont:[UIFont boldSystemFontOfSize:16.0]];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 2;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    if (section == 0) {
        return 2;
    } else if (section == 1) {
        return [self.names count] - 2;
    }
    return 0;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSLog(@"%s", __func__);
    static NSString *CellIdentifier = @"plainCell";
    if (indexPath.section == 0) {
        CellIdentifier = @"plainCell";
    } else {
        if (indexPath.row == 0) {
            CellIdentifier = @"buttonAndSwithAndSegCell";
        } else if (indexPath.row == 1) {
            CellIdentifier = @"buttonAndSwithCell";
        } else {
            CellIdentifier = @"plainCell";
        }
    }
    UITableViewCell *cell = (UITableViewCell*)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    NSLog(@"%s: cell = %@", __func__, cell);
    [self setCell:cell];
    [cell setSelectionStyle:UITableViewCellSelectionStyleBlue];
    [cell.textLabel setText:_names[indexPath.row]];
    cell.textLabel.font = [UIFont boldSystemFontOfSize:20];
    
    if (indexPath.section == 1) {
        if (indexPath.row == 0 || indexPath.row == 1) {
            [cell.textLabel setText:_names[indexPath.row+2]];
            UIImage *buttonUpImage = [UIImage imageNamed:@"button_up.png"];
            UIImage *buttonDownImage = [UIImage imageNamed:@"button_down.png"];
            UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
            [button setBackgroundImage:buttonUpImage
                              forState:UIControlStateNormal];
            [button setBackgroundImage:buttonDownImage
                              forState:UIControlStateHighlighted];
            [button setTitle:@"地图" forState:UIControlStateNormal];
            [button sizeToFit];
            [button setTag:indexPath.row];
            [button addTarget:self
                       action:@selector(tappedButton:)
             forControlEvents:UIControlEventTouchUpInside];
            CGFloat x = 260 * (280.0 / 320.0);
            button.frame = CGRectMake(x - 20, 3, 59, 31);
            [cell.contentView addSubview:button];
            
            UISwitch *uiswitch = (UISwitch*)[cell viewWithTag:2];
            NSString *uuid = self.selection[@"deviceUUID"];
            if (indexPath.row == 0) {
                [uiswitch setOn:chuSwitch animated:NO];
                UISegmentedControl *segment = (UISegmentedControl*)[cell viewWithTag:3];
                if (chuSwitch) {
                    segment.enabled = YES;
                    
                } else {
                    segment.enabled = NO;
                }
                [segment setSelectedSegmentIndex:chuSegment];
//                if (_radarController == nil) {
//                    _radarController = [self getRadarViewController];
//                }
//                if (![_radarController isConnected:uuid]) {
//                    button.enabled = NO;
//                    uiswitch.enabled = NO;
//                    segment.enabled = NO;
//                } else {
//                    button.enabled = YES;
//                    uiswitch.enabled = YES;
//                    segment.enabled = YES;
//                }
                
            } else {
                [uiswitch setOn:ruSwitch animated:NO];

                if ([_phobosManager isConnected:uuid]) {
                    uiswitch.enabled = NO;
                } else {
                    uiswitch.enabled = YES;
                }
            }
        } else if (indexPath.row == 2 || indexPath.row == 3) {
            [cell.textLabel setText:_names[indexPath.row+2]];
        }
    }
    return cell;
}

-(NSString*)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section{
    NSLog(@"%s: section = %d", __func__, section);
    switch (section) {
        case BIDDrawerSectionViewSelection:
            return @"";
        default:
            return nil;
    }
}

-(UIView*)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    NSLog(@"%s", __func__);
    MMSideDrawerSectionHeaderView * headerView =  [[MMSideDrawerSectionHeaderView alloc] initWithFrame:CGRectMake(0, 0, CGRectGetWidth(tableView.bounds), 20.0f)];
    [headerView setAutoresizingMask:UIViewAutoresizingFlexibleHeight|UIViewAutoresizingFlexibleWidth];
    [headerView setTitle:[tableView.dataSource tableView:tableView titleForHeaderInSection:section]];
    return headerView;
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    NSLog(@"%s", __func__);
    return 23.0;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    NSLog(@"%s", __func__);
    if (indexPath.section == 1) {
        if (indexPath.row == 0) {
            return 130.0;
        }
        if (indexPath.row == 1) {
            return 88.0;
        }
    }
    return 50.0;
}

#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSLog(@"%s: indexPath.row = %d", __func__, indexPath.row);
    UITableViewCell * cell = [tableView cellForRowAtIndexPath:indexPath];
    switch (indexPath.section) {
        case 0:
            if (indexPath.row == 0) {
                UIAlertView * alert = [[UIAlertView alloc] initWithTitle:@"确认删除" message:@"警告！不能撤销！" delegate:self cancelButtonTitle:@"取消" otherButtonTitles:@"确认", nil];
                [alert show];
            } else {
                [self popupRenameView:cell];
            }
            break;
        case 1:
            if (indexPath.row == 2 || indexPath.row == 3) { //查看温度、雷达
                cometoradar = true;
                if (indexPath.row == 2) {
                    cometoradarOnlyShowTemperature = false;
                }
                else {
                    cometoradarOnlyShowTemperature = false;
                }
                [[self getMMDrawerController] closeDrawerAnimated:YES completion:nil];
                
                [self setCheckMark];
                [_theManagedObjectPhobos setValue:[NSNumber numberWithBool:YES] forKey:kPhobosIfCheckedKey];
                [(BIDAppDelegate*)[UIApplication sharedApplication].delegate saveContext];
                
                UIApplication *app = [UIApplication sharedApplication];
                UITabBarController *root = (UITabBarController*)[app.windows[0] rootViewController];
                [root setSelectedIndex:1];
            }
            break;
        default:
            break;
    }
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}

- (void)setCheckMark {
    BIDAppDelegate *appDelegate = (BIDAppDelegate*)[UIApplication sharedApplication].delegate;
    NSManagedObjectContext *context = [appDelegate managedObjectContext];
    NSFetchRequest *request = [[NSFetchRequest alloc] initWithEntityName:kPhobosEntityName];
    NSError *error;
    NSArray *objects = [context executeFetchRequest:request error:&error];
    NSLog(@"%s: Phobos count = %d", __func__, [objects count]);
    if (objects == nil) {
        NSLog(@"There was an error!");
        return;
    }
    if ([objects count] <= 0) {
        return;
    }
    for (NSManagedObject *oneObject in objects) {
        [oneObject setValue:[NSNumber numberWithBool:NO] forKey:kPhobosIfCheckedKey];
    }
}

static bool cometoradar = false;
static bool cometoradarOnlyShowTemperature = false;
+ (BOOL)comeFromMe {
    if (cometoradar == true) {
        return YES;
    }
    return NO;
}

+ (void)resetComeToRadar {
    cometoradar = false;
}

+ (BOOL)onlyShowTemperature {
    if (cometoradarOnlyShowTemperature == true) {
        return YES;
    }
    return NO;
}

- (void)popupRenameView:(id)sender {
    NSLog(@"%s", __func__);
    //the controller we want to present as a popover
    BIDRenameViewController *controller = [self.storyboard instantiateViewControllerWithIdentifier:@"renamePhobos"];
    NSLog(@"controller = %@", controller);
    controller.delegate = self;
    controller.name = self.selection[@"name"];
    controller.uuid = self.selection[@"deviceUUID"];
    NSLog(@"text = %@", controller.textField.text);
    FPPopoverController *popover = [[FPPopoverController alloc] initWithViewController:controller];

    controller.popupRename = popover;
    popover.tint = FPPopoverLightGrayTint;
    
    //if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
    {
        popover.contentSize = CGSizeMake(212, 200);
    }
    popover.arrowDirection = FPPopoverArrowDirectionAny;
    
    [popover presentPopoverFromView:sender];
}

- (void)renameFinished:(NSString*)name {
    NSLog(@"%s", __func__);
    NSNotificationCenter *center = [NSNotificationCenter defaultCenter];
    NSDictionary *aDictionary = @{@"name": name,
                                  @"indexPath":_selection[@"indexPath"]};
    [center postNotificationName:@"Notification_reloadTableViewOneRow" object:aDictionary];
    [[self getMMDrawerController] closeDrawerAnimated:YES completion:nil];
}

- (NSIndexPath *)tableView:(UITableView *)tableView willSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.section == 1) {
        if (indexPath.row == 0 || indexPath.row == 1) {
            return nil;
        }
    }
    return indexPath;
}

- (BOOL)tableView:(UITableView *)tableView shouldHighlightRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.section == 1) {
        if (indexPath.row == 0 || indexPath.row == 1) {
            return NO;
        }
    }
    return YES;
}


#pragma mark - UIAlertViewDelegate
- (void)alertViewCancel:(UIAlertView *)alertView {
    NSLog(@"%s", __func__);
    
}

-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    NSLog(@"%s: buttonIndex = %d", __func__, buttonIndex);
    if (buttonIndex == 0) {
        //cancel - do nothing.
    } else if (buttonIndex == 1) {
        //ok
        [[(BIDAppDelegate*)[UIApplication sharedApplication].delegate managedObjectContext] deleteObject:_theManagedObjectPhobos];
        [(BIDAppDelegate*)[UIApplication sharedApplication].delegate saveContext];
        
        NSNotificationCenter *center = [NSNotificationCenter defaultCenter];
        [center postNotificationName:@"Notification_reloadTableView" object:_selection[@"indexPath"]];
        NSString *uuid = self.selection[@"deviceUUID"];
        NSLog(@"uuid = %@", uuid);
        [center postNotificationName:@"Notification_haveDeleteOneConnectedPeripheral" object:uuid];
        
        [[self getMMDrawerController] closeDrawerAnimated:YES completion:nil];
        
        deleteaphobos = true;
    }
}

static bool deleteaphobos = false;
+ (BOOL)ifDeleteAPhobos {
    if (deleteaphobos == true) {
        return YES;
    }
    return NO;
}

+ (void)resetIfDeleteAPhobosFlag {
    deleteaphobos = false;
}

#pragma mark CLLocationManagerDelegate Methods
- (void)startLocationMap {
    NSLog(@"%s", __func__);
//    self.locationManager = [[CLLocationManager alloc] init];
//    _locationManager.delegate = self;
//    _locationManager.desiredAccuracy = kCLLocationAccuracyBest;
//    [_locationManager startUpdatingLocation];

    double longitude, latitude;
    if (mapButtonTag) {
        longitude = [[_theManagedObjectPhobos valueForKey:kPhobosLongitudeChuKey] doubleValue];
        latitude = [[_theManagedObjectPhobos valueForKey:kPhobosLatitudeChuKey] doubleValue];
    } else {
        longitude = [[_theManagedObjectPhobos valueForKey:kPhobosLongitudeRuKey] doubleValue];
        latitude = [[_theManagedObjectPhobos valueForKey:kPhobosLatitudeRuKey] doubleValue];
    }
    CLLocationCoordinate2D coordinate = CLLocationCoordinate2DMake(latitude, longitude);

    if (_pointAnnotation == nil) {
        _pointAnnotation = [[BMKPointAnnotation alloc] init];
    } else {
        [_customMapView removeAnnotation:_pointAnnotation];
    }
    _pointAnnotation.coordinate = coordinate;
    NSString *alertime;
    if (mapButtonTag) {
        _pointAnnotation.title = @"上次出围报警";
        alertime = [_theManagedObjectPhobos valueForKey:kPhobosChuAlertTime];
        _pointAnnotation.subtitle = alertime;
    } else {
        _pointAnnotation.title = @"上次入围报警";
        alertime = [_theManagedObjectPhobos valueForKey:kPhobosRuAlertTime];
        _pointAnnotation.subtitle = alertime;
    }
    
//    _pointAnnotation.subtitle  = [_pointAnnotation.subtitle stringByAppendingString:alertime];
    [_customMapView addAnnotation:_pointAnnotation];
    [_customMapView selectAnnotation:_pointAnnotation animated:YES];
    
    BMKCoordinateRegion region = BMKCoordinateRegionMakeWithDistance(coordinate, 250, 250);
    _customMapView.showsUserLocation = NO;
//    _customMapView.userTrackingMode = BMKUserTrackingModeNone;
//    _customMapView.showsUserLocation = YES;
    [_customMapView setRegion:region animated:YES];
    NSLog(@"latitude = %f, longitude = %f", latitude, longitude);
}

# if 0
-(void)locationManager:(CLLocationManager *)manager didUpdateToLocation:(CLLocation *)newLocation fromLocation:(CLLocation *)oldLocation
{
    NSLog(@"%s", __func__);
}

- (void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray *)locations {
    NSLog(@"%s", __func__);
}

#pragma mark - MKMapViewDelegate
- (void)mapView:(MKMapView *)mapView didUpdateUserLocation:(MKUserLocation *)userLocation {
    CLLocation *newLocation = userLocation.location;
    BIDPlace *start = [[BIDPlace alloc] init];
    start.coordinate = newLocation.coordinate;
    start.title = @"海拔：";
    start.subtitle = @"经度：";
    
    
    //经度
    NSString *longitudeString = [NSString stringWithFormat:@"%g\u00B0",
                                 newLocation.coordinate.longitude];
    //纬度
    NSString *latitudeString = [NSString stringWithFormat:@"%g\u00B0",
                                newLocation.coordinate.latitude];
    NSString *altitudeString = [NSString stringWithFormat:@"%gm（",
                                newLocation.altitude];
    NSString *verticalAccuracyString = [NSString stringWithFormat:@"%gm）",
                                        newLocation.verticalAccuracy];
    
    start.subtitle = [start.subtitle stringByAppendingString:longitudeString];
    start.subtitle = [start.subtitle stringByAppendingString:@"，纬度："];
    start.subtitle = [start.subtitle stringByAppendingString:latitudeString];
//    start.subtitle = [start.subtitle stringByAppendingString:@"海拔："];
    start.title = [start.title stringByAppendingString:altitudeString];
    start.title = [start.title stringByAppendingString:@"精确度"];
    start.title = [start.title stringByAppendingString:verticalAccuracyString];
    
    [_customMapView addAnnotation:start];
    MKCoordinateRegion region = MKCoordinateRegionMakeWithDistance(newLocation.coordinate, 1500, 1500);
    [_customMapView setRegion:region animated:YES];
    if (self.lastPlace) {
        [mapView removeAnnotation:self.lastPlace];
    }
    self.lastPlace = start;
    
    NSLog(@"%s: latitude = %f, longitude = %f", __func__, newLocation.coordinate.latitude, newLocation.coordinate.longitude);
}
#endif

- (IBAction)ruSwitchChanged:(UISwitch *)sender {
    NSLog(@"%s", __func__);
    ruSwitch = sender.isOn;
    [_theManagedObjectPhobos setValue:[NSNumber numberWithBool:ruSwitch] forKey:kPhobosRuAlertKey];
    [(BIDAppDelegate*)[UIApplication sharedApplication].delegate saveContext];
    
    UInt32 majorminor = (UInt32)[[_theManagedObjectPhobos valueForKey:kPhobosMajorMinorKey] longLongValue];
    NSString *uuid = self.selection[@"deviceUUID"];
    if (ruSwitch) {
        [_phobosManager ruOpened:uuid MajorMinor:majorminor];
    } else {
        [_phobosManager ruClosed:uuid MajorMinor:majorminor];
    }
    NSLog(@"uuid = %@, ruSwitch = %@", uuid, ruSwitch?@"YES":@"NO");
}

- (void)setLinkLossValue:(NSNumber*)value {
    NSLog(@"%s", __func__);
    int segment = [value intValue];
    Byte byteValue;
    switch (segment) {
        case 0://都提醒
            byteValue = 0x02;
            break;
        case 1://仅电话提醒
            byteValue = 0x00;
            break;
        case 2://仅Phobos提醒
            byteValue = 0x02;
            break;
        default:
            byteValue = 0x00;
            break;
    }
    NSString *uuid = self.selection[@"deviceUUID"];
    [_phobosManager setLinkLoss:uuid value:byteValue];
}

- (IBAction)chuSwitchChanged:(UISwitch *)sender {
    NSLog(@"%s", __func__);
    chuSwitch = sender.isOn;
    
    int chuSegmentValue = [[_theManagedObjectPhobos valueForKey:kPhobosChuAlertTypeKey] intValue];
    NSLog(@"chuSegmentValue = %d", chuSegmentValue);
    if (chuSwitch) {
        if (chuSegmentValue == 0 || chuSegmentValue == 2) {
            [self performSelector:@selector(setLinkLossValue:) withObject:[NSNumber numberWithInt:chuSegmentValue]];
        }
    } else {
        if (chuSegmentValue == 0 || chuSegmentValue == 2) {
            [self performSelector:@selector(setLinkLossValue:) withObject:[NSNumber numberWithInt:0xFF]];
        }
    }
    [self.tableView reloadSections:[NSIndexSet indexSetWithIndexesInRange:NSMakeRange(1, self.tableView.numberOfSections-1)] withRowAnimation:UITableViewRowAnimationAutomatic];
    
    [_theManagedObjectPhobos setValue:[NSNumber numberWithBool:chuSwitch] forKey:kPhobosChuAlertKey];
    [(BIDAppDelegate*)[UIApplication sharedApplication].delegate saveContext];
}

- (IBAction)segmentChanged:(UISegmentedControl *)sender {
    static int lastSegment = 1;
    NSLog(@"%s", __func__);
    chuSegment = sender.selectedSegmentIndex;
#if 1
    if (lastSegment == 0 || lastSegment == 2) {
        if (chuSegment == 1) {
            [self performSelector:@selector(setLinkLossValue:) withObject:[NSNumber numberWithInt:chuSegment]];
        }
    } else { // lastSegment == 1
        [self performSelector:@selector(setLinkLossValue:) withObject:[NSNumber numberWithInt:chuSegment]];
    }
    lastSegment = chuSegment;
#else
    [self performSelector:@selector(setLinkLossValue:) withObject:[NSNumber numberWithInt:chuSegment]];
#endif
    
    [_theManagedObjectPhobos setValue:[NSNumber numberWithInt:chuSegment] forKey:kPhobosChuAlertTypeKey];
    [(BIDAppDelegate*)[UIApplication sharedApplication].delegate saveContext];
}

#pragma mark - BIDBLEManagerDelegate
- (void)stateUpdate {
    NSLog(@"%s", __func__);
    if (IAmVisible && (![BIDAppDelegate isBackground])) {
        [self setSwitchAndSeg];
    }
}

#pragma mark - BMKMapViewDelegate
/**
 *在地图View将要启动定位时，会调用此函数
 *@param mapView 地图View
 */
- (void)mapViewWillStartLocatingUser:(BMKMapView *)mapView
{
    NSLog(@"%s", __func__);
	NSLog(@"start locate");
}

/**
 *用户位置更新后，会调用此函数
 *@param mapView 地图View
 *@param userLocation 新的用户位置
 */
- (void)mapView:(BMKMapView *)mapView didUpdateUserLocation:(BMKUserLocation *)userLocation
{
    NSLog(@"%s", __func__);
	if (userLocation != nil) {
		NSLog(@"%f %f", userLocation.location.coordinate.latitude, userLocation.location.coordinate.longitude);
	} else {
        
    }
}

/**
 *在地图View停止定位后，会调用此函数
 *@param mapView 地图View
 */
- (void)mapViewDidStopLocatingUser:(BMKMapView *)mapView
{
    NSLog(@"%s", __func__);
    NSLog(@"stop locate");
}

/**
 *定位失败后，会调用此函数
 *@param mapView 地图View
 *@param error 错误号，参考CLError.h中定义的错误号
 */
- (void)mapView:(BMKMapView *)mapView didFailToLocateUserWithError:(NSError *)error
{
    NSLog(@"%s", __func__);
    NSLog(@"location error");
}

//#pragma mark -
//- (void)beginBaiduMapView {
//    NSLog(@"%s: _customMapView = %@", __func__, _customMapView);
//    if (_customMapView == nil) {
////        [self setMapView];
//        [self performSelector:@selector(beginBaiduMapView) withObject:nil afterDelay:1];
////        [self performSelectorOnMainThread:<#(SEL)#> withObject:<#(id)#> waitUntilDone:<#(BOOL)#>]
//        return;
//    }
//    _customMapView.delegate = (BIDAppDelegate*)[UIApplication sharedApplication].delegate;
//    _customMapView.showsUserLocation = NO;//先关闭显示的定位图层
//    _customMapView.userTrackingMode = BMKUserTrackingModeNone;//设置定位的状态
//    _customMapView.showsUserLocation = YES;//显示定位图层
//}
//
//- (void)stopBaiduMapView {
//    NSLog(@"%s", __func__);
//    _customMapView.userTrackingMode = BMKUserTrackingModeNone;
//    _customMapView.showsUserLocation = NO;
//    _customMapView.delegate = nil;
//    //    _baiduMapView = nil;
//}
@end
