//
//  BIDConstant.m
//  Phobos2
//
//  Created by Li Xianyu on 13-11-27.
//  Copyright (c) 2013年 Li Xianyu. All rights reserved.
//

#import "BIDConstant.h"

@implementation BIDConstant
+ (float)distanceFromPointX:(CGPoint)start distanceToPointY:(CGPoint)end {
    float distance;
    //下面就是高中的数学，不详细解释了
    CGFloat xDist = (end.x - start.x);
    CGFloat yDist = (end.y - start.y);
    distance = sqrt((xDist * xDist) + (yDist * yDist));
    //    return distance;
//    NSLog(@"distance = %f", distance);
    return ABS(distance);
}
@end
