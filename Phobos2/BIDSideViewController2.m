//
//  BIDSideViewController2.m
//  Phobos2
//
//  Created by Li Xianyu on 13-11-11.
//  Copyright (c) 2013年 Li Xianyu. All rights reserved.
//

#import "BIDSideViewController2.h"
#import "MMLogoView.h"
#import "MMSideDrawerTableViewCell.h"
#import "MMSideDrawerSectionHeaderView.h"

@interface BIDSideViewController2 ()

@end

@implementation BIDSideViewController2

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    NSLog(@"%s", __func__);
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    NSLog(@"%s", __func__);
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    _tableView = [[UITableView alloc] initWithFrame:self.view.bounds style:UITableViewStylePlain];
    [self.tableView setDelegate:self];
    [self.tableView setDataSource:self];
    [self.view addSubview:self.tableView];
    [self.tableView setAutoresizingMask:UIViewAutoresizingFlexibleWidth|UIViewAutoresizingFlexibleHeight];
    [self.tableView setSeparatorColor:[UIColor colorWithRed:49.0/255.0
                                                      green:54.0/255.0
                                                       blue:57.0/255.0
                                                      alpha:1.0]];
    [self.tableView setBackgroundColor:[UIColor colorWithRed:77.0/255.0
                                                       green:79.0/255.0
                                                        blue:80.0/255.0
                                                       alpha:1.0]];
    
    [self.view setBackgroundColor:[UIColor colorWithRed:66.0/255.0
                                                  green:69.0/255.0
                                                   blue:71.0/255.0
                                                  alpha:1.0]];
    
    CGSize logoSize = CGSizeMake(58, 62);
    MMLogoView * logo = [[MMLogoView alloc] initWithFrame:CGRectMake(CGRectGetMidX(self.tableView.bounds)-logoSize.width/2.0,
                                                                     -logoSize.height-logoSize.height/4.0,
                                                                     logoSize.width,
                                                                     logoSize.height)];
    [logo setAutoresizingMask:UIViewAutoresizingFlexibleLeftMargin|UIViewAutoresizingFlexibleRightMargin];
    [self.tableView addSubview:logo];
    [self.view setBackgroundColor:[UIColor clearColor]];
    
    self.names = @[@"呼叫", @"出围报警", @"入围报警", @"重命名", @"删除"];

}

-(void)viewWillAppear:(BOOL)animated {
    NSLog(@"%s", __func__);
    [super viewWillAppear:animated];
    
    NSNumber * chu = self.selection[@"chuwei1"];
    NSNumber * ru = self.selection[@"ruwei1"];
    NSLog(@"%@",[chu boolValue]?@"YES":@"NO");
    NSLog(@"%@",[ru boolValue]?@"YES":@"NO");
    NSIndexPath *indexpath = [NSIndexPath indexPathForRow:1 inSection:NO];
    UITableViewCell *cell = [self.tableView cellForRowAtIndexPath:indexpath];
    [cell setAccessoryType:[chu boolValue]?UITableViewCellAccessoryCheckmark:UITableViewCellAccessoryNone];
    
    indexpath = [NSIndexPath indexPathForRow:2 inSection:NO];
    cell = [self.tableView cellForRowAtIndexPath:indexpath];
    [cell setAccessoryType:[ru boolValue]?UITableViewCellAccessoryCheckmark:UITableViewCellAccessoryNone];

    [self.tableView reloadSections:[NSIndexSet indexSetWithIndexesInRange:NSMakeRange(0, self.tableView.numberOfSections-1)] withRowAnimation:UITableViewRowAnimationNone];
}

- (void)didReceiveMemoryWarning
{
    NSLog(@"%s", __func__);
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)tappedButton:(UIButton *)sender
{
    NSLog(@"%s", __func__);
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    NSLog(@"%s", __func__);
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    NSLog(@"%s", __func__);
    // Return the number of rows in the section.
    return [self.names count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSLog(@"%s", __func__);
    static NSString *CellIdentifier = @"Cell";
    
    UITableViewCell *cell = (UITableViewCell*)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    NSLog(@"%s: cell = %@", __func__, cell);
    if (cell == nil) {
        cell = [[MMSideDrawerTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
        [cell setSelectionStyle:UITableViewCellSelectionStyleBlue];
    }
    
//    switch (indexPath.section) {
//        case BIDDrawerSectionViewSelection:
//            [cell.textLabel setText:_names[indexPath.row]];
//        break;
//            
//        default:
//            break;
//    }
    [cell.textLabel setText:_names[indexPath.row]];
    cell.textLabel.font = [UIFont fontWithName:@"hehe" size:60];
    NSLog(@"accessoryView = %@", cell.accessoryView);
    if (indexPath.row == 1 || indexPath.row == 2) {
        UIImage *buttonUpImage = [UIImage imageNamed:@"button_up.png"];
        UIImage *buttonDownImage = [UIImage imageNamed:@"button_down.png"];
        UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
        [button setBackgroundImage:buttonUpImage
                          forState:UIControlStateNormal];
        [button setBackgroundImage:buttonDownImage
                          forState:UIControlStateHighlighted];
        [button setTitle:@"地图" forState:UIControlStateNormal];
        [button sizeToFit];
        [button addTarget:self
                   action:@selector(tappedButton:)
         forControlEvents:UIControlEventTouchUpInside];
        CGFloat x = 260 * (280.0 / 320.0);
        button.frame = CGRectMake(x - 40, 5, 51, 31);
        [cell.contentView addSubview:button];
    }
    return cell;
}

-(NSString*)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section{
    NSLog(@"%s: section = %d", __func__, section);
    switch (section) {
        case BIDDrawerSectionViewSelection:
            return @"";
        default:
            return nil;
    }
}

-(UIView*)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    NSLog(@"%s", __func__);
    MMSideDrawerSectionHeaderView * headerView =  [[MMSideDrawerSectionHeaderView alloc] initWithFrame:CGRectMake(0, 0, CGRectGetWidth(tableView.bounds), 20.0f)];
    [headerView setAutoresizingMask:UIViewAutoresizingFlexibleHeight|UIViewAutoresizingFlexibleWidth];
    [headerView setTitle:[tableView.dataSource tableView:tableView titleForHeaderInSection:section]];
    return headerView;
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    NSLog(@"%s", __func__);
    return 23.0;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    NSLog(@"%s", __func__);
    return 50.0;
}

#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSLog(@"%s: indexPath.row = %d", __func__, indexPath.row);
    static BOOL chu = NO;
    static BOOL ru = NO;
    UITableViewCell * cell = [tableView cellForRowAtIndexPath:indexPath];
    switch (indexPath.section) {
        case BIDDrawerSectionViewSelection:{
            if (indexPath.row == 1) {
                if (chu == NO) {
                    [cell setAccessoryType:UITableViewCellAccessoryCheckmark];
                    chu = YES;
                } else {
                    [cell setAccessoryType:UITableViewCellAccessoryNone];
                    chu = NO;
                }
            }
            if (indexPath.row == 2) {
                if (ru == NO) {
                    [cell setAccessoryType:UITableViewCellAccessoryCheckmark];
                    ru = YES;
                } else {
                    [cell setAccessoryType:UITableViewCellAccessoryNone];
                    ru = NO;
                }
            }
            break;
        }
        
        default:
            break;
    }
    if (indexPath.row == 4) {
        UIAlertView * alert = [[UIAlertView alloc] initWithTitle:@"确认删除" message:@"警告！不能撤销！" delegate:self cancelButtonTitle:@"取消" otherButtonTitles:@"确认", nil];
        [alert show];
    }
    //[tableView selectRowAtIndexPath:indexPath animated:NO scrollPosition:UITableViewScrollPositionNone];
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}

#pragma mark - UIAlertViewDelegate
- (void)alertViewCancel:(UIAlertView *)alertView {
    NSLog(@"%s", __func__);
    
}

-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    NSLog(@"%s: buttonIndex = %d", __func__, buttonIndex);
    if (buttonIndex == 0) {
        //cancel
    } else if (buttonIndex == 1) {
        //ok
    }
}
@end
