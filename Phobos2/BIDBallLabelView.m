//
//  BIDBallLabelView.m
//  Phobos2
//
//  Created by Li Xianyu on 13-11-21.
//  Copyright (c) 2013年 Li Xianyu. All rights reserved.
//

#import "BIDBallLabelView.h"
#import "BIDRenameViewController.h"
#import "FPPopoverController.h"
#import "BIDCallViewController.h"
#import "BIDConstant.h"
#import "BIDAppDelegate.h"

@interface BIDBallLabelView () <FPPopoverControllerDelegate, UIPopoverControllerDelegate>
@property (strong, nonatomic) UILabel *label;
@property (strong, nonatomic) UIImageView *imageView;

@property (strong, nonatomic) FPPopoverController *popover;
@property (strong, nonatomic) UIPopoverController *uiPopover;

@property (strong, nonatomic) NSString *uuid;
@end

@implementation BIDBallLabelView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

- (id)initWithLabel:(NSString *)label {
    self = [[BIDBallLabelView alloc] init];
    self.imageBall = [UIImage imageNamed:@"radar_BlueInsideCircle"];
    self.imageView = [[UIImageView alloc] initWithImage:_imageBall];
    [self addSubview:_imageView];
    
    self.label = [[UILabel alloc] initWithFrame:CGRectMake(_imageBall.size.width, 0, 160, _imageBall.size.height+2)];
    [_label setBackgroundColor:[UIColor clearColor]];
    [_label setTextColor:[UIColor whiteColor]];
    _label.text = label;
    [_label setShadowColor:[UIColor greenColor]];
    [self addSubview:_label];
    
    [self setFrame:CGRectMake(450, 450, 160, _imageBall.size.height+2)];
    return self;
}

- (id)initWithLabel:(NSString *)label thePoint:(CGPoint)point unKnowBle:(BOOL)unKnowBle UUID:(NSString*)uuid {
    NSLog(@"%s: uuid = %@", __func__, uuid);
    self = [[BIDBallLabelView alloc] init];
    _uuid = uuid;
    self.canMove = YES;
    self.rssiUpdating = NO;
    self.angleUpdating = NO;
    if (unKnowBle == NO) {
        self.imageBall = [UIImage imageNamed:@"radar_BlueInsideCircle"];
    } else {
        self.imageBall = [UIImage imageNamed:@"radar_GreyInsideCircle"];
    }
    
    self.imageView = [[UIImageView alloc] initWithImage:_imageBall];
    [self.imageView sizeToFit];
    [self addSubview:_imageView];
    
    self.label = [[UILabel alloc] initWithFrame:CGRectMake(_imageBall.size.width, 0, 100, _imageBall.size.height+2)];
    [_label setBackgroundColor:[UIColor clearColor]];
    [_label setTextColor:[UIColor whiteColor]];
    _label.text = label;
    //    [_label sizeToFit];
    [_label setShadowColor:[UIColor grayColor]];
    [self addSubview:_label];
    
    [self setFrame:CGRectMake(point.x - _imageBall.size.width/2, point.y - _imageBall.size.height/2, 35, _imageBall.size.height)];
    //    [self setFrame:CGRectMake(point.x, point.y, 35, 80)];
    //    [self setFrame:CGRectMake(point.x, point.y, _imageBall.size.width, _imageBall.size.height)];
    //    _curPoint = CGPointMake(self.frame.origin.x, self.frame.origin.y);
    _curPoint = point;
    CGFloat r = [BIDConstant distanceFromPointX:_curPoint distanceToPointY:CGPointMake(gcenterX, gcenterY)];
    _curSin = (_curPoint.y - gcenterY) / r;
    _curCos = (_curPoint.x - gcenterX) / r;
    //    NSLog(@"_curPoint.x = %f, _curPoint.y = %f", _curPoint.x, _curPoint.y);
    //    NSLog(@"x=%f, y=%f, _imageView.frame.size.width = %f, _imageView.frame.size.height = %f", _imageView.frame.origin.x,_imageView.frame.origin.y,_imageView.frame.size.width, _imageView.frame.size.height);
    //    NSLog(@"x=%f, y=%f, _label.frame.size.width = %f, _label.frame.size.height = %f", _label.frame.origin.x,_label.frame.origin.y,_label.frame.size.width, _label.frame.size.height);
    self.userInteractionEnabled = YES;
    //    [self addTarget:self action:@selector(touchMeLe) forControlEvents:UIControlEventTouchDown];
    UITapGestureRecognizer * oneTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(touchMeLe:)];
    [oneTap setNumberOfTapsRequired:1];
    [self addGestureRecognizer:oneTap];
    
    if (unKnowBle == NO) {
        self.alpha = 0.3f;
    }
    return self;
}

- (id)initWithLabel:(NSString *)label thePoint:(CGPoint)point unKnowBle:(BOOL)unKnowBle {
//    NSLog(@"%s", __func__);
    self = [[BIDBallLabelView alloc] init];
    self.canMove = YES;
    self.rssiUpdating = NO;
    self.angleUpdating = NO;
    if (unKnowBle == NO) {
        self.imageBall = [UIImage imageNamed:@"radar_BlueInsideCircle"];
    } else {
        self.imageBall = [UIImage imageNamed:@"radar_GreyInsideCircle"];
    }
    
    self.imageView = [[UIImageView alloc] initWithImage:_imageBall];
    [self.imageView sizeToFit];
    [self addSubview:_imageView];
    
    self.label = [[UILabel alloc] initWithFrame:CGRectMake(_imageBall.size.width, 0, 100, _imageBall.size.height+2)];
    [_label setBackgroundColor:[UIColor clearColor]];
    [_label setTextColor:[UIColor whiteColor]];
    _label.text = label;
//    [_label sizeToFit];
    [_label setShadowColor:[UIColor grayColor]];
    [self addSubview:_label];
    
    [self setFrame:CGRectMake(point.x - _imageBall.size.width/2, point.y - _imageBall.size.height/2, 35, _imageBall.size.height)];
//    [self setFrame:CGRectMake(point.x, point.y, 35, 80)];
//    [self setFrame:CGRectMake(point.x, point.y, _imageBall.size.width, _imageBall.size.height)];
//    _curPoint = CGPointMake(self.frame.origin.x, self.frame.origin.y);
    _curPoint = point;
    CGFloat r = [BIDConstant distanceFromPointX:_curPoint distanceToPointY:CGPointMake(gcenterX, gcenterY)];
    _curSin = (_curPoint.y - gcenterY) / r;
    _curCos = (_curPoint.x - gcenterX) / r;
//    NSLog(@"_curPoint.x = %f, _curPoint.y = %f", _curPoint.x, _curPoint.y);
//    NSLog(@"x=%f, y=%f, _imageView.frame.size.width = %f, _imageView.frame.size.height = %f", _imageView.frame.origin.x,_imageView.frame.origin.y,_imageView.frame.size.width, _imageView.frame.size.height);
//    NSLog(@"x=%f, y=%f, _label.frame.size.width = %f, _label.frame.size.height = %f", _label.frame.origin.x,_label.frame.origin.y,_label.frame.size.width, _label.frame.size.height);
    self.userInteractionEnabled = YES;
//    [self addTarget:self action:@selector(touchMeLe) forControlEvents:UIControlEventTouchDown];
    UITapGestureRecognizer * oneTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(touchMeLe:)];
    [oneTap setNumberOfTapsRequired:1];
    [self addGestureRecognizer:oneTap];
    
    if (unKnowBle == NO) {
        self.alpha = 0.3f;
    }
    return self;
}

- (void)rename:(NSString*)newname {
    self.label.text = newname;
}

- (void)touchMeLe:(UITapGestureRecognizer*)gesture {
//- (void)touchMeLe {
    _canMove = NO;
    BIDCallViewController *callController = [[BIDCallViewController alloc] initWithTitile:_label.text];
    UIButton *buttonClose = [UIButton buttonWithType:UIButtonTypeCustom];
    buttonClose.frame = CGRectMake(0, 0, 100, 60);
    [buttonClose setBackgroundColor:[UIColor darkGrayColor]];
    [buttonClose setTitle:@"呼叫" forState:UIControlStateNormal];
    [buttonClose setTitleColor:[UIColor yellowColor] forState:UIControlStateNormal];
    buttonClose.titleLabel.font = [UIFont boldSystemFontOfSize:30];
    [buttonClose addTarget:self
                    action:@selector(callMe)
          forControlEvents:UIControlEventTouchUpInside];
    [callController.view setFrame:CGRectMake(0, 0, buttonClose.frame.size.width, buttonClose.frame.size.height)];
    [callController.view addSubview:buttonClose];
//    [callController.view sizeToFit];
    
    if (isPad) {
        if (_uiPopover == nil) {
            _uiPopover = [[UIPopoverController alloc]
                          initWithContentViewController:callController];
            _uiPopover.delegate = self;
            _uiPopover.popoverContentSize = CGSizeMake(100, 60);
//            _uiPopover.contentViewController.preferredContentSize = buttonClose.frame.size;
            
        }
//        [_uiPopover presentPopoverFromBarButtonItem:sender
//                           permittedArrowDirections:UIPopoverArrowDirectionAny
//                                           animated:YES];
        [_uiPopover presentPopoverFromRect:self.frame inView:self.superview permittedArrowDirections:UIPopoverArrowDirectionAny animated:YES];
    } else {
        _popover = [[FPPopoverController alloc] initWithViewController:callController];
        _popover.delegate = self;
        _popover.tint = FPPopoverLightGrayTint;
        
        //if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
        {
            //_popover.contentSize = CGSizeMake(CGRectGetWidth(buttonClose.frame)+2, CGRectGetHeight(buttonClose.frame)+2);
            _popover.contentSize = CGSizeMake(120, 130);
        }
        _popover.arrowDirection = FPPopoverArrowDirectionUp;

        //    [popover presentPopoverFromView:self];
    //    CGRect statusBarRect = [[UIApplication sharedApplication] statusBarFrame];
        CGFloat x = _curPoint.x * _scaleNow - _offsetX;
    //    CGFloat y = _curPoint.y * _scaleNow - _offsetY + statusBarRect.size.height+25;//根据观察确定的25
        CGFloat y = _curPoint.y * _scaleNow - _offsetY;
        CGPoint screenPoint = CGPointMake(x, y);
        [_popover presentPopoverFromPoint:screenPoint ifUsePoint:YES];
    }
}

//- (void)touchMeLe:(UITapGestureRecognizer*)gesture {
//    _canMove = NO;
//    BIDCallViewController *callController = [[BIDCallViewController alloc] initWithTitile:_label.text];
//    UIButton *buttonClose = [UIButton buttonWithType:UIButtonTypeCustom];
//    buttonClose.frame = CGRectMake(0, 0, 100, 60);
//    [buttonClose setBackgroundColor:[UIColor darkGrayColor]];
//    [buttonClose setTitle:@"呼叫" forState:UIControlStateNormal];
//    [buttonClose setTitleColor:[UIColor yellowColor] forState:UIControlStateNormal];
//    buttonClose.titleLabel.font = [UIFont boldSystemFontOfSize:30];
//    [buttonClose addTarget:self
//                    action:@selector(callMe)
//          forControlEvents:UIControlEventTouchUpInside];
//    [callController.view setFrame:CGRectMake(0, 0, buttonClose.frame.size.width, buttonClose.frame.size.height)];
//    [callController.view addSubview:buttonClose];
//    //    [callController.view sizeToFit];
//    
//    _popover = [[FPPopoverController alloc] initWithViewController:callController];
//    _popover.delegate = self;
//    _popover.tint = FPPopoverLightGrayTint;
//    
//    //if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
//    {
//        //_popover.contentSize = CGSizeMake(CGRectGetWidth(buttonClose.frame)+2, CGRectGetHeight(buttonClose.frame)+2);
//        _popover.contentSize = CGSizeMake(120, 130);
//    }
//    _popover.arrowDirection = FPPopoverArrowDirectionUp;
//    CGFloat x = self.frame.origin.x - _offsetX;
//    CGFloat y = self.frame.origin.y - _offsetY;
//    CGPoint screenPoint = CGPointMake(x, y);
//    [_popover presentPopoverFromPoint:screenPoint ifUsePoint:YES];
//}

- (void)callMe {
    NSLog(@"Enter %s", __func__);
    _canMove = YES;
    if (isPad) {
        [_uiPopover dismissPopoverAnimated:YES];
    } else {
        [_popover dismissPopoverAnimated:YES];
        [_popover removeObservers];
    //    _popover = nil;
    }
    BIDBLEManager *bleManager = [BIDBLEManager sharedInstance];
    [bleManager soundBuzzer:_uuid];
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

#pragma mark - FPPopoverControllerDelegate
- (void)popoverControllerDidDismissPopover:(FPPopoverController *)popoverController {
    NSLog(@"%s: popoverController = %@", __func__, popoverController);
    _canMove = YES;
}

- (void)presentedNewPopoverController:(FPPopoverController *)newPopoverController
          shouldDismissVisiblePopover:(FPPopoverController*)visiblePopoverController {
    NSLog(@"%s", __func__);
}

#pragma mark - UIPopoverControllerDelegate
//- (void)popoverControllerDidDismissPopover:(UIPopoverController *)popoverController {
//    NSLog(@"%s", __func__);
//    _canMove = YES;
//}

#pragma mark -
- (void)animationDidStart:(CAAnimation *)anim {
//    NSLog(@"%s", __func__);
}

- (void)animationDidStop:(CAAnimation *)anim finished:(BOOL)flag {
//    NSLog(@"%s: anim = %@, flag = %@", __func__, anim, flag?@"YES":@"NO");
    if (flag || !flag) {
        CGPoint currentRect = [[self.layer presentationLayer] position];
        self.layer.position = currentRect;
        
        self.curPoint = CGPointMake(currentRect.x/_scaleNow, currentRect.y/_scaleNow);
        CGFloat r = [BIDConstant distanceFromPointX:_curPoint distanceToPointY:CGPointMake(gcenterX, gcenterY)];
        _curSin = (_curPoint.y - gcenterY) / r;
        _curCos = (_curPoint.x - gcenterX) / r;
//        NSLog(@"currentRect.x = %f, currentRect.y = %f,", currentRect.x, currentRect.y);
//        NSLog(@"self.frame.origin.x = %f, self.frame.origin.y = %f, self.frame.size.width = %f, self.frame.size.height = %f", self.frame.origin.x, self.frame.origin.y, self.frame.size.width, self.frame.size.height);
//        [self setFrame:CGRectMake(currentRect.x, currentRect.y, self.frame.size.width, self.frame.size.height)];
        [self setFrame:CGRectMake(self.frame.origin.x, self.frame.origin.y, self.frame.size.width, self.frame.size.height)];
        [self.layer removeAnimationForKey:@"myArc"];
        self.angleUpdating = NO;
    }
}
@end
