//
//  BIDRadarViewController6.h
//  Phobos2
//
//  Created by Li Xianyu on 13-12-9.
//  Copyright (c) 2013年 Li Xianyu. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BMapKit.h"

@interface BIDRadarViewController6iPad : UIViewController
@property (strong, nonatomic) BMKMapView *customMapView;
-(void) addOneBallForSideViewController;
- (void)showTheMap:(BOOL)flag managedObject:(NSManagedObject *)theManagedObjectPhobos;
//- (void)beginBaiduMapViewUpdate;
//- (void)stopBaiduMapViewUpdate;
- (BOOL)isChecked:(NSString*)uuid;
@end
