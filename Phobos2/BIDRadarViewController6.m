//
//  BIDRadarViewController6.m
//  Phobos2
//
//  Created by Li Xianyu on 13-12-9.
//  Copyright (c) 2013年 Li Xianyu. All rights reserved.
//

#import "BIDRadarViewController6.h"
#import "BIDScrollView.h"
#import "BIDBallLabelView.h"
#import "BIDRadarTableViewController.h"
#import "BIDAddNewPhobosViewController.h"
#import "BIDSideViewController.h"
#import "BIDAppDelegate.h"
#import "BIDConstant.h"
#import "FPPopoverController.h"
#import "BIDBLEManager.h"
#import "BIDSettingViewController.h"
#import <CoreBluetooth/CoreBluetooth.h>

@interface BIDRadarViewController6 () <FPPopoverControllerDelegate,UIScrollViewDelegate, BIDBLEManagerDelegate,CBCentralManagerDelegate>
@property (nonatomic,retain) BIDScrollView *myscrollview;
@property (nonatomic, strong) UIImageView *imageView;
@property (nonatomic, strong) UIImageView *batteryView;
@property (nonatomic, strong) UILabel *temperatureLable;

@property (strong, nonatomic) FPPopoverController *popover;
@property (strong, nonatomic) BIDRadarTableViewController *tableController;
@property (strong, nonatomic) NSTimer *updateTimer;
@property (strong, nonatomic) NSTimer *rssiTimer;

@property (assign, nonatomic) RadarState radarState;
@property (assign, nonatomic) CGRect myRadarBounds;
@property (assign, nonatomic) BOOL pauseBallCanMove;
@property (strong, nonatomic) CBCentralManager *cbCentralManager;
@property (strong, nonatomic) NSMutableDictionary *unknowBLEs;
@property (strong, nonatomic) BIDBLEManager *phobosManager;

- (IBAction)choosePhobos:(id)sender;
@end

@implementation BIDRadarViewController6 {
    CGFloat minScaleHeight;
    CGFloat minScaleWidth;
    BOOL IAmVisible;
    BOOL onlyShowTemperature;
}

- (id)initWithCoder:(NSCoder *)aDecoder {
    NSLog(@"%s", __func__);
    self = [super initWithCoder:aDecoder];
    if (self) {
        _phobosManager = [BIDBLEManager sharedInstance];
        [_phobosManager addDelegateObserver:self];
        NSLog(@"_phobosManager = %@", _phobosManager);
        NSNotificationCenter *center = [NSNotificationCenter defaultCenter];
        [center addObserver:self selector:@selector(applicationDidEnterBackground) name:UIApplicationDidEnterBackgroundNotification object:nil];
    }
    return self;
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)initBLE {
    NSLog(@"%s", __func__);
    NSLog(@"_phobosManager = %@, _unknowBLEs = %@", _phobosManager, _unknowBLEs);
    [_phobosManager initBLEManager];

    if (_unknowBLEs == nil) {
        _unknowBLEs = [[NSMutableDictionary alloc] initWithCapacity:1];
    }
    NSLog(@"_radarState = %d", _radarState);
    if (_radarState == RadarStateAllBLE) {//对于未知BLE设备，只能通过设备发出的广播中的RSSI来确定远近
        [_phobosManager removeDelegateObserver:self];
        NSDictionary *optionsdict = @{CBCentralManagerScanOptionAllowDuplicatesKey : [NSNumber numberWithBool:YES]};
        if (_cbCentralManager == nil) {
            _cbCentralManager = [[CBCentralManager alloc] initWithDelegate:self queue:nil];
        } else {
            [_cbCentralManager scanForPeripheralsWithServices:nil options:optionsdict];
        }
        [self letUsHaveAZoom];
    } else {
        [_phobosManager addDelegateObserver:self];
        if (_radarState == RadarStateAllPair) {
            for (NSString *uuid in _tableController.uuids) {
                [_phobosManager connectPhobos:uuid];
            }
        } else if (_radarState == RadarStateSelected) {
            for (int i = 0; i < _tableController.uuids.count; i++) {
                if ([_tableController.ifChecked[i] boolValue]) {
                    [_phobosManager connectPhobos:_tableController.uuids[i]];
                }
            }
        }
    }
}

- (void)viewDidLoad
{
    NSLog(@"%s", __func__);
    [super viewDidLoad];
    
	// Do any additional setup after loading the view.
    CGRect appRect = [UIScreen mainScreen].applicationFrame;
    NSLog(@"appRect: x = %f, y = %f, width = %f, height = %f", appRect.origin.x, appRect.origin.y, appRect.size.width, appRect.size.height);
    CGRect statusBarRect = [[UIApplication sharedApplication] statusBarFrame];
    NSLog(@"statusBarRect: x = %f, y = %f, width = %f, height = %f", statusBarRect.origin.x, statusBarRect.origin.y, statusBarRect.size.width, statusBarRect.size.height);
    //    _myRadarBounds = CGRectMake(0, statusBarRect.size.height+self.navigationController.navigationBar.bounds.size.height, self.view.bounds.size.width, self.view.bounds.size.height - self.tabBarController.tabBar.bounds.size.height - self.navigationController.navigationBar.bounds.size.height - statusBarRect.size.height);
    NSLog(@"myRadarBounds: x = %f, y = %f, width = %f, height = %f", _myRadarBounds.origin.x, _myRadarBounds.origin.y, _myRadarBounds.size.width, _myRadarBounds.size.height);
    _myRadarBounds = CGRectMake(0, statusBarRect.size.height, self.view.bounds.size.width, self.view.bounds.size.height - statusBarRect.size.height);
    NSLog(@"myRadarBounds: x = %f, y = %f, width = %f, height = %f", _myRadarBounds.origin.x, _myRadarBounds.origin.y, _myRadarBounds.size.width, _myRadarBounds.size.height);
    _myscrollview = [[BIDScrollView alloc] initWithFrame:_myRadarBounds];
    
    UIImageView *imageViewBackground = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"1"]];
    NSLog(@"myimage.image.size.width = %f, myimage.image.size.height = %f", imageViewBackground.image.size.width, imageViewBackground.image.size.height);
    [imageViewBackground setFrame:CGRectMake(0, 0, imageViewBackground.image.size.width, imageViewBackground.image.size.height)];
    [_myscrollview addSubview:imageViewBackground];
    _imageView = imageViewBackground;
    
    _myscrollview.directionalLockEnabled = NO; //只能一个方向滑动
    _myscrollview.pagingEnabled = NO; //是否翻页
    _myscrollview.backgroundColor = [UIColor darkGrayColor];
    _myscrollview.showsVerticalScrollIndicator = YES; //垂直方向的滚动指示
    _myscrollview.indicatorStyle = UIScrollViewIndicatorStyleWhite;//滚动指示的风格
    _myscrollview.showsHorizontalScrollIndicator = YES;//水平方向的滚动指示
    _myscrollview.delegate = self;
    
    minScaleWidth = self.view.frame.size.width / imageViewBackground.image.size.width;
    NSLog(@"self.view.frame.size.width = %f, myimage.image.size.width = %f", self.view.frame.size.width, imageViewBackground.image.size.width);
    minScaleHeight = self.view.frame.size.height / imageViewBackground.image.size.width;
    NSLog(@"minScaleWidth = %f, minScaleHeight = %f", minScaleWidth, minScaleHeight);
    [_myscrollview setMinimumZoomScale:minScaleWidth];
    [_myscrollview setMaximumZoomScale:1.0];
    [_myscrollview setZoomScale:minScaleHeight animated:YES];
    
    [self.view addSubview:_myscrollview];
    [self.myscrollview setContentOffset:CGPointMake(126.5f, 81.0f)];//根据trace观察定下来的坐标
    
    UITapGestureRecognizer * oneTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(oneTaple:)];
    [oneTap setNumberOfTapsRequired:1];
    [self.myscrollview addGestureRecognizer:oneTap];
    
    _batteryView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"battery8.png"]];
    [_batteryView setFrame:CGRectMake(appRect.size.width-39, appRect.size.height+20-55, 45, 55)];
    [self.view addSubview:_batteryView];
    _batteryView.alpha = 0.0f;
    
    _temperatureLable = [[UILabel alloc] initWithFrame:CGRectMake(10, appRect.size.height+20-35, 65, 55)];
    _temperatureLable.text = @"温度：";
    _temperatureLable.textColor = [UIColor whiteColor];
    _temperatureLable.alpha = 0.0;
    [self.view addSubview:_temperatureLable];
}

- (void)viewWillAppear:(BOOL)animated
{
    NSLog(@"%s", __func__);
    gInRadarView = true;
    [super viewWillAppear:animated];
    NSNotificationCenter *center = [NSNotificationCenter defaultCenter];
    [center addObserver:self selector:@selector(applicationDidBecomeActive) name:UIApplicationDidBecomeActiveNotification object:nil];
    [center addObserver:self selector:@selector(applicationWillEnterForeground) name:UIApplicationWillEnterForegroundNotification object:nil];
    [self addBallOnView];
    [self initBLE];
    
    [self performSelector:@selector(drawRadar) withObject:nil afterDelay:0.5];
    self.updateTimer = [NSTimer scheduledTimerWithTimeInterval:4.0
                                                        target:self
                                                      selector:@selector(drawRadar)
                                                      userInfo:nil
                                                       repeats:YES];
}

static bool gInRadarView = false;
- (void)viewDidAppear:(BOOL)animated {
    NSLog(@"%s", __func__);
    [super viewDidAppear:animated];
    [self hideTabBar:YES];
    _pauseBallCanMove = YES;
    IAmVisible = YES;
    
}

- (void)viewWillDisappear:(BOOL)animated {
    NSLog(@"%s", __func__);
    [super viewWillDisappear:animated];
    [NSObject cancelPreviousPerformRequestsWithTarget:self selector:@selector(hideTabBarByPerform:) object:[NSNumber numberWithBool:YES]];
//    [NSObject cancelPreviousPerformRequestsWithTarget:self selector:@selector(timeoutScan) object:nil];
    [self hideTabBar:NO];
    gtabNavFlag = YES;
    if (_cbCentralManager) {
        [_cbCentralManager stopScan];
    }
    NSNotificationCenter *center = [NSNotificationCenter defaultCenter];
    [center removeObserver:self name:UIApplicationDidBecomeActiveNotification object:nil];
    [center removeObserver:self name:UIApplicationWillEnterForegroundNotification object:nil];
    [_phobosManager stopScanPeripherals];
}

- (void)viewDidDisappear:(BOOL)animated
{
    NSLog(@"%s", __func__);
    [super viewDidDisappear:animated];
    IAmVisible = NO;
    gInRadarView = false;
    [self.updateTimer invalidate];
    self.updateTimer = nil;
    [self.rssiTimer invalidate];
    self.rssiTimer = nil;
    _batteryView.alpha = 0.0;
    _temperatureLable.alpha = 0.0;
    for (UIView *aView in self.myscrollview.subviews) {
        if ([aView isKindOfClass:[BIDBallLabelView class]]) {
            NSLog(@"remove one");
            [aView removeFromSuperview];
        }
    }
}

+ (BOOL)isInRadarView {
    if (gInRadarView == true) {
        return YES;
    } else {
        return NO;
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark -
- (void)renamePhobos:(NSMutableDictionary*)dictionary {
    NSLog(@"%s", __func__);
    BIDBLEManager *bleManager = [BIDBLEManager sharedInstance];
    [bleManager changePhobosName:dictionary];
}

- (void)readBattery:(NSString*)uuidBattery {
    NSLog(@"%s", __func__);
    BIDBLEManager *bleManager = [BIDBLEManager sharedInstance];
    [bleManager readBattery:uuidBattery];
}

- (void)readTemperature:(NSString*)uuid {
    BIDBLEManager *bleManager = [BIDBLEManager sharedInstance];
    [bleManager readTemperature:uuid];
}

- (void)stopCheckRSSI {
    NSLog(@"%s", __func__);
    [self.rssiTimer invalidate];
    self.rssiTimer = nil;
}

- (void)beginCheckRSSI {
    NSLog(@"Enter %s", __func__);
    if (_rssiTimer) {
        [_rssiTimer invalidate];
        _rssiTimer = nil;
    }
    self.rssiTimer = [NSTimer scheduledTimerWithTimeInterval:1.5
                                                      target:self
                                                    selector:@selector(beginRssiUpdate)
                                                    userInfo:nil
                                                     repeats:YES];
    NSLog(@"Leave %s", __func__);
}

- (void)beginRssiUpdate {
    NSLog(@"%s, _radarState = %d", __func__, _radarState);
    if (_radarState == RadarStateAllPair) {
        [_phobosManager readRSSI];
    }
    else if (_radarState == RadarStateSelected) {
        for (int i = 0; i < _tableController.ifChecked.count; i++) {
            if ([_tableController.ifChecked[i] boolValue]) {
                [_phobosManager readRSSIwithUUID:_tableController.uuids[i]];
            }
        }
    }
}

//cos(π/2-α) = sinα
- (CGFloat)getRandomYFromX:(CGFloat)x {
    if (x > 950.0) {
        x = 950.0f;
    } else if (x < 50.0) {
        x = 50.0f;
    }
    CGFloat y;
    CGFloat maxY;
    while (YES) {
        y = arc4random() % 1000;
        maxY = sqrtf((gradius * gradius) - (ABS((x-gcenterX) * (x - gcenterX))));
        if (y >= gcenterY - maxY && y <= gcenterY + maxY) {
            break;
        } else {
            NSInteger iY = maxY;
            y = arc4random() % (iY*2);
            y += gcenterY - maxY;
            break;
        }
    }
    return y;
}

- (void)letUsHaveAZoom {
    NSLog(@"%s", __func__);
    static BOOL flag = YES;
    CGFloat tmpScale;
    if (flag) {
        NSLog(@"flag = YES");
        tmpScale = 0.01f;
    } else {
        NSLog(@"flag = NO");
        tmpScale = -0.01f;
    }
    flag = !flag;
    if (_myscrollview.minimumZoomScale == _myscrollview.zoomScale) {
        [self.myscrollview setZoomScale:self.myscrollview.zoomScale+0.01f animated:YES];
    } else {
        [self.myscrollview setZoomScale:self.myscrollview.zoomScale+tmpScale animated:YES];
    }
    NSLog(@"minimumZoomScale = %f, zoomScale = %f", _myscrollview.minimumZoomScale, _myscrollview.zoomScale);
}

- (void)addBallOnView {
    NSLog(@"%s", __func__);
    _tableController = [[BIDRadarTableViewController alloc] init];
    [_tableController getCoreData];
    if ([BIDSideViewController comeFromMe]) {//只要能从BIDSideViewController过来，说明肯定曾经配对过一个Phobos
        NSLog(@"YES, I come from 'BIDSideViewController'");
        if ([BIDSideViewController onlyShowTemperature]) {
            onlyShowTemperature = YES;
        }
        else {
            onlyShowTemperature = NO;
        }
        NSLog(@"onlyShowTemperature = %@", onlyShowTemperature ? @"YES" : @"NO");
        _radarState = RadarStateSelected;
        [_tableController setSectionNumber:2];
        [BIDSideViewController resetComeToRadar];
        _tableController.comeFromSideViewController = YES;
        
        NSDictionary *aDict = [self getBallTextUuid];
        NSLog(@"ballText = %@", aDict[@"name"]);
        CGFloat randomX = arc4random() % 1000;
        CGFloat randomY = [self getRandomYFromX:randomX];
        [self.myscrollview addABallLabelWithLabel:aDict[@"name"] UUID:aDict[@"uuid"] pointOnMe:CGPointMake(randomX, randomY)];
    } else {
        _tableController.comeFromSideViewController = NO;
        
        NSLog(@"_tableController.dwarves.count = %d", _tableController.dwarves.count);
        if (_tableController.dwarves.count == 0) {
            //显示周边所有未配对蓝牙BLE
            _radarState = RadarStateAllBLE;
            [_tableController setSectionNumber:0];
            return;
        }
        
        NSInteger section = [_tableController getSectionNumber];
        NSLog(@"section = %d", section);
        if (section == 0) {
            //显示周边所有未配对蓝牙BLE
            _unknowBLEs = nil;
            _radarState = RadarStateAllBLE;
            return;
        }
        
        CGFloat randomX = arc4random() % 1000;
        CGFloat randomY = [self getRandomYFromX:randomX];
        for (int i = 0; i < _tableController.dwarves.count; i++) {
            if (section == 2) {
                if (YES == [_tableController.ifChecked[i] boolValue]) {
                    [_myscrollview addABallLabelWithLabel:_tableController.dwarves[i] UUID:_tableController.uuids[i] pointOnMe:CGPointMake(randomX, randomY)];
                }
            } else if (section == 1) {
                [_myscrollview addABallLabelWithLabel:_tableController.dwarves[i] UUID:_tableController.uuids[i] pointOnMe:CGPointMake(randomX, randomY)];
            }
            randomX = arc4random() % 1000;
            randomY = [self getRandomYFromX:randomX];
        }
        if (section == 2) {
            _radarState = RadarStateSelected;
        } else if (section == 1) {
            _radarState = RadarStateAllPair;
        }
    }
    [self letUsHaveAZoom];
}

- (void)reAddBallsOnView {
    NSLog(@"%s", __func__);
    _batteryView.alpha = 0.0;
    _temperatureLable.alpha = 0.0;
    for (UIView *aView in self.myscrollview.subviews) {
        if ([aView isKindOfClass:[BIDBallLabelView class]]) {
            NSLog(@"remove one");
            [aView removeFromSuperview];
        }
    }
    
    NSInteger section = [_tableController getSectionNumber];
    NSLog(@"section = %d", section);
    if (section == 0) {
        _unknowBLEs = nil;
        _radarState = RadarStateAllBLE;
        [self stopCheckRSSI];
        [self initBLE];
        return;
    }
    CGFloat randomX = arc4random() % 1000;
    CGFloat randomY = [self getRandomYFromX:randomX];
    NSLog(@"_tableController.dwarves.count = %d", _tableController.dwarves.count);
    for (int i = 0; i < _tableController.dwarves.count; i++) {
        if (section == 2) {
            if (YES == [_tableController.ifChecked[i] boolValue]) {
                [self.myscrollview addABallLabelWithLabel:_tableController.dwarves[i] UUID:_tableController.uuids[i] pointOnMe:CGPointMake(randomX, randomY)];
            }
        } else if (section == 1) {
            [_myscrollview addABallLabelWithLabel:_tableController.dwarves[i] UUID:_tableController.uuids[i] pointOnMe:CGPointMake(randomX, randomY)];
        }
        randomX = arc4random() % 1000;
        randomY = [self getRandomYFromX:randomX];
    }
    if (section == 2) {
        _radarState = RadarStateSelected;
    } else if (section == 1) {
        _radarState = RadarStateAllPair;
    }
    [self letUsHaveAZoom];
    [self initBLE];
    [self beginCheckRSSI];
}

- (NSDictionary *)getBallTextUuid {
    NSLog(@"%s", __func__);
    for (int i = 0; i < _tableController.ifChecked.count; i++) {
        if ([_tableController.ifChecked[i] boolValue]) {
            NSDictionary *adic = @{@"name" : _tableController.dwarves[i],
                                   @"uuid" : _tableController.uuids[i]};
            return adic;
        }
    }
    return nil;
}

- (NSString *)getNameByUuid:(NSString*)uuid {
    NSLog(@"%s", __func__);
    for (int i = 0; i < _tableController.ifChecked.count; i++) {
        if ([_tableController.uuids[i] isEqualToString:uuid]) {
            NSLog(@"got name : %@", _tableController.dwarves[i]);
            return _tableController.dwarves[i];
        }
    }
    return nil;
}

- (NSInteger)getAllCheckCount {
    NSInteger section = [_tableController getSectionNumber];
    NSInteger count = 0;
    if (section == 1) {
        count =  _tableController.uuids.count;
    } else if (section == 2) {
        for (NSNumber *aNumber in _tableController.ifChecked) {
            if ([aNumber boolValue]) {
                count++;
            }
        }
        
    }
    NSLog(@"%s: count = %d", __func__, count);
    return count;
}

/** 检查uuid是否正在显示
 */
- (BOOL)isChecked:(NSString*)uuid {
    NSInteger section = [_tableController getSectionNumber];

    if (section == 1) {
        return YES;
    } else if (section == 2) {
        for (int i = 0; i < _tableController.ifChecked.count; i++) {
            if ([_tableController.ifChecked[i] boolValue]) {
                if ([_tableController.uuids[i] isEqualToString:uuid]) {
                    return YES;
                }
            }
        }
    }
    return NO;
}

- (void)drawRadar {
    static NSInteger iCheckBallState = 0;
//    NSLog(@"%s", __func__);
    if (self.myscrollview) {
        [self.myscrollview drawRadar];
    }
    iCheckBallState++;
    if (iCheckBallState >= 3) {
        [self checkBallState];
        iCheckBallState = 0;
    }
}

- (void)checkBallState {
//    NSLog(@"%s: _radarState = %d", __func__, _radarState);
    if (_radarState == RadarStateAllBLE) {
        return;
    }
    if (_radarState == RadarStateAllPair) {
        for (NSString *uuid in _tableController.uuids) {
            if ([_phobosManager isConnected:uuid]) {
                [_myscrollview setABallAlpha:1.0 UUID:uuid flash:NO];
            } else {
                [_myscrollview setABallAlpha:0.3 UUID:uuid flash:NO];
            }
        }
    } else {
        for (int i = 0; i < _tableController.uuids.count; i++) {
            if ([_tableController.ifChecked[i] boolValue]) {
                if ([_phobosManager isConnected:_tableController.uuids[i]]) {
                    [_myscrollview setABallAlpha:1.0 UUID:_tableController.uuids[i] flash:NO];
                } else {
                    [_myscrollview setABallAlpha:0.3 UUID:_tableController.uuids[i] flash:NO];
                }
            }
        }
    }
}

- (void)startBackgroundTask {
    NSLog(@"%@", NSStringFromSelector(_cmd));
    UIApplication *app = [UIApplication sharedApplication];
    __block UIBackgroundTaskIdentifier taskId;
    taskId = [app beginBackgroundTaskWithExpirationHandler:^{
        NSLog(@"Background task#%d ran out of time and was terminated.", taskId);
        NSLog(@"哦，彻底地离开了后台了……taskId = %d", taskId);
        [app endBackgroundTask:taskId];
        
    }];
    if (taskId == UIBackgroundTaskInvalid) {
        NSLog(@"Failed to start background task! taskId=%d", UIBackgroundTaskInvalid);
        return;
    } else {
        NSLog(@"taskId = %d, UIBackgroundTaskInvalid = %d", taskId, UIBackgroundTaskInvalid);
    }
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        NSLog(@"Starting background task#%d with %f seconds remaining", taskId, app.backgroundTimeRemaining);
        if (_cbCentralManager) {
            [_cbCentralManager stopScan];
        }
        [self stopCheckRSSI];
        [_phobosManager appEnterBackground];
//        for (int i = 1; i < 10; i++) {
//            [NSThread sleepForTimeInterval:1.0];
//            NSLog(@" %d seconds passed... and task#%d with %f seconds remaining", i, taskId, app.backgroundTimeRemaining);
//        }
        NSLog(@"Finishing background task#%d with %f seconds remaining", taskId, app.backgroundTimeRemaining);
        [app endBackgroundTask:taskId];
    });
}

- (void)applicationDidEnterBackground {
    NSLog(@"%s", __func__);

    [self.updateTimer invalidate];
    self.updateTimer = nil;
    //[self startBackgroundTask];
    if (_cbCentralManager) {
        [_cbCentralManager stopScan];
    }
    [self stopCheckRSSI];
    [_phobosManager appEnterBackground];
}

- (void)applicationWillEnterForeground {
    NSLog(@"%s", __func__);
    if (!(_radarState == RadarStateAllBLE)) {
        if (_myscrollview) {
            for (BIDBallLabelView *aBallView in _myscrollview.subviews) {
                if ([aBallView isKindOfClass:[BIDBallLabelView class]]) {
                    aBallView.alpha = 0.3f;
                }
            }
        }
    }
    self.updateTimer = [NSTimer scheduledTimerWithTimeInterval:4.0
                                                        target:self
                                                      selector:@selector(drawRadar)
                                                      userInfo:nil
                                                       repeats:YES];
}

- (void)applicationDidBecomeActive {
    NSLog(@"%s", __func__);
    [self initBLE];
    [self hideTabBar:NO];
    [self performSelector:@selector(hideTabBarByPerform:) withObject:[NSNumber numberWithBool:YES] afterDelay:2.0f];
}

#pragma mark - UIScrollViewDelegate
//完成拖拽
- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate
{
//    NSLog(@"scrollViewDidEndDragging, decelerate = %d", decelerate);
    if (decelerate) {
        return;
    } else {
        [self updateOffset:scrollView];
    }
}

//减速停止了时执行，手触摸时执行执行
- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView
{
//    NSLog(@"scrollViewDidEndDecelerating");
    [self updateOffset:scrollView];
}

- (void)scrollViewDidZoom:(UIScrollView *)scrollView {
//    NSLog(@"scrollViewDidZoom:  zoomScale = %f", scrollView.zoomScale);
    [self.myscrollview setCurrentScale:scrollView.zoomScale];
    [self.myscrollview performSelectorOnMainThread:@selector(updateZoom)
                                        withObject:nil
                                     waitUntilDone:NO];
}

//设置放大缩小的视图，要是uiscrollview的subview
- (UIView *)viewForZoomingInScrollView:(UIScrollView *)scrollView
{
//    NSLog(@"viewForZoomingInScrollView");
    return _imageView;
}

//完成放大缩小时调用
- (void)scrollViewDidEndZooming:(UIScrollView *)scrollView withView:(UIView *)view atScale:(float)scale
{
//    NSLog(@"view=%@", view);
    NSLog(@"scale = %f, minScaleHeight = %f", scale, minScaleHeight);
    [scrollView setZoomScale:scale animated:YES];
    [self updateOffset:scrollView];
}

#if 0
//滚动动画停止时执行,代码改变时出发,也就是setContentOffset改变时
- (void)scrollViewDidEndScrollingAnimation:(UIScrollView *)scrollView
{
//    NSLog(@"scrollViewDidEndScrollingAnimation");
}

//将开始降速时
- (void)scrollViewWillBeginDecelerating:(UIScrollView *)scrollView
{
//    NSLog(@"scrollViewWillBeginDecelerating");
}

//只要滚动了就会触发
- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
}

//开始拖拽视图
- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView
{
}

//如果你不是完全滚动到滚轴视图的顶部，你可以轻点状态栏，那个可视的滚轴视图会一直滚动到顶部，那是默认行为，你可以通过该方法返回NO来关闭它
- (BOOL)scrollViewShouldScrollToTop:(UIScrollView *)scrollView
{
    NSLog(@"scrollViewShouldScrollToTop");
    return NO;
}

- (void)scrollViewDidScrollToTop:(UIScrollView *)scrollView
{
    NSLog(@"scrollViewDidScrollToTop");
}
#endif

#pragma mark -
- (void)updateOffset:(UIScrollView *)scrollView {
    for (BIDBallLabelView *aBall in self.myscrollview.subviews) {
        if ([aBall isKindOfClass:[BIDBallLabelView class]]) {
            aBall.offsetX = scrollView.contentOffset.x;
            aBall.offsetY = scrollView.contentOffset.y;
        }
        else {
            
        }
    }
}

#pragma mark -
static BOOL gtabNavFlag = YES;
-(void)oneTaple:(UITapGestureRecognizer*)gesture {
//    NSLog(@"%s: gtabNavFlag = %@", __func__, gtabNavFlag ? @"YES" : @"NO");
    if (gtabNavFlag) {
        [NSObject cancelPreviousPerformRequestsWithTarget:self selector:@selector(hideTabBarByPerform:) object:[NSNumber numberWithBool:YES]];
        [self hideTabBar:NO];
        [self performSelector:@selector(hideTabBarByPerform:) withObject:[NSNumber numberWithBool:YES] afterDelay:5];
    } else {
        [NSObject cancelPreviousPerformRequestsWithTarget:self selector:@selector(hideTabBarByPerform:) object:[NSNumber numberWithBool:YES]];
        [self hideTabBar:YES];
    }
    gtabNavFlag = !gtabNavFlag;
}

- (void)hideTabBar:(BOOL)hidden {
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDuration:1.0];
    for(UIView *view in self.tabBarController.view.subviews)
    {
        if([view isKindOfClass:[UITabBar class]])
        {
            if (hidden) {
                view.alpha = 0.0f;
                
            } else {
                view.alpha = 1.0f;
                self.navigationController.navigationBar.alpha = 1.0f;
            }
        }
    }
    if (hidden) {
        self.navigationController.navigationBar.alpha = 0.0f;
    } else {
        self.navigationController.navigationBar.alpha = 1.0f;
    }
    [UIView commitAnimations];
}

- (void)hideTabBarByPerform:(BOOL)hidden {
    gtabNavFlag = YES;
    [self hideTabBar:hidden];
}

#pragma mark -
- (IBAction)choosePhobos:(id)sender {
    NSLog(@"%s: sender = %@", __func__, sender);
    _pauseBallCanMove = NO;
    if (_cbCentralManager) {
        [_cbCentralManager stopScan];
    }
    if ([BIDAddNewPhobosViewController ifAddAPhobos] == YES) {
        _tableController = [[BIDRadarTableViewController alloc] init];
        [BIDAddNewPhobosViewController resetIfAddAPhobosFlag];
        [_tableController getCoreData];
    } else if ([BIDSideViewController ifDeleteAPhobos] == YES) {
        _tableController = [[BIDRadarTableViewController alloc] init];
        [BIDSideViewController resetIfDeleteAPhobosFlag];
        [_tableController getCoreData];
    }
    if (_tableController == nil) {
        _tableController = [[BIDRadarTableViewController alloc] init];
        [_tableController getCoreData];
    }
    
    _popover = [[FPPopoverController alloc] initWithViewController:_tableController];
    _popover.delegate = self;
    _popover.tint = FPPopoverLightGrayTint;
    
    //    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
    {
        _popover.contentSize = CGSizeMake(200, 400);
    }
    _popover.arrowDirection = FPPopoverArrowDirectionUp;
    
    //    [_popover presentPopoverFromView:sender];
    [_popover presentPopoverFromPoint:CGPointMake(290, 28) ifUsePoint:YES];
}

#pragma mark - FPPopoverControllerDelegate
- (void)popoverControllerDidDismissPopover:(FPPopoverController *)popoverController {
    NSLog(@"%s", __func__);
    [_tableController savePhobos];
    [_popover removeObservers];
    //    _popover = nil;
    
    [self reAddBallsOnView];
    _pauseBallCanMove = YES;
}

- (void)presentedNewPopoverController:(FPPopoverController *)newPopoverController
          shouldDismissVisiblePopover:(FPPopoverController*)visiblePopoverController {
    NSLog(@"%s", __func__);
}

#pragma mark - CBCentralManagerDelegate
- (void)centralManagerDidUpdateState:(CBCentralManager *)central {
    NSLog(@"%s: state = %d", __func__, central.state);
    switch (central.state) {
        case CBCentralManagerStatePoweredOn: {
            NSLog(@"CBCentralManagerStatePoweredOn, _radarState = %d", _radarState);
            if (_radarState == RadarStateAllBLE) {//显示周边所有未知蓝牙BLE设备
                NSDictionary *optionsdict = @{CBCentralManagerScanOptionAllowDuplicatesKey : [NSNumber numberWithBool:YES]};
                [_cbCentralManager scanForPeripheralsWithServices:nil options:optionsdict];
            }
            break;
        }
        case CBCentralManagerStatePoweredOff:
            NSLog(@"CBCentralManagerStatePoweredOff");
            break;
        case CBCentralManagerStateResetting:
            NSLog(@"CBCentralManagerStateResetting");
            break;
        case CBCentralManagerStateUnauthorized:
            NSLog(@"CBCentralManagerStateUnauthorized");
            break;
        case CBCentralManagerStateUnknown:
            NSLog(@"CBCentralManagerStateUnknown");
            break;
        case CBCentralManagerStateUnsupported:
            NSLog(@"CBCentralManagerStateUnsupported");
            break;
        default:
            break;
    }
}

- (void)centralManager:(CBCentralManager *)central didDiscoverPeripheral:(CBPeripheral *)peripheral advertisementData:(NSDictionary *)advertisementData RSSI:(NSNumber *)RSSI {
    NSLog(@"%s: _radarState = %d", __func__, _radarState);
    NSLog(@"Did discover peripheral : %@, rssi: %@, UUID: %@, advertisementData: %@", peripheral, RSSI, peripheral.identifier.UUIDString, advertisementData);
    if (_radarState == RadarStateAllBLE) {
        if ([_unknowBLEs objectForKey:peripheral.identifier.UUIDString] == nil) {
//            for (NSString *uuid in _tableController.uuids) {
//                if ([uuid isEqualToString:peripheral.identifier.UUIDString]) {
//                    NSLog(@"%@ alreay in Phobos array, so just return.", uuid);
//                    return;
//                }
//            }
            NSLog(@"哦，发现了一个新的未知BLE设备！");
            if (peripheral.name) {
                NSString *theName = [self getNameByUuid:peripheral.identifier.UUIDString];
                NSLog(@"theName = %@", theName);
                if (theName) {
                    [_unknowBLEs setObject:theName forKey:peripheral.identifier.UUIDString];
                }
                else {
                    [_unknowBLEs setObject:peripheral.name forKey:peripheral.identifier.UUIDString];
                }
            }
            else {
                [_unknowBLEs setObject:@"未知" forKey:peripheral.identifier.UUIDString];
            }
            CGFloat randomX = arc4random() % 1000;
            CGFloat randomY = [self getRandomYFromX:randomX];
            NSLog(@"_tableController.dwarves.count = %d", _tableController.dwarves.count);
            
            [self.myscrollview addABallLabelWithLabel:_unknowBLEs[peripheral.identifier.UUIDString] UUID:peripheral.identifier.UUIDString pointOnMe:CGPointMake(randomX, randomY) unKnowBle:YES];
            
            [self letUsHaveAZoom];
            
            NSDictionary *aDict = @{@"uuid" : peripheral.identifier.UUIDString,
                                    @"rssi" : RSSI};
            [self.myscrollview performSelectorOnMainThread:@selector(testBLErssi:) withObject:aDict waitUntilDone:NO];
        } else {
            NSDictionary *aDict = @{@"uuid" : peripheral.identifier.UUIDString,
                                    @"rssi" : RSSI};
            [self.myscrollview performSelectorOnMainThread:@selector(testBLErssi:) withObject:aDict waitUntilDone:NO];
        }
    }
}

#pragma mark - BIDBLEManagerDelegate
- (void)didConnectPeripheral:(NSString *)uuid {
    NSLog(@"%s", __func__);
    if (IAmVisible && (![BIDAppDelegate isBackground])) {
#if defined(RU_WEI_USE_IBEACON)
        BIDBLEManager *bleManager = [BIDBLEManager sharedInstance];
        if (![bleManager isConnected:uuid]) {
            [bleManager performSelector:@selector(connectPhobos:) withObject:uuid afterDelay:1.0];
            return;
        }
#endif
        [self beginCheckRSSI];
        [_myscrollview setABallAlpha:1.0f UUID:uuid flash:YES];
        if (1 == [self getAllCheckCount]) {
            if (onlyShowTemperature) {
                [self readTemperature:uuid];
            }
            else {
                [self readBattery:uuid];
            }
            
            NSString *name = [self getNameByUuid:uuid];
            NSDictionary *aDictionary = @{@"uuid" : uuid,
                                          @"name" : name};
            [self performSelector:@selector(renamePhobos:) withObject:aDictionary afterDelay:0.1];
        }
    }
}

- (void)didFailToConnectPeripheral:(NSString *)uuid {
    NSLog(@"%s", __func__);
    if (IAmVisible && (![BIDAppDelegate isBackground])) {
        [_myscrollview setABallAlpha:0.3 UUID:uuid flash:NO];
    }
}

- (void)didDisconnectPeripheral:(NSString *)uuid {
    NSLog(@"%s: IAmVisible = %@", __func__, IAmVisible?@"YES":@"NO");
    if (IAmVisible && (![BIDAppDelegate isBackground])) {
        [_myscrollview setABallAlpha:0.3 UUID:uuid flash:NO];
        NSLog(@"OH, app is not in background!!!");
        if ([self isChecked:uuid]) {
            NSLog(@"oh, let's reconnect : %@", uuid);
            [_phobosManager connectPhobos:uuid];
        }
    }
}

- (void)peripheralDidUpdateRSSI:(NSString *)uuid RSSI:(NSNumber *)rssi {
//    NSLog(@"%s", __func__);
//    NSLog(@"RSSI = %@", rssi);
    if (IAmVisible && (![BIDAppDelegate isBackground])) {
        NSDictionary *aDict = @{@"uuid" : uuid,
                                @"rssi" : rssi};
        [self.myscrollview performSelectorOnMainThread:@selector(testBLErssi:) withObject:aDict waitUntilDone:NO];
    }
}

//能走到这里，说明当前雷达界面，有且仅显示一个Phobos
- (void)didUpdateBattery:(float)batteryLevel UUID:(NSString *)uuid {
    NSLog(@"%s", __func__);
    if (![self isChecked:uuid]) {//如果不是当前正在显示的Phobos，则返回
        return;
    }
    
    NSString *imageName;
    if (batteryLevel > 90 && batteryLevel <= 100) {
        imageName = @"battery8.png";
    } else if (batteryLevel > 80 && batteryLevel <= 90) {
        imageName = @"battery7.png";
    } else if (batteryLevel > 70 && batteryLevel <= 80) {
        imageName = @"battery6.png";
    } else if (batteryLevel > 60 && batteryLevel <= 70) {
        imageName = @"battery5.png";
    } else if (batteryLevel > 50 && batteryLevel <= 60) {
        imageName = @"battery4.png";
    } else if (batteryLevel > 40 && batteryLevel <= 50) {
        imageName = @"battery3.png";
    } else if (batteryLevel > 30 && batteryLevel <= 40) {
        imageName = @"battery2.png";
    } else if (batteryLevel > 20 && batteryLevel <= 30) {
        imageName = @"battery1.png";
    } else if (batteryLevel > 10 && batteryLevel <= 20) {
        imageName = @"battery0.png";
    } else {
        imageName = @"battery0.png";
    }
    [_batteryView setImage:[UIImage imageNamed:imageName]];
    [UIView animateWithDuration:1.9 animations:^{
        _batteryView.alpha = 1.0;
    }];
}

- (void)didUpdateTemperature:(float)temperature {
    NSLog(@"%s: temperature = %f", __func__, temperature);
    if ([BIDSettingViewController getWendu] == 1) {
        _temperatureLable.text = [NSString stringWithFormat:@"%.1f ", temperature];
        _temperatureLable.text = [_temperatureLable.text stringByAppendingString:@"摄氏度"];
    } else {
        _temperatureLable.text = [NSString stringWithFormat:@"%.1f ", temperature];
        _temperatureLable.text = [_temperatureLable.text stringByAppendingString:@"华氏度"];
    }
    [_temperatureLable sizeToFit];
    [UIView animateWithDuration:1.0 animations:^{
        _temperatureLable.alpha = 1.0;
    }];
}
@end
