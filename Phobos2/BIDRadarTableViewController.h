//
//  BIDRadarTableViewController.h
//  Phobos2
//
//  Created by Li Xianyu on 13-11-23.
//  Copyright (c) 2013年 Li Xianyu. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BIDRadarTableViewController : UIViewController <UITableViewDataSource, UITableViewDelegate>
@property (assign, nonatomic) BOOL comeFromSideViewController;
@property (strong, nonatomic) NSMutableArray *dwarves;
@property (strong, nonatomic) NSMutableArray *uuids;
@property (strong, nonatomic) NSMutableArray *ifChecked;

- (void)savePhobos;
- (NSInteger)getCoreData;
- (NSInteger)getSectionNumber;
- (void)setSectionNumber:(NSInteger)section;
- (void)reLoadData;
@end
