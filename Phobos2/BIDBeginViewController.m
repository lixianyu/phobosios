//
//  BIDBeginViewController.m
//  Phobos2
//
//  Created by Li Xianyu on 13-11-10.
//  Copyright (c) 2013年 Li Xianyu. All rights reserved.
//

#import "BIDBeginViewController.h"
#import "UIViewController+MMDrawerController.h"
#import "MMLogoView.h"
#import "Animations.h"

static NSString *CellIdentifier = @"SimpleTableIdentifier";

@interface BIDBeginViewController ()

@end

@implementation BIDBeginViewController

- (id)initWithCoder:(NSCoder *)aDecoder {
    NSLog(@"%s", __func__);
    self = [super initWithCoder:aDecoder];
    if (self) {
        //self.editButtonItem.title = @"Delete";
//        self.navigationItem.rightBarButtonItem = self.editButtonItem;
       // self.navigationItem.rightBarButtonItem.title = @"配对";
        NSNotificationCenter *center = [NSNotificationCenter defaultCenter];
        //[center addObserver:self selector:@selector(applicationWillResignActive) name:UIApplicationWillResignActiveNotification object:nil];
        //[center addObserver:self selector:@selector(applicationDidBecomeActive) name:UIApplicationDidBecomeActiveNotification object:nil];
        [center addObserver:self selector:@selector(applicationDidEnterBackground) name:UIApplicationDidEnterBackgroundNotification object:nil];
        //[center addObserver:self selector:@selector(applicationWillEnterForeground) name:UIApplicationWillEnterForegroundNotification object:nil];
    }
    return self;
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    NSLog(@"%s", __func__);
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
//        self.title = @"Delete Me";
//        self.editButtonItem.title = @"Delete";
//        self.navigationItem.rightBarButtonItem = self.editButtonItem;
//        self.navigationItem.rightBarButtonItem.title = @"Delete";
    }
    return self;

}

- (id)initWithStyle:(UITableViewStyle)style
{
    NSLog(@"%s", __func__);
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
//        self.editButtonItem.title = @"Delete";
//        self.navigationItem.rightBarButtonItem = self.editButtonItem;
//        self.navigationItem.rightBarButtonItem.title = @"Delete";

    }
    return self;
}

- (void)applicationDidEnterBackground {
    NSInteger selectedIndex = [self.tableView indexPathForSelectedRow].row;
    [[NSUserDefaults standardUserDefaults] setInteger:selectedIndex forKey:@"selectedIndex"];
}


- (void)dismissTheAddNewView {
    [UIView animateWithDuration:0.5f
                     animations:^() {
                         self.semiTransparentView.alpha = 0.0f;
                         self.customIBActionSheetView.alpha = 0.0f;
                         self.tableView.alpha = 1.0f;
                     }];
    
}

- (void)setupAddNewPhobosView {
    UIStoryboard *storyBoard = [UIStoryboard storyboardWithName:@"Main_iPhone" bundle:nil];
    NSLog(@"%s : storyBoard = %@...", __func__, storyBoard);
    _addController = [storyBoard instantiateViewControllerWithIdentifier:@"addnewPhobos"];
    self.customIBActionSheetView = _addController.view;
    NSLog(@"customIBActionSheetView = %@", self.customIBActionSheetView);
    self.customIBActionSheetView.alpha = 0.0f;
    self.customIBActionSheetView.layer.cornerRadius = 6.0f;
    self.semiTransparentView = [[UIView alloc] initWithFrame:[UIScreen mainScreen].bounds];
    UITapGestureRecognizer *cancelTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(dismissTheAddNewView)];
    [self.semiTransparentView addGestureRecognizer:cancelTap];
    self.semiTransparentView.backgroundColor = [UIColor blackColor];
    self.semiTransparentView.alpha = 0.0f;
    [self.view addSubview:self.customIBActionSheetView];
    [self.view insertSubview:self.semiTransparentView belowSubview:self.customIBActionSheetView];
//    [self.semiTransparentView addSubview:self.customIBActionSheetView];
    
    float halfOfWidth = CGRectGetWidth([UIScreen mainScreen].bounds) / 2.0;
    float height = CGRectGetHeight([UIScreen mainScreen].bounds)-(self.navigationController.navigationBar.bounds.size.height)-(self.tabBarController.tabBar.bounds.size.height);
    // position the darkend background view
    self.semiTransparentView.frame = CGRectMake(0, 0, halfOfWidth * 2.0, height);
    self.semiTransparentView.center = CGPointMake(halfOfWidth, height / 2.0);
    
    self.customIBActionSheetView.frame = CGRectMake(0, 0, CGRectGetWidth(self.view.frame) - 40, 320);
    NSLog(@"self.view.frame = %@", self.customIBActionSheetView.frame);
    self.customIBActionSheetView.center = CGPointMake(halfOfWidth, height / 2.0 - 10);
    NSLog(@"customIBActionSheetView = %@", self.customIBActionSheetView);
}

- (void)viewDidLoad
{
    NSLog(@"%s", __func__);
    [super viewDidLoad];
    [self setupAddNewPhobosView];
    
    UITapGestureRecognizer * doubleTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(doubleTap:)];
    [doubleTap setNumberOfTapsRequired:2];
    [self.view addGestureRecognizer:doubleTap];
    
    CGSize logoSize = CGSizeMake(58, 62);
    MMLogoView * logo = [[MMLogoView alloc] initWithFrame:CGRectMake(CGRectGetMidX(self.tableView.bounds)-logoSize.width/2.0,
                                                                     -logoSize.height-logoSize.height/4.0,
                                                                     logoSize.width,
                                                                     logoSize.height)];
    [logo setAutoresizingMask:UIViewAutoresizingFlexibleLeftMargin|UIViewAutoresizingFlexibleRightMargin];
    [self.tableView addSubview:logo];
    
    [self.tableView registerClass:[UITableViewCell class] forCellReuseIdentifier:CellIdentifier];
    self.dwarves = @[@"Sleepy", @"Sneezy", @"Bashful", @"Happy", @"Doc",@"Sleepy", @"Sneezy", @"Bashful", @"Happy", @"Doc",@"Sleepy", @"Sneezy", @"Bashful", @"Happy", @"Doc",@"Sleepy", @"Sneezy", @"Bashful", @"Happy", @"Doc"];
}

- (void)viewWillAppear:(BOOL)animated {
    NSLog(@"%s", __func__);
    [super viewWillAppear:animated];
}

- (void)viewWillDisappear:(BOOL)animated {
    NSLog(@"%s", __func__);
    [super viewWillDisappear:animated];
    NSInteger selectedIndex = [self.tableView indexPathForSelectedRow].row;
    [[NSUserDefaults standardUserDefaults] setInteger:selectedIndex forKey:@"selectedIndex"];
}

- (void)viewDidAppear:(BOOL)animated {
    NSLog(@"%s", __func__);
    [super viewDidAppear:animated];
    NSNumber *indexNumber = [[NSUserDefaults standardUserDefaults] objectForKey:@"selectedIndex"];
    if (self.dwarves) {
        if (indexNumber) {
            NSInteger selectedIndex = [indexNumber intValue];
            [self.tableView selectRowAtIndexPath:[NSIndexPath indexPathForRow:selectedIndex inSection:0] animated:YES scrollPosition:UITableViewScrollPositionMiddle];
        } else {
            [self.tableView selectRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0] animated:YES scrollPosition:UITableViewScrollPositionMiddle];
        }
    }
}

- (void)viewDidDisappear:(BOOL)animated {
    NSLog(@"%s", __func__);
    [super viewDidDisappear:animated];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (id)getSideViewController {
    UIApplication *app = [UIApplication sharedApplication];
    UITabBarController *root = (UITabBarController*)[app.windows[0] rootViewController];
    NSLog(@"root = %@", root);
    NSMutableArray *controllers = (NSMutableArray*)[root viewControllers];
    NSLog(@"controllers[0] = %@", controllers[0]);
    MMDrawerController * mdc = controllers[0];
    return mdc.leftDrawerViewController;
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    return [self.dwarves count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
    
    // Configure the cell...
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    }
    cell.textLabel.text = self.dwarves[indexPath.row];
    if (indexPath.row == 0) {
        UIImage *image = [UIImage imageNamed:@"radar_BlueInsideCircle"];
        cell.imageView.image = image;
    }
    else {
        UIImage *image = [UIImage imageNamed:@"radar_GreyInsideCircle"];
        cell.imageView.image = image;
    }
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    UIViewController * destinationController = [self getSideViewController];
    NSLog(@"destinationController = %@", destinationController);
    if ([destinationController respondsToSelector:@selector(setDelegate:)]) {
        [destinationController setValue:self forKey:@"delegate"];
    }
    if ([destinationController respondsToSelector:@selector(setSelection:)]) {
        id object = [[NSNumber alloc] initWithFloat:7.2];
        NSDictionary *selection = @{@"indexPath" : indexPath,
                                    @"chuwei1" : [[NSNumber alloc] initWithBool:NO],
                                    @"ruwei1" : [[NSNumber alloc] initWithBool: YES],
                                    @"chuwei" : @YES,
                                    @"ruwei" : @NO,
                                    @"object" : object};
        [destinationController setValue:selection forKey:@"selection"];
    }
    [self.mm_drawerController toggleDrawerSide:MMDrawerSideLeft animated:YES completion:nil];
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    NSLog(@"%s", __func__);
    [super prepareForSegue:segue sender:sender];
}

#pragma mark - Button Handlers
-(void)doubleTap:(UITapGestureRecognizer*)gesture{
    [self.mm_drawerController bouncePreviewForDrawerSide:MMDrawerSideLeft completion:nil];
}

- (IBAction)addAPhobos:(id)sender {
    NSLog(@"%s", __func__);
//    [UIView animateWithDuration:0.5f
//                     animations:^() {
//                         self.semiTransparentView.alpha = 0.4f;
//                         self.customIBActionSheetView.alpha = 1.0f;
//                         self.tableView.alpha = 0.0f;
//                     }];
    self.semiTransparentView.alpha = 0.4f;
    //self.tableView.alpha = 0.0f;

    [Animations fadeIn:self.customIBActionSheetView andAnimationDuration:1.0 andWait:NO];
}
@end
