//
//  BIDBallScrollView.m
//  TestRadar
//
//  Created by Li Xianyu on 13-11-19.
//  Copyright (c) 2013年 Li Xianyu. All rights reserved.
//

#import "BIDBallScrollView.h"
#import "BIDBallLabelView.h"

@interface BIDBallScrollView()

@end

@implementation BIDBallScrollView
//等比率缩放
- (UIImage *)scaleImage:(UIImage *)image toScale:(float)scaleSize
{
    UIGraphicsBeginImageContext(CGSizeMake(image.size.width * scaleSize, image.size.height * scaleSize));
    [image drawInRect:CGRectMake(0, 0, image.size.width * scaleSize, image.size.height * scaleSize)];
    UIImage *scaledImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return scaledImage;
}

- (void)commonInit
{
    NSLog(@"%s", __func__);
    _ballAlpha = 0.5f;
//    self.imageBall = [UIImage imageNamed:@"leash_rangeIndicatorActive"];
    //self.imageBall = [UIImage imageNamed:@"radar_BlueInsideCircle"];
    self.imageBall = [UIImage imageNamed:@"radar_marker"];
    _imageBall = [self scaleImage:_imageBall toScale:0.5f];
#if 0
    self.currentPoint = CGPointMake((self.bounds.size.width / 2.0f) +
                                        (self.imageBall.size.width / 2.0f),
                                        (self.bounds.size.height / 2.0f) + (self.imageBall.size.height / 2.0f));
#else
    NSLog(@"self.bounds.size.width = %f, self.bounds.size.height = %f", self.bounds.size.width, self.bounds.size.height);
    self.currentPoint = CGPointMake(542, 542);
#endif
    
    BIDBallLabelView *ballLabelView = [[BIDBallLabelView alloc] initWithLabel:@"太阳宫" thePoint:CGPointMake(350, 199) unKnowBle:NO];
    [self addSubview:ballLabelView];
}

- (id)initWithCoder:(NSCoder *)coder {
    NSLog(@"%s", __func__);
    if (self = [super initWithCoder:coder]) {
        [self commonInit];
    }
    return self;
}

- (id)initWithFrame:(CGRect)frame
{
    NSLog(@"%s", __func__);
    self = [super initWithFrame:frame];
    if (self) {
        [self commonInit];
        
    }
    return self;
}

- (void)drawRect:(CGRect)rect
{
    NSLog(@"%s: x = %f, y = %f, _ballAlpha = %f", __func__, _currentPoint.x, _currentPoint.y, _ballAlpha);
    // Drawing code
//    [self.imageBall drawAtPoint:self.currentPoint];
    [self.imageBall drawAtPoint:self.currentPoint blendMode:kCGBlendModeNormal alpha:_ballAlpha];
}

#pragma mark -
- (void)setCurrentPoint:(CGPoint)newPoint {
    NSLog(@"%s: newPoint.x = %f, newPoint.y = %f", __func__, newPoint.x, newPoint.y);
#if 1
    _currentPoint = newPoint;
#else
    self.previousPoint = self.currentPoint;
    _currentPoint = newPoint;
    
    if (self.currentPoint.x < 0) {
        _currentPoint.x = 0;
        self.ballXVelocity = -self.ballXVelocity / 2;
    }
    if (self.currentPoint.y < 0){
        _currentPoint.y = 0;
        self.ballYVelocity = -self.ballYVelocity / 2;
    }
    if (self.currentPoint.x > self.bounds.size.width - self.imageBall.size.width) {
        _currentPoint.x = self.bounds.size.width - self.imageBall.size.width;
        self.ballXVelocity = -self.ballXVelocity / 2;
    }
    if (self.currentPoint.y > self.bounds.size.height - self.imageBall.size.height) {
        _currentPoint.y = self.bounds.size.height - self.imageBall.size.height;
        self.ballYVelocity = -self.ballYVelocity / 2;
    }
#endif
    [self setNeedsDisplay];
}

- (void)setCurrentPoint1:(CGPoint)newPoint
{
    NSLog(@"%s", __func__);
    self.previousPoint = self.currentPoint;
    _currentPoint = newPoint;

    if (self.currentPoint.x < 0) {
        _currentPoint.x = 0;
        self.ballXVelocity = -self.ballXVelocity / 2;
    }
    if (self.currentPoint.y < 0){
        _currentPoint.y = 0;
        self.ballYVelocity = -self.ballYVelocity / 2;
    }
    if (self.currentPoint.x > self.bounds.size.width - self.imageBall.size.width) {
        _currentPoint.x = self.bounds.size.width - self.imageBall.size.width;
        self.ballXVelocity = -self.ballXVelocity / 2;
    }
    if (self.currentPoint.y > self.bounds.size.height - self.imageBall.size.height) {
        _currentPoint.y = self.bounds.size.height - self.imageBall.size.height;
        self.ballYVelocity = -self.ballYVelocity / 2;
    }
    /*
     if (self.currentPoint.x < 0) {
     _currentPoint.x = 0;
     self.ballXVelocity = 0;
     }
     if (self.currentPoint.y < 0){
     _currentPoint.y = 0;
     self.ballYVelocity = 0;
     }
     if (self.currentPoint.x > self.bounds.size.width - self.image.size.width) {
     _currentPoint.x = self.bounds.size.width - self.image.size.width;
     self.ballXVelocity = 0;
     }
     if (self.currentPoint.y > self.bounds.size.height - self.image.size.height) {
     _currentPoint.y = self.bounds.size.height - self.image.size.height;
     self.ballYVelocity = 0;
     }
     */
    CGRect currentImageRect = CGRectMake(self.currentPoint.x, self.currentPoint.y,
                                         self.currentPoint.x + self.imageBall.size.width,
                                         self.currentPoint.y + self.imageBall.size.height);
    CGRect previousImageRect = CGRectMake(self.previousPoint.x, self.previousPoint.y,
                                          self.previousPoint.x + self.imageBall.size.width,
                                          self.currentPoint.y + self.imageBall.size.width);
    [self setNeedsDisplayInRect:CGRectUnion(currentImageRect,
                                            previousImageRect)];
}

- (void)update
{
    NSLog(@"%s", __func__);
    static NSDate *lastUpdateTime = nil;
    
    if (lastUpdateTime != nil) {
        NSTimeInterval secondsSinceLastDraw = -([lastUpdateTime timeIntervalSinceNow]);
        
        self.ballYVelocity = self.ballYVelocity - (self.acceleration.y * secondsSinceLastDraw);
        self.ballXVelocity = self.ballXVelocity + (self.acceleration.x * secondsSinceLastDraw);
        
//        CGFloat xAcceleration = secondsSinceLastDraw * self.ballXVelocity * 500;
//        CGFloat yAcceleration = secondsSinceLastDraw * self.ballYVelocity * 500;
#if 0
        self.currentPoint = CGPointMake(self.currentPoint.x + xAcceleration,
                                      self.currentPoint.y + yAcceleration);
#else
        self.currentPoint = CGPointMake(100, 100);
#endif
    }
    // Update last time with current time
    lastUpdateTime = [[NSDate alloc] init];
}

//缩放之后调用此函数
- (void) update1 {
    NSLog(@"%s", __func__);
#if 0
    CGFloat x = self.currentPoint.x * self.currentScale;
    CGFloat y = self.currentPoint.y * self.currentScale;
#else
    CGFloat x = 542 * self.currentScale;
    CGFloat y = 542 * self.currentScale;

#endif
    NSLog(@"x = %f, y = %f", x, y);
    self.currentPoint = CGPointMake(x, y);
}

- (void)updateDragging {
    NSLog(@"%s", __func__);
//    self.currentPoint = CGPointMake(_currentPoint.x - _draggingPoint.x, _currentPoint.y - _draggingPoint.y);
    [self setNeedsDisplay];
}

#pragma mark -
- (void) drawRadar {
    CALayer *waveLayer=[CALayer layer];
//    waveLayer.frame = CGRectMake(self.frame.size.width/2, self.frame.size.height/2, 10, 10);
    waveLayer.frame = CGRectMake(_currentPoint.x-4, _currentPoint.y-3, 10, 10);
    int colorInt=arc4random()%7;
    switch (colorInt) {
        case 0:
            waveLayer.borderColor =[UIColor redColor].CGColor;
            break;
        case 1:
            waveLayer.borderColor =[UIColor grayColor].CGColor;
            break;
        case 2:
            waveLayer.borderColor =[UIColor purpleColor].CGColor;
            break;
        case 3:
            waveLayer.borderColor =[UIColor orangeColor].CGColor;
            break;
        case 4:
            waveLayer.borderColor =[UIColor yellowColor].CGColor;
            break;
        case 5:
            waveLayer.borderColor =[UIColor greenColor].CGColor;
            break;
        case 6:
            waveLayer.borderColor =[UIColor blueColor].CGColor;
            break;
        default:
            waveLayer.borderColor =[UIColor blackColor].CGColor;
            break;
    }
    waveLayer.borderColor = [UIColor yellowColor].CGColor;
    waveLayer.borderWidth =0.1;
    waveLayer.cornerRadius =5.0;
    
    [self.layer addSublayer:waveLayer];
    [self scaleBegin:waveLayer];
}

-(void)scaleBegin:(CALayer *)aLayer
{
    const float maxScale=140.0;
//    NSLog(@"transform.m11 = %f", aLayer.transform.m11);
    if (aLayer.transform.m11 < maxScale) {
        if (aLayer.transform.m11==1.0) {
            [aLayer setTransform:CATransform3DMakeScale( 1.1, 1.1, 5.0)];
            
        }else{
            [aLayer setTransform:CATransform3DScale(aLayer.transform, 1.1, 1.1, 1.0)];
        }
        if (aLayer.transform.m11 < 10) {
            [self performSelector:_cmd withObject:aLayer afterDelay:0.005];
        }
        else if (aLayer.transform.m11 < 20) {
            [self performSelector:_cmd withObject:aLayer afterDelay:0.01];
        }
        else if (aLayer.transform.m11 < 30) {
            [self performSelector:_cmd withObject:aLayer afterDelay:0.02];
        }
        else if (aLayer.transform.m11 < 40) {
            [self performSelector:_cmd withObject:aLayer afterDelay:0.03];
        }
        else if (aLayer.transform.m11 < 50) {
            [self performSelector:_cmd withObject:aLayer afterDelay:0.04];
        }
        else if (aLayer.transform.m11 < 60) {
            [self performSelector:_cmd withObject:aLayer afterDelay:0.05];
        }
        else {
            [self performSelector:_cmd withObject:aLayer afterDelay:0.06];
        }
        
    } else {
        [aLayer removeFromSuperlayer];
    }
}
@end
