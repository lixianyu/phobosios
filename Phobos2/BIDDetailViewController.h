//
//  BIDDetailViewController.h
//  MDTest
//
//  Created by Li Xianyu on 13-12-18.
//  Copyright (c) 2013年 Li Xianyu. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BIDDetailViewController : UIViewController <UISplitViewControllerDelegate>

@property (strong, nonatomic) id detailItem;

@end
